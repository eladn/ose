#ifndef JOS_NET_DSM_DSM_PROTOCOL_H
#define JOS_NET_DSM_DSM_PROTOCOL_H

#include <net/dsm/dsm.h>

/* TODO :: maybe replace with some X-Macro, if there is more relevant things to associate with a status code */
typedef enum
{
    DSC__SUCCESS = 0,
    DSC__NOT_MASTER,
    DSC__REJECTED

} DsmStatusCode;

// DsmMsg X-Macro
#define DSM_MSG_LIST \
/*              cap_name,                        name*/\
X(         HANDSHAKE_REQ,                handshake_req)\
X(        HANDSHAKE_RESP,               handshake_resp)\
X(        SHARE_PAGE_REQ,               share_page_req)\
X(       SHARE_PAGE_RESP,              share_page_resp)\
X(       SHARE_PAGE_DONE,              share_page_done)\
X(    EXCLUSIVE_PAGE_REQ,           exclusive_page_req)\
X(   EXCLUSIVE_PAGE_RESP,          exclusive_page_resp)\
X(     NEW_EXCLUSIVE_REQ,            new_exclusive_req)\
X(    NEW_EXCLUSIVE_RESP,           new_exclusive_resp)\

#define DSM_MSG_CAPNAME2ENUM(cap_name) DSM_MSG__ ## cap_name
#define DSM_MSG_NAME2STRUCT(name) dsm_ ## name ## _t
#define DSM_MSG_NAME2HANDLER(name) dsm_proto_handle ## name
#define DSM_MSG_NAME2FIELD(name) name

// initiator means it generates a new req id, and the following messages in this transaction will all have the same one.
#define DSM_MSG_TYPE__IS_INITIATOR(type) (((type) == DSM_MSG_CAPNAME2ENUM(SHARE_PAGE_REQ)) || ((type) == DSM_MSG_CAPNAME2ENUM(EXCLUSIVE_PAGE_REQ)) || ((type) == DSM_MSG_CAPNAME2ENUM(NEW_EXCLUSIVE_REQ)))

// if this message expects to get a page IN RESPONSE (meaning - it doesn't send a page itself, but might get one back)
#define DSM_MSG_TYPE__IS_EXPECTING_PAGE(type) ((type) == DSM_MSG_CAPNAME2ENUM(SHARE_PAGE_REQ))

/* Dsm Message Types */
#define X(cap_name,name) DSM_MSG_CAPNAME2ENUM(cap_name),
typedef enum {
    DSM_MSG_LIST
    DSM_NR_MSG_TYPES
} DsmMsgType;
#undef X

/*********************************************/
/****** Concrete Message Structs *************/
/*********************************************/
/* Force the packing of the message structures */
#pragma pack(push, 1)
/*
 * Identity Structure
 * retains all information needed to identify both net_thread endpoints of a message,
 * wll be prepended to every message in the network for validation purposes.
 */
typedef struct
{
    DsmMsgType msg_type;
    dsm_peer_net_id_t from_peer_id;
    dsm_peer_net_id_t to_peer_id;
    void* nva; // := Net Virutal Address;
    dsm_req_id_t req_id;
} dsm_msg_header_t;

/*
 * TODO :: document
 */
typedef struct
{
} dsm_handshake_req_t;

/*
 * TODO :: document
 */
typedef struct
{
    DsmStatusCode status; // OK <==> data page is present
} dsm_handshake_resp_t;

/*
 * Share Page Request
 * Contains the Virtual Net Address of the requested page.
 */
typedef struct
{
} dsm_share_page_req_t;

/*
 * Share Page Response
 * Contains the Virtual Net Address of the requested page,
 * a status code with the following semantics:
 *     DSC__SUCCESS - the page is being shared with you, and contained in the response's body.
 *     DSC__NOT_MASTER - you requested this page from a thread that is not its master, and thus has no privilege to grant you sharing.
 *     < more return codes may be introduced later>
 */
typedef struct
{
    DsmStatusCode status; // OK <==> data page is present
} dsm_share_page_resp_t;

/*
 * Share Page Done
 * Used to let the master (who just shared the page with us moments ago) that we finished updating
 * our internal state regarding this page, and thus he can continue service requests for this page.
 */
typedef struct
{
} dsm_share_page_done_t;

/*
 * Exclusive Page Request
 * Contains the Virtual Net Address of the requested page.
 */
typedef struct
{
} dsm_exclusive_page_req_t;

/*
 * Exclusive Page Response
 * Contains the Virtual Net Address of the requested page,
 * a status code with the following semantics:
 *     DSC__SUCCESS - the old master forfeited its master status, you are now the legal master of this page.
 *     DSC__NOT_MASTER - you requested this page from a thread that is not its master, and thus has no privilege to grant you sharing.
 *     < more return codes may be introduced later>
 */
typedef struct
{
    DsmStatusCode status;   
} dsm_exclusive_page_resp_t;

/*
 * Notify some thread that you are the new exclusive master of this page,
 * thus it has to mark it as "Uncached", and note to himslef that the sender is now the master.
 * Contains the Virtual Net Address of the page.
 */
typedef struct
{
} dsm_new_exclusive_req_t;

/*
 * After getting MeNewExclusive_Req, notify the new master that you invalidated your local copy and know he is the master.
 */
typedef struct
{
    DsmStatusCode status;
} dsm_new_exclusive_resp_t;

struct dsm_msg
{
    dsm_msg_header_t header;
    /* Dsm Message Types */
    #define X(cap_name,name) DSM_MSG_NAME2STRUCT(name) name;
    union {
        DSM_MSG_LIST
    };
    #undef X

};

/* Stop packing structures (important! other files include this one) */
#pragma pack(pop)


int dsm_proto_become_shared(events_loop_t *loop, net_env_data_t *self, void *va);
int dsm_proto_become_exclusive(events_loop_t *loop, net_env_data_t *self, void *va);
int dsm_proto_dispatch_req(events_loop_t *loop, net_env_data_t *self, dsm_msg_t *full_resp);
int dsm_proto_send_dsm_msg(events_loop_t *loop, net_env_data_t *self, dsm_msg_t *new_msg);
int dsm_receive_response(events_loop_t *loop, net_env_data_t *self, dsm_net_page_t *npg, dsm_peer_net_id_t peer_id, dsm_req_id_t req_id, dsm_msg_t *net_msg_buf, void *response_page_buf);
void dsm_proto_send_share_page_resp_aux(events_loop_t *loop, net_env_data_t *self, dsm_net_page_t *npg, dsm_peer_net_id_t to_peer_id, dsm_req_id_t req_id, int to_rej);
void dsm_proto_send_share_page_resp(events_loop_t *loop, net_env_data_t *self, dsm_net_page_t *npg, dsm_peer_net_id_t to_peer_id, dsm_req_id_t req_id);
void dsm_proto_send_exclusive_page_resp(events_loop_t *loop, net_env_data_t *self, dsm_net_page_t *npg, dsm_peer_net_id_t to_peer_id, DsmStatusCode status, dsm_req_id_t req_id);
int dsm_receive_req_from_sock(events_loop_t *loop, net_env_data_t *self, int sock, dsm_msg_t *net_msg_buf);
int dsm_receive_resp_from_sock(events_loop_t *loop, net_env_data_t *self, int sock, dsm_msg_t *net_msg_buf);

#endif /* JOS_NET_DSM_DSM_PROTOCOL_H */