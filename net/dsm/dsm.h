#ifndef JOS_NET_DSM_DSM_H
#define JOS_NET_DSM_DSM_H

#include <lwip/sockets.h>
//#include <lwip/sys.h>  TODO :: UNCOMMENT THIS!
#include <lwip/inet.h>
#include <inc/mmu.h>
#include <inc/env.h>
#include <inc/lib.h>

#define DSM_RANDOM_VA_RANGE_BEGIN 0x0ffff000
#define DSM_VA_BEGIN 0x0e000000  /* TODO: maybe change it */


#define FOR_EACH_PEER(cur_peer, net_env) \
     for (cur_peer = ((net_env)->peers); \
          cur_peer < ((net_env)->peers + (net_env)->npeers); \
          ++cur_peer)

#define FOR_EACH_NET_PAGE(cur_npg, net_env) \
     for (cur_npg = ((net_env)->net_page_arr); \
          cur_npg < ((net_env)->net_page_arr + (net_env)->range_npages); \
          ++cur_npg)        

typedef uint32_t dsm_req_id_t;
typedef uint32_t dsm_peer_net_id_t;
typedef struct dsm_peer {
    dsm_peer_net_id_t id;
    struct sockaddr_in net_addr;
    int sock;
    /* one side works with odd numbers and the other side with even. parity is
    determined in the initialization by the net ids (who has greater net id) */
    dsm_req_id_t next_req_id;
    /* who's turn to yield next. starting by who has greater net id */
    int yield_turn;
} dsm_peer_t;

#define MAX_NET_ENVS 10
#define MAX_NET_ENV_PEERS 10

typedef enum
{
    NPS__UNCACHED = 0,
    NPS__SHARED_REGULAR = 1,
    NPS__SHARED_MASTER = 2,
    NPS__EXCLUSIVE = 3
} NetPageState;

// state is SM or EX <==> the second bit is on.
#define NPS__PAGE_MASTER 0x2

typedef struct net_env_data net_env_data_t;
typedef struct dsm_msg dsm_msg_t;
typedef struct dsm_net_page dsm_net_page_t;
typedef struct dsm_async_waiting_msg dsm_async_waiting_msg_t;

struct dsm_async_waiting_msg {
    /* zero tid if not waiting transaction */
    thread_id_t tid;
    dsm_req_id_t req_id;
    dsm_peer_net_id_t peer_id;
    /* the following 2 fields should be filled by the waiter before
    it goes to sleep (performed by `dsm_receive_response()`).
    The incoming messages dispacher will wake this thread up when the
    matching msg arrives! */
    dsm_msg_t *response_msg_buf;
    void *response_page_buf;
};

struct dsm_net_page
{
    NetPageState npg_state;
    dsm_peer_net_id_t master_peer_id;
    //sys_sem_t sem; //   TODO :: UNCOMMENT THIS!
    dsm_async_waiting_msg_t waiting_transaction;
};

#define NPG2NVA(net_env, npg) ((void*)((net_env)->range_start_va+(((npg) - (net_env)->net_page_arr)*PGSIZE)))

#define NVA2NPG(net_env, nva) ((dsm_net_page_t*)((net_env)->net_page_arr) + (((nva)-(net_env)->range_start_va)/PGSIZE))

struct net_env_data {
    int valid;
    envid_t envid;
    dsm_peer_net_id_t my_net_id;
    void *range_start_va;
    size_t range_npages;
    dsm_net_page_t* net_page_arr;
    size_t npeers;
    dsm_peer_t peers[MAX_NET_ENV_PEERS];
};

// Definitions for requests from clients to network server
enum {
    // The following messages pass a page containing an DSMipc.
    
    DSMREQ_INIT_NET_ENV = 1,
    DSMREQ_PGFAULT_REPORT,
};

union DSMipc {
    struct DSMreq_init_net_env {
        dsm_peer_net_id_t my_id;
        void *range_start_va;
        size_t range_npages;
        size_t npeers;
        dsm_peer_t peers[0];
    } init_net_env;

    struct DSMreq_pgfault_report {
        void *va;
        uint32_t err_flags;
    } pgfault_report;

    char __page_padding[PGSIZE];
};

int dsm_init(dsm_peer_net_id_t my_id, void *range_start_va, size_t range_npages, dsm_peer_t *peers, size_t npeers);

/* DataStructures & helper functions */

net_env_data_t* find_free_net_env_buf();

net_env_data_t* find_net_env_by_envid(envid_t envid);

dsm_peer_t* find_peer_by_id(net_env_data_t *net_env, dsm_peer_net_id_t peer_id);

int find_socket_of_peer_by_id(net_env_data_t *net_env, dsm_peer_net_id_t peer_id);

// void dsm_net_page_lock(dsm_net_page_t *npg);

// void dsm_net_page_release(dsm_net_page_t *npg);

#endif /* JOS_NET_DSM_DSM_H */
