#include <inc/lib.h>

#include <arch/thread.h>
#include <arch/threadq.h>
#include <arch/setjmp.h>

static thread_id_t max_tid;
static struct thread_context *cur_tc;

static struct thread_queue thread_queue;
static struct thread_queue sleeping_queue;
static struct thread_queue kill_queue;

void
thread_init(void) {
    static int initialized = 0;
    if (initialized) return;
    threadq_init(&thread_queue);
    max_tid = 0;
    initialized = 1;
}

// starts from 1. never get 0.
static thread_id_t thread_get_new_thread_id() {
    max_tid++;
    if (max_tid == 0) max_tid = 1;
    return max_tid;
}

uint32_t
thread_id(void) {
    if (!cur_tc) return 0;
    return cur_tc->tc_tid;
}

int
thread_is_in_thread(void) {
    return cur_tc != NULL;
}

void
thread_wakeup(volatile uint32_t *addr) {
    struct thread_context *tc = thread_queue.tq_first;
    while (tc) {
	if (tc->tc_wait_addr == addr)
	    tc->tc_wakeup = 1;
	tc = tc->tc_queue_link;
    }
}

static struct thread_context * find_thread_by_id(thread_id_t tid) {
    struct thread_context * thread = thread_queue.tq_first;
    for (; thread; thread = thread->tc_queue_link) {
        if (thread->tc_tid == tid) {
            return thread;
        }
    }
    return NULL;
}

void
thread_sleep(void) {
    assert(cur_tc);
    assert(thread_queue.tq_first); // this is not the last thread in the system.
    
    //threadq_print(&sleeping_queue);
    threadq_push(&sleeping_queue, cur_tc);
    //threadq_print(&sleeping_queue);


    if (jos_setjmp(&cur_tc->tc_jb) != 0)
        return;
    cur_tc = NULL;
    thread_yield();
    assert(0);
}

int
thread_wakeup_by_id(thread_id_t tid) {
    assert(tid != cur_tc->tc_tid);

    //threadq_print(&sleeping_queue);
    struct thread_context * thread = threadq_pop_extract_by_id(&sleeping_queue, tid);
    //threadq_print(&sleeping_queue);
    if (!thread) return -E_NOT_FOUND;
    threadq_push(&thread_queue, thread);
    return 0;
}

int
thread_assign_user_data(thread_id_t tid, void *data) {
    struct thread_context *thread = find_thread_by_id(tid);
    if (!thread) return -1;
    thread->data = data;
    return 0;
}

int
thread_retrieve_user_data(thread_id_t tid, void **pdata) {
    assert(pdata != NULL);
    struct thread_context *thread = find_thread_by_id(tid);
    if (!thread) return -1;
    *pdata = thread->data;
    return 0;
}

int
thread_retrieve_my_user_data(void **pdata) {
    assert(pdata != NULL);
    if (!cur_tc) return -1;
    *pdata = cur_tc->data;
    return 0;
}

void
thread_wait(volatile uint32_t *addr, uint32_t val, uint32_t msec) {
    uint32_t s = sys_time_msec();
    uint32_t p = s;

    cur_tc->tc_wait_addr = addr;
    cur_tc->tc_wakeup = 0;

    while (p < msec) {
	if (p < s)
	    break;
	if (addr && *addr != val)
	    break;
	if (cur_tc->tc_wakeup)
	    break;

	thread_yield();
	p = sys_time_msec();
    }

    cur_tc->tc_wait_addr = 0;
    cur_tc->tc_wakeup = 0;
}

int
thread_wakeups_pending(void)
{
    struct thread_context *tc = thread_queue.tq_first;
    int n = 0;
    while (tc) {
	if (tc->tc_wakeup)
	    ++n;
	tc = tc->tc_queue_link;
    }
    return n;
}

int
thread_onhalt(void (*fun)(thread_id_t)) {
    if (cur_tc->tc_nonhalt >= THREAD_NUM_ONHALT)
	return -E_NO_MEM;

    cur_tc->tc_onhalt[cur_tc->tc_nonhalt++] = fun;
    return 0;
}

static thread_id_t
alloc_tid(void) {
    thread_id_t tid = thread_get_new_thread_id();
    if (tid == (uint32_t)~0)
	panic("alloc_tid: no more thread ids");
    return tid;
}

static void
thread_set_name(struct thread_context *tc, const char *name)
{
    strncpy(tc->tc_name, name, name_size - 1);
    tc->tc_name[name_size - 1] = 0;
}

static void
thread_entry(void) {
    cur_tc->tc_entry(cur_tc->tc_arg);
    thread_halt();
}

int
thread_create(thread_id_t *tid, const char *name, 
		void (*entry)(uint32_t), uint32_t arg) {
    struct thread_context *tc = malloc(sizeof(struct thread_context));
    if (!tc)
	return -E_NO_MEM;

    memset(tc, 0, sizeof(struct thread_context));
    
    thread_set_name(tc, name);
    tc->tc_tid = alloc_tid();

    tc->tc_stack_bottom = malloc_page();//malloc(stack_size);

    if (!tc->tc_stack_bottom) {
	free(tc);
	return -E_NO_MEM;
    }

    void *stacktop = tc->tc_stack_bottom + stack_size;
    // Terminate stack unwinding
    stacktop = stacktop - 4;
    memset(stacktop, 0, 4);
    
    memset(&tc->tc_jb, 0, sizeof(tc->tc_jb));
    tc->tc_jb.jb_esp = (uint32_t)stacktop;
    tc->tc_jb.jb_eip = (uint32_t)&thread_entry;
    tc->tc_entry = entry;
    tc->tc_arg = arg;

    threadq_push(&thread_queue, tc);

    if (tid)
	*tid = tc->tc_tid;
    return 0;
}

static void
thread_clean(struct thread_context *tc) {
    if (!tc) return;

    int i;
    for (i = 0; i < tc->tc_nonhalt; i++)
	   tc->tc_onhalt[i](tc->tc_tid);

    threadq_pop_extract_by_id(&sleeping_queue, tc->tc_tid);

    free_page(tc->tc_stack_bottom);//free(tc->tc_stack_bottom);
    free(tc);
}

void
thread_halt() {
    // right now the kill_queue will never be more than one
    // clean up a thread if one is on the queue
    thread_clean(threadq_pop(&kill_queue));

    threadq_push(&kill_queue, cur_tc);
    cur_tc = NULL;
    thread_yield();
    // WHAT IF THERE ARE NO MORE THREADS? HOW DO WE STOP?
    // when yield has no thread to run, it will return here!
    exit();
}

void
thread_yield(void) {
    struct thread_context *next_tc = threadq_pop(&thread_queue);

    if (!next_tc) return;

    if (cur_tc) {
	if (jos_setjmp(&cur_tc->tc_jb) != 0) {
	    return;
    }
	threadq_push(&thread_queue, cur_tc);
    }

    cur_tc = next_tc;
    jos_longjmp(&cur_tc->tc_jb, 1);
}

static void
print_jb(struct thread_context *tc) {
    cprintf("jump buffer for thread %s:\n", tc->tc_name);
    cprintf("\teip: %x\n", tc->tc_jb.jb_eip);
    cprintf("\tesp: %x\n", tc->tc_jb.jb_esp);
    cprintf("\tebp: %x\n", tc->tc_jb.jb_ebp);
    cprintf("\tebx: %x\n", tc->tc_jb.jb_ebx);
    cprintf("\tesi: %x\n", tc->tc_jb.jb_esi);
    cprintf("\tedi: %x\n", tc->tc_jb.jb_edi);
}
