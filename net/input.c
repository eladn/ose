#include "ns.h"

void
input(envid_t ns_envid)
{
	binaryname = "ns_input";

    cprintf("Input helper: my env id: %x\n", thisenv->env_id);

	// LAB 6: Your code here:
	// 	- read a packet from the device driver
	//	- send it to the network server
	// Hint: When you IPC a page to the network server, it will be
	// reading from it for a while, so don't immediately receive
	// another packet in to the same physical page.

    struct jif_pkt *pkt = (struct jif_pkt *)find_unmapped_va(0);

    while(1) {
        sys_net_receive_packet(pkt);
        ipc_send(ns_envid, NSREQ_INPUT, (void *)pkt, PTE_P|PTE_U);
        sys_page_unmap(0, (void *)pkt);
    }

}
