#ifndef JOS_NET_TYPES_H
#define JOS_NET_TYPES_H

typedef struct mac_addr {
    union {
        uint64_t full;
        struct {
            uint16_t high;
            uint32_t low;
        } __attribute__ ((packed));
        struct {
            uint16_t word[3];
        } __attribute__ ((packed));
        struct {
            uint8_t byte[6];
        } __attribute__ ((packed));
    };
} mac_addr_t;

#endif /* JOS_NET_TYPES_H */
