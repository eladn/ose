#include "ns.h"
#include <inc/lib.h>

// blocks if the tx queue is full
int net_transmit(void* packet, size_t len) {
	int r;

#ifdef NET_ZERO_COPY
	static int is_cow_handler_registered = 0;
	if (!is_cow_handler_registered) {
		extern int cow_pgfault_handler(struct UTrapframe *utf);
		is_cow_handler_registered = 1;
		r = reg_pgfault_handler(cow_pgfault_handler);
		if (r < 0) return r;
	}
#endif

	/* The loop actually is not relevant because we use the 
	flag to block until there is free space in the tx queue. */
	while (1) {
		r = sys_net_transmit_packet(packet, len, 1 /* block till inserted to tx queue */);
		if (r == 0) return 0;
		if (r != -E_FULL_QUEUE) {
			panic("net_transmit: Received error: %e", r);
		}
		sys_yield();
	}
	return 0;
}

void
output(envid_t ns_envid)
{
	binaryname = "ns_output";

	cprintf("Output helper: my env id: %x\n", thisenv->env_id);

	// LAB 6: Your code here:
	// 	- read a packet from the network server
	//	- send the packet to the device driver

	struct jif_pkt *pkt = (struct jif_pkt *)find_unmapped_va(0);
	envid_t from_env;

	while(1) {
		int perm_store = PTE_P|PTE_U;
		int32_t r = ipc_recv(&from_env, (void *)pkt, &perm_store);
		assert((perm_store & (PTE_P|PTE_U)) == (PTE_P|PTE_U));
		assert(r == NSREQ_OUTPUT);
		assert(from_env == ns_envid);
		
		net_transmit(pkt->jp_data, pkt->jp_len);
		sys_page_unmap(0, (void *)pkt);
	}

}
