#include <inc/lib.h>

void* find_unmapped_va(uintptr_t start_from) {
	uintptr_t addr;
	for (addr = MAX(UTEXT, MIN((USTACKTOP - PGSIZE), ROUNDUP((uintptr_t)start_from, PGSIZE)));
		addr < USTACKTOP; addr += PGSIZE) {
		if (!PTE_IS_MAPPED(uvpd[PDX(addr)]) || !PTE_IS_MAPPED(uvpt[PGNUM(addr)])) {
			return (void*)addr;
		}
	}
	return NULL;
}
