#include <net/dsm/dsm.h>
#include <inc/lib.h>
/*
poll sockets
*/

net_env_data_t net_envs[MAX_NET_ENVS];

/* DataStructures helper functions */

net_env_data_t* find_free_net_env_buf() {
    int i;
    for (i = 0; i < MAX_NET_ENVS; ++i) {
        if (!net_envs[i].valid)
            net_envs[i].valid = 1;
            return &net_envs[i];
    }
    return NULL;
}

net_env_data_t* find_net_env_by_envid(envid_t envid) {
    int i;
    for (i = 0; i < MAX_NET_ENVS; ++i) {
        if (net_envs[i].valid && net_envs[i].envid == envid)
            return &net_envs[i];
    }
    return NULL;
}

dsm_peer_t* find_peer_by_id(net_env_data_t *net_env, dsm_peer_net_id_t peer_id) {
    int i;
    for (i = 0; i < MAX_NET_ENV_PEERS; ++i) {
        if (net_env->peers[i].id == peer_id)
            return &net_env->peers[i];
    }
    return NULL;
}

int find_socket_of_peer_by_id(net_env_data_t *net_env, dsm_peer_net_id_t peer_id) {
    dsm_peer_t* peer = find_peer_by_id(net_env, peer_id);
    if (!peer) return -1;
    return peer->sock;
}

// void dsm_net_page_lock(dsm_net_page_t *npg) {
//     sys_sem_wait(npg->sem);
// }

// void dsm_net_page_release(dsm_net_page_t *npg) {
//     sys_sem_signal(npg->sem);
// }
