#include <inc/utils.h>
#include <inc/stdio.h>
#include <inc/string.h>
#include <inc/mmu.h>

long atoi(char *str)
{
	return strtol(str, NULL, 10);
}

long xtoi(char *str)
{
    return strtol(str, NULL, 16);
}

void sprint_pte_flags(int perm, char *str){
	#define FLG(flag, sym, v,n) snprintf(str++,2,"%s",(perm&PTE_##flag)?#sym:"-");
	PTE_FLAGS_LIST
	#undef FLG
	*str = '\0';
}

void print_pte_flags(int perm){
	char str[NFLAGS+1] = {0};
	sprint_pte_flags(perm, str);
	cprintf(str);
}

