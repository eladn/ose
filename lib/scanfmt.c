
#include <inc/types.h>
#include <inc/stdio.h>
#include <inc/string.h>
#include <inc/stdarg.h>
#include <inc/error.h>
#include <inc/utils.h>

/*
	Note: not fully implemented. all unimplemented sections are marked with `TODO`s.
	currently reads only numeric types.
*/

/* function template for reading a number (from various types) from input. */
#define __read_number_func_ctor(name, type) \
	static inline int read_number__##name (int (*getch)(void*, int), \
											void* putdat, \
											int32_t *numptr, \
											int base, \
											int num_neg_sign) { \
		int retval = 1; \
		int ch = 0; \
		type num = 0; \
		while ( (ch = getch(putdat, 0)) && VALID_DIGIT_CHAR(ch, base) ) { \
			retval = 0; \
			getch(putdat, 1); /*increment the read cursor*/  \
			num *= (type)base; \
			num += (type)ASCII_TO_NUM(ch); \
		} \
		*((type*)numptr) = num; \
		if (num_neg_sign) { num = -num; } \
		return retval; \
	}
#define get_read_number_func(lflag, is_signed) \
	( ((lflag) == 0)  \
	? ((is_signed) ? read_number__int : read_number__unsigned)  \
	: ((lflag) == 1)  \
	? ((is_signed) ? read_number__long : read_number__unsigned_long)  \
	: ((is_signed) ? read_number__long_long : read_number__unsigned_long_long) )

__read_number_func_ctor(int, int)
__read_number_func_ctor(long, long)
__read_number_func_ctor(long_long, long long)
__read_number_func_ctor(unsigned, unsigned)
__read_number_func_ctor(unsigned_long, unsigned long)
__read_number_func_ctor(unsigned_long_long, unsigned long long)




void scanfmt(int (*getch)(void*, int), void *putdat, const char *fmt, ...);

void
vscanfmt(int (*getch)(void*, int), void *putdat, const char *fmt, va_list ap)
{
	register const char *p;
	register int ch, fmt_ch, err;
	int32_t *numptr;
	int base, lflag, num_neg_sign, is_signed, ret;

	while(1) {
		while ((fmt_ch = *(unsigned char *) fmt++) != '%') {
			if (fmt_ch == '\0')
				return;
			ch = getch(putdat, 0/* do not increment read cursor yet */);
			if (ch == '\0')
				return;
			if (ch != fmt_ch) {
				return;  // TOOD: is it error?
			}
			getch(putdat, 1); /* successful read char, now increment the read cursor */
		}

		/*
		TODO: 	Read spaces and ignore them.
				One space in fmt may represent more than one space in the input.
				Where to handle it? is it the right place?
		*/
	
		is_signed = 0;
		lflag = 0;
		num_neg_sign = 0;
		base = 0; /* for gcc */
	reswitch:
		switch (fmt_ch = *(unsigned char *) fmt++) {
		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;

		// character
		case 'c':
			ch = getch(putdat, 1);
			if (ch == 0) {
				return;
			}
			*va_arg(ap, int*) = (int)ch; // TODO: check!
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL) {
				return;
			}

			// TODO: implement!

			break;

		// (signed) decimal
		case 'd':
			ch = getch(putdat, 0);
			if (ch == '-') {
				num_neg_sign = 1;
				getch(putdat, 1); // increment the read cursor
			}
			// TODO: read `+` sign (if exists)?
			base = 10;
			is_signed = 1;
			goto number;

		// unsigned decimal
		case 'u':
			base = 10;
			goto number;

		// (unsigned) octal
		case 'o':
			base = 8;
			goto number;

		// pointer
		case 'p':
			/* TODO: maybe read `0x` if exists */
			base = 16;
			goto number;

		// (unsigned) hexadecimal
		case 'x':
			/* TODO: maybe read `0x` if exists */
			base = 16;
		number:
			// TODO: check!
			numptr = va_arg(ap, int32_t*); /* pointer to variable to store the read number */
			ret = (get_read_number_func(lflag, is_signed))(getch, putdat, numptr, base, num_neg_sign);
			if (ret != 0) {
				return; // TODO: handle error!
			}
			break;

		// escaped '%' character
		case '%':
			// TODO: implement!
			break;

		// unrecognized escape sequence
		// TODO: what to do with it?
		default:
			// TODO: implement!
			break;

		}

		// TODO: impl !
	}

	return;
}


void
scanfmt(int (*getch)(void*, int), void *putdat, const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vscanfmt(getch, putdat, fmt, ap);
	va_end(ap);
}


struct sscanbuf {
	char *buf;
	char *ebuf;
	int cnt;
};

static int
sscangetch(struct sscanbuf *b, int inc_read_cursor)
{
	if (inc_read_cursor) {
		b->cnt++; // TODO: maybe sould be inside the second if?
	}
	if (b->buf < b->ebuf) {
		return *b->buf++;
	}
	return 0;
}

int
vsnscanf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sscanbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;

	// print the string to the buffer
	vscanfmt((void*)sscangetch, &b, fmt, ap);

	return b.cnt;
}

int
snscanf(char *buf, int n, const char *fmt, ...)
{
	va_list ap;
	int rc;

	va_start(ap, fmt);
	rc = vsnscanf(buf, n, fmt, ap);
	va_end(ap);

	return rc;
}
