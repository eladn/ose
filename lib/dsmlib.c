#include <inc/lib.h>
#include <net/dsm/dsm.h>

//static union DSMipc ipcbuf __attribute__((aligned(PGSIZE)));

/*
user library helper
*/

static int dsmipc(union DSMipc *ipcbuf, unsigned msg_type) {
    static envid_t dsmenv;
    if (dsmenv == 0) {
        if ((dsmenv = ipc_find_env(ENV_TYPE_DSM)) == 0)
            return -E_FAULT;
    }

    //static_assert(sizeof(ipcbuf) == PGSIZE);

    ipc_send(dsmenv, msg_type, ipcbuf, (PTE_P|PTE_U|PTE_W));
    return ipc_recv_from_env(dsmenv, NULL, NULL, NULL);
}

static struct {
    int initialized;
    dsm_peer_net_id_t my_id;
    void* range_start_va;
    size_t range_npages;
} dsm_this_net_env;

#define IS_ADDR_NET_SHARED(va) \
    (   (uintptr_t)dsm_this_net_env.range_start_va <= (uintptr_t)(va) && \
        ((uintptr_t)dsm_this_net_env.range_start_va) + (PGSIZE*dsm_this_net_env.range_npages) > (uintptr_t)(va) )

static int dsm_pgfault_handler(struct UTrapframe *utf) {
    void *addr = (void *) utf->utf_fault_va;
    uint32_t err = utf->utf_err;
    int r;

    assert(dsm_this_net_env.initialized);

    if (!IS_ADDR_NET_SHARED(addr)) {
        /* multiple pgfault handlers. maybe the next handler will succeed. */
        /* If all handlers failed, the framework would panic() automatically. */
        return -1;
    }

    cprintf("DSMlib: pgfault handler va: %p!\n", addr);

    /* Ask the dsmsrv env to attempt handle the fault. */
    union DSMipc *ipcbuf = malloc_page();
    if (!ipcbuf) return -E_NO_MEM;

    ipcbuf->pgfault_report.va = addr;
    ipcbuf->pgfault_report.err_flags = err;
    if ((r = dsmipc(ipcbuf, DSMREQ_PGFAULT_REPORT)) < 0) {
        free_page(ipcbuf);
        /* multiple pgfault handlers. maybe the next handler will succeed. */
        /* If all handlers failed, the framework would panic() automatically. */
        return -1;
    }

    //int perm = (PTE_P|PTE_U) | ((err & FEC_WR) ? PTE_W : 0);
    //sys_page_map(0, ipcbuf, 0, addr, perm);
    free_page(ipcbuf);

    return 0;
}

int dsm_init(dsm_peer_net_id_t my_id, void *range_start_va, 
             size_t range_npages, dsm_peer_t *peers, size_t npeers)
{
    int r;
    union DSMipc *ipcbuf = malloc_page();
    if (!ipcbuf) return -E_NO_MEM;

    ipcbuf->init_net_env.my_id = my_id;
    ipcbuf->init_net_env.range_start_va = range_start_va;
    ipcbuf->init_net_env.range_npages = range_npages;
    ipcbuf->init_net_env.npeers = npeers;
    memcpy(ipcbuf->init_net_env.peers, peers, npeers*sizeof(*peers));

    r = dsmipc(ipcbuf, DSMREQ_INIT_NET_ENV);
    free_page(ipcbuf);
    if (r < 0) return r;

    /*
        Note: current reg method does not taking the handlers ordering into account.
        Means that when a fault happens the dispatcher calls all registered pgfault
        handlers, by unknown order, until the first successfully reated the fault.
    */
    reg_pgfault_handler(dsm_pgfault_handler);

    /* store local copy of those items for user local checkings */
    dsm_this_net_env.initialized = 1;
    dsm_this_net_env.my_id = my_id;
    dsm_this_net_env.range_start_va = range_start_va;
    dsm_this_net_env.range_npages = range_npages;

    return 0;
}
