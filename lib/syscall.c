// System call stubs.

#include <inc/syscall.h>
#include <inc/lib.h>
#include <inc/cloak.h>

static inline int32_t
syscall(int num, int check, uint32_t a1, uint32_t a2, uint32_t a3, uint32_t a4, uint32_t a5)
{
	int32_t ret;

	// Generic system call: pass system call number in AX,
	// up to five parameters in DX, CX, BX, DI, SI.
	// Interrupt kernel with T_SYSCALL.
	//
	// The "volatile" tells the assembler not to optimize
	// this instruction away just because we don't use the
	// return value.
	//
	// The last clause tells the assembler that this can
	// potentially change the condition codes and arbitrary
	// memory locations.

	asm volatile("int %1\n"
		: "=a" (ret)
		: "i" (T_SYSCALL),
		  "a" (num),
		  "d" (a1),
		  "c" (a2),
		  "b" (a3),
		  "D" (a4),
		  "S" (a5)
		: "cc", "memory");

	if(check && ret > 0)
		panic("syscall %d returned %d (> 0)", num, ret);

	return ret;
}


/* define all auto-generated syscall wrappers */
#define COMPARE_void(x) x
#define X(name, nparams, return_type, type1, type2, type3, type4, type5, gen_wrapper) \
WHEN(gen_wrapper) ( \
	return_type SYSCALL_NAME2FUNC(name) CALL_WITH(nparams, type1 arg1, type2 arg2, type3 arg3, type4 arg4, type5 arg5) { \
		WHEN(NOT_EQUAL(return_type, void))(return ) \
		(return_type)(syscall(SYSCALL_NAME2NUM(name), 0, ZERO_REST_OUTOF5(nparams, (uint32_t)arg1, (uint32_t)arg2, (uint32_t)arg3, (uint32_t)arg4, (uint32_t)arg5))); \
	} \
)

SYSCALL_LIST
#undef X
#undef COMPARE_void
