#include <utils/args_parser.h>
#include <inc/string.h>
#include <inc/assert.h>

#define IS_VALID_SNAME(ch) ASCII_IS_LETTER(ch)
#define IS_VALID_LNAME(str) IS_VALID_SNAME(*str)

#define IS_PARAM_NAME_STR(str) \
	(*(str) == '-' &&  \
		(   ((str)[1] == '-' && IS_VALID_LNAME(&(str)[2])) ||  \
		    (IS_VALID_SNAME((str)[1]))   ))
#define IS_PARAM_SNAME_STR(str) \
	(*(str) == '-' && IS_VALID_SNAME((str)[1]))
#define IS_PARAM_LNAME_STR(str) \
	(*(str) == '-' && (str)[1] == '-' && IS_VALID_LNAME(&(str)[2]))

#define IS_SEPARATOR(word) \
	(word[0] == '-' && word[1] == '-' && word[2] == '\0')

char* cli_arg_type_to_str[] = {
    "integer", "boolean", "float", "text", "integer-range", "float-range"
};

/* print error only when err verbose flag is set */
#define EPRINTF(...) if((flags) & CLI_VERBOSE_ERRORS) cprintf(__VA_ARGS__)


/*   < global function forward declarations >   */
void cli_print_usage(struct cli_params_config *config);
int cli_parse(	struct cli_params_config *config, 
				struct cli_param_arguments* params_args, 
				int nr_words, 
				char **words,
				int flags);
/*   </ global function forward declarations >   */



/*   < static function forward declarations >   */
static inline int cli_arg_value_to_str_by_type(
	enum cli_args_val_type args_val_type, 
	int base, 
	char *range_str_buf, 
	int buf_size, 
	ARG_MULTITYPE *value);
static inline int cli_param_arg_value_to_str(
	struct cli_param *cur_param, 
	char *range_str_buf, 
	int buf_size, 
	ARG_MULTITYPE *value);
static inline int cli_param_arg_value_to_str_norange(
	struct cli_param *cur_param, 
	char *range_str_buf, 
	int buf_size, 
	ARG_MULTITYPE *value);
static int cli_param_range_to_str(
	struct cli_param *cur_param, 
	char *range_str_buf, 
	int buf_size);
static int cli_param_choises_to_str(
	struct cli_param *param, 
	char *choises_str_buf, 
	int buf_size, 
	char* separator);
static int cli_par_arg_type_to_str(
	struct cli_param *param, 
	char *buffer, 
	int buffer_len);
static struct cli_param* cli_find_param_by_short_name(
	struct cli_params_config *config, 
	char short_name);
static struct cli_param* cli_find_param_by_long_name(
	struct cli_params_config *config, 
	char* name);
static struct cli_param* cli_find_param_by_name(
	struct cli_params_config *config, 
	char* arg);
static struct cli_param* find_next_pos_param_separator(
	struct cli_params_config *config, 
	struct cli_param *after_param);
static struct cli_param* find_next_pos_param_set(
	struct cli_params_config *config, 
	struct cli_param *after_param);
static inline int verify_value_in_param_range(
	struct cli_param *cur_param, 
	ARG_MULTITYPE *value,
	int flags);
static inline int verify_value_in_param_choises(
	struct cli_param *cur_param, 
	ARG_MULTITYPE *value,
	int flags);
static void print_param_names_of_req_group(
	struct cli_params_config *config, 
	int req_group_idx);
static int parse_param_argument(
	struct cli_param *cur_param, 
	struct cli_param_arguments *cur_param_arguments, 
	char *cur_word,
	size_t *cur_param_nr_parsed_args,
	int flags);
static int cli_verify_req_groups(
	struct cli_params_config *config, 
	struct cli_param_arguments* params_args,
	int flags);
static int cli_par_is_value_in_choises(
	struct cli_param *param, 
	ARG_MULTITYPE *value);
static void cli_param_print_usage(
	struct cli_params_config *config, 
	struct cli_param *param);
static int cli_pos_params_has_separator(struct cli_params_config *config);

/*   </ static function forward declarations >   */




static inline int cli_arg_value_to_str_by_type(enum cli_args_val_type args_val_type, int base, char *range_str_buf, int buf_size, ARG_MULTITYPE *value) {
    if (buf_size < 1) return 0;
    switch(args_val_type) {
        case CLI_ARG_TYPE_BOOL:
            return snprintf(range_str_buf, buf_size, "%s", value->as_int ? "True" : "False");
        case CLI_ARG_TYPE_INT:
        case CLI_ARG_TYPE_INT_RANGE:
            return snprintf(range_str_buf, buf_size,
                        base == 16 ? "0x%x" : "%d", 
                        value->as_long);
        case CLI_ARG_TYPE_FLOAT:
        case CLI_ARG_TYPE_FLOAT_RANGE:
            return snprintf(range_str_buf, buf_size, "%f", value->as_float);
        case CLI_ARG_TYPE_TEXT:
            return snprintf(range_str_buf, buf_size, "%s", value->as_strptr);
        default:
        return 0;
    }
    return 0;
}
static inline int cli_param_arg_value_to_str(struct cli_param *cur_param, char *range_str_buf, int buf_size, ARG_MULTITYPE *value) {
    return cli_arg_value_to_str_by_type(
        cur_param->args_val_type, 
        cur_param->numeric_attr.default_numeric_base, 
        range_str_buf, buf_size, value);
}
static inline int cli_param_arg_value_to_str_norange(struct cli_param *cur_param, char *range_str_buf, int buf_size, ARG_MULTITYPE *value) {
    return cli_arg_value_to_str_by_type(
        CLI_PAR_ARG_TYPE_NORANGE(cur_param->args_val_type), 
        cur_param->numeric_attr.default_numeric_base, 
        range_str_buf, buf_size, value);
}

static int cli_param_range_to_str(struct cli_param *cur_param, char *range_str_buf, int buf_size) {
    char *fmt;
    int idx = 0;

    assert(buf_size > 0);

    if (buf_size <= idx) goto exit;
    range_str_buf[idx++] = '[';

    if (cur_param->numeric_attr.range.min_val.is_set)
        idx += cli_arg_value_to_str_by_type(cur_param->args_val_type, cur_param->numeric_attr.default_numeric_base, range_str_buf+idx, buf_size-idx, &cur_param->numeric_attr.range.min_val.value);

    if (buf_size <= idx) goto exit;
    range_str_buf[idx++] = ':';

    if (cur_param->numeric_attr.range.max_val.is_set)
        idx += cli_arg_value_to_str_by_type(cur_param->args_val_type, cur_param->numeric_attr.default_numeric_base, range_str_buf+idx, buf_size-idx, &cur_param->numeric_attr.range.max_val.value);

    if (buf_size <= idx) goto exit;
    range_str_buf[idx++] = ']';

    if (buf_size <= idx) goto exit;
    range_str_buf[idx] = '\0';

exit:
    range_str_buf[buf_size-1] = '\0';
    return min(buf_size, idx);
}

static int cli_param_choises_to_str(struct cli_param *param, char *choises_str_buf, int buf_size, char* separator) {
    char *fmt;
    int idx = 0;

    assert(buf_size > 0);

    if (separator == NULL) {
        separator = ", ";
    }

    if (buf_size <= idx) goto exit;
    choises_str_buf[idx++] = '{';

    int choise_idx;
    for(choise_idx = 0; choise_idx < CLI_PAR_MAX_NR_CHOISES && param->choises[choise_idx].is_set; ++choise_idx) {
        if (choise_idx > 0) {
            if (buf_size <= idx) break;
            idx += snprintf(choises_str_buf+idx, buf_size-idx, "%s", separator);
            //choises_str_buf[idx++] = separator;
        }
        idx += cli_param_arg_value_to_str(param, choises_str_buf+idx, buf_size-idx, &param->choises[choise_idx].value);
    }

    if (buf_size <= idx) goto exit;
    choises_str_buf[idx++] = '}';

    if (buf_size <= idx) goto exit;
    choises_str_buf[idx++] = '\0';

exit:
    choises_str_buf[buf_size-1] = '\0';
    return min(buf_size, idx);
}


static int cli_par_arg_type_to_str(struct cli_param *param, char *buffer, int buffer_len) {
    assert(param != NULL && buffer != NULL && buffer_len > 0);
    int len = 0;

    /* Explicity print range if it is set (bounded) and not "trivial".
    If it is simply positive/non-negative, do not print explicity,
    but just print "positive"/"non-negative" before the type-name. */
    int range_is_any_positive = CLI_PAR_ARG_NUMERIC_RANGE_IS_ANY_POSITIVE(param);
    int range_is_any_non_negative = CLI_PAR_ARG_NUMERIC_RANGE_IS_ANY_NON_NEGATIVE(param);
    int flg_print_range =   !CLI_PAR_HAS_CHOISES(param)
                            && CLI_PAR_IS_NUMERIC(param)
                            && RANGE_IS_SET_AND_BOUND((param)->numeric_attr.range)
                            && (CLI_PAR_IS_UNSIGNED(param) || 
                                   (!range_is_any_positive 
                                	&& !range_is_any_non_negative));

    if (CLI_PAR_HAS_CHOISES(param)) {
        len = cli_param_choises_to_str(param, buffer, buffer_len, " | ");
    } else {
        len = snprintf(buffer, buffer_len, "%s%s%s%s",
            (   (CLI_PAR_IS_NUMERIC(param) && !CLI_PAR_IS_UNSIGNED(param))
                    ? range_is_any_positive
                        ? "positive "
                        : range_is_any_non_negative
                            ? "non-negative "
                            : ""
                            : ""),
            (   CLI_PAR_IS_NUMERIC(param) && ((param)->numeric_attr.default_numeric_base != 0)
                ? NUMERICAL_BASE_TO_STR((param)->numeric_attr.default_numeric_base)
                : CLI_ARG_TYPE_TO_STR((param)->args_val_type)  ),
            (   CLI_PAR_IS_NUMERIC(param)
                && ((param)->numeric_attr.default_numeric_base != 0)
                && CLI_ARG_TYPE_IS_RANGE((param)->args_val_type) ? "-range" : ""  ),
            (   flg_print_range ? " in " : ""   ));
    }

    if (flg_print_range) {
        if (len >= buffer_len-1) return len;
        len += cli_param_range_to_str(param, buffer+len, buffer_len-len);
    }

    return len;
}

static struct cli_param* cli_find_param_by_short_name(struct cli_params_config *config, char short_name) {
	struct cli_param* cur_param;
	size_t name_idx;
	assert(config != NULL);
	FOREACH_CLI_PARAM(config, cur_param) {
		if (cur_param->is_separator) continue;
		for (name_idx = 0; name_idx < CLI_PARSER_MAX_SHORT_NAMES && cur_param->short_names[name_idx] != '\0'; ++name_idx) {
			if (short_name == cur_param->short_names[name_idx]) return cur_param;
		}
	}
	return NULL;
}

static struct cli_param* cli_find_param_by_long_name(struct cli_params_config *config, char* name) {
	struct cli_param* cur_param;
	size_t name_idx;
	assert(config != NULL && name != NULL);
	FOREACH_CLI_PARAM(config, cur_param) {
		if (cur_param->is_separator) continue;
		for (name_idx = 0; name_idx < CLI_PARSER_MAX_LONG_NAMES && cur_param->long_names[name_idx] != NULL; ++name_idx) {
			if (streq(name, cur_param->long_names[name_idx])) return cur_param;
		}
	}
	return NULL;
}

/* not used */
static struct cli_param* cli_find_param_by_name(struct cli_params_config *config, char* arg) {
	assert(config != NULL && arg != NULL);
	assert(arg[0] != '\0' && arg[1] != '\0');

	int is_short = IS_PARAM_SNAME_STR(arg);
	char* name = arg + 1 + (uint32_t)(!is_short);

	if (is_short) {
		return cli_find_param_by_short_name(config, *name);
	} else {
		return cli_find_param_by_long_name(config, name);
	}
	
	return NULL;
}

static struct cli_param* find_next_pos_param_separator(
	struct cli_params_config *config, struct cli_param *after_param) {
	
	struct cli_param *cur_param;
	for (cur_param = after_param; (cur_param - config->pos_params) < config->nr_pos_params; ++cur_param) {
		if (cur_param->is_separator) {
			return cur_param;
		}
	}
	return NULL;
}

static struct cli_param* find_next_pos_param_set(struct cli_params_config *config, struct cli_param *after_param) {
	/* find next positional set to use after we will encounter `--` */
	struct cli_param *next_positional_separator = find_next_pos_param_separator(config, after_param);
	if (next_positional_separator != NULL && !IS_LAST_POS_PARAM(config, next_positional_separator)) {
		return next_positional_separator+1;
	}
	return NULL;
}

static inline int verify_value_in_param_range(struct cli_param *cur_param, ARG_MULTITYPE *value, int flags) {
	if (!CLI_PAR_IS_VAL_IN_RANGE(cur_param, value)) {
		/* out of allowed range. */
		
		char range_str_buf[128];
		cli_param_range_to_str(cur_param, range_str_buf, 128);

		char given_buf[100];
		cli_param_arg_value_to_str_norange(cur_param, given_buf, 100, value);

		EPRINTF("ERROR: param `%s` requires an operand in range %s (given '%s').\n", 
			cur_param->long_names[0], 
			range_str_buf,
			given_buf);
		return -CLI_E_OUTOFRANGE;
	}
	return 0;
}

static inline int verify_value_in_param_choises(struct cli_param *cur_param, ARG_MULTITYPE *value, int flags) {
	if (CLI_PAR_HAS_CHOISES(cur_param) && !cli_par_is_value_in_choises(cur_param, value)) {
		/* not in choises. */
		
		int choises_str_buf_size = sizeof(OPTIONAL_TYPE(ARG_MULTITYPE)) * CLI_PAR_MAX_NR_CHOISES * 16;
		char choises_str_buf[choises_str_buf_size];
		cli_param_choises_to_str(cur_param, choises_str_buf, choises_str_buf_size, ", ");
		
		char given_buf[100];
		cli_param_arg_value_to_str(cur_param, given_buf, 100, value);

		EPRINTF("ERROR: param `%s` requires an operand in those choises %s (given: '%s').\n", 
			cur_param->long_names[0], 
			choises_str_buf,
			given_buf);
		return -CLI_E_OUTOFCHOISES;
	}
	return 0;
}

static void print_param_names_of_req_group(struct cli_params_config *config, int req_group_idx) {
	struct cli_param *cur_param;
	int is_first = 1;
	assert(req_group_idx > 0 && req_group_idx < config->nr_req_groups);
	FOREACH_CLI_PARAM(config, cur_param) {
		if (cur_param->is_separator) continue;
		if (cur_param->require_group_idx != req_group_idx) continue;
		if (!is_first) {
			cprintf(", ");
		}
		cprintf(cur_param->long_names[0]);
		is_first = 0;
	}
}

static int parse_param_argument(struct cli_param *cur_param, 
	struct cli_param_arguments *cur_param_arguments, 
	char *cur_word,
	size_t *cur_param_nr_parsed_args,
	int flags) {

	char* endptr;
	long num;
	int ret;

	assert(cur_param != NULL);
	assert(cur_param_arguments != NULL);
	assert(cur_word != NULL);
	assert(cur_param_nr_parsed_args != NULL);
	assert(CLI_PAR_GET_MAX_ARGS(cur_param) > 0);
	assert(CLI_PAR_GET_MAX_ARGS(cur_param) - *cur_param_nr_parsed_args > 0);

	switch(cur_param->args_val_type) {
		case CLI_ARG_TYPE_INT:
			num = strtol(cur_word, &endptr, cur_param->numeric_attr.default_numeric_base);
			/* TODO: (design) positive / non-negative? maybe use requirements flags? */
			if (cur_param->numeric_attr.is_positive && *cur_word == '-') {
				/* TODO: print error msg. */
				EPRINTF("ERROR: param `%s` requires a positive operand (given negative).\n", cur_param->long_names[0]);
				return -CLI_E_OUTOFRANGE;
			}
			if (*endptr != '\0') {
				/* cur_word has not completly read. */
				/* TODO: print error msg. */
				EPRINTF("ERROR: param `%s` requires a numerial operand (given '%s').\n", cur_param->long_names[0], cur_word);
				return -CLI_E_ARGTYPE;
			}

			cur_param_arguments->values[*cur_param_nr_parsed_args].value.as_long = num;
			ret = verify_value_in_param_range(cur_param, &cur_param_arguments->values[*cur_param_nr_parsed_args].value, flags);
			if (ret < 0) {
				return ret;
			}
			ret = verify_value_in_param_choises(cur_param, &cur_param_arguments->values[*cur_param_nr_parsed_args].value, flags);
			if (ret < 0) {
				return ret;
			}
			cur_param_arguments->values[(*cur_param_nr_parsed_args)++].is_set = 1;

			break;
	    case CLI_ARG_TYPE_BOOL:
	    	if (strieq(cur_word, "1") || strieq(cur_word, "t") || strieq(cur_word, "true") || strieq(cur_word, "y") || strieq(cur_word, "yes")) {
	    		cur_param_arguments->values[*cur_param_nr_parsed_args].value.as_uint32_t = 1;
	    		cur_param_arguments->values[(*cur_param_nr_parsed_args)++].is_set = 1;
	    		return 0;
	    	} else if (strieq(cur_word, "0") || strieq(cur_word, "f") || strieq(cur_word, "false") || strieq(cur_word, "n") || strieq(cur_word, "no")) {
	    		cur_param_arguments->values[*cur_param_nr_parsed_args].value.as_uint32_t = 0;
	    		cur_param_arguments->values[(*cur_param_nr_parsed_args)++].is_set = 1;
	    		return 0;
	    	} else {
	    		/* TODO: print error msg. */
	    		EPRINTF("ERROR: invalid operand. param `%s` expects boolean operand. \n", cur_param->long_names[0]);
	    		return -CLI_E_ARGTYPE;
	    	}
			break;
	    case CLI_ARG_TYPE_FLOAT:
	    	/* TODO: implement! */
	    	EPRINTF("arg parser: NOT IMPLEMENTED!\n");
	    	return -CLI_E_NOTIMPL;
	    	break;
	    case CLI_ARG_TYPE_TEXT:
	    	cur_param_arguments->values[*cur_param_nr_parsed_args].value.as_range.start.as_strptr = cur_word;
	    	/* TODO: decide how to use ranges here. */
			/*ret = verify_value_in_param_range(cur_param, &cur_param_arguments->values[*cur_param_nr_parsed_args].value, flags);
			if (ret < 0) {
				return ret;
			}*/
			ret = verify_value_in_param_choises(cur_param, &cur_param_arguments->values[*cur_param_nr_parsed_args].value, flags);
			if (ret < 0) {
				return ret;
			}

			cur_param_arguments->values[(*cur_param_nr_parsed_args)++].is_set = 1;

	    	break;
	    case CLI_ARG_TYPE_INT_RANGE:
			num = strtol(cur_word, &endptr, cur_param->numeric_attr.default_numeric_base);
			
			/* TODO: (design) what to do with is_positive here? */

			/* store the start of range */
			cur_param_arguments->values[*cur_param_nr_parsed_args].value.as_range.start.as_long = num;
			ret = verify_value_in_param_range(cur_param, &cur_param_arguments->values[*cur_param_nr_parsed_args].value, flags);
			if (ret < 0) {
				return ret;
			}
			ret = verify_value_in_param_choises(cur_param, &cur_param_arguments->values[*cur_param_nr_parsed_args].value, flags);
			if (ret < 0) {
				return ret;
			}

			/* if *endptr == '\0' end end num equals the start num. */

			if (*endptr != '\0') {
				if (*endptr == ':' || *endptr == ',') {
					num = strtol(endptr+1, &endptr, cur_param->numeric_attr.default_numeric_base);
				} else if (*endptr == '+' || *endptr == '-') {
					/* expecting delta from start after `+` or `-` sign */
					num += strtol(endptr, &endptr, 10);
				} else {
					/* cur_word has not completly read. */
					/* TODO: print error msg. */
					EPRINTF("ERROR: param `%s` requires a numerial range operand (given '%s').\n", cur_param->long_names[0], cur_word);
					return -CLI_E_ARGTYPE;
				}
			}

			if (*endptr != '\0') {
				/* cur_word has not completly read. */
				/* TODO: print error msg. */
				EPRINTF("ERROR: param `%s` requires a numerial range operand (given '%s').\n", cur_param->long_names[0], cur_word);
				return -CLI_E_ARGTYPE;
			}

			/* store the end of range */
			cur_param_arguments->values[*cur_param_nr_parsed_args].value.as_range.end.as_long = num;
			ret = verify_value_in_param_range(cur_param, &cur_param_arguments->values[*cur_param_nr_parsed_args].value, flags);
			if (ret < 0) {
				return ret;
			}
			ret = verify_value_in_param_choises(cur_param, &cur_param_arguments->values[*cur_param_nr_parsed_args].value, flags);
			if (ret < 0) {
				return ret;
			}

			cur_param_arguments->values[(*cur_param_nr_parsed_args)++].is_set = 1;

	    	break;
	    case CLI_ARG_TYPE_FLOAT_RANGE:
	    	/* TODO: implement! */
	    	EPRINTF("arg parser: NOT IMPLEMENTED!\n");
	    	return -CLI_E_NOTIMPL;
			break;
		default:
			assert(0);
			break;
	}
	
	return 0;
}


/* 
	TODO: (design) consider allowing receiving a single string and check for spaces internally.
	TODO: check for `"` for handling long text arguments.
	TODO: handle auto-completion (flags & CLI_PRINT_SUGGESTIONS):
			1. decide how to parse with & without the last word!
			   and decide how to print the union of allowed options.
			3. if there is only one option to complete the last word,
			   just change it without printing!
*/
int cli_parse(		struct cli_params_config *config, 
					struct cli_param_arguments* params_args, 
					int nr_words, 
					char **words,
					int flags) {
	size_t nr_params;
	int cur_word_idx;
	struct cli_param* cur_param = NULL;
	struct cli_param* cur_positional_set_start = NULL;
	struct cli_param_arguments* cur_param_arguments = NULL;
	char *cur_word;
	int reading_positional = 0;
	char* sub_word = NULL;
	size_t cur_param_nr_parsed_args = 0;  /* number of arguments already read by current param */
	int ret;
	char *short_names_sequence = NULL;  /* flags sequence after `-` perfix. example: `-amfz8` */

	assert(config != NULL && params_args != NULL);

	if (config->is_menu) {
		/* TODO: handle menu! */
		EPRINTF("ERROR: submenu not implemented!\n");
		assert(0);
		return -CLI_E_NOTIMPL;
	}

	nr_params = config->nr_pos_params + config->nr_named_params;
	memset(params_args, 0, nr_params * sizeof(struct cli_param_arguments));
	/*for (cur_param_arguments = params_args; cur_param_arguments - params_args < nr_params; ++cur_param_arguments) {
		cur_param_arguments->is_flag_set = 0;
		cur_param_arguments->values[0].is_set = 0;
	}*/

	cur_param = NULL;
	if (config->nr_pos_params > 0) {
		reading_positional = 1;
		cur_positional_set_start = config->pos_params;
		cur_param = cur_positional_set_start;
	}

	/* iterate over input words one-by-one, while splitting word into 2 subwords
	when encountering `=` sign right after long name `--param` specification. */
	for (cur_word_idx = 1; 
		sub_word != NULL || cur_word_idx < nr_words; 
		cur_word_idx += (int)(sub_word == NULL && short_names_sequence == NULL)) {

		if (short_names_sequence == NULL) {
			cur_word = (sub_word == NULL) ? words[cur_word_idx] : sub_word;
			sub_word = NULL;
		}

		if (short_names_sequence != NULL && !IS_VALID_SNAME(*short_names_sequence)) {
			assert(*short_names_sequence != '\0');
			cur_word = short_names_sequence;
			// TODO: What to do with the subword here? 
			//       How to handle `=` sign in that case?
			//       Notice that `=` sign might appear after an invalid sname. ex: `-at54=8`
			//       Currently, for short names sequence, we do not treat `=` sign as separator
			//       between param names and arguments (as we do in long params names).
			sub_word = NULL;
			short_names_sequence = NULL;
		}

		/* detect next positional set (separator `--`) or another param specification.
		   end of current parameter arguments. */
		if (  	(short_names_sequence != NULL 
		  		 && IS_VALID_SNAME(*short_names_sequence)
		  		 /*&& cli_find_param_by_short_name(config, *cur_word) != NULL*/ ) ||
				(cur_word == words[cur_word_idx] /* not a subword */ 
				 && (IS_SEPARATOR(cur_word) || IS_PARAM_NAME_STR(cur_word)))  ) {
			
			/* verify last param is not expecting additional vital arguments. */
			if (cur_param != NULL && 
				CLI_PAR_ARGS_FIELD(config, params_args, cur_param)->is_flag_set &&
				cur_param->nargs.min_val.is_set && cur_param->nargs.min_val.value > cur_param_nr_parsed_args) {
				/* TODO: print error */
				EPRINTF("ERROR: param `%s` expecting %d argument(s) (given %d).\n", 
					cur_param->long_names[0],
					cur_param->nargs.min_val.value, cur_param_nr_parsed_args);
				return -CLI_E_INVALIDNARGS;
			}

			/* current word is not a subword (not after `=` sign) and 
			current word is `--` to start new positional set */
			if(short_names_sequence == NULL && IS_SEPARATOR(cur_word)) {
				if (cur_positional_set_start != NULL) {
					/* find next positional set to use after we will encounter `--` */
					cur_positional_set_start = find_next_pos_param_set(config, cur_positional_set_start);
				}
				if (cur_positional_set_start == NULL) {
					/* no next positional set to read! */
					/* TODO: print error message! */
					EPRINTF("ERROR: A separator `--` has been used, but no next positional parameters set to read!\n");
					return -CLI_E_INVALIDSEPARATOR;
				}
				reading_positional = 1;
				cur_param = cur_positional_set_start;
				cur_param_nr_parsed_args = 0;

				continue;
			}

			/* current word is not a subword (not after `=` sign) and 
			current word is a new param specification */
			if (short_names_sequence != NULL || IS_PARAM_NAME_STR(cur_word)) {

				/* end of positional section */
				if (reading_positional) {
					reading_positional = 0;
				}

				// replace first '=' by '\0' and mark the rest of the arg as the next arg to parse.
				if (short_names_sequence == NULL && IS_PARAM_LNAME_STR(cur_word)) {
					sub_word = strchr(cur_word, '=');
					if (sub_word != NULL) {
						*sub_word = '\0';
						sub_word++;
						if (*sub_word == '\0') {
							sub_word = NULL;
						}
					}
				}
				
				/* allow specifying multiple short param names.
				   ex: `-amzxARG_FOR_X`, such that only the last can get argument. */
				if (short_names_sequence == NULL && IS_PARAM_SNAME_STR(cur_word)) {
					short_names_sequence = cur_word+1;
					assert(*short_names_sequence != '\0');
				}
				if (short_names_sequence != NULL) {
					assert(*short_names_sequence != '\0');
					cur_param = cli_find_param_by_short_name(config, *short_names_sequence);
					if (cur_param == NULL) {
						/* TODO: print error! */
						EPRINTF("ERROR: unknown param short name `%c`!\n", *short_names_sequence);
						return -CLI_E_UNKNOWNPARAM;
					}
					short_names_sequence += 1;
					if (*short_names_sequence == '\0') short_names_sequence = NULL;
				} else {
					cur_param = cli_find_param_by_long_name(config, cur_word+2);
					if (cur_param == NULL) {
						/* TODO: print error! */
						EPRINTF("ERROR: unknown param long name `%s`!\n", cur_word+2);
						return -CLI_E_UNKNOWNPARAM;
					}
				}
				assert(cur_param != NULL);
				
				cur_param_arguments = CLI_PAR_ARGS_FIELD(config, params_args, cur_param);
				if (cur_param_arguments->is_flag_set) { /* named param already set */
					/* TODO: print error! */
					EPRINTF("ERROR: named param `%s` set more than once!\n",
						cur_param->long_names[0]);
					return -CLI_E_ALREADYSET;
				}

				/* TODO: (design) do we want to forbid using `=` sign for multi-arg params? */
				
				cur_param_arguments->is_flag_set = 1;
				cur_param_nr_parsed_args = 0;

				continue;
			}

		}

		/* if needed, set cur_param to the next expected param by next_positional_param
			and inc next_positional_param */
		if (reading_positional) {
			assert(CLI_PAR_IS_POS(config, cur_param));
			/* positional param must expect arguments */
			assert(cur_param->nargs.max_val.is_set == 0 || cur_param->nargs.max_val.value > 0);
			/* after a positional param with variable number of args, 
			a separator is required (if it is not the last param) */
			assert(!IS_PAR_NARGS_VARIABLE(cur_param) || IS_LAST_POS_PARAM(config, cur_param) || (cur_param+1)->is_separator);

			/* cur_param read all of it args */
			if (CLI_PAR_GET_MAX_ARGS(cur_param) == cur_param_nr_parsed_args) {
				if (IS_LAST_POS_PARAM(config, cur_param) || (cur_param+1)->is_separator) {
					/* End of current pos params set. Nothing to do with this argument! */
					/* TODO: print error */
					EPRINTF("ERROR: param `%s` cannot recieve more than %d arguments!\n", 
						cur_param->long_names[0], CLI_PAR_GET_MAX_ARGS(cur_param));
					return -CLI_E_INVALIDNARGS;
				}

				/* next positional param */
				cur_param++;
				cur_param_nr_parsed_args = 0;
			}
		}

		/*
		if we got here, we have to read the current word as an argument of `cur_param`.
		*/

		if (cur_param_nr_parsed_args == CLI_PAR_GET_MAX_ARGS(cur_param)) {
			/* cannot read more than `CLI_PARAM_MAX_NR_ARGS` args per param */
			/* TODO: print error */
			EPRINTF("ERROR: param `%s` cannot recieve more than %d arguments!\n", 
						cur_param->long_names[0], CLI_PAR_GET_MAX_ARGS(cur_param));
			return -CLI_E_INVALIDNARGS;
		}

		cur_param_arguments = CLI_PAR_ARGS_FIELD(config, params_args, cur_param);
		cur_param_arguments->is_flag_set = 1;

		/* TODO: parse this word as the next arg of current param */
		int ret = parse_param_argument(cur_param, cur_param_arguments, cur_word, &cur_param_nr_parsed_args, flags);
		if (ret < 0) {
			return ret;
		}

		//cur_param_nr_parsed_args++;

	}

	/* verify last param is not expecting additional vital arguments. */
	if (cur_param != NULL && 
		CLI_PAR_ARGS_FIELD(config, params_args, cur_param)->is_flag_set &&
		cur_param->nargs.min_val.is_set && cur_param->nargs.min_val.value > cur_param_nr_parsed_args) {
		/* TODO: print error */
		EPRINTF("ERROR: param `%s` expecting %d arguments (given %d).\n", 
			cur_param->long_names[0],
			cur_param->nargs.min_val.value, cur_param_nr_parsed_args);
		return -CLI_E_INVALIDNARGS;
	}

	/* verify params requirements. */
	ret = cli_verify_req_groups(config, params_args, flags);
	if (ret < 0) return ret;

	return 0;
}

static int cli_verify_req_groups(struct cli_params_config *config, struct cli_param_arguments* params_args, int flags) {
	assert(config != NULL && params_args != NULL);
	
	size_t idx;
	struct cli_param *cur_param;

	/*
	Using a single pass over all possible params, we aggregate number of
	params given for each requirement group.
	*/
	assert(config->nr_req_groups < 256); /* just for case. TODO: define & use `MAX_NR_REQ_GRPS` constant. */
	/* counts given params that belongs to this req-group */
	size_t req_group_histogram[config->nr_req_groups]; 
	memset(req_group_histogram, 0, sizeof(size_t) * config->nr_req_groups);

	FOREACH_CLI_PARAM(config, cur_param) {
		if (cur_param->is_separator) continue;
		if (cur_param->require_group_idx == 0) continue;
		if (cur_param->require_group_idx == CLI_OPTIONAL_VAL) continue;
		if (cur_param->require_group_idx == CLI_REQUIRED_VAL) {
			if (CLI_PAR_ARGS_FIELD(config, params_args, cur_param)->is_flag_set) continue;
			/* TODO: print error */
			EPRINTF("ERROR: param `%s` is required but not given.\n", 
				cur_param->long_names[0]);
			return -CLI_E_PARREQ;
		}

		assert(cur_param->require_group_idx > 0);
		assert(cur_param->require_group_idx < config->nr_req_groups);

		if (CLI_PAR_ARGS_FIELD(config, params_args, cur_param)->is_flag_set) {
			req_group_histogram[cur_param->require_group_idx]++;
		}
	}

	/* for each req-group verify given number of params from this group is in the required range */
	for (idx = 1 /* 1st req-grp is dummy */; idx < config->nr_req_groups; ++idx) {
		if (!RANGE_IS_NUMERIC_VAL_IN(config->req_groups[idx], , _nr_params, req_group_histogram[idx], size_t)) {
			/* TODO: print error */
			char range_str_buf[128];
			RANGE_TO_TEXT(config->req_groups[idx], , _nr_params, range_str_buf, 128, "%u");
			EPRINTF("ERROR: Please specify %s parameter(s) out of {", 
				range_str_buf);
			print_param_names_of_req_group(config, idx);
			EPRINTF("} (%d given).\n", 
				req_group_histogram[idx]);
			return -CLI_E_PARREQGRP;
		}
	}
	return 0;
}

static int cli_par_is_value_in_choises(struct cli_param *param, ARG_MULTITYPE *value) {
    int choise_idx;
    for(choise_idx = 0; choise_idx < CLI_PAR_MAX_NR_CHOISES && param->choises[choise_idx].is_set; ++choise_idx) {
    	// if (strncmp((char*)&param->choises[choise_idx].value, (char*)value, sizeof(ARG_MULTITYPE)) == 0) return 1;
    	if (CLI_PAR_IS_NUMERIC(param)) {
        	if (param->choises[choise_idx].value.as_uint32_t == value->as_uint32_t) return 1;
    	} else {
    		if (strcmp(param->choises[choise_idx].value.as_strptr, value->as_strptr) == 0) return 1;
    	}
    }
    return 0;
}

#define USAGE_INDENTATION "  "
#define FMT_BOLD "\033[1m"
#define FMT_NORMAL "\033[0m"

static void cli_param_print_usage(struct cli_params_config *config, struct cli_param *param) {
	int is_first = 1;
	int name_idx;
	assert(param != NULL);
	if (param->is_separator) {
		assert(CLI_PAR_IS_POS(config, param));
		struct cli_param *prev_param = ((param-1) >= config->pos_params) ? (param-1) : NULL;
		cprintf(USAGE_INDENTATION "--\n" USAGE_INDENTATION USAGE_INDENTATION "Use separator to skip to the next positional parameters set.\n");
		if (prev_param != NULL && IS_PAR_NARGS_VARIABLE(prev_param)) {
			cprintf(USAGE_INDENTATION USAGE_INDENTATION "Because last parameter gets variable number of arguments.\n");
		}
		return;
	}
	cprintf(USAGE_INDENTATION);
	for (name_idx = 0;
		name_idx < CLI_PARSER_MAX_LONG_NAMES && param->long_names[name_idx] != NULL;
		++name_idx) {
		if (!is_first)
			cprintf(", ");
		is_first = 0;
		cprintf("--%s", param->long_names[name_idx]);
	}
	/* if no long names print one more space to align identation */
	if (is_first) {
		cprintf(" ");
	}
	for (name_idx = 0;
		name_idx < CLI_PARSER_MAX_SHORT_NAMES && param->short_names[name_idx] != '\0';
		++name_idx) {
		if (!is_first)
			cprintf(", ");
		is_first = 0;
		cprintf("-%c", param->short_names[name_idx]);
	}

	/* use function to calc name including range if set or explicit choises set */
	int str_type_info_len = CLI_PAR_ARG_TYPE_TO_STR_LEN(param);
	char str_type_info[str_type_info_len];
	char nargs_range_str_buf[128];
	if (IS_PAR_MIGHT_GET_ARGS(param)) {
		cli_par_arg_type_to_str(param, str_type_info, str_type_info_len);
		RANGE_TO_TEXT(param->nargs, , _val, nargs_range_str_buf, 128, "%u");
	}

	/* print number of operands & operand type as formatted synopsis string. */
	if (IS_PAR_MIGHT_GET_ARGS(param)) {

		cprintf("  %s%s%s%s%s%s%s%s%s", 

			/* mark operand as [optional] (open brackets) */
			(IS_PAR_MUST_GET_ARGS(param) ? "" : "["),

			/* formatted operand value type */
			/* for explicit options-set, wrapped with `{}` (wrapped by `cli_param_choises_to_str()`) */
			/* for a general type, wrap the type-name with `<>` */
			CLI_PAR_HAS_CHOISES(param) ? "" : "<",
			
			/* name including range if set or explicit choises set */
			str_type_info,

			/*!CLI_PAR_HAS_CHOISES(param) && CLI_PAR_IS_NUMERIC(param) && (param->numeric_attr.default_numeric_base != 0)
				? NUMERICAL_BASE_TO_STR(param->numeric_attr.default_numeric_base)
				: CLI_PAR_HAS_CHOISES(param) ? choises_str_buf : CLI_ARG_TYPE_TO_STR(param->args_val_type),
			!CLI_PAR_HAS_CHOISES(param) && CLI_PAR_IS_NUMERIC(param) && (param->numeric_attr.default_numeric_base != 0) && CLI_ARG_TYPE_IS_RANGE(param->args_val_type)
				? "-range" : "",*/
			CLI_PAR_HAS_CHOISES(param) ? "" : ">",

			/* variable number of operands */
			(!IS_PAR_MIGHT_GET_JUST_SINGLE_ARG(param) ? ", [...]" : ""),

			/* number of allowed operands */
			(!IS_PAR_MIGHT_GET_JUST_SINGLE_ARG(param) ? " (" : ""),
			(!IS_PAR_MIGHT_GET_JUST_SINGLE_ARG(param) ? nargs_range_str_buf : ""),
			(!IS_PAR_MIGHT_GET_JUST_SINGLE_ARG(param) ? ")" : ""),

			/* mark operand as [optional] (close brackets) */
			(IS_PAR_MUST_GET_ARGS(param) ? "" : "]"));
	}

	if (param->require_group_idx == CLI_REQUIRED_VAL) {
		cprintf(" !REQUIRED");
	} /*else if (param->require_group_idx == CLI_OPTIONAL_VAL || param->require_group_idx == 0) {
		cprintf(" [OPTIONAL]");
	}*/
	cprintf("\n");

	if (param->description != NULL) {
		cprintf(USAGE_INDENTATION USAGE_INDENTATION "%s\n", param->description);
	}
	
	/* print number of operands & operand type. */
	if (IS_PAR_MIGHT_GET_ARGS(param)) {
		cprintf(USAGE_INDENTATION USAGE_INDENTATION "Takes %s operand(s), each %s %s.\n", 
			nargs_range_str_buf,
			CLI_PAR_HAS_CHOISES(param) ? "of the following options" : "of type",
			str_type_info);
	} else {
		cprintf(USAGE_INDENTATION USAGE_INDENTATION "Takes no operands.\n");
	}

	/* TODO: print allowed range if set */

	if (param->require_group_idx > 0) {
		char range_str_buf[128];
		RANGE_TO_TEXT(config->req_groups[param->require_group_idx], , _nr_params, range_str_buf, 128, "%u");
		cprintf(USAGE_INDENTATION USAGE_INDENTATION "Use %s parameter(s) out of {", 
			range_str_buf);
		print_param_names_of_req_group(config, param->require_group_idx);
		cprintf("}.\n");
	}
}

static int cli_pos_params_has_separator(struct cli_params_config *config) {
	struct cli_param *cur_param;
	assert(config != NULL);

	FOREACH_CLI_POS_PARAM(config, cur_param) {
		if (cur_param->is_separator) return 1;
	}
	return 0;
}

void cli_print_usage(struct cli_params_config *config) {
	struct cli_param *cur_param;
	assert(config != NULL);

	/* TODO: print synopis with specifying the param names */
	cprintf(FMT_BOLD "SYNOPSIS" FMT_NORMAL "\n");
	cprintf(USAGE_INDENTATION "%s%s%s\n", config->name,
		config->nr_pos_params > 0 ? " [positional parameters]" : "",
		config->nr_named_params > 0 ? " [named parameters]" : "");

	if (config->description != NULL) {
		cprintf(FMT_BOLD "DESCRIPTION" FMT_NORMAL "\n");
		cprintf(USAGE_INDENTATION "%s\n", config->description);
	}

	if (config->nr_pos_params > 0) {
		cprintf(FMT_BOLD "POSITIONAL PARAMETERS" FMT_NORMAL "\n");
		cprintf(USAGE_INDENTATION "Arguments might be given by position, without specifying parameter names.\n");
		if (cli_pos_params_has_separator(config)) {
			cprintf(USAGE_INDENTATION "A separator `--` might be used (where explicity stated), \n"USAGE_INDENTATION"in order to skip to the next positional params set.\n");
		}
		cprintf(USAGE_INDENTATION "Positional parametes can also be given using their names.\n");
		cprintf(USAGE_INDENTATION "After specifying a parameter using it's name, it is impossible to \n"USAGE_INDENTATION"specify additional positional arguments, until using the \n"USAGE_INDENTATION"next separator `--` (if allowed).\n");
		FOREACH_CLI_POS_PARAM(config, cur_param) {
			cli_param_print_usage(config, cur_param);
		}
	}

	if (config->nr_named_params > 0) {
		cprintf(FMT_BOLD "NAMED PARAMETERS" FMT_NORMAL "\n");
		FOREACH_CLI_NAMED_PARAM(config, cur_param) {
			cli_param_print_usage(config, cur_param);
		}
	}
}
