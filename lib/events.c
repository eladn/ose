#include <inc/lib.h>
#include <net/ns.h>
#include <inc/events.h>

#define EVENTS_BUFFER_ENTRIES 2048

#define EV_DEBUG 0 /* TODO: turn off ! */
#define ev_debug(...) if (EV_DEBUG) cprintf(__VA_ARGS__)

static event_t events_buffer[EVENTS_BUFFER_ENTRIES];

static event_t * alloc_event() {
    int i;
    for (i = 0; i < EVENTS_BUFFER_ENTRIES; ++i)
        if (events_buffer[i].type == EV_NOT_USED) {
            events_buffer[i].type = EV_EMBARGO;
            return &events_buffer[i];
        }
    return NULL;
}

static void free_event(event_t * event) {
    assert(event);
    event->type = EV_NOT_USED;
}

static int ev__add_event_to_loop(events_loop_t * loop, event_t * event) {
    assert(loop);
    assert(event);
    LIST_INSERT_HEAD(&loop->events_queue, event, events_queue);
    return 0;
}

static int ev__detach_event_from_loop(events_loop_t * loop, event_t * event) {
    assert(loop);
    assert(event);
    LIST_REMOVE(event, events_queue);
    return 0;
}

static event_t * ev__find_nsipc_response_event(events_loop_t * loop, ns_req_id_t req_id) {
    assert(loop);
    event_t * event;
    FOREACH_EVENT(loop, event) {
        if (event->type != EV_NSIPC_RESPONSE || event->req_id != req_id) continue;
        return event;
    }
    return NULL;
}

static event_t * ev__find_ipc_event(events_loop_t * loop) {
    assert(loop);
    event_t * event;
    FOREACH_EVENT(loop, event) {
        if (event->type != EV_IPC) continue;
        return event;
    }
    return NULL;
}


/* Get an un-mapped address in memory, in order to receive an IPC mapping to it. */
#define NETREQ_VA_BEGIN 0x0ffff000  /* TODO: maybe change it */
static union Nsipc * ipc_recv_getbuf(void) {
    int r;
    void *va = find_unmapped_va(NETREQ_VA_BEGIN);
    if (!va) {
        panic("NS: buffer overflow");
        return 0;
    }
    return va;
}
static void ipc_recv_putbuf(void *va) {
    sys_page_unmap(0, va);
}


static void ev__update_fds_bitmaps(events_loop_t * loop) {
    assert(loop);

    FD_ZERO(&loop->all_fds);
    FD_ZERO(&loop->read_fds);
    FD_ZERO(&loop->write_fds);

    int maxfdn = 0;

    event_t * event;
    FOREACH_EVENT(loop, event) {
        if (!(event->type & (EV_FD_READABLE|EV_FD_WRITABLE))) continue;
        maxfdn = max(maxfdn, event->fd + 1);
        FD_SET(event->fd, &loop->all_fds);
        if (event->type & EV_FD_READABLE)
            FD_SET(event->fd, &loop->read_fds);
        if (event->type & EV_FD_WRITABLE)
            FD_SET(event->fd, &loop->write_fds);
    }
    loop->maxfdn = maxfdn;
    ev_debug("EV: ev__update_fds_bitmaps() found : %d fds\n", maxfdn);
}

static void ev__fd_listen_handler_thread(uint32_t arg);

static void ev__select_thread(uint32_t arg) {
    int r;
    events_loop_t *loop = (events_loop_t *)arg;
    assert(loop);

    ev_debug("EV: inside ev__select_thread(): thread just created! thread id: %d\n", thread_id());

    while (!loop->stop) {
        int maxfdn = loop->maxfdn;
        /* if no fds to listen to - we exit the thread. */
        if (maxfdn < 1) break;
        fd_set read_fds = loop->read_fds;
        fd_set write_fds = loop->write_fds;
        fd_set except_fds = loop->except_fds;
        struct timeval timeout = { .tv_sec = 5 };

        /* check that we are still attached to the loop */
        void *userdata;
        if ((r = thread_retrieve_my_user_data((void**)&userdata)) < 0)
            panic("EV: inside ev__select_thread(): Couldn't get user data (loop). err: %e\n", r);
        if (userdata != loop) {
            // we disconnected from loop - do not continue!
            ev_debug("EV: inside ev__select_thread(): we disconnected from loop - exit thread! thread id: %d\n", thread_id());
            return;
        }

        /* We set timeout after 5sec to check if loop still alive or changed.
        If loop event changed, the loop would disconnect us as the main listener
        and create a new listener thread instead. We will still get the timeout msg
        (or other msg about fds availability from nsipc) bacause we are also a regular
        thread that is registered to the loop, just because we called `ev_select()`. */

        ev_debug("EV: inside ev__select_thread(): just before ev_select(). maxfdn: %d thread id: %d\n", maxfdn, thread_id());

        int nready = ev_select(loop, maxfdn, &read_fds, &write_fds, &except_fds, &timeout);

        ev_debug("EV: inside ev__select_thread(): returned from ev_select(). nready: %d thread id: %d\n", nready, thread_id());

        if (nready == 0) continue;

        event_t * event;
        FOREACH_EVENT(loop, event) {
            if (!(event->type & (EV_FD_READABLE|EV_FD_WRITABLE))) continue;
            event_type_t events = 0;
            if (FD_ISSET(event->fd, &read_fds))
                events |= EV_FD_READABLE;
            if (FD_ISSET(event->fd, &write_fds))
                events |= EV_FD_WRITABLE;
            if (!events) continue;
            ev_debug("EV: inside ev__select_thread(): found event [addr: %x] mathing fd %d with selectevents: %x returned from ev_select(). my thread id: %d\n", event, event->fd, events, thread_id());
            if (!event->repeated) ev__detach_event_from_loop(loop, event);
            event->reported_fd_events = events; // so the user would know actual events.
            if (event->waiting_thread_id) {
                thread_wakeup_by_id(event->waiting_thread_id);
                continue;
                // event is freed by the registerer ev_thread_wait_for_sock(), after we'll yield.
            }
            else if (event->fd_listen_cb) {
                thread_id_t tid;
                r = thread_create(&tid, "ev__fd_listen_handler_thread", ev__fd_listen_handler_thread, (uint32_t)event);
                if (r < 0) panic("EV: Couldn't create ev__fd_listen_handler_thread thread!\n");
                r = thread_assign_user_data(tid, (void*)loop);
                if (r < 0) panic("EV: Couldn't store `loop` as user data of ev__fd_listen_handler_thread thread!\n");
                continue;
                // ev__fd_listen_handler_thread() is freeing the event.
            } else {
                panic("EV: inside ev__select_thread(): found event waiting on signalled fd %d, that has no callback and no thread id. my thread id: %d\n", event->fd, thread_id());
            }
            if (!event->repeated) free_event(event);
        }

        ev__update_fds_bitmaps(loop);

        thread_yield();
    }

    /* Mark the loop we are not available. */
    loop->select_tid = 0;
}

static void ev__ipc_handler_thread(uint32_t arg) {
    event_t *event = (event_t *)arg;
    events_loop_t *loop;
    int r;

    if ((r = thread_retrieve_my_user_data((void**)&loop)) < 0)
        panic("EV: inside ev__ipc_handler_thread(): Couldn't get user data (loop).\n");

    assert(event);
    assert(loop);

    event->ipc_recv_cb(loop, event->ipc_recv_from_env, event->ipc_recv_value,
                       event->ipc_recv_pg, event->ipc_recv_perm, event->user_context);

    ipc_recv_putbuf(event->ipc_recv_pg);

    if (!event->repeated) free_event(event);
}

static void ev__fd_listen_handler_thread(uint32_t arg) {
    event_t *event = (event_t *)arg;
    events_loop_t *loop;
    int r;

    if ((r = thread_retrieve_my_user_data((void**)&loop)) < 0)
        panic("EV: inside ev__fd_listen_handler_thread(): Couldn't get user data (loop).\n");

    assert(event);
    assert(loop);

    event->fd_listen_cb(loop,
                        event->fd,
                        event->reported_fd_events /* updated by the select() listener */,
                        event->user_context);

    if (!event->repeated) free_event(event);
}

int ev_init_loop(events_loop_t *loop) {
    assert(loop);
    memset(loop, 0, sizeof(*loop));
    thread_init();
    return 0;
}

static int ev__reset_select_thread(events_loop_t * loop) {
    assert(loop);
    int r;
    thread_id_t select_tid, old_select_tid;

    old_select_tid = loop->select_tid;

    r = thread_create(&select_tid, "ev__select_thread", ev__select_thread, (uint32_t)loop);
    if (r < 0) panic("ev__loop() : Couldn't create ev__select_thread thread!\n");
    loop->select_tid = select_tid;
    r = thread_assign_user_data(select_tid, (void*)loop);
    if (r < 0) panic("ev__loop() : Couldn't store `loop` as user data of ev__select_thread thread!\n");

    ev_debug("EV: ev__reset_select_thread(): create a new select thread. new thread id: %u old thread id: %u.\n",
        select_tid, old_select_tid);

    if (old_select_tid) {
        /* detach old select handler - it will automatically exit in a few sec */
        thread_assign_user_data(old_select_tid, NULL);
        // We don't care if it failed (if thread already not running for some reason).
    }

    return 0;
}

static void ev__loop_on_halt(thread_id_t tid) {
    cprintf("\n\n <<<< ev loop halted!! threadid %d >>>> \n\n", tid);
}

static void ev__loop(uint32_t arg) {
    static envid_t nsenv;
    if (nsenv == 0) {
        nsenv = ipc_find_env(ENV_TYPE_NS);
        assert(nsenv != 0);
    }

    thread_onhalt(ev__loop_on_halt);

    events_loop_t *loop = (events_loop_t *)arg;
    assert(loop);
    int r;
    envid_t from_env;
    int recv_value;
    int recv_perm_store;
    loop->stop = 0;
    while (!loop->stop) {
        // init the ev__select_thread if no such and needed
        if (!loop->select_tid && loop->maxfdn > 0) {
            ev__reset_select_thread(loop);
            thread_yield(); // let the thread created run
        }

        // let all waiting threads to run before we block for ipc receive
        thread_yield();
        //thread_yield();
        //thread_yield();
        //thread_yield();

        void *ipc_pg;// = ipc_recv_getbuf();
        envid_t enforce_recv_from_nsenv = (loop->wait_for_ipc ? 0 : nsenv);
        ev_debug("EV: loop before receive IPC. %s (%x).\n", 
            (enforce_recv_from_nsenv == 0 ? "Allow IPC from anyone. " : "Allow IPC only from nsenv env id: "),
            enforce_recv_from_nsenv);
        //recv_value = ipc_recv_from_env(enforce_recv_from_nsenv, &from_env, ipc_pg, &recv_perm_store);
        r = ipc_async_receive(enforce_recv_from_nsenv, &from_env, 
                              &recv_value, &ipc_pg, &recv_perm_store);
        if (r < 0) panic("EV: ipc_async_receive() returned: %e\n\n", r);
        ev_debug("EV: loop after receive IPC from: %x \n", from_env);
        event_t *event;
        if (from_env == nsenv) {
            assert(ipc_pg != NULL);
            // The netserver sends us back the same request page that the user
            // initialy sent to it, with the matching req_id, so we can know
            // where to route this response message!
            // The response value contains the retval of the requested service.
            assert((recv_perm_store & (PTE_P|PTE_U)) == (PTE_P|PTE_U));
            ns_req_id_t req_id = *((ns_req_id_t*)ipc_pg);

            // A response message has been received from network server.
            // Dispatch it to the waiting thread that initiated the matching request.
            // If not found matching waiting thread, we just drop it.
            event = ev__find_nsipc_response_event(loop, req_id);
            ev_debug("EV: received ipc from network-server. req_id: %u event: %p ipc_pg: %p perm: %x waiting thread id: %u.\n",
                (uint32_t)req_id, event, ipc_pg, recv_perm_store, (event == NULL ? 0 : event->waiting_thread_id));
            if (event) {
                event->ipc_recv_value = recv_value;
                ev__detach_event_from_loop(loop, event);
                if ((r = thread_wakeup_by_id(event->waiting_thread_id)) < 0)
                    panic("PANIC: EV: loop couldn't find thread matching to response from network-server. req_id: %u event: %p waiting thread id: %u.\n", (uint32_t)req_id, event, event->waiting_thread_id);
                // `ev_thread_wait_for_nsipc_response()` is freeing the event!
            }
            free_page(ipc_pg);
        } else if ((event = ev__find_ipc_event(loop)) != NULL) {
            /* handle IPC message (not from network-server). create thread for calling the callback from there. */
            /* TODO: currently we route a single incoming IPC for a single event. reconsider it. */
            /* TODO: we might want to allow the events to filter msgs by sender. */

            if (event->waiting_thread_id) {
                thread_wakeup_by_id(event->waiting_thread_id);
                continue;
                // event is freed by the registerer ev_thread_ipc_recv(), after we'll yield.
                // NOTE: REEIVED PAGE HAS TO BE UNMAPPED BY THE RECEIVING USER.
            }
            else if (event->ipc_recv_cb) {
                event->ipc_recv_from_env = from_env;
                event->ipc_recv_value = recv_value;
                event->ipc_recv_pg = ipc_pg;
                event->ipc_recv_perm = recv_perm_store;
                if (!event->repeated) {
                    ev__detach_event_from_loop(loop, event);
                    loop->wait_for_ipc--;
                }
                thread_id_t tid;
                r = thread_create(&tid, "ev__ipc_handler_thread", ev__ipc_handler_thread, (uint32_t)event);
                if (r < 0) panic("ev__loop() : Couldn't create ev__ipc_handler_thread thread!\n");
                r = thread_assign_user_data(tid, (void*)loop);
                if (r < 0) panic("ev__loop() : Couldn't store `loop` as user data of ev__ipc_handler_thread thread!\n");
                thread_yield(); // let the thread created run
                // ev__ipc_handler_thread() is freeing the event.
                // recived page unmapped by ev__ipc_handler_thread after the user callback has returned. (TODO: reconsider)
            }
        }
    }
}

int ev_loop_stop(events_loop_t *loop) {
    assert(loop);
    loop->stop = 1;
    return 0;
}

int ev_loop(events_loop_t *loop) {
    assert(loop);
    thread_id_t loop_tid;
    int r;

    if (thread_is_in_thread()) {
        ev__loop((uint32_t)loop);
    } else {
        // we are not inside of a thread. we have to create one.
        r = thread_create(&loop_tid, "loopmain", ev__loop, (uint32_t)loop);
        if (r < 0) return r;
        loop->loop_tid = loop_tid;
        thread_yield();
        // never coming here, because not called from thread!
        assert(0);
    }

    return 0;
}

int ev_func_wait_for_ipc(events_loop_t *loop, ipc_recv_cb_t cb, int repeated, void *user_context) {
    assert(loop);
    assert(cb);
    event_t * event;
    if ((event = alloc_event()) == NULL)
        return -E_NO_MEM;

    memset(event, 0, sizeof(*event));
    event->type = EV_IPC;
    event->ipc_recv_cb = cb;
    event->repeated = repeated;
    event->user_context = user_context;

    ev__add_event_to_loop(loop, event);

    loop->wait_for_ipc++;

    return 0;
}

int ev_thread_ipc_recv(events_loop_t *loop, envid_t *from_env,
                       void **ipc_recv_pg, int *ipc_recv_perm_store) {
    assert(loop);
    event_t * event;
    if ((event = alloc_event()) == NULL)
        return -E_NO_MEM;

    memset(event, 0, sizeof(*event));
    event->type = EV_IPC;
    event->waiting_thread_id = thread_id();

    ev__add_event_to_loop(loop, event);

    // go to next thread.
    // we assert there exist another thread (the loop main thread).
    thread_sleep();

    /* Woke up! meaning we got the ack msg from network server with retval ! */

    int ipc_recv_value = event->ipc_recv_value;
    if (from_env)
        *from_env = event->ipc_recv_from_env;
    if (ipc_recv_pg)
        *ipc_recv_pg = event->ipc_recv_pg;
    if (ipc_recv_perm_store)
        *ipc_recv_perm_store = event->ipc_recv_perm;

    // The receiver is responsible to unmap the page.
    // Hence we do not have to: ipc_recv_putbuf(event->ipc_recv_pg);

    free_event(event);

    return ipc_recv_value;
}

int ev_thread_wait_for_nsipc_response(events_loop_t *loop, ns_req_id_t req_id) {
    assert(loop);
    event_t * event;
    if ((event = alloc_event()) == NULL)
        return -E_NO_MEM;

    memset(event, 0, sizeof(*event));
    event->type = EV_NSIPC_RESPONSE;
    event->req_id = req_id;
    event->waiting_thread_id = thread_id();

    ev__add_event_to_loop(loop, event);

    ev_debug("EV: ev_thread_wait_for_nsipc_response() registered event. req_id: %u event: %p current thread id: %u.\n", ((uint32_t)req_id), event, event->waiting_thread_id);

    // go to next thread.
    // we assert there exist another thread (the loop main thread).
    thread_sleep();

    /* Woke up! meaning we got the ack msg from network server with retval ! */

    int ipc_recv_value = event->ipc_recv_value;
    ipc_recv_putbuf(event->ipc_recv_pg);
    free_event(event);

    return ipc_recv_value;
}

int ev_thread_wait_for_sock(events_loop_t *loop, int fd, event_type_t type) {
    assert(loop);
    if (!type || (type & ~(EV_FD_READABLE|EV_FD_WRITABLE)) )
        return -E_INVAL;

    volatile event_t * event;
    if ((event = alloc_event()) == NULL)
        return -E_NO_MEM;

    memset((void*)event, 0, sizeof(*event));
    event->type = type;
    event->fd = fd;
    event->waiting_thread_id = thread_id();

    ev__add_event_to_loop(loop, (event_t*)event);

    FD_SET(fd, &loop->all_fds);
    if (type & EV_FD_READABLE)
        FD_SET(fd, &loop->read_fds);
    if (type & EV_FD_WRITABLE)
        FD_SET(fd, &loop->write_fds);

    loop->maxfdn = max(loop->maxfdn, fd+1);

    ev__reset_select_thread(loop);

    thread_sleep();

    int events = event->reported_fd_events;

    assert(!event->repeated);
    free_event((void*)event);

    return events; /* it has been changed by the select() listener */
}

int ev_func_wait_for_sock(events_loop_t *loop, fd_listen_cb_t cb, int fd, 
                          event_type_t type, int repeated, void *user_context)
{
    assert(loop);
    assert(cb);
    if (!type || (type & ~(EV_FD_READABLE|EV_FD_WRITABLE)) )
        return -E_INVAL;

    event_t * event;
    if ((event = alloc_event()) == NULL)
        return -E_NO_MEM;

    memset((void*)event, 0, sizeof(*event));
    event->type = type;
    event->fd = fd;
    event->fd_listen_cb = cb;
    event->repeated = repeated;
    event->user_context = user_context;

    ev__add_event_to_loop(loop, (event_t*)event);

    FD_SET(fd, &loop->all_fds);
    if (type & EV_FD_READABLE)
        FD_SET(fd, &loop->read_fds);
    if (type & EV_FD_WRITABLE)
        FD_SET(fd, &loop->write_fds);

    loop->maxfdn = max(loop->maxfdn, fd+1);

    ev_debug("EV: ev_func_wait_for_sock() added fd %d with event addr %x \n", fd, event);

    ev__reset_select_thread(loop);

    return 0;
}
