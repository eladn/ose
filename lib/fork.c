// implement fork from user space

#include <inc/string.h>
#include <inc/lib.h>
#include <inc/mmu.h>

// PTE_COW marks copy-on-write page table entries.
// It is one of the bits explicitly allocated to user processes (PTE_AVAIL).
#define PTE_COW		0x800

//
// Custom page fault handler - if faulting page is copy-on-write,
// map in our own private writable copy.
//
int
cow_pgfault_handler(struct UTrapframe *utf)
{
	void *addr = (void *) utf->utf_fault_va;
	uint32_t err = utf->utf_err;
	int r;

	// Check that the faulting access was (1) a write, and (2) to a
	// copy-on-write page.  If not, panic.
	// Hint:
	//   Use the read-only page table mappings at uvpt
	//   (see <inc/memlayout.h>).

	// LAB 4: Your code here.
	// We check that the page-table of this pte is indeed mapped in the pgdir,
	// otherwise we could get another pgfault while trying to deref *pte,
	// in order to check flags.
	pte_t *pte = USER_PTE_OF_ADDR(addr);
	if ((err & FEC_WR) == 0  
		|| !PTE_IS_MAPPED(*USER_PDE_OF_ADDR(addr)) 
		|| (PTE_FLAGS(*pte) & PTE_COW) == 0) {
		
		/* multiple pgfault handlers. maybe the next handler will succeed. */
		/* If all handlers failed, the framework would panic() automatically. */
		return -1;
		/* now no need to panic.. */ //panic("pgfault: segfault at %p.", addr);
	}

	// Allocate a new page, map it at a temporary location (PFTEMP),
	// copy the data from the old page to the new page, then move the new
	// page to the old page's address.
	// Hint:
	//   You should make three system calls.

	// LAB 4: Your code here.
	void *page_addr = ROUNDDOWN(addr, PGSIZE);
	uint32_t perm = ((PTE_FLAGS(*pte) & PTE_SYSCALL) & ~PTE_COW) | PTE_W;
	assert((perm & (PTE_U | PTE_P)) == (PTE_U | PTE_P));
	if ((r = sys_page_alloc(0, PFTEMP, perm)) < 0)
		panic("sys_page_alloc: %e", r);
	memmove(PFTEMP, page_addr, PGSIZE);
	if ((r = sys_page_map(0, PFTEMP, 0, page_addr, perm)) < 0)
		panic("sys_page_map: %e", r);
	if ((r = sys_page_unmap(0, PFTEMP)) < 0)
		panic("sys_page_unmap: %e", r);

	return 0; // succeed!
}

//
// Map our virtual page pn (address pn*PGSIZE) into the target envid
// at the same virtual address.  If the page is writable or copy-on-write,
// the new mapping must be created copy-on-write, and then our mapping must be
// marked copy-on-write as well.  (Exercise: Why do we need to mark ours
// copy-on-write again if it was already copy-on-write at the beginning of
// this function?)
//
// Returns: 0 on success, < 0 on error.
// It is also OK to panic on error.
//
static int
duppage(envid_t envid, unsigned pn)
{
	// LAB 4: Your code here.
	int r;
	pte_t *pte = ((pte_t*)uvpt) + pn;
	void* addr = (void*)(pn << PTXSHIFT);

	assert((uintptr_t)addr < UTOP);
	if ((PTE_FLAGS(*pte) & (PTE_COW | PTE_W)) != 0 && (PTE_FLAGS(*pte) & (PTE_SHARE)) == 0) {
		uint32_t perm = (((PTE_FLAGS(*pte) & PTE_SYSCALL) | PTE_COW) & ~PTE_W);
		assert((perm & (PTE_U | PTE_P)) == (PTE_U | PTE_P));
		if ((r = sys_page_map(0, addr, envid, addr, perm)) < 0) {
			return r;
		}
		if ((r = sys_page_map(0, addr, 0, addr, perm)) < 0) {
			return r;
		}
	} else { /* RO or SHAREd */
		// RO Note [lab 4]: if one of the envs re-maps this page to write, it will be catastrophique.
		// SHARE Note [lab 5]: if we decide to take action because of the above note, we need to KEEP
		// this functionality for the cases when PTE_SHARE is on.
		if ((r = sys_page_map(0, addr, envid, addr, PTE_FLAGS(*pte) & PTE_SYSCALL)) < 0) {
			return r;
		}
	}

	return 0;
}

//
// User-level fork with copy-on-write.
// Set up our page fault handler appropriately.
// Create a child.
// Copy our address space and page fault handler setup to the child.
// Then mark the child as runnable and return.
//
// Returns: child's envid to the parent, 0 to the child, < 0 on error.
// It is also OK to panic on error.
//
// Hint:
//   Use uvpd, uvpt, and duppage.
//   Remember to fix "thisenv" in the child process.
//   Neither user exception stack should ever be marked copy-on-write,
//   so you must allocate a new page for the child's user exception stack.
//
envid_t
fork(void)
{
	// LAB 4: Your code here.
	int r, ret;
	envid_t envid;
	reg_pgfault_handler(cow_pgfault_handler); // step 1.
	if ((envid = sys_exofork()) < 0) return envid; // step 2.

	if (envid == 0) {
		/* here we are the child */
		assert(isreg_pgfault_handler(cow_pgfault_handler));

		envid_t env_id = sys_getenvid();
		assert(env_id >= 0);
		thisenv = &envs[ENVX(env_id)];

		return 0;
	}

	/* here we are the parent */
	/* stage 3: iterate through mapped pages & duplicate to child */
	assert(IS_ALIGNED_TO_PT(UTOP));
	assert(UTOP < (uintptr_t)uvpt);
	volatile pde_t *pde;
	FOREACH_PDE(uvpd, pde) {
		volatile pte_t *pte;
		uint32_t pde_idx = pde - uvpd;
		volatile pte_t *pg_table = PGADDR(PDX(uvpt), pde_idx, 0);

		if ((uintptr_t)PGADDR(pde_idx, 0, 0) >= UTOP) break;

		FOREACH_PTE(pg_table, pte) {
			uint32_t pte_idx = pte - pg_table;
			if ((uintptr_t)PGADDR(pde_idx, pte_idx, 0) == (UXSTACKTOP - PGSIZE))
				continue;

			if ((r = duppage(envid, VPN(pde_idx, pte_idx))) < 0) {
				goto fail;
			}
		}
	}

	/* call multiple syscalls through a single syscall. */
	extern void _pgfault_upcall(void);
	syscall_frame_t syscalls[] = {
		/* stage 4: set page fault entry point for child.
		   note that the library global `_pgfault_handler` has already been copied above. */
		SYSCALL_FRAME(SYS_page_alloc, envid, (UXSTACKTOP - PGSIZE), (PTE_U | PTE_P | PTE_W), 0, 0),
		SYSCALL_FRAME(SYS_env_set_pgfault_upcall, envid, _pgfault_upcall, 0, 0, 0),
		/* stage 5: mark child env as runnable */
		SYSCALL_FRAME(SYS_env_set_status, envid, ENV_RUNNABLE, 0, 0, 0),
	};
	int runned = sys_multiple_syscalls(syscalls, 3, 1 /* stop on failure */);
	if ((r = syscalls[0].retval) < 0
		|| (r = syscalls[1].retval) < 0
		|| (r = syscalls[2].retval) < 0)
		goto fail;
	assert(runned == 3);

	return envid;

fail:
	ret = sys_env_destroy(envid);
	assert(ret == 0);
	return r;
}

// Challenge!
int
sfork(void)
{
	panic("sfork not implemented");
	return -E_INVAL;
}
