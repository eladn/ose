// User-level page fault handler support.
// Rather than register the C page fault handler directly with the
// kernel as the page fault handler, we register the assembly language
// wrapper in pfentry.S, which in turns calls the registered C
// function.

#include <inc/lib.h>


// Assembly language pgfault entrypoint defined in lib/pfentry.S.
extern void _pgfault_upcall(void);

// Pointer to currently installed C-language pgfault handler.
void (*_pgfault_handler)(struct UTrapframe *utf);

//
// Set the page fault handler function.
// If there isn't one yet, _pgfault_handler will be 0.
// The first time we register a handler, we need to
// allocate an exception stack (one page of memory with its top
// at UXSTACKTOP), and tell the kernel to call the assembly-language
// _pgfault_upcall routine when a page fault occurs.
//
void
set_pgfault_handler(void (*handler)(struct UTrapframe *utf))
{
	int r;

	if (_pgfault_handler == 0) {
		// First time through!
		// LAB 4: Your code here.
		if ((r = sys_page_alloc(0, (void*)(UXSTACKTOP - PGSIZE), (PTE_U | PTE_P | PTE_W))) < 0)
			panic("set_pgfault_handler: ERROR allocating user exception stack. err: %e.", r);
		if ((r = sys_env_set_pgfault_upcall(0, (void*)_pgfault_upcall)) < 0)
			panic("set_pgfault_handler: ERROR setting user pgfault upcall handler. err: %e.", r);
	}

	// Save handler pointer for assembly to call.
	_pgfault_handler = handler;
}


#define MAX_PGFAULT_HANDLERS 10

static pgfault_handler_t multiple_pgfault_handlers_list[MAX_PGFAULT_HANDLERS];

void
multiple_pgfault_handlers_dispatcher(struct UTrapframe *utf)
{
	int r;
	pgfault_handler_t handler;
	int i;
	void *addr = (void *) utf->utf_fault_va;

	for (i = 0; i < MAX_PGFAULT_HANDLERS; ++i) {
		handler = multiple_pgfault_handlers_list[i];
		if (!handler) break;
		if ((r = handler(utf)) == 0) return; // this one successfully handled the pgfault!
	}

	/* None of the registered pgfault handlers could handle the fault.. now we panic! */
	panic("pgfault: segfault at %p.", addr);
}

int
isreg_pgfault_handler(pgfault_handler_t handler)
{
	int i;

	for (i = 0; i < MAX_PGFAULT_HANDLERS; ++i) {
		if (multiple_pgfault_handlers_list[i] == handler)
			return 1; // handler found.
	}

	return 0; // handler not found.
}

int
reg_pgfault_handler(pgfault_handler_t handler)
{
	int i;

	set_pgfault_handler(multiple_pgfault_handlers_dispatcher);

	if (isreg_pgfault_handler(handler))
		return -E_ALREADY_SET;

	/* currently just add to the first free place in the list... */
	/* TODO: We might want to control the order somehow.. */
	for (i = 0; i < MAX_PGFAULT_HANDLERS; ++i) {
		if (multiple_pgfault_handlers_list[i]) continue;
		
		// found a free place for handler
		multiple_pgfault_handlers_list[i] = handler;
		return 0;
	}

	return -E_FULL; // Not found a free space..
}
