// User-level IPC library routines

#include <inc/lib.h>


#define IPC_DEBUG 0 /* TODO: turn off ! */
#define ipc_debug(...) if (IPC_DEBUG) cprintf(__VA_ARGS__)

// Receive a value via IPC and return it.
// If 'pg' is nonnull, then any page sent by the sender will be mapped at
//	that address.
// If 'from_env_store' is nonnull, then store the IPC sender's envid in
//	*from_env_store.
// If 'perm_store' is nonnull, then store the IPC sender's page permission
//	in *perm_store (this is nonzero iff a page was successfully
//	transferred to 'pg').
// If the system call fails, then store 0 in *fromenv and *perm (if
//	they're nonnull) and return the error.
// Otherwise, return the value sent by the sender
//
// Hint:
//   Use 'thisenv' to discover the value and who sent it.
//   If 'pg' is null, pass sys_ipc_recv a value that it will understand
//   as meaning "no page".  (Zero is not the right value, since that's
//   a perfectly valid place to map a page.)
// BLOCKING till receive!
int32_t
ipc_recv_from_env(envid_t only_from_env, envid_t *from_env_store, void *pg, int *perm_store)
{
	// LAB 4: Your code here.
	int r;

	if (pg == NULL) {
		pg = (void*)UTOP;
	}

	if ((r = sys_ipc_recv_from_env(pg, only_from_env, 0 /* BLOCK! */)) < 0) {
		if (from_env_store != NULL)
			*from_env_store = 0;
		if (perm_store != NULL)
			*perm_store = 0;
		return r;
	}

	// The first place in the `env_ipc_recv_msgs` array is saved for blocking-receivers.
	const volatile ipc_msg_t *msg = &thisenv->env_ipc_recv_msgs[0];

	if (from_env_store != NULL) {
		*from_env_store = msg->from;
	}
	//ipc_debug("ipc_recv_from_env(): to env: %x from env: %x value: %d perm_store addr: %x perm: %x\n", thisenv->env_id, thisenv->env_ipc_from, thisenv->env_ipc_value, perm_store, thisenv->env_ipc_perm);
	if (perm_store != NULL) {
		*perm_store = msg->perm;
	}

	return msg->value;
}

int32_t
ipc_recv(envid_t *from_env_store, void *pg, int *perm_store)
{
	return ipc_recv_from_env(0 /* allow from anyone */, from_env_store, pg, perm_store);
}

int
ipc_async_receive(envid_t only_from_env, envid_t *from_env_store, int *value_store, void **pg_store, int *perm_store) {
	assert(thread_is_in_thread());
	int r;
	int recv_ret;
	void *pg;
	int page_added_to_recv_queue = 0;


	// We have to supply an unmapped va to receive.
	// We allocate a page for that cause, in order to get a free va
	// and make it not available for other threads.
	// This page will be unmapped if the sender has not shared a page with us.
	pg = malloc_page();
	if (!pg) return -E_NO_MEM;
#define PRINT_BUCKET 80000
int i = -1;
	while (1) {
		i++;
		if (((i) % PRINT_BUCKET) == 0) ipc_debug("ipc_async_receive(): in while begin (this env: %x)\n", thisenv->env_id);

		recv_ret = sys_ipc_recv_async_consume(only_from_env, from_env_store, value_store, pg_store, perm_store);
		if (recv_ret == 0 && *perm_store == 0) {
			//ipc_debug("ipc_async_receive(): received! and no shared page\n");
			// As said above, here we unmap the previous allocated page
			// (maybe by another thread), because the sender did not shared
			// a page with us.
			// Note: We use the async `sys_ipc_recv_from_env(,,1)` only from 
			// here in the user, and hence we know that we allocated a page
			// in that addr.
			assert(*pg_store != NULL);
			free_page(*pg_store);
			*pg_store = NULL;
		}

		// We have to add a single page to the receive waiting queue, for the page we consumpted.
		// It is ok to first ask for waiting msg and only then to add the page,
		// because threads are NOT automatically-preemptive.
		if (!page_added_to_recv_queue) {
			if (((i) % PRINT_BUCKET) == 0) ipc_debug("ipc_async_receive(): !page_added_to_recv_queue!\n");
			r = sys_ipc_recv_from_env(pg, only_from_env, 1);
			if (r == 0) {
				page_added_to_recv_queue = 1;
			}
			else {
				assert(r == -E_FULL);
			}
			// After a message has been received we surely can add a new async receive msg to queue.
			// Note: Important! DO NOT thread_yield() between the consume & the async receive.
			// Otherwise we would have to spin here to wait for empty place in queue even if we
			// already got a message.
			if (recv_ret == 0) {
				assert(r == 0);
				break;
			}
		}
		if (recv_ret == 0) {
			//ipc_debug("ipc_async_receive(): received!\n");
			break;
		}
		sys_yield();
		thread_yield();
	}

	ipc_debug("ipc_async_receive(): RECEIVED! (this env: %x from env: %x)\n", thisenv->env_id, *from_env_store);

	return 0;
}


// Send 'val' (and 'pg' with 'perm', if 'pg' is nonnull) to 'toenv'.
// This function keeps trying until it succeeds.
// It should panic() on any error other than -E_IPC_NOT_RECV.
//
// Hint:
//   Use sys_yield() to be CPU-friendly.
//   If 'pg' is null, pass sys_ipc_try_send a value that it will understand
//   as meaning "no page".  (Zero is not the right value.)
/*void
ipc_send(envid_t to_env, uint32_t val, void *pg, int perm)
{
	// LAB 4: Your code here.
	int r;
	if (pg == NULL) {
		pg = (void*)UTOP;
	}
	while (1) {
		r = sys_ipc_try_send(to_env, val, pg, perm);
		if (r == 0) return;
		if (r != -E_IPC_NOT_RECV) {
			panic("ipc_send: Received error: %e", r);
		}
		sys_yield();
	}
}*/
static void
ipc_send_aux(envid_t to_env, uint32_t val, void *pg, int perm, int move)
{
	// LAB 4: Your code here.
	int r1, r2;
	if (pg == NULL) {
		pg = (void*)UTOP;
	}
	syscall_frame_t syscalls[] = {
		SYSCALL_FRAME(SYS_ipc_try_send, to_env, val, pg, perm, 0),
		SYSCALL_FRAME(SYS_page_unmap, 0, pg, 0, 0, 0),
	};
	int i = 0;
	while (1) {
		int nr_syscalls_frames = (move ? 2 : 1);  // call `sys_page_unmap()` only when `move` is set.
		int nr_runned = sys_multiple_syscalls(syscalls, nr_syscalls_frames, 1 /* stop on failure */);

		/* r = sys_ipc_try_send(to_env, val, pg, perm); */

		r1 = syscalls[0].retval;
		r2 = (move ? syscalls[1].retval : 0);

		if (r1 == 0 && r2 == 0) {
			if (((i++) % PRINT_BUCKET) == 0) ipc_debug("ipc_send_aux() RECEIVED. error: %d (this env: %x to_env: %x, pg: %x, perm: %x, move: %d)\n",
												r1, thisenv->env_id, to_env, pg, perm, move);
			return;
		}

		if (r1 == -E_IPC_NOT_RECV || r1 == -E_FULL) {
			if (((i++) % PRINT_BUCKET) == 0) ipc_debug("ipc_send_aux() spin. error: %d (this env: %x to_env: %x, pg: %x, perm: %x, move: %d)\n",
												r1, thisenv->env_id, to_env, pg, perm, move);
			sys_yield();
			if (thread_is_in_thread() /*&& thisenv->env_id == 1002*/ /* user env */)
				thread_yield();
			continue;
		}

		if (r1 != 0) {
			panic("ipc_send: sys_ipc_try_send() Received error: %e  args: (to_env: %x, pg: %x, perm: %x, move: %d)\n", 
				r1, to_env, pg, perm, move);
		} else {
			panic("ipc_send: sys_page_unmap() Received error: %e\n", r2);
		}
	}
}
void
ipc_send(envid_t to_env, uint32_t val, void *pg, int perm)
{
	ipc_send_aux(to_env, val, pg, perm, 0/*do not move*/);
}
void
ipc_move(envid_t to_env, uint32_t val, void *pg, int perm)
{
	ipc_send_aux(to_env, val, pg, perm, 1/*move*/);
}


// Find the first environment of the given type.  We'll use this to
// find special environments.
// Returns 0 if no such environment exists.
envid_t
ipc_find_env(enum EnvType type)
{
	int i;
	for (i = 0; i < NENV; i++)
		if (envs[i].env_type == type)
			return envs[i].env_id;
	return 0;
}
