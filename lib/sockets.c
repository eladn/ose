#include <inc/lib.h>
#include <lwip/sockets.h>
#include <inc/utils.h>
#include <inc/events.h>

static ssize_t devsock_read(struct Fd *fd, void *buf, size_t n);
static ssize_t devsock_write(struct Fd *fd, const void *buf, size_t n);
static int devsock_close(struct Fd *fd);
static int devsock_stat(struct Fd *fd, struct Stat *stat);

struct Dev devsock =
{
	.dev_id =	's',
	.dev_name =	"sock",
	.dev_read =	devsock_read,
	.dev_write =	devsock_write,
	.dev_close =	devsock_close,
	.dev_stat =	devsock_stat,
};

static int
fd2sockid(int fd)
{
	struct Fd *sfd;
	int r;

	if ((r = fd_lookup(fd, &sfd)) < 0)
		return r;
	if (sfd->fd_dev_id != devsock.dev_id)
		return -E_NOT_SUPP;
	return sfd->fd_sock.sockid;
}

static int
alloc_sockfd(events_loop_t *loop, int sockid)
{
	struct Fd *sfd;
	int r;

	if ((r = fd_alloc(&sfd)) < 0
	    || (r = sys_page_alloc(0, sfd, PTE_P|PTE_W|PTE_U|PTE_SHARE)) < 0) {
		nsipc_close(loop, sockid);
		return r;
	}

	sfd->fd_dev_id = devsock.dev_id;
	sfd->fd_omode = O_RDWR;
	sfd->fd_sock.sockid = sockid;
	return fd2num(sfd);
}

int
ev_accept(events_loop_t *loop, int s, struct sockaddr *addr, socklen_t *addrlen)
{
	int r;
	if ((r = fd2sockid(s)) < 0)
		return r;
	if ((r = nsipc_accept(loop, r, addr, addrlen)) < 0)
		return r;
	return alloc_sockfd(loop, r);
}
int
accept(int s, struct sockaddr *addr, socklen_t *addrlen)
{
	return ev_accept(NULL, s, addr, addrlen);
}

int
ev_bind(events_loop_t *loop, int s, struct sockaddr *name, socklen_t namelen)
{
	int r;
	if ((r = fd2sockid(s)) < 0)
		return r;
	return nsipc_bind(loop, r, name, namelen);
}
int
bind(int s, struct sockaddr *name, socklen_t namelen)
{
	return ev_bind(NULL, s, name, namelen);
}

int
ev_shutdown(events_loop_t *loop, int s, int how)
{
	int r;
	if ((r = fd2sockid(s)) < 0)
		return r;
	return nsipc_shutdown(loop, r, how);
}
int
shutdown(int s, int how)
{
	return ev_shutdown(NULL, s, how);
}

static int
devsock_close(struct Fd *fd)
{
	if (pageref(fd) == 1)
		return nsipc_close(NULL, fd->fd_sock.sockid);
	else
		return 0;
}

int
ev_close(events_loop_t *loop, int s)
{
	int r;
	if ((r = fd2sockid(s)) < 0)
		return r;
	return nsipc_close(loop, r);
}

int
ev_connect(events_loop_t *loop, int s, const struct sockaddr *name, socklen_t namelen)
{
	int r;
	if ((r = fd2sockid(s)) < 0)
		return r;
	return nsipc_connect(loop, r, name, namelen);
}
int
connect(int s, const struct sockaddr *name, socklen_t namelen)
{
	return ev_connect(NULL, s, name, namelen);
}

int
ev_listen(events_loop_t *loop, int s, int backlog)
{
	int r;
	if ((r = fd2sockid(s)) < 0)
		return r;
	return nsipc_listen(loop, r, backlog);
}
int
listen(int s, int backlog)
{
	return ev_listen(NULL, s, backlog);
}

// #define SELECTREAD (1<<0)
// #define SELECTWRITE (1<<1)
// #define SELECTEXCEPT (1<<2)
// struct selectfd {
//    int   fd;         /* file descriptor */
//    short events;     /* requested events */
//    short revents;    /* returned events */
// };


int fdset2sockset(int maxfd, fd_set fdreadset, fd_set fdwriteset, fd_set fdexceptset,
	int *maxsock, sock_set *sreadset, sock_set *swriteset, sock_set *sexceptset)
{
	int sockid;
	int i;
	*maxsock = 0;

	SOCK_ZERO(sreadset);
	SOCK_ZERO(swriteset);
	SOCK_ZERO(sexceptset);

	for (i = 0; i < maxfd; i++) {
		if (!FD_ISSET(i, &fdreadset) && !FD_ISSET(i, &fdwriteset) && !FD_ISSET(i, &fdexceptset)) {
			continue;
		}
		if ((sockid = fd2sockid(i)) < 0)
			return sockid;
		*maxsock = max(*maxsock, sockid + 1);
		if (FD_ISSET(i, &fdreadset))
			SOCK_SET(sockid, sreadset);
		if (FD_ISSET(i, &fdwriteset))
			SOCK_SET(sockid, swriteset);
		if (FD_ISSET(i, &fdexceptset))
			SOCK_SET(sockid, sexceptset);
	}

	return 0;
}

int fdset_clr_matching_unset_sockets(int maxfd, fd_set *fdreadset, fd_set *fdwriteset, fd_set *fdexceptset,
	sock_set sreadset, sock_set swriteset, sock_set sexceptset)
{
	return 0;  // TODO: fix!!
	int sockid;
	int i;
	for (i = 0; i < maxfd; i++) {
		if (!FD_ISSET(i, fdreadset) && !FD_ISSET(i, fdwriteset) && !FD_ISSET(i, fdexceptset)) {
			continue;
		}
		if ((sockid = fd2sockid(i)) < 0)
			return sockid;
		if (FD_ISSET(i, fdreadset) && !SOCK_ISSET(sockid, &sreadset))
			FD_CLR(i, fdreadset);
		if (FD_ISSET(i, fdwriteset) && !SOCK_ISSET(sockid, &swriteset))
			FD_CLR(i, fdwriteset);
		if (FD_ISSET(i, fdexceptset) && !SOCK_ISSET(sockid, &sexceptset))
			FD_CLR(i, fdexceptset);
	}

	return 0;
}

/* not used because there is no transformation `sockid2fd()` */
/*int sockset2fdset(int maxsock, sock_set sreadset, sock_set swriteset, sock_set sexceptset,
	int *maxfd, fd_set *fdreadset, fd_set *fdwriteset, fd_set *fdexceptset)
{
	
	*maxfd = 0;

	FD_ZERO(fdreadset);
	FD_ZERO(fdwriteset);
	FD_ZERO(fdexceptset);

	for (i = 0; i < maxsock; i++) {
		if (!SOCK_ISSET(&sreadset, i) && !SOCK_ISSET(&swriteset, i) && !SOCK_ISSET(&sexceptset, i)) {
			continue;
		}
		if ((fdid = sockid2fd(i)) < 0)
			return fdid;
		*maxfd = max(*maxfd, fdid);
		if (SOCK_ISSET(&sreadset, i))
			FD_SET(fdreadset, fdid);
		if (SOCK_ISSET(&swriteset, i))
			FD_SET(fdwriteset, fdid);
		if (SOCK_ISSET(&sexceptset, i))
			FD_SET(fdexceptset, fdid);
	}

	return 0;
}*/

int ev_select_interruptable(events_loop_t *loop, int maxfd, fd_set *readset, fd_set *writeset,
       					 fd_set *exceptset,  struct timeval *timeout,
       					 ipc_recv_cb cb, void *ipc_pg, void *cb_arg)
{
	int r;
	sock_set sreadset, swriteset, sexceptset;
	fd_set fd_readset, fd_writeset, fd_exceptset;
	int maxsock = 0;
	int nready;

	/* in order to allow sending NULL parameters to select() */
	FD_SAFE_CPY(readset, &fd_readset);
	FD_SAFE_CPY(writeset, &fd_writeset);
	FD_SAFE_CPY(exceptset, &fd_exceptset);

	r = fdset2sockset(maxfd, fd_readset, fd_writeset, fd_exceptset,
					&maxsock, &sreadset, &swriteset, &sexceptset);
	if (r < 0) return r;

	nready = nsipc_select_interruptable(loop, maxsock, &sreadset, &swriteset, &sexceptset, 
										timeout, cb, ipc_pg, cb_arg);

	r = fdset_clr_matching_unset_sockets(
					maxfd, &fd_readset, &fd_writeset, &fd_exceptset,
					sreadset, swriteset, sexceptset);
	if (r < 0) return r;

	/* in order to allow sending NULL parameters to select*() */
	if (readset) *readset = fd_readset;
	if (writeset) *writeset = fd_writeset;
	if (exceptset) *exceptset = fd_exceptset;

	return nready;
}
int select_interruptable(int maxfd, fd_set *readset, fd_set *writeset,
       					 fd_set *exceptset,  struct timeval *timeout,
       					 ipc_recv_cb cb, void *ipc_pg, void *cb_arg)
{
	return ev_select_interruptable(NULL, maxfd, readset, writeset,
       					 exceptset, timeout,
       					 cb, ipc_pg, cb_arg);
}

int
ev_select(events_loop_t *loop, int maxfd, fd_set *readset, fd_set *writeset,
       fd_set *exceptset, struct timeval *timeout)
{
	return ev_select_interruptable(loop, maxfd, readset, writeset, exceptset,
								timeout, NULL, NULL, NULL);
}
int
select(int maxfd, fd_set *readset, fd_set *writeset,
       fd_set *exceptset, struct timeval *timeout)
{
	return ev_select(NULL, maxfd, readset, writeset, exceptset, timeout);
}

/* receive is like device read but with flags (for O_NONBLOCK) */
int
ev_receive(events_loop_t *loop, int fd, void *buf, size_t n, int flags)
{
	int r;
	int s;
	if ((s = fd2sockid(fd)) < 0)
		return s;
	if (loop) {
		//r = ev_thread_wait_for_sock(loop, fd, EV_FD_READABLE);
		//if (r < 0) return r;
	}
	r = nsipc_recv(loop, s, buf, n, flags);
	return r;
}
int
receive(int fd, void *buf, size_t n, int flags)
{
	return ev_receive(NULL, fd, buf, n, flags);
}

int
ev_receiven(events_loop_t *loop, int fd, void *buf, size_t n) {
	int m = 0, tot = 0;

	for (tot = 0; tot < n; tot += m) {
		m = ev_receive(loop, fd, buf + tot, MIN(1518, n - tot), 0);
		if (m < 0)
			return m;
		/*if (m == 0)
			break;*/
	}
	return tot;
}

/* send is like device write but with flags (for O_NONBLOCK) */
int
ev_send(events_loop_t *loop, int fd, const void *buf, size_t n, int flags)
{
	int s;
	if ((s = fd2sockid(fd)) < 0)
		return s;
	int tot = 0;
	int m;
	while (tot < n) {
		m = nsipc_send(loop, s, buf+tot, MIN(n-tot, 1518), flags);
		if (m < 0) return m;
		tot += m;
	}
	return tot;
}
int
send(int fd, const void *buf, size_t n, int flags)
{
	return ev_send(NULL, fd, buf, n, flags);
}

static ssize_t
devsock_read(struct Fd *fd, void *buf, size_t n)
{
	return nsipc_recv(NULL, fd->fd_sock.sockid, buf, n, 0);
}

static ssize_t
devsock_write(struct Fd *fd, const void *buf, size_t n)
{
	return nsipc_send(NULL, fd->fd_sock.sockid, buf, n, 0);
}

static int
devsock_stat(struct Fd *fd, struct Stat *stat)
{
	strcpy(stat->st_name, "<sock>");
	return 0;
}

int
ev_socket(events_loop_t *loop, int domain, int type, int protocol)
{
	int r;
	if ((r = nsipc_socket(loop, domain, type, protocol)) < 0)
		return r;
	return alloc_sockfd(loop, r);
}
int
socket(int domain, int type, int protocol)
{
	return ev_socket(NULL, domain, type, protocol);
}

int ev_spill(events_loop_t *loop, int rd_fd, int wr_fd)
{
	int tot, m, r, nsent, totsent;
	struct Stat stat;
	if ((r = fstat(rd_fd, &stat)) < 0)
		return r;

	int buff_len = MIN(1518, MAX(4, stat.st_size));
	char *buf = malloc_page();
	if (!buf) return -E_NO_MEM;

	for (tot = 0; tot < stat.st_size; tot += m) {
		m = readn(rd_fd, (char*)buf, buff_len);
		if (m < 0) {
			free_page(buf);
			return m;
		}
		if (m == 0) break;
		totsent = 0;
		while (totsent < m) {
			assert(totsent < 1518);
			if((nsent = ev_send(loop, wr_fd, (char*)buf+totsent, m-totsent, 0)) < 0) {
				free_page(buf);
				return nsent;
			}
			totsent += nsent;
		}
		assert(totsent == m);
	}

	free_page(buf);
	return tot;
}