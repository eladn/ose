#include <inc/ns.h>
#include <inc/lib.h>
#include <lwip/sockets.h>

#define NSIPC_DEBUG 0
#define nsipc_debug(...) if(NSIPC_DEBUG) cprintf(__VA_ARGS__)

// Virtual address at which to receive page mappings containing client requests.
#define REQVA		0x0ffff000
union Nsipc shared_nsipcbuf __attribute__((aligned(PGSIZE)));

static ns_req_id_t next_req_id;
static inline ns_req_id_t get_new_req_id() {
	return ++next_req_id;
}


/* TODO: consider using the 1st version (shared_nsipcbuf) for tests */
#ifdef __TEST_1111__
static union Nsipc * nsipc_getbuf(void) {
	static_assert(sizeof(shared_nsipcbuf) == PGSIZE);
	return &shared_nsipcbuf;
}
static void nsipc_putbuf(union Nsipc* buf) {
	return;
}
#else
#define NETREQ_VA_BEGIN 0x0ffff000  /* TODO: maybe change it */
static union Nsipc * nsipc_getbuf(void) {
	int r;
	void *va = find_unmapped_va(NETREQ_VA_BEGIN);
	if (!va) {
		panic("NS: buffer overflow");
		return 0;
	}
	if ((r = sys_page_alloc(0, va, (PTE_P|PTE_U|PTE_W))) < 0) {
		panic("NS: buffer overflow");
		return 0;
	}
	return va;
}
static void nsipc_putbuf(void *va) {
	sys_page_unmap(0, va);
}
#endif


// Send an IP request to the network server, and wait for a reply.
// The request body should be in nsipcbuf, and parts of the response
// may be written back to nsipcbuf.
// type: request code, passed as the simple integer IPC value.
// Returns 0 if successful, < 0 on failure.
static int
nsipc_interruptable(events_loop_t *loop, union Nsipc *nsipcbuf, unsigned type, 
					ipc_recv_cb cb, void *ipc_pg, void *cb_arg)
{
	int r;
	static envid_t nsenv;
	if (nsenv == 0)
		nsenv = ipc_find_env(ENV_TYPE_NS);

	//static_assert(sizeof(nsipcbuf) == PGSIZE);

	nsipc_debug("[%08x] nsipc %d\n", thisenv->env_id, type);

	/* Stage 1: send the request to the network-server */
	int req_id = get_new_req_id();
	nsipcbuf->req_id = req_id;
	nsipc_debug("nsipc: before send\n");
	ipc_send(nsenv, type, nsipcbuf, PTE_P|PTE_W|PTE_U);
	nsipc_debug("nsipc: after send\n");

	if (loop) {
		r = ev_thread_wait_for_nsipc_response(loop, req_id);
		return r;
	}
	/* Stage 2: if the  the request to the network-server */
	if (cb == NULL)
		return ipc_recv_from_env(nsenv, NULL, NULL, NULL);

	envid_t from_env;
	int recv_value;
	int recv_perm_store;
	while (1) {
		recv_value = ipc_recv(&from_env, ipc_pg, &recv_perm_store);
		if (from_env == nsenv) return recv_value;
		cb(from_env, recv_value, ipc_pg, recv_perm_store, cb_arg);
	}

	assert(0);
	return -1;
}
static int
nsipc(events_loop_t *loop, union Nsipc *nsipcbuf, unsigned type)
{
	return nsipc_interruptable(loop, nsipcbuf, type, NULL, NULL, NULL);
}




int
nsipc_accept(events_loop_t *loop, int s, struct sockaddr *addr, socklen_t *addrlen)
{
	int r;
	union Nsipc * nsipcbuf = nsipc_getbuf();

	nsipcbuf->accept.req_s = s;
	nsipcbuf->accept.req_addrlen = *addrlen;
	if ((r = nsipc(loop, nsipcbuf, NSREQ_ACCEPT)) >= 0) {
		struct Nsret_accept *ret = &nsipcbuf->acceptRet;
		memmove(addr, &ret->ret_addr, ret->ret_addrlen);
		*addrlen = ret->ret_addrlen;
	}

	nsipc_putbuf(nsipcbuf);
	return r;
}

int
nsipc_bind(events_loop_t *loop, int s, struct sockaddr *name, socklen_t namelen)
{
	int r;
	union Nsipc * nsipcbuf = nsipc_getbuf();

	nsipcbuf->bind.req_s = s;
	memmove(&nsipcbuf->bind.req_name, name, namelen);
	nsipcbuf->bind.req_namelen = namelen;
	r = nsipc(loop, nsipcbuf, NSREQ_BIND);

	nsipc_putbuf(nsipcbuf);
	return r;
}

int
nsipc_shutdown(events_loop_t *loop, int s, int how)
{
	int r;
	union Nsipc * nsipcbuf = nsipc_getbuf();

	nsipcbuf->shutdown.req_s = s;
	nsipcbuf->shutdown.req_how = how;
	r = nsipc(loop, nsipcbuf, NSREQ_SHUTDOWN);

	nsipc_putbuf(nsipcbuf);
	return r;
}

int
nsipc_close(events_loop_t *loop, int s)
{
	int r;
	union Nsipc * nsipcbuf = nsipc_getbuf();

	nsipcbuf->close.req_s = s;
	r = nsipc(loop, nsipcbuf, NSREQ_CLOSE);

	nsipc_putbuf(nsipcbuf);
	return r;
}

int
nsipc_connect(events_loop_t *loop, int s, const struct sockaddr *name, socklen_t namelen)
{
	int r;
	union Nsipc * nsipcbuf = nsipc_getbuf();

	nsipcbuf->connect.req_s = s;
	memmove(&nsipcbuf->connect.req_name, name, namelen);
	nsipcbuf->connect.req_namelen = namelen;
	r = nsipc(loop, nsipcbuf, NSREQ_CONNECT);

	nsipc_putbuf(nsipcbuf);
	return r;
}

int
nsipc_listen(events_loop_t *loop, int s, int backlog)
{
	int r;
	union Nsipc * nsipcbuf = nsipc_getbuf();

	nsipcbuf->listen.req_s = s;
	nsipcbuf->listen.req_backlog = backlog;
	r = nsipc(loop, nsipcbuf, NSREQ_LISTEN);

	nsipc_putbuf(nsipcbuf);
	return r;
}

int
nsipc_select_interruptable(events_loop_t *loop, int maxsock, sock_set *readset, sock_set *writeset, sock_set *exceptset,
               struct timeval *timeout, ipc_recv_cb cb, void *ipc_pg, void *cb_arg)
{
	union Nsipc * nsipcbuf = nsipc_getbuf();
	int nready;
	nsipcbuf->select.maxsock = maxsock;
	nsipcbuf->select.readset = *readset;
	nsipcbuf->select.writeset = *writeset;
	nsipcbuf->select.exceptset = *exceptset;
	nsipcbuf->select.timeout = *timeout;

	nready = nsipc_interruptable(loop, nsipcbuf, NSREQ_SELECT, cb, ipc_pg, cb_arg);

	*readset = nsipcbuf->selectRet.readset;
	*writeset = nsipcbuf->selectRet.writeset;
	*exceptset = nsipcbuf->selectRet.exceptset;
	*timeout = nsipcbuf->selectRet.timeout;

	nsipc_putbuf(nsipcbuf);
	return nready;
}

int
nsipc_select(events_loop_t *loop, int maxsock, sock_set *readset,
			 sock_set *writeset, sock_set *exceptset,
             struct timeval *timeout)
{
	return nsipc_select_interruptable(loop, maxsock, readset, writeset, exceptset,
									  timeout, NULL, NULL, NULL);
}

int
nsipc_recv(events_loop_t *loop, int s, void *mem, int len, unsigned int flags)
{
	int r;
	union Nsipc * nsipcbuf = nsipc_getbuf();

	nsipcbuf->recv.req_s = s;
	nsipcbuf->recv.req_len = len;
	nsipcbuf->recv.req_flags = flags | O_NONBLOCK;

	if ((r = nsipc(loop, nsipcbuf, NSREQ_RECV)) >= 0) {
		assert(r < 1600 && r <= len);
		memmove(mem, nsipcbuf->recvRet.ret_buf, r);
	}

	nsipc_putbuf(nsipcbuf);

	return r;
}

int
nsipc_send(events_loop_t *loop, int s, const void *buf, int size, unsigned int flags)
{
	int r;
	union Nsipc * nsipcbuf = nsipc_getbuf();

	nsipcbuf->send.req_s = s;
	assert(size < 1600);
	memmove(&nsipcbuf->send.req_buf, buf, size);
	nsipcbuf->send.req_size = size;
	nsipcbuf->send.req_flags = flags | O_NONBLOCK;
	
	r = nsipc(loop, nsipcbuf, NSREQ_SEND);

	nsipc_putbuf(nsipcbuf);
	return r;
}

int
nsipc_socket(events_loop_t *loop, int domain, int type, int protocol)
{
	int r;
	union Nsipc * nsipcbuf = nsipc_getbuf();

	nsipcbuf->socket.req_domain = domain;
	nsipcbuf->socket.req_type = type;
	nsipcbuf->socket.req_protocol = protocol;
	r = nsipc(loop, nsipcbuf, NSREQ_SOCKET);

	nsipc_putbuf(nsipcbuf);
	return r;
}
