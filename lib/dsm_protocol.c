#include <net/dsm/dsm.h>
#include <net/dsm/dsm_protocol.h>
#include <inc/lib.h>


#define DSM_DEBUG 0 /* TODO: set flag off */
#define dsm_debug(...) if (DSM_DEBUG) cprintf(__VA_ARGS__);


extern dsm_async_waiting_msg_t socket_waiting_incoming_resp[];
extern dsm_async_waiting_msg_t socket_waiting_incoming_req[];

#define NPG_IS_WAITING_TRANS(npg) (npg->waiting_transaction.tid != 0)

static void dsm_uncache_net_page(net_env_data_t *self, dsm_net_page_t *npg, dsm_peer_net_id_t new_master_peer_id) {
    npg->npg_state = NPS__UNCACHED;
    npg->master_peer_id = new_master_peer_id;
    sys_page_unmap(self->envid, NPG2NVA(self, npg));
}

static int dsm_disable_write_for_net_page(net_env_data_t *self, dsm_net_page_t *npg) {
    uint32_t  perm = (PTE_U | PTE_P);
    int r = sys_page_map(self->envid, NPG2NVA(self, npg), self->envid, NPG2NVA(self, npg), perm);
    dsm_debug("DSMSRV: dsm_disable_write_for_net_page() va %x envid %x.. err: %d\n", NPG2NVA(self, npg), self->envid, r);
    return r;
}

static int dsm_enable_write_for_net_page(net_env_data_t *self, dsm_net_page_t *npg) {
    uint32_t  perm = (PTE_U | PTE_P | PTE_W);
    int r = sys_page_map(self->envid, NPG2NVA(self, npg), self->envid, NPG2NVA(self, npg), perm);
    dsm_debug("DSMSRV: dsm_enable_write_for_net_page() va %x envid %x.. err: %d\n", NPG2NVA(self, npg), self->envid, r);
    return r;
}

static int dsm_get_new_req_id(net_env_data_t *self, dsm_peer_net_id_t peer_id) {
    dsm_peer_t *peer = find_peer_by_id(self, peer_id);
    assert(peer);
    dsm_req_id_t myreq = peer->next_req_id;
    peer->next_req_id += 2;
    return myreq;
}

int dsm_receive_resp_from_sock(events_loop_t *loop, net_env_data_t *self, int sock, dsm_msg_t *net_msg_buf)
{
    while (socket_waiting_incoming_resp[sock].tid != 0)
        thread_yield();

    socket_waiting_incoming_resp[sock].tid = thread_id();
    socket_waiting_incoming_resp[sock].response_msg_buf = net_msg_buf;
    /* We go to sleep. the `incoming_msg_handler` will wake us up when msg arrives. */
    thread_sleep();

    dsm_debug("DSMSRV: dsm_receive_resp_from_sock() woke up.\n");

    /* Let the next waiting thread to use it.
    (actually we dont use this method again after handshake..) */
    socket_waiting_incoming_resp[sock].tid = 0;
    socket_waiting_incoming_resp[sock].response_msg_buf = NULL;
    return 0;
}

int dsm_receive_req_from_sock(events_loop_t *loop, net_env_data_t *self, int sock, dsm_msg_t *net_msg_buf)
{
    while (socket_waiting_incoming_req[sock].tid != 0)
        thread_yield();

    socket_waiting_incoming_req[sock].tid = thread_id();
    socket_waiting_incoming_req[sock].response_msg_buf = net_msg_buf;
    /* We go to sleep. the `incoming_msg_handler` will wake us up when msg arrives. */
    thread_sleep();

    dsm_debug("DSMSRV: dsm_receive_req_from_sock() woke up.\n");

    /* Let the next waiting thread to use it.
    (actually we dont use this method again after handshake..) */
    socket_waiting_incoming_req[sock].tid = 0;
    socket_waiting_incoming_req[sock].response_msg_buf = NULL;
    return 0;
}

int dsm_receive_response(events_loop_t *loop, net_env_data_t *self, 
    dsm_net_page_t *npg, dsm_peer_net_id_t peer_id, dsm_req_id_t req_id, 
    dsm_msg_t *net_msg_buf, void *response_page_buf)
{
    dsm_debug("DSMSRV: dsm_receive_response() peerid: %u req_id %u\n", peer_id, req_id);
    assert(npg);
    assert(!NPG_IS_WAITING_TRANS(npg));
    npg->waiting_transaction.tid = thread_id();
    npg->waiting_transaction.peer_id = peer_id;
    npg->waiting_transaction.req_id = req_id;
    npg->waiting_transaction.response_msg_buf = net_msg_buf;
    npg->waiting_transaction.response_page_buf = response_page_buf;

    /* we go to sleep until the matching response arrives. */
    thread_sleep();

    /* the incoming messages dispacher woke wake us up! */

    return 0;
}

int dsm_proto_send_dsm_msg(events_loop_t *loop, net_env_data_t *self, dsm_msg_t *new_msg) {

    // 1. infer buf_len from msg fields [could leverage DSM_MSG_LIST X-Macro]
    /*int buf_len = sizeof(dsm_msg_header_t);
    #define X(cap_name,name) case DSM_MSG_CAPNAME2ENUM(cap_name): buf_len += sizeof(DSM_MSG_NAME2STRUCT(name)); break;
    switch (new_msg->header.msg_type) {
        DSM_MSG_LIST
        default: assert(0);
    }
    #undef X*/
    int buf_len = sizeof(dsm_msg_t);

    // 2. get the correct socket from msg header
    int sock = find_socket_of_peer_by_id(self, new_msg->header.to_peer_id);
    if (sock < 0) return sock;


    dsm_debug("DSMSRV: dsm_proto_send_dsm_msg() : new_msg type: %d req_id: %d \n",
            new_msg->header.msg_type, new_msg->header.req_id);

    // 3. send the fucking message through the socket :)
    int r = ev_send(loop, sock, new_msg, buf_len, 0);

    if (r < 0) return r;
    assert(r == buf_len);

    // iff  (msg_type is shared_page_resp && status is SUCCESS, also send the page data itself
    if (new_msg->header.msg_type == DSM_MSG_CAPNAME2ENUM(SHARE_PAGE_RESP)
        && new_msg->share_page_resp.status == DSC__SUCCESS)
    {
        dsm_debug("DSMSRV: dsm_proto_send_dsm_msg() : send page!\n");
        // we map this netenv page to our mem-area.
        void *temp_page = find_unmapped_va(DSM_RANDOM_VA_RANGE_BEGIN);
        if (!temp_page) return -E_NO_MEM;
        r = sys_page_map(self->envid, new_msg->header.nva, 0, temp_page, (PTE_P|PTE_U));
        if (r < 0) return r;
        ssize_t sent = ev_send(loop, sock, temp_page, PGSIZE, 0);
        assert(sent == PGSIZE); // in future - handle various errors...
        sys_page_unmap(0, temp_page);
    }

    return 0;
}

//__attribute__((aligned(4096))) static uint8_t page_buf[PGSIZE];

static int dsm_proto_send_fieldless_msg(
    events_loop_t *loop, 
    net_env_data_t *self, 
    dsm_net_page_t *npg, 
    DsmMsgType msg_type, 
    dsm_peer_net_id_t to_peer_id, 
    dsm_msg_t *response_msg, 
    void *page_buf, 
    dsm_req_id_t req_id)
{

    /*dsm_debug("DSMSRV: dsm_proto_send_fieldless_msg() params: npg: %p response_msg: %p.\n",
        npg, response_msg);*/

    dsm_msg_t new_msg; // local var on stack, fine because will be sent before func returns...

    new_msg.header.msg_type = msg_type;
    new_msg.header.from_peer_id = self->my_net_id;
    new_msg.header.to_peer_id = to_peer_id;
    new_msg.header.nva = NPG2NVA(self, npg);
    new_msg.header.req_id = DSM_MSG_TYPE__IS_INITIATOR(msg_type) ? dsm_get_new_req_id(self, to_peer_id) : req_id;

    int r;
    while((r = dsm_proto_send_dsm_msg(loop, self,&new_msg)) < 0) {
        /* looping until success.... */;
    }

    // the caller does not expect response
    if (!response_msg) return 0;

    // 1. yield/block until getting response...
    int sock = find_socket_of_peer_by_id(self, new_msg.header.to_peer_id);
    if(sock < 0) panic("DSMSRV: dsm_proto_send_fieldless_msg() coukdnt find sock.\n");
    r = dsm_receive_response(loop, self, npg, to_peer_id, 
        new_msg.header.req_id, response_msg,
          DSM_MSG_TYPE__IS_EXPECTING_PAGE(msg_type)
            ? page_buf/* we might get a page, if so - put it in page_buf */
            : NULL /* we do not expect a page here */
        );
    if (r < 0) return r;

    return 0;
}

static int dsm_proto_send_fieldless_msg_to_master(events_loop_t *loop, net_env_data_t *self, dsm_net_page_t *npg, DsmMsgType msg_type, dsm_msg_t *response, void *page_buf, dsm_req_id_t req_id) {
    return dsm_proto_send_fieldless_msg(loop, self, npg, msg_type, npg->master_peer_id, response, page_buf, req_id);
}

static void dsm_proto_send_share_page_req(events_loop_t *loop, net_env_data_t *self, dsm_net_page_t *npg, void *page_buf, dsm_req_id_t *out_req_id) {
    dsm_msg_t full_resp;
    dsm_share_page_resp_t* resp;
    dsm_proto_send_fieldless_msg_to_master(loop, self, npg, DSM_MSG_CAPNAME2ENUM(SHARE_PAGE_REQ), &full_resp, page_buf, 0);
    resp = &full_resp.share_page_resp;

    // if our master was not updated, probe all peers until finding correct master
    while (resp->status == DSC__NOT_MASTER) {
        dsm_peer_t *cur_peer = NULL;
        FOR_EACH_PEER(cur_peer, self) {
            npg->master_peer_id = cur_peer->id;
            dsm_proto_send_fieldless_msg_to_master(loop, self, npg, DSM_MSG_CAPNAME2ENUM(SHARE_PAGE_REQ), &full_resp, page_buf, 0);
            resp = &full_resp.share_page_resp;
            if (resp->status == DSC__SUCCESS) break;
        }
    }
    dsm_debug("dsm_proto_send_share_page_req() found master that sent us the page!!!\n");
    *out_req_id = full_resp.header.req_id;
    return;
}



void dsm_proto_send_share_page_resp_aux(events_loop_t *loop, net_env_data_t *self, dsm_net_page_t *npg, dsm_peer_net_id_t to_peer_id, dsm_req_id_t req_id, int to_rej) {
    dsm_msg_t new_msg; // local var on stack, fine because will be sent before func returns...

    new_msg.header.msg_type = DSM_MSG_CAPNAME2ENUM(SHARE_PAGE_RESP);
    new_msg.header.from_peer_id = self->my_net_id;
    new_msg.header.to_peer_id = to_peer_id;
    new_msg.header.nva = NPG2NVA(self, npg);
    new_msg.header.req_id = req_id;

    dsm_share_page_resp_t* resp = &new_msg.share_page_resp;

    resp->status = to_rej
                    ? DSC__REJECTED
                    : (npg->npg_state & NPS__PAGE_MASTER)
                        ? DSC__SUCCESS
                        : DSC__NOT_MASTER;

    dsm_debug("DSMSRV: dsm_proto_send_share_page_resp_aux() before dsm_proto_send_dsm_msg().\n");
    int r;
    // send resp itself
    while((r = dsm_proto_send_dsm_msg(loop, self,&new_msg)) < 0) {
        dsm_debug("DSMSRV: dsm_proto_send_share_page_resp_aux() inside loop of dsm_proto_send_dsm_msg().\n");
        /* looping until success.... */;
    }

    dsm_debug("DSMSRV: dsm_proto_send_share_page_resp_aux() sent SHARE_PAGE_RESP msg. status: %x.\n", resp->status);
    if (resp->status == DSC__NOT_MASTER) {
        dsm_debug("DSMSRV: dsm_proto_send_share_page_resp_aux(): NOT THE MASTER.\n");
        return; // if we are not masters, we have nothing more to do...
    }

    // TODO :: block until gettin share_page_done from the brother.


    return;
}

void dsm_proto_send_share_page_resp(events_loop_t *loop, net_env_data_t *self, dsm_net_page_t *npg, dsm_peer_net_id_t to_peer_id, dsm_req_id_t req_id) {
    dsm_proto_send_share_page_resp_aux(loop, self, npg, to_peer_id, req_id, 0);
}

static int dsm_proto_handle_share_page_req(events_loop_t *loop, net_env_data_t *self, dsm_msg_header_t *msg_header, dsm_share_page_req_t *req) {
    assert(msg_header->msg_type == DSM_MSG__SHARE_PAGE_REQ);

    dsm_debug("DSMSRV: dsm_proto_handle_share_page_req()\n");

    dsm_net_page_t *npg = NVA2NPG(self, msg_header->nva);
    //dsm_net_page_lock(npg); //   TODO :: UNCOMMENT THIS!
    int r;
    switch (npg->npg_state){
        case NPS__EXCLUSIVE:
            // Change our state from EX to SM:
            // remove PTE_W cuz we DON'T write to a shared copy
            if ((r = dsm_disable_write_for_net_page(self, npg)) < 0) {
                panic("DSMSRV: dsm_proto_handle_share_page_req() in dsm_disable_write_for_net_page()\n");
                return r;
            }
            npg->npg_state = NPS__SHARED_MASTER;
            /* intentional fall-through to SM */
        case NPS__UNCACHED: /* intentional fall-through to SM */
        case NPS__SHARED_REGULAR: /* intentional fall-through to SM */
        case NPS__SHARED_MASTER:
            dsm_proto_send_share_page_resp(loop, self, npg, msg_header->from_peer_id, msg_header->req_id);
            break;
        default: assert(0);
    }
    dsm_debug("DSMSRV: dsm_proto_handle_share_page_req() FINISH\n");
    //dsm_net_page_release(npg); //   TODO :: UNCOMMENT THIS!
    return 0;
}

static void dsm_proto_send_share_page_done(events_loop_t *loop, net_env_data_t *self, dsm_net_page_t *npg, dsm_req_id_t req_id) {
    dsm_proto_send_fieldless_msg_to_master(loop, self, npg, DSM_MSG_CAPNAME2ENUM(SHARE_PAGE_DONE), NULL, NULL, req_id);
}

void dsm_proto_send_exclusive_page_resp(events_loop_t *loop, net_env_data_t *self, dsm_net_page_t *npg, dsm_peer_net_id_t to_peer_id, DsmStatusCode status, dsm_req_id_t req_id) {
    dsm_msg_t new_msg; // local var on stack, fine because will be sent before func returns...

    dsm_debug("DSMSRV: dsm_proto_send_exclusive_page_resp().\n");

    new_msg.header.msg_type = DSM_MSG_CAPNAME2ENUM(EXCLUSIVE_PAGE_RESP);
    new_msg.header.from_peer_id = self->my_net_id;
    new_msg.header.to_peer_id = to_peer_id;
    new_msg.header.nva = NPG2NVA(self, npg);
    new_msg.header.req_id = req_id;

    dsm_exclusive_page_resp_t* resp = &new_msg.exclusive_page_resp;

    resp->status = status;

    int r;
    // send resp itself
    while((r = dsm_proto_send_dsm_msg(loop, self, &new_msg)) <0) {
        /* looping until success.... */;
    }
    return;
}

static int dsm_proto_handle_exclusive_page_req(events_loop_t *loop, net_env_data_t *self, dsm_msg_header_t *msg_header, dsm_exclusive_page_req_t *req) {
    assert(msg_header->msg_type == DSM_MSG__EXCLUSIVE_PAGE_REQ);

    dsm_net_page_t *npg = NVA2NPG(self, msg_header->nva);
    //dsm_net_page_lock(npg); //   TODO :: UNCOMMENT THIS!

    dsm_debug("DSMSRV: dsm_proto_handle_exclusive_page_req(). My page state: %d\n", npg->npg_state);
    int r;
    switch (npg->npg_state){
        case NPS__EXCLUSIVE:
            // Change our state from EX to U:
            // remove PTE_W cuz we DON'T write to a shared copy
            if ((r = dsm_disable_write_for_net_page(self, npg)) < 0) {
                return r;
            }
            /* intentional fall-through to SM */
        case NPS__SHARED_MASTER:
            // Change our state from SM to U:
            dsm_debug("DSMSRV: dsm_proto_handle_exclusive_page_req(). uncaching net page.\n");
            dsm_uncache_net_page(self, npg, msg_header->from_peer_id);
            //dsm_net_page_release(npg); //   TODO :: UNCOMMENT THIS!
            dsm_proto_send_exclusive_page_resp(loop, self, npg, msg_header->from_peer_id, DSC__SUCCESS, msg_header->req_id);
            break;
        case NPS__UNCACHED: /* intentional fall-through to SR */
        case NPS__SHARED_REGULAR:
            dsm_proto_send_exclusive_page_resp(loop, self, npg, msg_header->from_peer_id, DSC__NOT_MASTER, msg_header->req_id);
            break;
        default: assert(0);
    }
    return 0;
}

static dsm_exclusive_page_resp_t* dsm_proto_send_exclusive_page_req(events_loop_t *loop, net_env_data_t *self, dsm_net_page_t *npg, void *page_buf) {
    dsm_msg_t full_resp;
    dsm_exclusive_page_resp_t* resp;
    dsm_proto_send_fieldless_msg_to_master(loop, self, npg, DSM_MSG_CAPNAME2ENUM(EXCLUSIVE_PAGE_REQ), &full_resp, page_buf, 0);
    resp = &full_resp.exclusive_page_resp;

    // if our master was not updated, probe all peers until finding correct master
    while (resp->status == DSC__NOT_MASTER) {
        dsm_peer_t *cur_peer = NULL;
        FOR_EACH_PEER(cur_peer, self) {
            npg->master_peer_id = cur_peer->id;
            dsm_proto_send_fieldless_msg_to_master(loop, self, npg, DSM_MSG_CAPNAME2ENUM(EXCLUSIVE_PAGE_REQ), &full_resp, page_buf, 0);
			resp = &full_resp.exclusive_page_resp;
            if (resp->status == DSC__SUCCESS) break;
        }
    }
    return resp;
}

static dsm_new_exclusive_resp_t* dsm_proto_send_new_exclusive_req(events_loop_t *loop, net_env_data_t *self, dsm_net_page_t *npg, dsm_peer_net_id_t to_peer_id) {
	dsm_msg_t full_resp;
    dsm_new_exclusive_resp_t* resp;
    dsm_proto_send_fieldless_msg_to_master(loop, self, npg, DSM_MSG_CAPNAME2ENUM(NEW_EXCLUSIVE_REQ), &full_resp, NULL, 0);
    resp = &full_resp.new_exclusive_resp;
    return resp;
}

static void dsm_proto_send_new_exclusive_resp(events_loop_t *loop, net_env_data_t *self, dsm_net_page_t *npg, dsm_req_id_t req_id) {
    dsm_proto_send_fieldless_msg_to_master(loop, self, npg, DSM_MSG_CAPNAME2ENUM(NEW_EXCLUSIVE_RESP), NULL, NULL, req_id);
}

static int dsm_proto_handle_new_exclusive_req(events_loop_t *loop, net_env_data_t *self, dsm_msg_header_t *msg_header, dsm_new_exclusive_req_t *req) {
    // we are notified of a new master for a page, need to invalidate the copy we have for it & update MID to new master;
    dsm_net_page_t *npg = NVA2NPG(self, msg_header->nva);

    dsm_uncache_net_page(self, npg, msg_header->from_peer_id);

    // ACK the new master.
    dsm_proto_send_new_exclusive_resp(loop, self, npg, msg_header->req_id);

    return 0;
}

static int dsm_proto_broadcast_new_exclusive(events_loop_t *loop, net_env_data_t *self, dsm_net_page_t *npg) {
    dsm_peer_t *cur_peer = NULL;
    FOR_EACH_PEER(cur_peer, self) {
          dsm_proto_send_new_exclusive_req(loop, self, npg, cur_peer->id);
    }
    return 0;
}

int dsm_proto_become_shared(events_loop_t *loop, net_env_data_t *self, void *va) {
    //TODO: chack that this va is in range!!!

    dsm_debug("DSMSRV: dsm_proto_become_shared() va %p\n", va);

    dsm_net_page_t *npg = NVA2NPG(self, va);

    assert(npg->npg_state == NPS__UNCACHED);

    dsm_req_id_t req_id;

    void *page = malloc_page();

    /* always success */
    dsm_proto_send_share_page_req(loop, self, npg, page, &req_id);

    npg->npg_state = NPS__SHARED_REGULAR;

    int r;
    r = sys_page_map(0, page, self->envid, va, (PTE_U | PTE_P)); // PTE_W cuz we gonna copy to this va
    if (r < 0) panic("DSMSRV: dsm_proto_become_shared() Couldnt map page to user va %p. err: %e\n", r);// return r;

    /******** evacuate net pages if too many SR pages *********/
    /* TODO TODO TODO :: BOM!  refactor, take "max SR ratio" to be a defined constant,
       and have a responsible counter that updates live as we mark/unmark pages as SR,
       instead of counting inside a loop like maniacs */
    // 1. cont how many SR pages we have.
    /*
    int sr_pg_count = 0;
    dsm_net_page_t *cur_npg;
    FOR_EACH_NET_PAGE(cur_npg, self) {
        sr_pg_count += cur_npg->npg_state == NPS__SHARED_REGULAR;
        // 2. if we have more than 1/8 of the total pages as SR, throw one out.
        if (sr_pg_count >= self->range_npages/8) {
            dsm_uncache_net_page(self, npg, npg->master_peer_id);
            break; // currently throwing out just one page at a time...
        }
    }    

    memcpy(NPG2NVA(self, npg), page, PGSIZE); //ev_receiven(loop, find_socket_of_peer_by_id(self, npg->master_peer_id), NPG2NVA(self, npg), PGSIZE);

    sys_page_unmap(0, NPG2NVA(self, npg));

    npg->npg_state = NPS__SHARED_REGULAR;

    dsm_proto_send_share_page_done(loop, self, npg, req_id);
*/

    free_page(page);

    return 0;
}

int dsm_proto_become_exclusive(events_loop_t *loop, net_env_data_t *self, void *va) {

    //TODO: chack that this va is in range!!!


    void *page_buf = malloc_page();

    dsm_net_page_t *npg = NVA2NPG(self, va);

    dsm_debug("DSMSRV: dsm_proto_become_exclusive() va: %x. my state: %d\n", va, npg->npg_state);

    assert(npg->npg_state != NPS__EXCLUSIVE);
    dsm_exclusive_page_resp_t *resp;
    int r;
    switch (npg->npg_state){
        case NPS__UNCACHED:
            r = dsm_proto_become_shared(loop, self, va); // go from U to SR;
            if (r < 0) {free_page(page_buf); return r;}
            /* intentional fall-through to SR */
        case NPS__SHARED_REGULAR:
            dsm_debug("DSMSRV: dsm_proto_become_exclusive() va: %x. my state: %d calling dsm_proto_send_exclusive_page_req()\n", va, npg->npg_state);
            resp =
              dsm_proto_send_exclusive_page_req(loop, self, npg, page_buf);
            assert(resp->status == DSC__SUCCESS); // NOT_MASTER case handled within "send_X" funcs //
            /* intentional fall-through to SM */
        case NPS__SHARED_MASTER:
            dsm_debug("DSMSRV: dsm_proto_become_exclusive() va: %x. my state: %d calling dsm_proto_broadcast_new_exclusive()\n", va, npg->npg_state);
            r = dsm_proto_broadcast_new_exclusive(loop, self, npg);
            if (r < 0) {
                free_page(page_buf);
                return r;
            }
            npg->npg_state = NPS__EXCLUSIVE;
            npg->master_peer_id = self->my_net_id;
            // remove PTE_W cuz we DON'T write to a shared copy
            dsm_debug("DSMSRV: dsm_proto_become_exclusive() va: %x. my state: %d calling dsm_enable_write_for_net_page()\n", va, npg->npg_state);
            if ((r = dsm_enable_write_for_net_page(self, npg)) < 0) {
                free_page(page_buf);
                return r;
            }
            break;
        default: assert(0);
    }

    free_page(page_buf);

    dsm_debug("DSMSRV: dsm_proto_become_exclusive() END SUCCESS. va: %x\n", va);

    return 0;
}

int dsm_proto_dispatch_req(events_loop_t *loop, net_env_data_t *self, dsm_msg_t *full_resp) {
    if (full_resp->header.msg_type == DSM_MSG__SHARE_PAGE_REQ) {
        return dsm_proto_handle_share_page_req(loop, self, &full_resp->header, &full_resp->share_page_req);
    }
    if (full_resp->header.msg_type == DSM_MSG__EXCLUSIVE_PAGE_REQ) {
        return dsm_proto_handle_exclusive_page_req(loop, self, &full_resp->header, &full_resp->exclusive_page_req);
    }
    if (full_resp->header.msg_type == DSM_MSG__NEW_EXCLUSIVE_REQ) {
        return dsm_proto_handle_new_exclusive_req(loop, self, &full_resp->header, &full_resp->new_exclusive_req);
    }
    assert(0); // dispatch should not be called for non-REQ messages.
}