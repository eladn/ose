// Public definitions for the POSIX-like file descriptor emulation layer
// that our user-land support library implements for the use of applications.
// See the code in the lib directory for the implementation details.

#ifndef JOS_INC_FD_H
#define JOS_INC_FD_H

#include <inc/types.h>
#include <inc/fs.h>
#include <inc/utils.h>

struct Fd;
struct Stat;
struct Dev;

// Per-device-class file descriptor operations
struct Dev {
	int dev_id;
	const char *dev_name;
	ssize_t (*dev_read)(struct Fd *fd, void *buf, size_t len);
	ssize_t (*dev_write)(struct Fd *fd, const void *buf, size_t len);
	int (*dev_close)(struct Fd *fd);
	int (*dev_stat)(struct Fd *fd, struct Stat *stat);
	int (*dev_trunc)(struct Fd *fd, off_t length);
};

struct FdFile {
	int id;
};

struct FdSock {
	int sockid;
};

struct Fd {
	int fd_dev_id;
	off_t fd_offset;
	int fd_omode;
	union {
		// File server files
		struct FdFile fd_file;
		// Network sockets
		struct FdSock fd_sock;
	};
};

struct Stat {
	char st_name[MAXNAMELEN];
	off_t st_size;
	int st_isdir;
	struct Dev *st_dev;
};

char*	fd2data(struct Fd *fd);
int	fd2num(struct Fd *fd);
int	fd_alloc(struct Fd **fd_store);
int	fd_close(struct Fd *fd, bool must_exist);
int	fd_lookup(int fdnum, struct Fd **fd_store);
int	dev_lookup(int devid, struct Dev **dev_store);

extern struct Dev devfile;
extern struct Dev devsock;
extern struct Dev devcons;
extern struct Dev devpipe;

// [ moved from fd.c ]
// Maximum number of file descriptors a program may hold open concurrently
#define MAXFD		32

/* FD_SET used for select() */
#ifndef FD_SET
  #undef  FD_SETSIZE
  /* Make FD_SETSIZE match MAXFD */
  #define FD_SETSIZE    MAXFD
  #define FD_SET(n, p)  ((p)->fd_bits[(n)/32] |=  (1 << ((n) & 31)))
  #define FD_CLR(n, p)  ((p)->fd_bits[(n)/32] &= ~(1 << ((n) & 31)))
  #define FD_ISSET(n,p) ((p)->fd_bits[(n)/32] &   (1 << ((n) & 31)))
  #define FD_ZERO(p)    memset((void*)(p),0,sizeof(*(p)))
  #define FD_SAFE_CPY(src, dst) do { \
      if ((src) != NULL) { *(dst) = *(src); } else { FD_ZERO(dst); } \
  } while(0)
  #define FD_FIND_NEXT(p) ({ \
    int i; \
    uint32_t retval = 0; \
    for (i = sizeof(*(p))/sizeof((p)->fd_bits[0]) - 1; i != -1; --i) { \
        retval = msb32(*((uint32_t*)((p)->fd_bits) + i)); \
        if (retval != 0) break; \
    } \
    if (i == -1) i = 0; \
    ((retval-1) + (i*32)); \
  })
  #define FD_FOREACH_SET(n,p) \
    for (n = FD_FIND_NEXT(p); n != -1; FD_CLR(n,p), n = FD_FIND_NEXT(p))

  typedef struct fd_set {
      uint32_t fd_bits [(FD_SETSIZE+31)/32];
    } fd_set;
#endif /* FD_SET */

#endif	// not JOS_INC_FD_H
