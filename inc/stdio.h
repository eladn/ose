#ifndef JOS_INC_STDIO_H
#define JOS_INC_STDIO_H

#include <inc/stdarg.h>
#include <inc/types.h>

#ifndef NULL
#define NULL	((void *) 0)
#endif /* !NULL */

// lib/stdio.c
void	cputchar(int c);
int	getchar(void);
int getchar_peekable(int inc_read_cursor);
int	iscons(int fd);

// lib/printfmt.c
void	printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);
void	vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list);
int	snprintf(char *str, int size, const char *fmt, ...);
int	vsnprintf(char *str, int size, const char *fmt, va_list);

// lib/printf.c
int	cprintf(const char *fmt, ...);
int	vcprintf(const char *fmt, va_list);

// lib/fprintf.c
int	printf(const char *fmt, ...);
int	fprintf(int fd, const char *fmt, ...);
int	vfprintf(int fd, const char *fmt, va_list);

// lib/fscanf.c
int	scanf(const char *fmt, ...);
int	fscanf(int fd, const char *fmt, ...);
int	vfscanf(int fd, const char *fmt, va_list);

// lib/scanfmt.c
void	scanfmt(int (*getch)(void*, int), void *putdat, const char *fmt, ...);
void	vscanfmt(int (*getch)(void*, int), void *putdat, const char *fmt, va_list);
int	snscanf(char *str, int size, const char *fmt, ...);
int	vsnscanf(char *str, int size, const char *fmt, va_list);

// kern/scanf.c /* TODO: should be under `lib/` ? */
int	cscanf(const char *fmt, ...);
int	vcscanf(const char *fmt, va_list);

// lib/readline.c
char*	readline(const char *prompt);

// kern/console.c
void clr_screen();

// kern/console.c
int get_serial_terminal_size(size_t *cols, size_t *rows);

// colors
#define CLR_DEFAULT           "\x1B[0m"

#define CLR_FG_DEFAULT        "\x1B[39m"
#define CLR_FG_BLACK          "\x1B[30m"
#define CLR_FG_RED            "\x1B[31m"
#define CLR_FG_GREEN          "\x1B[32m"
#define CLR_FG_YELLOW         "\x1B[33m"
#define CLR_FG_BLUE           "\x1B[34m"
#define CLR_FG_MAGENTA        "\x1B[35m"
#define CLR_FG_CYAN           "\x1B[36m"
#define CLR_FG_LIGHT_GRAY     "\x1B[37m"

#define CLR_FG_GRAY           "\x1B[90m"
#define CLR_FG_LIGHT_RED      "\x1B[91m"
#define CLR_FG_LIGHT_GREEN    "\x1B[92m"
#define CLR_FG_LIGHT_YELLOW   "\x1B[93m"
#define CLR_FG_LIGHT_BLUE     "\x1B[94m"
#define CLR_FG_LIGHT_MAGENTA  "\x1B[95m"
#define CLR_FG_LIGHT_CYAN     "\x1B[96m"
#define CLR_FG_WHITE          "\x1B[97m"

#define CLR_BG_DEFAULT        "\x1B[49m"
#define CLR_BG_BLACK          "\x1B[40m"
#define CLR_BG_RED            "\x1B[41m"
#define CLR_BG_GREEN          "\x1B[42m"
#define CLR_BG_YELLOW         "\x1B[43m"
#define CLR_BG_BLUE           "\x1B[44m"
#define CLR_BG_MAGENTA        "\x1B[45m"
#define CLR_BG_CYAN           "\x1B[46m"
#define CLR_BG_LIGHT_GRAY     "\x1B[47m"

#define CLR_BG_GRAY           "\x1B[100m"
#define CLR_BG_LIGHT_RED      "\x1B[101m"
#define CLR_BG_LIGHT_GREEN    "\x1B[102m"
#define CLR_BG_LIGHT_YELLOW   "\x1B[103m"
#define CLR_BG_LIGHT_BLUE     "\x1B[104m"
#define CLR_BG_LIGHT_MAGENTA  "\x1B[105m"
#define CLR_BG_LIGHT_CYAN     "\x1B[106m"
#define CLR_BG_WHITE          "\x1B[107m"

// get a string literal, and wrap it with specific color-escapes,
// making it printed in that color if used inside printf for example.
// returning to the default color after that string.
#define COLOR(STATIC_TXT, CLR) (CLR_##CLR  STATIC_TXT  CLR_DEFAULT) 

#endif /* !JOS_INC_STDIO_H */
