#ifndef JOS_VGA_H
#define JOS_VGA_H

enum vga_shape_types {
	VGA_SHAPE_FILLED_RECT,
	VGA_SHAPE_BITMAP_IMAGE,
	VGA_SHAPE_TEXT,
};

struct vga_rect {
	unsigned left, top;
	unsigned width, height;
	unsigned color;
};

enum vga_text_attr {
	VGA_TEXT_BOLD = (1<<0),
	VGA_TEXT_WRAP_AND_ALIGN_LEFT = (1<<1),
	VGA_TEXT_WRAP_TO_START = (1<<2),
	VGA_TEXT_RTL = (1<<3),
};

struct vga_text {
	int font;
	unsigned left, top;
	int attributes;
	char* text;
};

struct vga_shape {
	int shape_type;
	union {
		struct vga_rect rect;
		struct vga_text text;
	};
};

//int sys_vga_draw(struct shape* objs, size_t nobjs);

#endif /* JOS_VGA_H */
