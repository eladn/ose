// Main public header file for our user-land support library,
// whose code lives in the lib directory.
// This library is roughly our OS's version of a standard C library,
// and is intended to be linked into all user-mode applications
// (NOT the kernel or boot loader).

#ifndef JOS_INC_LIB_H
#define JOS_INC_LIB_H 1

#include <inc/types.h>
#include <inc/stdio.h>
#include <inc/stdarg.h>
#include <inc/string.h>
#include <inc/error.h>
#include <inc/assert.h>
#include <inc/env.h>
#include <inc/memlayout.h>
#include <inc/syscall.h>
#include <inc/trap.h>
#include <inc/fs.h>
#include <inc/fd.h>
#include <inc/args.h>
#include <inc/malloc.h>
#include <inc/ns.h>
#include <inc/cloak.h>
#include <inc/events.h>
#include <fs/idereg.h>

#define USED(x)		(void)(x)

// main user program
void	umain(int argc, char **argv);

// libmain.c or entry.S
extern const char *binaryname;
extern const volatile struct Env *thisenv;
extern const volatile struct Env envs[NENV];
extern const volatile struct PageInfo pages[];

// exit.c
void	exit(void);

// pgfault.c
void	set_pgfault_handler(void (*handler)(struct UTrapframe *utf));
typedef int (*pgfault_handler_t)(struct UTrapframe *utf);
int reg_pgfault_handler(pgfault_handler_t handler);
int isreg_pgfault_handler(pgfault_handler_t handler);

// readline.c
char*	readline(const char *buf);

// syscall.c
void	sys_cputs(const char *string, size_t len);
int	sys_cgetc(void);
int sys_cgetc_peekable(int inc_read_cursor);
envid_t	sys_getenvid(void);
int	sys_env_destroy(envid_t);
void	sys_yield(void);
static envid_t sys_exofork(void);
int	sys_env_set_status(envid_t env, int status);
int sys_env_set_data(envid_t envid, void *data);
int	sys_env_set_trapframe(envid_t env, struct Trapframe *tf);
int	sys_env_set_pgfault_upcall(envid_t env, void *upcall);
int	sys_page_alloc(envid_t env, void *pg, int perm);
int	sys_page_map(envid_t src_env, void *src_pg,
		     envid_t dst_env, void *dst_pg, int perm);
int	sys_page_unmap(envid_t env, void *pg);
int	sys_ipc_try_send(envid_t to_env, uint32_t value, void *pg, unsigned perm);
int	sys_ipc_recv(void *rcv_pg);
unsigned int sys_time_msec(void);
int sys_multiple_syscalls(syscall_frame_t* calls_frame, size_t nr_calls, int stop_on_fail);

#include <inc/drivers/vga.h>
int sys_vga_draw(struct vga_shape* objs, size_t nobjs);
int sys_vga_setmode(unsigned width, unsigned height, unsigned clrs, int modex);
int sys_vga_text_mode(unsigned width, unsigned height);

int sys_sleep_if_eq(uint32_t *a1, uint32_t *a2);
int sys_sleep_if_neq(uint32_t *a1, uint32_t *a2);
int sys_map_ide_channel(void *dstva);

// This must be inlined.  Exercise for reader: why?
static __inline envid_t __attribute__((always_inline))
sys_exofork(void)
{
	envid_t ret;
	__asm __volatile("int %2"
		: "=a" (ret)
		: "a" (SYS_exofork),
		  "i" (T_SYSCALL)
	);
	return ret;
}

typedef void (*ipc_recv_cb)(envid_t from_env, int32_t value, void *pg, int perm_store, void *cb_arg);

// ipc.c
void	ipc_send(envid_t to_env, uint32_t value, void *pg, int perm);
void    ipc_move(envid_t to_env, uint32_t value, void *pg, int perm);
int32_t ipc_recv(envid_t *from_env_store, void *pg, int *perm_store);
int32_t ipc_recv_from_env(envid_t only_from_env, envid_t *from_env_store, 
						  void *pg, int *perm_store);
int32_t ipc_async_receive(envid_t only_from_env, envid_t *from_env_store, 
                          int *value_store, void **pg_store, int *perm_store);
envid_t	ipc_find_env(enum EnvType type);

// fork.c
#define	PTE_SHARE	0x400
envid_t	fork(void);
envid_t	sfork(void);	// Challenge!

// fd.c
int	close(int fd);
ssize_t	read(int fd, void *buf, size_t nbytes);
ssize_t	write(int fd, const void *buf, size_t nbytes);
int	seek(int fd, off_t offset);
void	close_all(void);
ssize_t	readn(int fd, void *buf, size_t nbytes);
ssize_t writen(int fdnum, const void *buf, size_t n);
int	dup(int oldfd, int newfd);
int	fstat(int fd, struct Stat *statbuf);
int	stat(const char *path, struct Stat *statbuf);
ssize_t spill(int rd_fd, int wr_fd);

// file.c
int	open(const char *path, int mode);
int	ftruncate(int fd, off_t size);
int	remove(const char *path);
int	sync(void);

// pageref.c
int	pageref(void *addr);

// sockets.c
int     accept(int s, struct sockaddr *addr, socklen_t *addrlen);
int     bind(int s, struct sockaddr *name, socklen_t namelen);
int     shutdown(int s, int how);
int     connect(int s, const struct sockaddr *name, socklen_t namelen);
int     listen(int s, int backlog);
int     select(int maxfd, fd_set *readset, fd_set *writeset, fd_set *exceptset, 
               struct timeval *timeout);
int 	select_interruptable(int maxfd, fd_set *readset, fd_set *writeset,
	       					 fd_set *exceptset,  struct timeval *timeout,
	       					 ipc_recv_cb cb, void *ipc_pg, void *cb_arg);
int     socket(int domain, int type, int protocol);
int     receive(int fd, void *buf, size_t n, int flags);
int     send(int fd, const void *buf, size_t n, int flags);

// sockets.c - used in an events loop context. means the asked thread yields under response.
int     ev_accept(events_loop_t *loop, int s, struct sockaddr *addr, socklen_t *addrlen);
int     ev_bind(events_loop_t *loop, int s, struct sockaddr *name, socklen_t namelen);
int     ev_shutdown(events_loop_t *loop, int s, int how);
int     ev_close(events_loop_t *loop, int s);
int     ev_connect(events_loop_t *loop, int s, const struct sockaddr *name, socklen_t namelen);
int     ev_listen(events_loop_t *loop, int s, int backlog);
int     ev_select(events_loop_t *loop, int maxfd, fd_set *readset, fd_set *writeset, fd_set *exceptset, 
               struct timeval *timeout);
int 	ev_select_interruptable(events_loop_t *loop, int maxfd, fd_set *readset, fd_set *writeset,
	       					 fd_set *exceptset,  struct timeval *timeout,
	       					 ipc_recv_cb cb, void *ipc_pg, void *cb_arg);
int     ev_socket(events_loop_t *loop, int domain, int type, int protocol);
int     ev_receive(events_loop_t *loop, int fd, void *buf, size_t n, int flags);
int     ev_receiven(events_loop_t *loop, int fd, void *buf, size_t n);
int     ev_send(events_loop_t *loop, int fd, const void *buf, size_t n, int flags);
int     ev_spill(events_loop_t *loop, int rd_fd, int wr_fd);

// nsipc.c
int     nsipc_accept(events_loop_t *loop, int s, struct sockaddr *addr, socklen_t *addrlen);
int     nsipc_bind(events_loop_t *loop, int s, struct sockaddr *name, socklen_t namelen);
int     nsipc_shutdown(events_loop_t *loop, int s, int how);
int     nsipc_close(events_loop_t *loop, int s);
int     nsipc_connect(events_loop_t *loop, int s, const struct sockaddr *name, socklen_t namelen);
int     nsipc_listen(events_loop_t *loop, int s, int backlog);
int     nsipc_select(events_loop_t *loop, int maxsock, sock_set *readset, sock_set *writeset,
                     sock_set *exceptset, struct timeval *timeout);
int     nsipc_select_interruptable(events_loop_t *loop, int maxsock, sock_set *readset, 
				sock_set *writeset, sock_set *exceptset, struct timeval *timeout,
                ipc_recv_cb cb, void *ipc_pg, void *cb_arg);
int     nsipc_recv(events_loop_t *loop, int s, void *mem, int len, unsigned int flags);
int     nsipc_send(events_loop_t *loop, int s, const void *buf, int size, unsigned int flags);
int     nsipc_socket(events_loop_t *loop, int domain, int type, int protocol);

// spawn.c
envid_t	spawn(const char *program, const char **argv);
envid_t	spawnl(const char *program, const char *arg0, ...);
envid_t	exec(const char *program, const char **argv);
envid_t	execl(const char *program, const char *arg0, ...);

// console.c
void	cputchar(int c);
int	getchar(void);
int	iscons(int fd);
int	opencons(void);

// pipe.c
int	pipe(int pipefds[2]);
int	pipeisclosed(int pipefd);

// wait.c
void	wait(envid_t env);

/* File open modes */
#define	O_RDONLY	0x0000		/* open for reading only */
#define	O_WRONLY	0x0001		/* open for writing only */
#define	O_RDWR		0x0002		/* open for reading and writing */
#define	O_ACCMODE	0x0003		/* mask for above modes */

#define	O_CREAT		0x0100		/* create if nonexistent */
#define	O_TRUNC		0x0200		/* truncate to zero length */
#define	O_EXCL		0x0400		/* error if already exists */
#define O_MKDIR		0x0800		/* create directory, not regular file */

// usermem.c
void* find_unmapped_va(uintptr_t search_start_addr);


/* extern all auto-generated syscall wrappers */
#define X(name, nparams, return_type, type1, type2, type3, type4, type5, gen_wrapper) \
WHEN(gen_wrapper) ( \
	return_type SYSCALL_NAME2FUNC(name) CALL_WITH(nparams, type1, type2, type3, type4, type5); \
)

SYSCALL_LIST
#undef X


static inline void *malloc_page() {
    void* va = find_unmapped_va(0); // TODO: start addr !!
    if (!va) return NULL;
    int r;
    r = sys_page_alloc(0, va, (PTE_U | PTE_P | PTE_W));
    if (r != 0) return NULL;
    return va;
}

static inline void free_page(void *va) {
    sys_page_unmap(0, va);
}

#endif	// !JOS_INC_LIB_H
