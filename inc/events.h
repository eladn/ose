#ifndef JOS_INC_EVENTS_H
#define JOS_INC_EVENTS_H

#include <inc/lib.h>
#include <inc/utils.h>
#include <net/lwip/jos/arch/thread.h>
#include <net/lwip/jos/arch/queue.h>

typedef enum event_type event_type_t;
typedef struct event event_t;
typedef struct events_loop events_loop_t;

typedef void (*ipc_recv_cb_t)(events_loop_t *loop, envid_t from_env, int ipc_recv_value,
                              void *ipc_recv_pg, int ipc_recv_perm, void *user_context);
typedef void (*fd_listen_cb_t)(events_loop_t *loop, int fd, event_type_t events, void *user_context);

enum event_type {
    EV_NOT_USED = 0,
    EV_EMBARGO = (1<<0),
    EV_FD_READABLE = (1<<1),
    EV_FD_WRITABLE = (1<<2),
    EV_NSIPC_RESPONSE = (1<<3),
    EV_IPC = (1<<4),
};

// TODO: union it! type is the tag.
struct event {
    LIST_ENTRY(event) events_queue;
    volatile event_type_t type;
    thread_id_t waiting_thread_id;
    ipc_recv_cb_t ipc_recv_cb;
    fd_listen_cb_t fd_listen_cb;
    int fd;
    event_type_t reported_fd_events;
    ns_req_id_t req_id;
    envid_t ipc_recv_from_env;
    int ipc_recv_value;
    void *ipc_recv_pg;
    int ipc_recv_perm;
    void *user_context;
    int repeated;
};

struct events_loop {
    int maxfdn;
    fd_set all_fds;
    fd_set read_fds;
    fd_set write_fds;
    fd_set except_fds;
    LIST_HEAD(, event) events_queue;
    thread_id_t loop_tid;
    thread_id_t select_tid;
    int wait_for_ipc;
    volatile int stop;
};

int ev_init_loop(events_loop_t *loop);
int ev_loop(events_loop_t *loop);
int ev_loop_stop(events_loop_t *loop);
int ev_thread_wait_for_nsipc_response(events_loop_t *loop, ns_req_id_t req_id);
int ev_thread_wait_for_sock(events_loop_t *loop, int fd, event_type_t type);
int ev_func_wait_for_sock(events_loop_t *loop, fd_listen_cb_t cb, int fd, event_type_t type, 
                          int repeated, void *user_context);
int ev_func_wait_for_ipc(events_loop_t *loop, ipc_recv_cb_t cb, int repeated, void *user_context);
int ev_thread_ipc_recv(events_loop_t *loop, envid_t *from_env,
                       void **ipc_recv_pg, int *ipc_recv_perm_store);

#define FOREACH_EVENT(loop, event) \
    LIST_FOREACH(event, &(loop)->events_queue, events_queue)

#endif /* JOS_INC_EVENTS_H */
