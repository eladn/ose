
#ifndef JOS_INC_KDBG_UTILS_H
#define JOS_INC_KDBG_UTILS_H

#include <kern/pmap.h>

extern size_t npages;  /* defined in pmap.c and set by `i386_detect_memory()` */

// The actual maximum address that is accesible (by actual connected memory to the machine).
#define MAX_AVAILABLE_PHYSICAL_ADDR ((npages * PGSIZE) - 1)

// MACRO that checks if a Virtual Address is currently mapped by finding a PTE matching it.
#define VA_IS_MAPPED(va) (kdbg_va2pa_walk(curr_pgdir, (uintptr_t)(va)) != ~0)

// MACRO that checks if a Virtual Address is currently mapped and its dest paddr is accessible in memory
#define VA_IS_MAPPED_AND_EXISTS_MEM(va) \
	({physaddr_t pa = kdbg_va2pa_walk(curr_pgdir, (uintptr_t)(va)); \
	(pa != ~0 && pa <= MAX_AVAILABLE_PHYSICAL_ADDR); })

// this is an auxilary function, implementing the behavior of kdbg_va2pa_walk,
// for function doc please read kdbg_va2pa_walk's comment
static physaddr_t
kdbg_va2pa_walk_aux(pde_t *pgdir, uintptr_t va, int to_print) {
	if (to_print) {
		cprintf("parsing VA:\t%x\n"
			"PDX(VA):\t%d\n"
			"PTX(VA):\t%d\n"
			,va, PDX(va), PTX(va));
	}

	pte_t *pte;
	pde_t *pde;

	// get the relevant entry in the Page Directory, which will point to a specific Page Table.
	pde = &pgdir[PDX(va)];
	if (to_print) cprintf("PDE POINTS TO:\t%x,\twith flags:\t%x\n", PTE_ADDR(*pde), PTE_FLAGS(*pde));
	if (!(*pde & PTE_P))
		return ~0;

	// get the relevant entry in the Page Table we found, which will point to a specific Physical Page.
	pte = &(((pte_t*) KADDR(PTE_ADDR(*pde)))[PTX(va)]);
	if (to_print) cprintf("PTE POINTS TO:\t%x,\twith flags:\t%x\n", PTE_ADDR(*pte), PTE_FLAGS(*pte));
	if (!(*pte & PTE_P))
		return ~0;

	// return the Physical Page's address
	return PTE_ADDR(*pte);
}


// a helper func that reutrns the PA which a given VA is mapped to.
// if this VA is not mapped, then an all-1-bit address is returned.
// NOTE :: the VA's offset is ignored, so the PA returned is always page-aligned.
static inline physaddr_t
kdbg_va2pa_walk(pde_t *pgdir, uintptr_t va) {
	return kdbg_va2pa_walk_aux(pgdir, va, 0);
}

// same as kdbg_va2pa_walk above, but will print traces along the address translation process.
static inline physaddr_t
kdbg_va2pa_printwalk(pde_t *pgdir, uintptr_t va) {
	return kdbg_va2pa_walk_aux(pgdir, va, 1);
}

#endif /* JOS_INC_KDBG_UTILS_H */
