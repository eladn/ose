#ifndef JOS_INC_CYC_RW_BUFFER_H
#define JOS_INC_CYC_RW_BUFFER_H

#include <inc/types.h>

/**
 	TODO: CHECK this whole module !
**/

typedef struct cyc_rw_buffer {
	uint32_t buffer_size;
	uint32_t rpos;
	uint32_t wpos;
	uint8_t buf[0];
} cyc_rw_buffer_t;

/*
#define CRWB_DECLARE_BUFFER_TYPE(type_name, buf_size) \
	const size_t __cyc_rw_buffer_##type_name##_buffer_size = buf_size;
	typedef struct cyc_rw_buffer_##type_name { \
		size_t buffer_size;
		char* buff; \
		uint32_t rpos; \
		uint32_t wpos; \
		uint8_t buf[buf_size]; \
	} cyc_rw_buffer_##type_name##_t
*/

#define CRWB_DECLARE_GLOBAL(name, size) \
	struct cyc_rw_buffer name = CRWB_INITALIZER(size);
	static char __cyc_rw_buffer__##name##_buffer[size]

#define CRWB_DECLARE_INFUNC(name, size) \
	struct cyc_rw_buffer name = CRWB_INITALIZER(size);
	char __cyc_rw_buffer__##name##_buffer[size]

#define CRWB_INITALIZER(size) { \
		.buffer_size = (uint32_t)(size),
		.rpos = 0, \
		.wpos = 0, \
	}

#define CRWB_INIT(buf, size) do { \
		*(buf) = CRWB_INITALIZER(size); \
	} while (0)


#define CRWB_READ_AVAIL_BYTES(buf) (((buf)->wpos - (buf)->rpos) % (buf)->buffer_size)
#define CRWB_WRITE_AVAIL_BYTES(buf) (((buf)->rpos - (buf)->wpos) % (buf)->buffer_size)

#define CRWB_READ_CURSOR_MOVE(buf, nr_chars) do { \
		(buf)->rpos = (uint32_t)(( (int32_t)((buf)->rpos) + (int32_t)(nr_chars) ) % (int32_t)(buf)->buffer_size); \
	} while(0)

inline static uint32_t crwb_read_n(struct cyc_rw_buffer *buf, void *pdst, uint32_t nr_bytes) {
	uint32_t first_stage_read;

	assert(buf->rpos < buf->buffer_size);
	assert(buf->wpos < buf->buffer_size);


	if (CRWB_READ_AVAIL_BYTES(buf) < nr_bytes) {
		return 0;
	}

	first_stage_read = min(buf->buffer_size - buf->rpos, nr_bytes);
	memcpy(pdst, &buf->buf[buf->rpos], min(buf->buffer_size - buf->rpos, nr_bytes));
	if (first_stage_read < nr_bytes) {
		memcpy(((char*)pdst)+first_stage_read, buf->buf, (nr_bytes - first_stage_read));
	}
	CRWB_READ_CURSOR_MOVE(buf, nr_bytes);
	return nr_bytes;
}

inline static uint32_t crwb_read_byte(struct cyc_rw_buffer *buf, char *pdst) {
	assert(buf->rpos < buf->buffer_size);
	assert(buf->wpos < buf->buffer_size);

	if (CRWB_READ_AVAIL_BYTES(buf) == 0) {
		return 0;
	}

	*pdst = buf->buf[buf->rpos];
	CRWB_READ_CURSOR_MOVE(buf, 1);
	return 1;
}


/* TODO: impl the write! */


#endif /* JOS_INC_CYC_RW_BUFFER_H */
