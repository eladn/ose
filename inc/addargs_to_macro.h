#ifndef ADDARGS_TO_MACRO_H
#define ADDARGS_TO_MACRO_H

/* AddArg() :: BEGIN */

#define AA_Args(...) __VA_ARGS__
#define AA_STRIP_PARENS(X) X
#define AA_PASS_PARAMETERS(X) AA_STRIP_PARENS( AA_Args X )

/*  Macro logics to determine whether to STRIP_PARENS() for the additional args or not.
    So that additional args could be: a single arg or multiple args inside parens. example:
      AddArgs(0, A(1,2,3))           // Expands to A(0,1,2,3)
      AddArgs((0), A(1,2,3))         // Expands to A(0,1,2,3)
      AddArgs((10,11,12), A(1,2,3))  // Expands to A(10,11,12,1,2,3)
*/
#define AA_PRIMITIVE_CAT(a, ...) a ## __VA_ARGS__
#define AA_CHECK_N(x, n, ...) n
#define AA_CHECK(...) AA_CHECK_N(__VA_ARGS__, 0,)
#define AA_PROBE(x) x, 1,
#define AA_IS_PAREN(x) AA_CHECK(AA_IS_PAREN_PROBE x)
#define AA_IS_PAREN_PROBE(...) AA_PROBE(~)
#define AA_IIF(c) AA_PRIMITIVE_CAT(AA_IIF_, c)
#define AA_IIF_0(t, ...) __VA_ARGS__
#define AA_IIF_1(t, ...) t
#define AA_PASS_PARAMS(X) AA_IIF(AA_IS_PAREN(X))(AA_PASS_PARAMETERS(X),X)

#define AA_FIRST_ARG(F, ...) F
#define AA_DROP_ARG(F, ...) __VA_ARGS__

#define Add_WRAP(x) AA_PRIMITIVE_CAT(WRAP_, x)
#define Add_NO(x) AA_PRIMITIVE_CAT(NO_, x)
#define Add_REAL(x) AA_PRIMITIVE_CAT(REAL_, x)

#define NO_ApplyMacro

#define AddArgs(n, a) AddArgs_AUX(n, AA_PASS_PARAMETERS(Add_NO(Add_WRAP(a))))
#define AddArgs_AUX(n, M) AA_FIRST_ARG(M)(AA_PASS_PARAMS(n), AA_DROP_ARG(M))

/* AddArg() :: END */

#endif /* ADDARGS_TO_MACRO_H */
