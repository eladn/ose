#ifndef JOS_INC_SYSCALL_H
#define JOS_INC_SYSCALL_H

#include <inc/types.h>

#define SYSCALL_NAME2NUM(name) SYS_ ## name
#define SYSCALL_NAME2FUNC(name) sys_ ## name

#define SYSCALL_LIST \
/*name,              nparams,       return,             type1,             type2,    type3,    type4,    type5, gen_wrapper)*/\
X(cputs,                   2,         void,      const char *,            size_t,     void,     void,     void,           1)\
X(cgetc,                   0,          int,              void,              void,     void,     void,     void,           1)\
X(cgetc_peekable,          1,          int,               int,              void,     void,     void,     void,           1)\
X(getenvid,                0,      envid_t,              void,              void,     void,     void,     void,           1)\
X(env_destroy,             1,          int,           envid_t,              void,     void,     void,     void,           1)\
X(yield,                   0,         void,              void,              void,     void,     void,     void,           1)\
X(exofork,                 0,      envid_t,              void,              void,     void,     void,     void,           0)\
X(env_set_status,          2,          int,           envid_t,               int,     void,     void,     void,           1)\
X(env_set_data,            2,          int,           envid_t,             void*,     void,     void,     void,           1)\
X(env_set_trapframe,       2,          int,           envid_t, struct Trapframe*,     void,     void,     void,           1)\
X(page_alloc,              3,          int,           envid_t,             void*,      int,     void,     void,           1)\
X(page_map,                5,          int,           envid_t,             void*,  envid_t,    void*,      int,           1)\
X(page_unmap,              2,          int,           envid_t,             void*,     void,     void,     void,           1)\
X(env_set_pgfault_upcall,  2,          int,           envid_t,             void*,     void,     void,     void,           1)\
X(ipc_try_send,            4,          int,           envid_t,          uint32_t,    void*, unsigned,     void,           1)\
X(ipc_recv,                1,          int,             void*,              void,     void,     void,     void,           1)\
X(ipc_recv_from_env,       3,          int,             void*,           envid_t,      int,     void,     void,           1)\
X(ipc_recv_async_consume,  5,          int,           envid_t,          envid_t*,     int*,   void**,     int*,           1)\
X(time_msec,               0, unsigned int,              void,              void,     void,     void,     void,           1)\
X(multiple_syscalls,       3,          int,  syscall_frame_t*,            size_t,      int,     void,     void,           1)\
X(vga_draw,                2,          int, struct vga_shape*,            size_t,     void,     void,     void,           1)\
X(vga_setmode,             4,          int,          unsigned,          unsigned, unsigned,      int,     void,           1)\
X(vga_text_mode,           2,          int,          unsigned,          unsigned,     void,     void,     void,           1)\
X(exec,                    4,          int,          uint32_t,          uint32_t,    void*, uint32_t,     void,           1)\
X(sleep_if_eq,             2,          int,         uint32_t*,         uint32_t*,     void,     void,     void,           1)\
X(sleep_if_neq,            2,          int,         uint32_t*,         uint32_t*,     void,     void,     void,           1)\
X(map_ide_channel,         1,          int,             void*,              void,     void,     void,     void,           1)\
X(net_transmit_packet,     3,          int,             void*,            size_t,      int,     void,     void,           1)\
X(net_receive_packet,      1,          int,             void*,              void,     void,     void,     void,           1)\
X(net_get_mac_addr,        1,          int,         uint64_t*,              void,     void,     void,     void,           1)\

#define CALL_WITH(...) (TAKE(__VA_ARGS__))
#define TAKE(nparams, ...) TAKE_ ## nparams (__VA_ARGS__)
#define TAKE_0(...) 
#define TAKE_1(a1, ...) a1
#define TAKE_2(a1, a2, ...) a1, a2
#define TAKE_3(a1, a2, a3, ...) a1, a2, a3
#define TAKE_4(a1, a2, a3, a4, ...) a1, a2, a3, a4
#define TAKE_5(a1, a2, a3, a4, a5) a1, a2, a3, a4, a5

#define ZERO_REST_OUTOF5(nparams, ...) ZERO_REST_OUTOF5_ ## nparams (__VA_ARGS__)
#define ZERO_REST_OUTOF5_0(...) 0, 0, 0, 0, 0
#define ZERO_REST_OUTOF5_1(a1, ...) a1, 0, 0, 0, 0
#define ZERO_REST_OUTOF5_2(a1, a2, ...) a1, a2, 0, 0, 0
#define ZERO_REST_OUTOF5_3(a1, a2, a3, ...) a1, a2, a3, 0, 0
#define ZERO_REST_OUTOF5_4(a1, a2, a3, a4, ...) a1, a2, a3, a4, 0
#define ZERO_REST_OUTOF5_5(a1, a2, a3, a4, a5) a1, a2, a3, a4, a5


/* system call numbers */
#define X(name, nparams, ret, t1, t2, t3, t4, t5, gen_wrapper) \
	SYSCALL_NAME2NUM(name), 
enum {
	SYSCALL_LIST
	NSYSCALLS
};
#undef X

static const char* const syscall_name[] = {
#define X(name, nparams, return, type1, type2, type3, type4, type5, gen_wrapper) \
	"sys_" #name,
	SYSCALL_LIST
#undef X
};

typedef struct syscall_frame {
	uint32_t syscallno;
	uint32_t a1, a2, a3, a4, a5;
	int32_t retval;
} syscall_frame_t;

#define SYSCALL_FRAME(no, val1, val2, val3, val4, val5) { \
	.syscallno = ((uint32_t)(no)), \
	.a1 = ((uint32_t)(val1)), \
	.a2 = ((uint32_t)(val2)), \
	.a3 = ((uint32_t)(val3)), \
	.a4 = ((uint32_t)(val4)), \
	.a5 = ((uint32_t)(val5)), \
	.retval = 0 }

#endif /* !JOS_INC_SYSCALL_H */
