#ifndef JOS_INC_UTILS_H
#define JOS_INC_UTILS_H

#include <inc/types.h>
#include <inc/applyxn.h>

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

long atoi(char *str);
long xtoi(char *str);

void print_pte_flags(int perm);
void sprint_pte_flags(int perm, char *str);

#ifndef min
#define min(a,b) (((a) < (b)) ? (a) : (b))
#endif
#ifndef max
#define max(a,b) (((a) > (b)) ? (a) : (b))
#endif


/* TODO: check! */
static inline size_t nr_digits(int32_t num, int base) {
	size_t cnt = 0;
	while (num > 0)
		num /= base;
	return cnt;
}
/* TODO: check! */
static inline size_t nr_digits_unsigned(uint32_t num, int base) {
	size_t cnt = 0;
	while (num != 0) {
		num /= base;
		++cnt;
	}
	return cnt;
}

// check if a given char is a valid decimal digit (meaning it's in [0-9])
#define ASCII_IS_NUM(ch) ((ch) >= '0' && (ch) <= '9')

// check if a given char is a valid hex digit (meaning it's in [0-9] or [a-f] or [A-F])
#define ASCII_IS_HEX(ch) \
	( ASCII_IS_NUM(ch) || ((ch) >= 'a' && (ch) <= 'f') || ((ch) >= 'A' && (ch) <= 'F') )

// check if a given char is a valid digit in a given base.
// NOTE :: supports only bases 2-10 and 16
#define VALID_DIGIT_CHAR(ch, base) \
	( (ASCII_IS_NUM(ch) && ((ch) - '0') < (base)) || ((base) == 16 && ASCII_IS_HEX(ch)) )

// gets the numeric value of given digit-representing-char.
// NOTE :: assuming legal digit-char according to any base the user chooses.
#define ASCII_TO_NUM(ch) \
	( ASCII_IS_NUM(ch) ? \
		((ch) - '0') : \
		((ch) >= 'a' && (ch) <= 'f') ? \
		((ch) - 'a') : \
		((ch) - 'A') )

#define ASCII_IS_LETTER(ch) \
    ( ((ch) >= 'a' && (ch) <= 'z') || ((ch) >= 'A' && (ch) <= 'Z') )

#define ASCII_IS_CAPITAL_LETTER(ch) \
    ((ch) >= 'A' && (ch) <= 'Z')

#define ASCII_TO_LOWER(ch) \
    ( ASCII_IS_CAPITAL_LETTER(ch) ? ((ch) - 'A' + 'a') : (ch) )

#define FIRST_ARG(N, ...) N

/* we set multiple version to allow the preprosessor to apply "recursively"  */
#define PRIMITIVE_CAT(a, ...) a ## __VA_ARGS__
#define PRIMITIVE_CAT_(a, ...) a ## __VA_ARGS__
#define PRIMITIVE_CAT__(a, ...) a ## __VA_ARGS__
#define PRIMITIVE_CAT___(a, ...) a ## __VA_ARGS__
#define PRIMITIVE_CAT____(a, ...) a ## __VA_ARGS__
#define PRIMITIVE_CAT_____(a, ...) a ## __VA_ARGS__

#define Paste2(a,b) a ## b
#define XPASTE2(a,b) Paste(a,b)
#define Paste2_(a,b) a ## b
#define XPASTE2_(a,b) Paste(a,b)
#define Paste2__(a,b) a ## b
#define XPASTE2__(a,b) Paste(a,b)

#define STRINGIFY(a) #a
#define STRINGIFY_ITEM(T,N,A,a) STRINGIFY(a)
#define STRINGIFY_AND_COMMA(a) #a,
#define STRINGIFY_AND_COMMA_ITEM(T,N,A,a) STRINGIFY_AND_COMMA(a)

/* we set multiple version to allow the preprosessor to apply "recursively"  */
#define STRINGIFYn(...) APPLYXn(STRINGIFY_ITEM,, __VA_ARGS__)
#define STRINGIFY_n(...) APPLYX_n(STRINGIFY_ITEM, __VA_ARGS__)
#define STRINGIFY__n(...) APPLYX__n(STRINGIFY_ITEM, __VA_ARGS__)
#define STRINGIFY___n(...) APPLYX___n(STRINGIFY_ITEM, __VA_ARGS__)
#define STRINGIFY____n(...) APPLYX___n(STRINGIFY_ITEM, __VA_ARGS__)

#define STRINGIFY_AND_COMMAn(...) APPLYXn(STRINGIFY_AND_COMMA_ITEM,, __VA_ARGS__)
#define STRINGIFY_AND_COMMA_n(...) APPLYX_n(STRINGIFY_AND_COMMA_ITEM,, __VA_ARGS__)
#define STRINGIFY_AND_COMMA__n(...) APPLYX__n(STRINGIFY_AND_COMMA_ITEM,, __VA_ARGS__)
#define STRINGIFY_AND_COMMA___n(...) APPLYX___n(STRINGIFY_AND_COMMA_ITEM,, __VA_ARGS__)
#define STRINGIFY_AND_COMMA____n(...) APPLYX___n(STRINGIFY_AND_COMMA_ITEM,, __VA_ARGS__)



#define MULTITYPE_UNION(str_size) \
    union { \
        float as_float; \
        uint32_t as_uint32_t; \
        int32_t as_int32_t; \
        long as_long; \
        int as_int; \
        char *as_strptr; \
        char as_str[str_size]; \
    } 

#define MULTITYPE_UNION_WITH_RANGE(str_size) \
    union { \
        MULTITYPE_UNION(str_size); \
        struct { \
            MULTITYPE_UNION(str_size) start; \
            MULTITYPE_UNION(str_size) end; \
        } as_range; \
    }

#define OPTIONAL_TYPE(type) \
    struct { \
    	int is_set; \
    	type value; \
    }
#define OPTIONAL_TYPE_SET(p_opt, val) do { \
		(p_opt)->is_set = 1; \
		(p_opt)->value = (val); \
	} while (0)
#define OPTIONAL_TYPE_INIT_VAL(val) { \
        .is_set = 1, \
        .value = (val) \
    }
#define OPTIONAL_TYPE_INIT_INNER_FIELD_VAL(field, val) { \
        .is_set = 1, \
        .value.field = (val) \
    }
#define OPTIONAL_TYPE_INIT_UNSET { \
        .is_set = 0 \
    }


#define RANGE_TYPE(type, prefix, suffix) \
    OPTIONAL_TYPE(type) prefix ## min ## suffix; \
    OPTIONAL_TYPE(type) prefix ## max ## suffix; 
    
#define RANGE_INIT_UNSET \
        {   OPTIONAL_TYPE_INIT_UNSET, OPTIONAL_TYPE_INIT_UNSET }
#define MULTITYPE_RANGE_INIT_BOTH(type, min, max) \
        {   OPTIONAL_TYPE_INIT_INNER_FIELD_VAL(as_##type, min), \
            OPTIONAL_TYPE_INIT_INNER_FIELD_VAL(as_##type, max) }
#define MULTITYPE_RANGE_INIT_MAX(type, max) \
        {   OPTIONAL_TYPE_INIT_UNSET, \
            OPTIONAL_TYPE_INIT_INNER_FIELD_VAL(as_##type, max) }
#define MULTITYPE_RANGE_INIT_MIN(type, min) \
        {   OPTIONAL_TYPE_INIT_INNER_FIELD_VAL(as_##type, min), \
            OPTIONAL_TYPE_INIT_UNSET }

#define RANGE_INIT_BOTH(min, max) \
        {   OPTIONAL_TYPE_INIT_VAL(min), \
            OPTIONAL_TYPE_INIT_VAL(max) }
#define RANGE_INIT_MAX(max) \
        {   OPTIONAL_TYPE_INIT_UNSET, \
            OPTIONAL_TYPE_INIT_VAL(max) }
#define RANGE_INIT_MIN(min) \
        {   OPTIONAL_TYPE_INIT_VAL(min), \
            OPTIONAL_TYPE_INIT_UNSET }

#define RANGE_HAS_ONLY_MIN(range, prefix, suffix) \
    (   (range).prefix ## min ## suffix.is_set && \
        !(range).prefix ## max ## suffix.is_set   )

#define RANGE_HAS_ONLY_MAX(range, prefix, suffix) \
    (   !(range).prefix ## min ## suffix.is_set && \
        (range).prefix ## max ## suffix.is_set   )

#define RANGE_IS_ANY(range, prefix, suffix) \
    (   !(range).prefix ## min ## suffix.is_set && \
        !(range).prefix ## max ## suffix.is_set   )

#define RANGE_IS_SINGLE_VALUE(range, prefix, suffix) \
    (   (range).prefix ## min ## suffix.is_set && \
        (range).prefix ## max ## suffix.is_set && \
        (range).prefix ## min ## suffix.value == (range).prefix ## max ## suffix.value  )

#define MULTITYPE_RANGE_IS_SINGLE_VALUE(range, prefix, suffix, type) \
    (   (range).prefix ## min ## suffix.is_set && \
        (range).prefix ## max ## suffix.is_set && \
        (range).prefix ## min ## suffix.value.as_##type == (range).prefix ## max ## suffix.value.as_##type  )

#define MULTITYPE_RANGE_IS_NUMERIC_VAL_IN(range, val_ptr, type) \
    ( (!(range).min_val.is_set || (range).min_val.value.as_##type <= (val_ptr)->as_##type ) && \
      (!(range).max_val.is_set || (range).max_val.value.as_##type >= (val_ptr)->as_##type ) )

#define RANGE_IS_NUMERIC_VAL_IN(range, prefix, suffix, val, type) \
    ( (!(range).prefix ## min ## suffix.is_set || (range).prefix ## min ## suffix.value <= (val) ) && \
      (!(range).prefix ## max ## suffix.is_set || (range).prefix ## max ## suffix.value >= (val) ) )

#define MULTITYPE_RANGE_IS_ANY_FLOAT_POSITIVE(range, type) \
    ( (range).min_val.is_set && (range).min_val.value.as_##type > (type)0 && (range).min_val.value.as_##type <= (type)FLOAT_EPS )
#define MULTITYPE_RANGE_IS_ANY_INT_POSITIVE(range, type) \
    ( (range).min_val.is_set && (range).min_val.value.as_##type == (type)1 )
#define MULTITYPE_RANGE_IS_ANY_NON_NEGATIVE(range, type) \
    ( (range).min_val.is_set && (range).min_val.value.as_##type >= (type)0 && (range).min_val.value.as_##type <= (type)FLOAT_EPS )

#define RANGE_IS_SET_AND_BOUND(range) \
    ( (range).min_val.is_set || (range).max_val.is_set )

/* this is a macro and not a function because range is not a type but a type generator. */
#define RANGE_TO_STR(range, prefix, suffix, range_str_buf, buf_size, fmtstr) ({ \
    int buffer_idx = 0; \
    do { \
        assert((buf_size) > 0); \
        if ((buf_size) <= buffer_idx) break; \
        range_str_buf[buffer_idx++] = '['; \
        if ((range).prefix ## min ## suffix.is_set) \
            buffer_idx += snprintf((range_str_buf)+buffer_idx, (buf_size)-buffer_idx, fmtstr, (range).prefix ## min ## suffix.value); \
        if (buf_size <= buffer_idx) break; \
        range_str_buf[buffer_idx++] = ':'; \
        if ((range).prefix ## max ## suffix.is_set) \
            buffer_idx += snprintf((range_str_buf)+buffer_idx, (buf_size)-buffer_idx, fmtstr, (range).prefix ## max ## suffix.value); \
        if (buf_size <= buffer_idx) break; \
        range_str_buf[buffer_idx++] = ']'; \
        if (buf_size <= buffer_idx) break; \
        range_str_buf[buffer_idx++] = '\0'; \
    } while(0); \
    range_str_buf[buf_size-1] = '\0'; \
    /* ret val */ min(buf_size, buffer_idx); })

#define RANGE_TO_TEXT(range, prefix, suffix, range_str_buf, buf_size, fmtstr) ({ \
    int buffer_idx = 0; \
    do { \
        assert((buf_size) > 0); \
        if (RANGE_IS_ANY(range, prefix, suffix)) { \
            buffer_idx += snprintf((range_str_buf)+buffer_idx, (buf_size)-buffer_idx, "none or more"); \
            break; \
        } \
        if (RANGE_IS_SINGLE_VALUE(range, prefix, suffix)) { \
            buffer_idx += snprintf((range_str_buf)+buffer_idx, (buf_size)-buffer_idx, "exactly " fmtstr, (range).prefix ## min ## suffix.value); \
            break; \
        } \
        if (RANGE_HAS_ONLY_MIN(range, prefix, suffix)) { \
            buffer_idx += snprintf((range_str_buf)+buffer_idx, (buf_size)-buffer_idx, "at least " fmtstr, (range).prefix ## min ## suffix.value); \
            break; \
        } \
        if (RANGE_HAS_ONLY_MAX(range, prefix, suffix)) { \
            buffer_idx += snprintf((range_str_buf)+buffer_idx, (buf_size)-buffer_idx, "none or up to " fmtstr, (range).prefix ## max ## suffix.value); \
            break; \
        } \
        buffer_idx += snprintf((range_str_buf)+buffer_idx, (buf_size)-buffer_idx, "between " fmtstr " and " fmtstr, \
                        (range).prefix ## min ## suffix.value, (range).prefix ## max ## suffix.value); \
        if (buf_size <= buffer_idx) break; \
        range_str_buf[buffer_idx++] = '\0'; \
    } while(0); \
    range_str_buf[buf_size-1] = '\0'; \
    /* ret val */ min(buf_size, buffer_idx); })


#ifndef FLOAT_EPS
#define FLOAT_EPS ((float)0.000000000001)
#endif

#define NUMERICAL_BASE_TO_STR(base) \
    (   ((base) == 10) ? "decimal" : \
        ((base) == 16) ? "hexadecimal" : \
        ((base) == 8) ? "octal" : \
        ((base) == 2) ? "binary" : "" )

#define CYCLIC_BUF_NEXT(buffer, size, ptr) \
    (((ptr) == (buffer) + (size) - 1) ? (buffer) : ((ptr) + 1))
#define CYCLIC_BUF_PREV(buffer, size, ptr) \
    (((ptr) == (buffer)) ? ((buffer) + (size) - 1) : ((ptr) - 1))
#define CYCLIC_BUF_ITERATE(buffer, size, start, cur) \
    for (cur = (start); \
        cur != NULL; \
        cur = CYCLIC_BUF_NEXT(buffer, size, cur), \
        cur = (((cur) == (start)) ? NULL : (cur)))

// returns the position[1-32] of the most significant bit that is set on.
// if no bit is set (x is 0) returns 0.
static inline uint32_t msb32(uint32_t x) {
    static const uint32_t bval[] = {0,1,2,2,3,3,3,3,4,4,4,4,4,4,4,4};
    uint32_t r = 0;
    if (x & 0xFFFF0000) { r += 16/1; x >>= 16/1; }
    if (x & 0x0000FF00) { r += 16/2; x >>= 16/2; }
    if (x & 0x000000F0) { r += 16/4; x >>= 16/4; }
    return r + bval[x];
}

#ifndef offsetof
#define offsetof(type, field_name) \
    ( (uint32_t)(&(((type *)0)->field_name)) )
#endif

static inline uint16_t
__htons(uint16_t n)
{
  return ((n & 0xff) << 8) | ((n & 0xff00) >> 8);
}

static inline uint32_t
__htonl(uint32_t n)
{
  return ((n & 0xff) << 24) |
    ((n & 0xff00) << 8) |
    ((n & 0xff0000UL) >> 8) |
    ((n & 0xff000000UL) >> 24);
}

#endif /* JOS_INC_UTILS_H */

