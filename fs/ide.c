/*
 * Minimal PIO-based (non-interrupt-driven) IDE driver code.
 * For information about what all this IDE/ATA magic means,
 * see the materials available on the class references page.
 */

#include "fs.h"
#include <inc/x86.h>
#include <inc/mmu.h>
#include <fs/idereg.h>
#include <fs/fs.h>

#define PADDR(va) ( PTE_ADDR(uvpt[va2vpn(va)]) | PGOFF(va) )

#define IDE_BSY		0x80
#define IDE_DRDY	0x40
#define IDE_DF		0x20
#define IDE_ERR		0x01

// For debug
#define ide_verbose ( 0 /* TODO: change to 0 */ )
#define ide_dma_debug 0

int ide_enable_dma = 1;


static int ide_wait(struct ide_channel *idec);
static int ide_wait_ready();


// On FS env initializing we will ask the kernel to map us the ide channel and save our va here.
extern struct ide_channel *idec; // from fs.c
static int diskno = 1;  // for old pio ops



static void
ide_select_drive(struct ide_channel *idec)
{
    outb(idec->cmd_addr + IDE_REG_DEVICE, 0xE0 | (idec->diskno << 4));
}

static void
ide_dma_irqack(struct ide_channel *idec)
{
    outb(idec->bm_addr + IDE_BM_STAT_REG,
         inb(idec->bm_addr + IDE_BM_STAT_REG));
}



/*
For DMA Read+Write. Supports up to 64KB (16 pages) data chunk.
I used this explanation: http://wiki.osdev.org/ATA/ATAPI_using_DMA
*/
int
ide_dma_transfer(uint32_t secno, void *va, size_t nsecs, int is_write)
{
    ASSERT_ALIGNED_TO_PAGE(va);
    assert(nsecs >= BLKSECTS && (nsecs % BLKSECTS) == 0); /* transfer full blocks */
    uint32_t nbytes = nsecs * SECTSIZE; /* always multiple of full blocks */
    nbytes = MIN(nbytes, MAX_TOT_PRD_TBL_NBYTES); /* max 64K (16 pages) */
    nsecs = (nbytes / SECTSIZE);

    // Prepare a PRDT in system memory.
    // Note: Even if the whole VA area is continuous, the mapped PAs might not be!
    // Hence we make a separate PRD-entry for each PGSIZE.
    // We assume given va is aligned to PGSIZE.
    memset(idec->bm_prd, 0, sizeof(struct ide_prd) * NPRDSLOTS);
    uint32_t remaining_nbytes = nbytes;
    int prd_idx;
    for (prd_idx = 0; remaining_nbytes != 0; ++prd_idx) {
        uint32_t bytes = MIN(remaining_nbytes, PGSIZE);
        remaining_nbytes -= bytes;
        idec->bm_prd[prd_idx].addr = PADDR(va + prd_idx * PGSIZE);
        idec->bm_prd[prd_idx].count = nbytes;
    }
    idec->bm_prd[prd_idx-1].count |= IDE_PRD_EOT; /* mark last entry end of prd table. */

    // We updated the prd table in memory. We should enfore flushing our updates into
    // memory before we let the IDE to access it.
    wmb();

    // Send the physical PRDT address to the Bus Master PRDT Register.
    uint32_t phys = PADDR(idec->bm_prd);
    assert(phys % 4 == 0);
    if (ide_dma_debug) cprintf("ATA: Setting bus 0 PRDT pointer to 0x%x.\n", phys);
    outl(idec->bm_addr + IDE_BM_PRDT_REG, phys);


    // Set the direction of the data transfer by setting the Read/Write bit in the Bus Master Command Register.
    uint8_t bm_cmd = inb(idec->bm_addr) & ~(IDE_BM_CMD_START);
    if (is_write)
        bm_cmd |= IDE_BM_CMD_WRITE;
    else
        bm_cmd &= ~IDE_BM_CMD_WRITE;
    outb(idec->bm_addr + IDE_BM_CMD_REG, bm_cmd);
    if (ide_dma_debug) cprintf("ATA: bm_cmd: 0x%x   after modify: 0x%x   bm status: 0x%x\n", 
        (uint32_t)bm_cmd,
        (uint32_t)inb(idec->bm_addr + IDE_BM_CMD_REG), 
        (uint32_t)inb(idec->bm_addr + IDE_BM_STAT_REG));
    

    // Clear the Error and Interrupt bit in the Bus Master Status Register.
    uint8_t bm_stat = inb(idec->bm_addr + IDE_BM_STAT_REG) & ~(IDE_BM_STAT_ERROR|IDE_BM_STAT_INTR);
    outb(idec->bm_addr + IDE_BM_STAT_REG, bm_stat);
    if (ide_dma_debug) cprintf("ATA: bm_stat: 0x%x   after modify: 0x%x\n", 
        bm_stat, 
        (uint32_t)inb(idec->bm_addr + IDE_BM_STAT_REG));


    // Select the drive.
    ide_select_drive(idec);


    // Send the LBA and sector count to their respective ports.
    outb(idec->cmd_addr+IDE_REG_SECTOR_COUNT, nsecs);
    outb(idec->cmd_addr+IDE_REG_LBA_LOW, secno & 0xFF);
    outb(idec->cmd_addr+IDE_REG_LBA_MID, (secno >> 8) & 0xFF);
    outb(idec->cmd_addr+IDE_REG_LBA_HI, (secno >> 16) & 0xFF);
    outb(idec->cmd_addr+IDE_REG_DEVICE,
         (0xE0 | ((idec->diskno)<<4) | ((secno>>24)&0x0F)));
    

    // Send the DMA transfer command to the ATA controller.
    outb(idec->cmd_addr+IDE_REG_CMD, (is_write ? IDE_CMD_WRITE_DMA : IDE_CMD_READ_DMA));


    // Important: must set before of starting the async data transfer (bolow).
    // See below comment about `atomically` starting command & sleeping.
    xchg(&idec->irq_wait, 1);


    if (ide_dma_debug)
        cprintf("ATA: Setting IDE_BM_CMD_START and then wait for DMA to complete... going to sleep..\n");

    // Set the Start/Stop bit on the Bus Master Command Register.
    bm_cmd = (inb(idec->bm_addr + IDE_BM_CMD_REG) | (IDE_BM_CMD_START));
    outb(idec->bm_addr + IDE_BM_CMD_REG, bm_cmd);
    if (ide_dma_debug) cprintf("ATA: bm_cmd: 0x%x   after modify: 0x%x   status: 0x%x\n", 
        (uint32_t)bm_cmd,
        (uint32_t)inb(idec->bm_addr + IDE_BM_CMD_REG), 
        (uint32_t)inb(idec->bm_addr + IDE_BM_STAT_REG));


    // The above start command and the sleep instruction should happen atomically.
    // Because the cpu might get the interrupt and handle it before we go to sleep.
    // When the cpu handles the interrupt it checks for `idec->irq_wait`, and if set it set it to zero.
    // Hence, in kernel (while IF is locked) send us to sleep IFF the irq_wait is still 1,
    //  means that the irq interrupt has not happened yet.
    uint32_t expected = 1;
    sys_sleep_if_eq((uint32_t*)&(idec->irq_wait), (uint32_t*)&expected);
    if (ide_dma_debug) cprintf("IDE Woke up!\n");


    // When an interrupt arrives (after the transfer is complete), respond by resetting the Start/Stop bit.
    bm_stat = inb(idec->bm_addr + IDE_BM_STAT_REG);
    //assert(bm_stat & IDE_BM_STAT_INTR);
    if (bm_stat & IDE_BM_STAT_ERROR) return -1;
    outb(idec->bm_addr + IDE_BM_STAT_REG, bm_stat & IDE_BM_STAT_INTR /* TODO: write 0 or 1 ? */);
    if (ide_dma_debug) cprintf("ATA: bm status after interrupt: 0x%x  after unset the intr flag: 0x%x\n",  
        bm_stat, (uint32_t)inb(idec->bm_addr + IDE_BM_STAT_REG));
    


    // Read the controller and drive status to determine if the transfer completed successfully.
    uint8_t status;
    // TODO: decide whether to yield or to busy wait.
    while ((status = inb(idec->cmd_addr + IDE_REG_STATUS)) & IDE_STAT_BSY) /*sys_yield()*/;
    if (ide_dma_debug) cprintf("ATA: stats 0x%x\n", (uint32_t)status);
    // TODO: check for other errors!
    if (status & IDE_STAT_ERR) {
        if (ide_dma_debug) cprintf("ATA: error. ide cmd status: 0x%x!\n", status);
        return -E_IO;
    }

    return nsecs;
}

int
ide_dma_read(uint32_t secno, void *dst, size_t nsecs) {
    return ide_dma_transfer(secno, dst, nsecs, 0);
}

int
ide_dma_write(uint32_t secno, const void *src, size_t nsecs) {
    return ide_dma_transfer(secno, (void*)src, nsecs, 1);
}

static void
ide_string_shuffle(char *s, int len)
{
    int i;
    for (i = 0; i < len; i += 2) {
        char c = s[i+1];
        s[i+1] = s[i];
        s[i] = c;
    }

    s[len-1] = '\0';            /* force the string to end... */
}

static int
ide_pio_in(struct ide_channel *idec, void *buf, uint32_t num_sectors)
{
    char *cbuf = (char *) buf;

    for (; num_sectors > 0; num_sectors--, cbuf += SECTSIZE) {
        int r = ide_wait(idec);
        if (r < 0)
            return r;

        if ((idec->ide_status & (IDE_STAT_DF | IDE_STAT_ERR)))
            return -E_IO;

        insl(idec->cmd_addr + IDE_REG_DATA, cbuf, SECTSIZE/4);
    }

    return 0;
}

static int __attribute__((__unused__))
ide_pio_out(struct ide_channel *idec, const void *buf, uint32_t num_sectors)
{
    const char *cbuf = (const char *) buf;

    for (; num_sectors > 0; num_sectors--, cbuf += SECTSIZE) {
        int r = ide_wait(idec);
        if (r < 0)
            return r;

        if ((idec->ide_status & (IDE_STAT_DF | IDE_STAT_ERR)))
            return -E_IO;

        outsl(idec->cmd_addr + IDE_REG_DATA, cbuf, SECTSIZE / 4);
    }

    return 0;
}

static union {
    struct identify_device id;
    char buf[512];
} identify_buf;

int
ide_channel_init(struct ide_channel *idec)
{
    int i;
    outb(idec->cmd_addr + IDE_REG_DEVICE, idec->diskno << 4);
    outb(idec->cmd_addr + IDE_REG_CMD, IDE_CMD_IDENTIFY);

    cprintf("Probing IDE disk %d..\n", idec->diskno);
    if (ide_pio_in(idec, &identify_buf, 1) < 0)
        return -E_INVAL;

    ide_string_shuffle(identify_buf.id.serial,
                       sizeof(identify_buf.id.serial));
    ide_string_shuffle(identify_buf.id.model,
                       sizeof(identify_buf.id.model));
    ide_string_shuffle(identify_buf.id.firmware,
                       sizeof(identify_buf.id.firmware));

    int udma_mode = -1; // We do not use UDMA currently.

    if (ide_verbose)
        cprintf("IDE device (%d sectors, UDMA %d%s): %1.40s\n",
                identify_buf.id.lba_sectors, udma_mode,
                idec->bm_addr ? ", bus-master" : "",
                identify_buf.id.model);

    if (!(identify_buf.id.hwreset & IDE_HWRESET_CBLID)) {
        cprintf("IDE: 80-pin cable absent, not enabling UDMA\n");
        udma_mode = -1;
    }

    // NOT IN USE. We do not use UDMA currently.
    //udma_mode = 0;
    if (udma_mode >= 0) {
        outb(idec->cmd_addr + IDE_REG_DEVICE, idec->diskno << 4);
        outb(idec->cmd_addr + IDE_REG_FEATURES, IDE_FEATURE_XFER_MODE);
        outb(idec->cmd_addr + IDE_REG_SECTOR_COUNT,
             IDE_XFER_MODE_UDMA | udma_mode);
        outb(idec->cmd_addr + IDE_REG_CMD, IDE_CMD_SETFEATURES);

        ide_wait(idec);
        if ((idec->ide_status & (IDE_STAT_DF | IDE_STAT_ERR)))
            cprintf("IDE: Unable to enable UDMA\n");
    }

    // Enable write-caching
    outb(idec->cmd_addr + IDE_REG_DEVICE, idec->diskno << 4);
    outb(idec->cmd_addr + IDE_REG_FEATURES, IDE_FEATURE_WCACHE_ENA);
    outb(idec->cmd_addr + IDE_REG_CMD, IDE_CMD_SETFEATURES);

    ide_wait(idec);
    if ((idec->ide_status & (IDE_STAT_DF | IDE_STAT_ERR)))
        cprintf("IDE: Unable to enable write-caching\n");

    // Enable read look-ahead
    outb(idec->cmd_addr + IDE_REG_DEVICE, idec->diskno << 4);
    outb(idec->cmd_addr + IDE_REG_FEATURES, IDE_FEATURE_RLA_ENA);
    outb(idec->cmd_addr + IDE_REG_CMD, IDE_CMD_SETFEATURES);

    ide_wait(idec);
    if ((idec->ide_status & (IDE_STAT_DF | IDE_STAT_ERR)))
        cprintf("IDE: Unable to enable read look-ahead\n");

    // The disk size
    idec->dk_bytes = identify_buf.id.lba_sectors * 512;

    uint8_t bm_status = inb(idec->bm_addr + IDE_BM_STAT_REG);
    if (bm_status & IDE_BM_STAT_SIMPLEX)
        cprintf("Simplex-mode IDE bus master, potential problems later..\n");

    //cprintf("IDE BM status: 0x%x\n", bm_status);

    // check whether the device is DMA capable.
    // TODO: why is it not turned on??
    //ide_enable_dma = !!(bm_status & (idec->diskno ? IDE_BM_STAT_D1_DMA : IDE_BM_STAT_D0_DMA));

    // Enable interrupts (clear the IDE_CTL_NIEN bit)
    if (ide_enable_dma)
        outb(idec->ctl_addr, 0);

    return 0;
}


static int ide_wait(struct ide_channel *idec)
{
    uint64_t ts_start = read_tsc();
    for (;;) {
        idec->ide_status = inb(idec->cmd_addr + IDE_REG_STATUS);
        if ((idec->ide_status & (IDE_STAT_BSY | IDE_STAT_DRDY)) == IDE_STAT_DRDY)
            break;

        uint64_t ts_diff = read_tsc() - ts_start;
        if (ts_diff > 1024 * 1024 * 1024) {
            cprintf("ide_wait: stuck for %u cycles, status %02x\n",
                    ts_diff, idec->ide_status);
            return -E_BUSY;
        }
    }

    if (idec->ide_status & IDE_STAT_ERR) {
        idec->ide_error = inb(idec->cmd_addr + IDE_REG_ERROR);
        cprintf("ide_wait: error, status %02x error bits %02x\n",
                idec->ide_status, idec->ide_error);
    }

    if (idec->ide_status & IDE_STAT_DF)
        cprintf("ide_wait: data error, status %02x\n", idec->ide_status);

    return 0;
}




/* OLD PIO code */

static int
ide_wait_ready()
{
	int r;

	while (((r = inb(0x1F7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY)
		/* do nothing */;

	if ((r & (IDE_DF|IDE_ERR)) != 0)
		return -1;
	return 0;
}

bool
ide_probe_disk1(void)
{
	int r, x;

	// wait for Device 0 to be ready
	ide_wait_ready();

	// switch to Device 1
	outb(0x1F6, 0xE0 | (1<<4));

	// check for Device 1 to be ready for a while
	for (x = 0;
	     x < 1000 && ((r = inb(0x1F7)) & (IDE_BSY|IDE_DF|IDE_ERR)) != 0;
	     x++)
		/* do nothing */;

	// switch back to Device 0
	outb(0x1F6, 0xE0 | (0<<4));

	cprintf("Device 1 presence: %d\n", (x < 1000));
	return (x < 1000);
}

void
ide_set_disk(int d)
{
	if (d != 0 && d != 1)
		panic("bad disk number");
	diskno = d;
}

int
ide_pio_read(uint32_t secno, void *dst, size_t nsecs)
{
	int r;

	assert(nsecs <= 256);

	ide_wait_ready();

	outb(0x1F2, nsecs);
	outb(0x1F3, secno & 0xFF);
	outb(0x1F4, (secno >> 8) & 0xFF);
	outb(0x1F5, (secno >> 16) & 0xFF);
	outb(0x1F6, 0xE0 | ((diskno&1)<<4) | ((secno>>24)&0x0F));
	outb(0x1F7, 0x20);	// CMD 0x20 means read sector

	for (; nsecs > 0; nsecs--, dst += SECTSIZE) {
		if ((r = ide_wait_ready()) < 0)
			return r;
		insl(0x1F0, dst, SECTSIZE/4);
	}

	return 0;
}

int
ide_pio_write(uint32_t secno, const void *src, size_t nsecs)
{
	int r;

	assert(nsecs <= 256);

	ide_wait_ready();

	outb(0x1F2, nsecs);
	outb(0x1F3, secno & 0xFF);
	outb(0x1F4, (secno >> 8) & 0xFF);
	outb(0x1F5, (secno >> 16) & 0xFF);
	outb(0x1F6, 0xE0 | ((diskno&1)<<4) | ((secno>>24)&0x0F));
	outb(0x1F7, 0x30);	// CMD 0x30 means write sector

	for (; nsecs > 0; nsecs--, src += SECTSIZE) {
		if ((r = ide_wait_ready()) < 0)
			return r;
		outsl(0x1F0, src, SECTSIZE/4);
	}

	return 0;
}



/* USE DMA for IDE IO */

int
ide_read(uint32_t secno, void *dst, size_t nsecs) {
    if (ide_enable_dma)
	   return ide_dma_read(secno, dst, nsecs);
    return ide_pio_read(secno, dst, nsecs);
}
int
ide_write(uint32_t secno, const void *src, size_t nsecs) {
    if (ide_enable_dma)
	   return ide_dma_write(secno, src, nsecs);
    return ide_pio_write(secno, src, nsecs);
}
