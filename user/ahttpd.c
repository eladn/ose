#include <inc/lib.h>
#include <lwip/sockets.h>
#include <lwip/inet.h>

#define PORT 80
#define VERSION "0.1"
#define HTTP_VERSION "1.0"

#define E_BAD_REQ   1000

#define BUFFSIZE 1024
#define MAXPENDING 5    // Max connection requests

#define AHTTPD_DEBUG 0
#define ahttpd_debug(...) if (AHTTPD_DEBUG) cprintf(__VA_ARGS__)

struct http_request {
    int sock;
    char *url;
    char *version;
};

struct responce_header {
    int code;
    char *header;
};

struct responce_header headers[] = {
    { 200,  "HTTP/" HTTP_VERSION " 200 OK\r\n"
        "Server: jhttpd/" VERSION "\r\n"},
    {0, 0},
};

struct error_messages {
    int code;
    char *msg;
};

struct error_messages errors[] = {
    {400, "Bad Request"},
    {404, "Not Found"},
};

static void
die(char *m)
{
    cprintf("%s\n", m);
    exit();
}

static void
req_free(struct http_request *req)
{
    free(req->url);
    free(req->version);
}

static int
send_header(events_loop_t * loop, struct http_request *req, int code)
{
    struct responce_header *h = headers;
    while (h->code != 0 && h->header!= 0) {
        if (h->code == code)
            break;
        h++;
    }

    if (h->code == 0)
        return -1;

    int len = strlen(h->header);
    if (ev_send(loop, req->sock, h->header, len, 0) != len) {
        die("Failed to send bytes to client");
    }

    return 0;
}

static int
send_data(events_loop_t * loop, struct http_request *req, int fd)
{
    // LAB 6: Your code here.
    return ev_spill(loop, fd, req->sock);
}

static int
send_size(events_loop_t * loop, struct http_request *req, off_t size)
{
    char buf[64];
    int r;

    r = snprintf(buf, 64, "Content-Length: %ld\r\n", (long)size);
    if (r > 63)
        panic("buffer too small!");

    if (ev_send(loop, req->sock, buf, r, 0) != r)
        return -1;

    return 0;
}

static const char*
mime_type(const char *file)
{
    //TODO: for now only a single mime type
    return "text/html";
}

static int
send_content_type(events_loop_t * loop, struct http_request *req)
{
    char buf[128];
    int r;
    const char *type;

    type = mime_type(req->url);
    if (!type)
        return -1;

    r = snprintf(buf, 128, "Content-Type: %s\r\n", type);
    if (r > 127)
        panic("buffer too small!");

    if (ev_send(loop, req->sock, buf, r, 0) != r)
        return -1;

    return 0;
}

static int
send_header_fin(events_loop_t * loop, struct http_request *req)
{
    const char *fin = "\r\n";
    int fin_len = strlen(fin);

    if (ev_send(loop, req->sock, fin, fin_len, 0) != fin_len)
        return -1;

    return 0;
}

// given a request, this function creates a struct http_request
static int
http_request_parse(struct http_request *req, char *request)
{
    const char *url;
    const char *version;
    int url_len, version_len;

    if (!req)
        return -1;

    if (strncmp(request, "GET ", 4) != 0)
        return -E_BAD_REQ;

    // skip GET
    request += 4;

    // get the url
    url = request;
    while (*request && *request != ' ')
        request++;
    url_len = request - url;

    req->url = malloc(url_len + 1);
    memmove(req->url, url, url_len);
    req->url[url_len] = '\0';

    // skip space
    request++;

    version = request;
    while (*request && *request != '\n')
        request++;
    version_len = request - version;

    req->version = malloc(version_len + 1);
    memmove(req->version, version, version_len);
    req->version[version_len] = '\0';

    // no entity parsing

    return 0;
}

static int
send_error(events_loop_t * loop, struct http_request *req, int code)
{
    char buf[512];
    int r;

    struct error_messages *e = errors;
    while (e->code != 0 && e->msg != 0) {
        if (e->code == code)
            break;
        e++;
    }

    if (e->code == 0)
        return -1;

    r = snprintf(buf, 512, "HTTP/" HTTP_VERSION" %d %s\r\n"
                   "Server: jhttpd/" VERSION "\r\n"
                   "Connection: close"
                   "Content-type: text/html\r\n"
                   "\r\n"
                   "<html><body><p>%d - %s</p></body></html>\r\n",
                   e->code, e->msg, e->code, e->msg);
    if (ev_send(loop, req->sock, buf, r, 0) != r)
        return -1;
    return 0;
}

static int
send_file(events_loop_t * loop, struct http_request *req)
{
    int r;
    off_t file_size = -1;
    int fd;

    // open the requested url for reading
    // if the file does not exist, send a 404 error using send_error
    // if the file is a directory, send a 404 error using send_error
    // set file_size to the size of the file

    // LAB 6: Your code here.
    if ((fd = open(req->url, O_RDONLY)) < 0) {
        if (fd == -E_BAD_PATH || fd == -E_NOT_FOUND) {
            r = send_error(loop, req, 404); // file does not exist ==> return 404.
        } else { // some other error occured (e.g. too many open files on fs), just fail and report.
            r = fd; // fd here is the error code from "open"
        }
        return r;
    }

    struct Stat stat;
    if ((r = fstat(fd, &stat)) < 0)
        goto end;

    if (stat.st_isdir) {
        r = send_error(loop, req, 404); // url points to a directory ==> return 404.
        goto end;
    }
    file_size = stat.st_size;

    if ((r = send_header(loop, req, 200)) < 0)
        goto end;

    if ((r = send_size(loop, req, file_size)) < 0)
        goto end;

    if ((r = send_content_type(loop, req)) < 0)
        goto end;

    if ((r = send_header_fin(loop, req)) < 0)
        goto end;

    r = send_data(loop, req, fd);

end:
    ev_close(loop, fd);
    return r;
}

events_loop_t main_loop;

static void
handle_client(events_loop_t *loop, int sock, event_type_t events, void *user_context)
{
    struct http_request con_d;
    int r;
    char *buffer;
    int received = -1;
    struct http_request *req = &con_d;
    int nready;

    assert(events & EV_FD_READABLE);
    assert(user_context == NULL);

    ahttpd_debug("AHttpD: handle_client() called. sock: %d. my thread id: %u\n", sock, thread_id());

    buffer = malloc_page();

    while (1)
    {
        /*struct timeval timeout = { .tv_sec = 5 };
        fd_set readset;
        FD_ZERO(&readset);
        FD_SET(sock, &readset);
        cprintf("ahttpd: before select()\n");
        nready = select(sock+1, &readset, 0, 0, &timeout);
        cprintf("ahttpd: select() returned nready=%d\n", nready);
        if (nready == 0) {
            continue;
        }*/

        ahttpd_debug("AHttpD: handle_client() before receive. my thread id: %u\n", thread_id());

        // Receive message
        if ((received = ev_receive(loop, sock, buffer, PGSIZE, 0)) < 0)
            panic("failed to read");

        ahttpd_debug("AHttpD: handle_client() after receive. my thread id: %u\n", thread_id());

        memset(req, 0, sizeof(req));

        req->sock = sock;

        send_error(loop, req, 400);
        break;


        r = http_request_parse(req, buffer);
        if (r == -E_BAD_REQ)
            send_error(loop, req, 400);
        else if (r < 0)
            panic("parse failed");
        else
            send_file(loop, req);

        req_free(req);

        // no keep alive
        break;
    }

    ahttpd_debug("AHttpD: handle_client() out of loop. before socket close. my thread id: %u\n", thread_id());

    free_page(buffer);
    ev_close(loop, sock);
}


static void spinner(uint32_t arg) {
    int r;
    events_loop_t * loop = (events_loop_t *)arg;
    assert(loop);

    ahttpd_debug("spinner runs...\n");

    while(1) {
        r = ev_loop(loop);
        if (r < 0) panic("ahttpd spinner: loop error: %d\n", r);

        ahttpd_debug("loop escaped!\n\n");
    }
}

void accepter_thread(uint32_t arg)
{
    int serversock, clientsock;
    struct sockaddr_in server, client;
    events_loop_t * loop = (events_loop_t *)arg;
    assert(loop);
    int r;

    ahttpd_debug("accepter_thread runs...\n");

    // Create the TCP socket
    if ((serversock = ev_socket(loop, PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
        die("Failed to create socket");

    // Construct the server sockaddr_in structure
    memset(&server, 0, sizeof(server));     // Clear struct
    server.sin_family = AF_INET;            // Internet/IP
    server.sin_addr.s_addr = htonl(INADDR_ANY); // IP address
    server.sin_port = htons(PORT);          // server port

    // Bind the server socket
    if (ev_bind(loop, serversock, (struct sockaddr *) &server,
         sizeof(server)) < 0)
    {
        die("Failed to bind the server socket");
    }

    // Listen on the server socket
    if (ev_listen(loop, serversock, MAXPENDING) < 0)
        die("Failed to listen on server socket");

    ahttpd_debug("Waiting for http connections...\n");

    while (1) {
        unsigned int clientlen = sizeof(client);
        // Wait for client connection
        if ((clientsock = ev_accept(loop, serversock,
                     (struct sockaddr *) &client,
                     &clientlen)) < 0)
        {
            printf("Failed to accept client connection");
            return;
        }

        ahttpd_debug("AHttpD server accepted a new connection.\n");

        r = ev_func_wait_for_sock(loop, handle_client, clientsock, EV_FD_READABLE, 0, NULL);
        if (r < 0) panic("AHttpD: accepter couldn't ev_func_wait_for_sock() for the new conn. err: %e.\n", r);

        /*r = thread_create(&tid, "handle_client", handle_client, (uint32_t)clientsock);
        if (r < 0) panic("thread_create(handle_client) returned %e\n", r);*/
        thread_yield(); // let it run // what happens without it?
    }

    ev_close(loop, serversock);
}

void
umain(int argc, char **argv)
{
    int r;

    binaryname = "jhttpd";

    thread_id_t tid;

    cprintf("AHttpD: my env id: %x\n", thisenv->env_id);

    r = ev_init_loop(&main_loop);
    if (r < 0) panic("ev_init_loop() returned %e\n", r);

    r = thread_create(&tid, "accepter_thread", accepter_thread, (uint32_t)&main_loop);
    if (r < 0) panic("thread_create(accepter_thread) returned %e\n", r);
    r = thread_create(&tid, "spinner", spinner, (uint32_t)&main_loop);
    if (r < 0) panic("thread_create(spinner) returned %e\n", r);

    thread_yield(); // does not return bacause we are not in a thread now.
    assert(0);
}
