#include <inc/lib.h>
#include <net/dsm/dsm.h>
#include <inc/utils.h>


//#define DSM_P1 /* TODO: REMOVE! */


void yield() {
    int i;
    for (i = 0; i < 500; i++)
        sys_yield();
    return;
}

#ifdef DSM_P1
static dsm_peer_net_id_t dsm_my_id = 0x0;
#endif
#ifdef DSM_P2
static dsm_peer_net_id_t dsm_my_id = 0x1;
#endif
#ifdef DSM_P3
static dsm_peer_net_id_t dsm_my_id = 0x2;
#endif
#ifdef DSM_P4
static dsm_peer_net_id_t dsm_my_id = 0x3;
#endif

#define NPEERS 2
static dsm_peer_t peers[NPEERS];

void umain(int argc, char **argv) {
    int i;

    cprintf("dsmdemo: my env_id: %x\n\n", thisenv->env_id);

    // init the peers array
    i = 0;
#ifndef DSM_P1
    peers[i].id = 0x0,
    peers[i].net_addr.sin_family = AF_INET,
    //peers[i].net_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    inet_aton("127.0.0.1", &peers[i].net_addr.sin_addr);
    peers[i].net_addr.sin_port = htons(26002);
    i++;
#endif
#ifndef DSM_P2
    peers[i].id = 0x1,
    peers[i].net_addr.sin_family = AF_INET,
    //peers[i].net_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    inet_aton("127.0.0.1", &peers[i].net_addr.sin_addr);
    peers[i].net_addr.sin_port = htons(25002);
    i++;
#endif
#if NPEERS > 2 && !(defined (DSM_P3))
    peers[i].id = 0x2,
    peers[i].net_addr.sin_family = AF_INET,
    //peers[i].net_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    inet_aton("127.0.0.1", &peers[i].net_addr.sin_addr);
    peers[i].net_addr.sin_port = htons(24001);
    i++;
#endif
#if NPEERS > 3 && !(defined (DSM_P4))
    peers[i].id = 0x3,
    peers[i].net_addr.sin_family = AF_INET,
    //peers[i].net_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    inet_aton("127.0.0.1", &peers[i].net_addr.sin_addr);
    peers[i].net_addr.sin_port = htons(23001);
    i++;
#endif

    int r;
    void *range_start_va = (void*)0xa00000;
    size_t range_npages = 8;
    r = dsm_init(dsm_my_id, range_start_va, range_npages, peers, NPEERS-1);
    if (r < 0)
        panic("DsmDemo: Could not init the dsm. error: %e\n", r);


    cprintf("DsmDemo: after init!\n");

    // demo of using the memory ..

volatile int * ptr = (int*)range_start_va;

/*
#ifdef DSM_P1
    *ptr = 10;
    while(*ptr != 12) sys_yield();
    cprintf("%d\n", *ptr);
#else
    while(*ptr != 10) sys_yield();
    cprintf("%d\n", *ptr);
#endif
*/


#ifdef DSM_P1

    *ptr = 10;
    cprintf("\n\nha after write %d\n\n", *ptr);
    while (*ptr == 10) yield();
    cprintf("\n\nha after read %d\n\n", *ptr);

#else

    while (*ptr != 10) yield();
    cprintf("\n\nha after read %d\n\n", *ptr);

    *ptr = 11;
    cprintf("\n\nha after write %d\n\n", *ptr);

#endif


    while (1);
}
