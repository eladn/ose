// idle loop

#include <inc/lib.h>
#include <inc/drivers/vga.h>
#include <inc/assert.h>

void
umain(int argc, char **argv)
{
	int in;
	int ret;
	struct vga_rect clr = { .left = 0, .top = 0, .width = 320, .height = 200, .color = 0xFF };
	struct vga_rect rect1 = { .left = 10, .top = 10, .width = 20, .height = 20, .color = 0x03 };
	struct vga_rect rect2 = { .left = 20, .top = 20, .width = 20, .height = 20, .color = 0x11 };
	struct vga_shape objs[] = {{.rect = clr}, {.rect = rect1}, {.rect = rect2}};
	size_t nobjs = 3;

	ret = sys_vga_setmode(320, 200, 256, 0);
	assert(ret == 0);

	ret = sys_vga_draw(objs, nobjs);
	assert(ret == 0);

	cscanf("%c", &in);

	ret = sys_vga_text_mode(0, 0);
	assert(ret == 0);
}

