#include <inc/lib.h>
#include <lwip/sockets.h>
#include <lwip/inet.h>
#include <net/dsm/dsm.h>
#include <net/dsm/dsm_protocol.h>

volatile int server_initialized = 0;

#define DSM_DEBUG 0 /* TODO: set flag off */
#define DSM_DEBUG2 1
#define dsm_debug(...) if (DSM_DEBUG) cprintf(__VA_ARGS__);
#define dsm_debug2(...) if (DSM_DEBUG2) cprintf(__VA_ARGS__);

struct {
    net_env_data_t *net_env;
    dsm_peer_t *peer;
} socket_to_env[MAXFD];

fd_set all_peers_sockset;
static int maxsock = 0;
struct sockaddr_in server_addr;
int serversock;

events_loop_t main_loop;


extern net_env_data_t *net_envs;

/* used to store handshake req/resp from the peer. */
/* Note: other msgs are passed through the matching NetPageInfo struct. */
dsm_async_waiting_msg_t socket_waiting_incoming_resp[MAXFD];
dsm_async_waiting_msg_t socket_waiting_incoming_req[MAXFD];

/* receive IPCs */
#define DSM_IPC_REQ_BUFF_QUEUE_SIZE  20
#define DSM_IPC_REQ_BUFF_REQVA       (0x0ffff000 - DSM_IPC_REQ_BUFF_QUEUE_SIZE * PGSIZE)
static bool buse[DSM_IPC_REQ_BUFF_QUEUE_SIZE];
static int next_i(int i) { return (i+1) % DSM_IPC_REQ_BUFF_QUEUE_SIZE; }
static int prev_i(int i) { return (i ? i-1 : DSM_IPC_REQ_BUFF_QUEUE_SIZE-1); }
static void * get_buffer(void) {
    void *va;

    int i;
    for (i = 0; i < DSM_IPC_REQ_BUFF_QUEUE_SIZE; i++)
        if (!buse[i]) break;

    if (i == DSM_IPC_REQ_BUFF_QUEUE_SIZE) {
        panic("NS: buffer overflow");
        return 0;
    }

    va = (void *)(DSM_IPC_REQ_BUFF_REQVA + i * PGSIZE);
    buse[i] = 1;

    return va;
}
static void put_buffer(void *va) {
    int i = ((uint32_t)va - DSM_IPC_REQ_BUFF_REQVA) / PGSIZE;
    buse[i] = 0;
}

static int dsm_env_init(events_loop_t *loop, envid_t envid, dsm_peer_net_id_t my_id, void *range_start_va, size_t range_npages,
                        dsm_peer_t *peers, size_t npeers);

static void dsm_ipc_recv_handler(events_loop_t *loop, envid_t from_env, int msg_type,
                          void *ipc_recv_pg, int ipc_recv_perm, void *user_context)
{
    int r;
    union DSMipc *req = (union DSMipc *)ipc_recv_pg;
    net_env_data_t *netenv;

    while (!server_initialized) thread_yield();

    switch(msg_type) {
        case DSMREQ_INIT_NET_ENV:
            assert((ipc_recv_perm & (PTE_P | PTE_U)) == (PTE_P | PTE_U));
            r = dsm_env_init(loop,
                             from_env,
                             req->init_net_env.my_id,
                             req->init_net_env.range_start_va,
                             req->init_net_env.range_npages,
                             req->init_net_env.peers,
                             req->init_net_env.npeers);
            break;
        case DSMREQ_PGFAULT_REPORT:
            dsm_debug2("DSMSRV: pgfault report from env: %x va %x!\n", from_env, req->pgfault_report.va);
            assert(ipc_recv_perm & (PTE_P|PTE_U|PTE_W));
            netenv = find_net_env_by_envid(from_env);
            if (!netenv) {
                panic("DSMSRV: netenv not found!\n");
                r = -1;
                break;
            }
            if (req->pgfault_report.err_flags & FEC_WR)
                r = dsm_proto_become_exclusive(loop, netenv, req->pgfault_report.va);
            else
                r = dsm_proto_become_shared(loop, netenv, req->pgfault_report.va);
            break;
        default:
            // Unknown msg type - just ignore
            //panic("dsm_ipc_recv_handler(): unknown msg type!\n");
            cprintf("Invalid request code %d from %08x\n", msg_type, from_env);
            r = -E_INVAL;
            break;
    }

    sys_page_unmap(0, ipc_recv_pg);
    dsm_debug("DSMSRV: Done handling IPC. returned err: %d\n", r);
    ipc_send(from_env, r, NULL, 0);
    
    return;
}

char* dsm_msg_num2capname[DSM_NR_MSG_TYPES]  = {
#define X(cap, name) #cap ,
    DSM_MSG_LIST
#undef X
};

#define DSM_MSG_NUM2CAPNAME(num) ( ((num) < DSM_NR_MSG_TYPES) ? dsm_msg_num2capname[(num)] : "<unknown>" )

static void __incoming_msg_handler(events_loop_t *loop, int sock, event_type_t events, void *user_context) {
    net_env_data_t *net_env = (net_env_data_t *)user_context;
    assert(net_env);
    dsm_msg_t msg;
    int r = ev_receiven(loop, sock, &msg, sizeof(msg));
    if (r < 0) panic("PANIC: DSM: incoming_msg_handler() ev_receiven msg err: %e\n", r);

    dsm_debug2("DSMSRV: new msg arrived! type: %s(num:%d) req_id: %u status: %d peer id: %u \n",
        DSM_MSG_NUM2CAPNAME(msg.header.msg_type), msg.header.msg_type, msg.header.req_id, msg.share_page_resp.status, msg.header.from_peer_id);

    if (msg.header.msg_type == DSM_MSG_CAPNAME2ENUM(HANDSHAKE_RESP)
        || msg.header.msg_type == DSM_MSG_CAPNAME2ENUM(HANDSHAKE_REQ)) {

        dsm_async_waiting_msg_t *waiting_msg_on_sock = 
            (msg.header.msg_type == DSM_MSG_CAPNAME2ENUM(HANDSHAKE_REQ))
            ? &socket_waiting_incoming_req[sock]
            : &socket_waiting_incoming_resp[sock];

        if (!waiting_msg_on_sock->tid)
            panic("PANIC: DSM: incoming_msg_handler() - received unexpected handshake msg.\n");

        assert(waiting_msg_on_sock->response_msg_buf != NULL);
        memcpy(waiting_msg_on_sock->response_msg_buf, &msg, sizeof(msg));
        if ((r = thread_wakeup_by_id(waiting_msg_on_sock->tid)) < 0)
            panic("PANIC: DSM: incoming_msg_handler() initialize. couldn't find initializer thread. waiting thread id: %u.\n", waiting_msg_on_sock->tid);
        return;
    }

    dsm_peer_t *peer = socket_to_env[sock].peer;
    assert(peer); /* we are after handshake */

    assert(msg.header.nva);
    dsm_net_page_t *npg = NVA2NPG(net_env, msg.header.nva);
    assert(npg);
    cprintf("npg->waiting_transaction.tid: %u npg->waiting_transaction.req_id: %u npg->waiting_transaction.peer_id: %u \n",
        npg->waiting_transaction.tid, npg->waiting_transaction.req_id, npg->waiting_transaction.peer_id);
    if (npg->waiting_transaction.tid
        && msg.header.req_id == npg->waiting_transaction.req_id
        && peer->id == npg->waiting_transaction.peer_id)
    {
        dsm_debug("DSMSRV: last msg is a response to an awaiting transaction!\n");

        thread_id_t tid = npg->waiting_transaction.tid;
        void *response_msg_buf = npg->waiting_transaction.response_msg_buf;
        void *response_page_buf = npg->waiting_transaction.response_page_buf;

        /* Important: clear awaiting transaction before calling ev_receiven() again! */
        npg->waiting_transaction.tid = 0;
        npg->waiting_transaction.peer_id = 0;
        npg->waiting_transaction.req_id = 0;
        npg->waiting_transaction.response_msg_buf = NULL;
        npg->waiting_transaction.response_page_buf = NULL;

        assert(response_msg_buf);
        memcpy(response_msg_buf, &msg, sizeof(msg));
        if (msg.header.msg_type == DSM_MSG_CAPNAME2ENUM(SHARE_PAGE_RESP)
            && msg.share_page_resp.status == DSC__SUCCESS)
        {
            dsm_debug("DSMSRV: new page arrived with last msg!\n");
            assert(response_page_buf);
            int r = ev_receiven(loop, sock, response_page_buf, PGSIZE);
            if (r < 0) panic("PANIC: DSM: incoming_msg_handler() ev_receiven PAGE err: %e\n", r);
            assert(r == PGSIZE);
            dsm_debug("DSMSRV: new page read finish!\n");
        }

        if ((r = thread_wakeup_by_id(tid)) < 0)
            panic("PANIC: DSM: incoming_msg_handler(). couldn't find matching thread to wake up. waiting thread id: %u.\n", npg->waiting_transaction.tid);
        return;
    }

    /* incoming message from a peer that we already waiting for a response from */
    /* we defined in our protocol description who wins. it changes in turns. */
    if (npg->waiting_transaction.tid
        && msg.header.req_id != npg->waiting_transaction.req_id
        && peer->id == npg->waiting_transaction.peer_id)
    {
        int yield_now = peer->yield_turn;
        peer->yield_turn = 1 - peer->yield_turn;
        /* if we should not yield now (the peer should yield), we response to his msg
        with NO SUCC */
        if (!yield_now) {
            // TODO: respond to the msg with no succ
            if (msg.header.msg_type == DSM_MSG_CAPNAME2ENUM(EXCLUSIVE_PAGE_REQ)) {
                dsm_proto_send_exclusive_page_resp(loop, net_env, npg, msg.header.from_peer_id, DSC__REJECTED, msg.header.req_id);
            }
            if (msg.header.msg_type == DSM_MSG_CAPNAME2ENUM(SHARE_PAGE_REQ)) {
                dsm_proto_send_share_page_resp_aux(loop, net_env, npg, msg.header.from_peer_id, msg.header.req_id, 1);
            }
            return;
        }
        // Note: when it is our time to yield, the peer will just send us a msg with no succ.
    }

    r = dsm_proto_dispatch_req(loop, net_env, &msg);
    if (r < 0) panic("PANIC: DSM: incoming_msg_handler() -> dsm_proto_dispatch_req() returned err: %e\n", r);

    dsm_debug("DSMSRV: Done handling new msg! type: %d req_id: %u status: %d peer id: %u \n",
        msg.header.msg_type, msg.header.req_id, msg.share_page_resp.status, msg.header.from_peer_id);
}

static void incoming_msg_handler(events_loop_t *loop, int sock, event_type_t events, void *user_context) {
    __incoming_msg_handler(loop, sock, events, user_context);
    int r = ev_func_wait_for_sock(loop, incoming_msg_handler, sock, EV_FD_READABLE,
                          0 /* not repeated */, user_context);
    if (r < 0) panic("DSMSRV: incoming_msg_handler(): couldnt set ev_func_wait_for_sock() err: %e\n", r);
}



/* Protocol messages functions */

int initiate_handshake(events_loop_t *loop, net_env_data_t *self, dsm_peer_t *peer) {
    int r;

    dsm_msg_t new_msg;
    
    memset(&new_msg, 0, sizeof(new_msg));

    new_msg.header.msg_type = DSM_MSG_CAPNAME2ENUM(HANDSHAKE_REQ);
    new_msg.header.from_peer_id = self->my_net_id;
    new_msg.header.to_peer_id = peer->id;
    new_msg.header.nva = 0;
    new_msg.header.req_id = 0;

    //int r = ev_send(loop, peer->sock, &new_msg, buf_len, 0);
    //if (r < 0) return r;
    r = dsm_proto_send_dsm_msg(loop, self, &new_msg);
    if (r < 0) return r;

    /* we go to sleep until the matching response arrives. */
    r = dsm_receive_resp_from_sock(loop, self, peer->sock, &new_msg);
    if (r < 0) return r;

    if (new_msg.handshake_resp.status != DSC__SUCCESS)
        return -1;

    return 0;
}

int respond_to_handshake(events_loop_t *loop, net_env_data_t *self, int peer_socket) {
    int r;

    dsm_msg_t full_msg;
    full_msg = full_msg; // TODO : remove, just for compiler to stop crying about uninit;

    // receive the "handshake_req" into full_msg;
    r = dsm_receive_req_from_sock(loop, self, peer_socket, &full_msg);

    dsm_peer_t* peer = find_peer_by_id(self, full_msg.header.from_peer_id);
    peer->sock = peer_socket;
    socket_to_env[peer_socket].peer = peer;

    memset(&full_msg, 0, sizeof(full_msg));

    full_msg.header.msg_type = DSM_MSG_CAPNAME2ENUM(HANDSHAKE_RESP);
    full_msg.header.from_peer_id = self->my_net_id;
    full_msg.header.to_peer_id = peer->id;
    full_msg.header.nva = 0;
    //full_msg.header.req_id = req_id; // just leaving req_id the same as it was.

    full_msg.handshake_resp.status = DSC__SUCCESS;

    r = dsm_proto_send_dsm_msg(loop, self, &full_msg);
    if (r < 0) return r;

    cprintf("DSMSRV: Successfully finished handshake response with peer_id: %x, my net id: %x env_id: %x\n",
        peer->id, self->my_net_id, self->envid);

    return 0;
}



/* Initializations for new net env */

int accept_peer_connection(events_loop_t *loop, net_env_data_t *net_env) {
    int r;
    int clientsock;
    struct sockaddr_in client_addr;
    unsigned int peer_addr_len = sizeof(client_addr);
    assert(loop);

    // Wait for client connection
    if ((clientsock = ev_accept(loop, serversock,
                 (struct sockaddr *) &client_addr,
                 &peer_addr_len)) < 0)
    {
        panic("Failed to accept client connection");
    }

    cprintf("DSMSRV: Successfully received connection to peer_id: %x env_id: %x\n",
        net_env->my_net_id, net_env->envid);

    socket_to_env[clientsock].net_env = net_env;
    /* socket_to_env[clientsock].net_env and peer->sock are set
    in `respond_to_handshake()` */
    FD_SET(clientsock, &all_peers_sockset);

    r = ev_func_wait_for_sock(loop, incoming_msg_handler, clientsock, EV_FD_READABLE, 
                              0 /* not repeated */, net_env);
    if (r < 0) return r;

    // wait & receive & respond to handshake !
    if ((r = respond_to_handshake(loop, net_env, clientsock)) < 0)
        return r;

    return 0;
}

int initiate_peer_connection(events_loop_t *loop, net_env_data_t *net_env, dsm_peer_t *peer) {
    int clientsock;
    int r;

    clientsock = ev_socket(loop, AF_INET, SOCK_STREAM, 0);
    if (clientsock < 0)
        panic("DSMSRV: ERROR opening socket\n");

    peer->sock = clientsock;
    socket_to_env[clientsock].net_env = net_env;
    socket_to_env[clientsock].peer = peer;

    r = ev_func_wait_for_sock(loop, incoming_msg_handler, clientsock, EV_FD_READABLE,
                          0 /* not repeated */, net_env);
    if (r < 0) return r;
    FD_SET(clientsock, &all_peers_sockset);


    dsm_debug("DSMSRV: Trying to connect to peer id: %x my peer_id: %x env_id: %x sock: %d port: %d\n",
        peer->id, net_env->my_net_id, net_env->envid, clientsock, (uint32_t)ntohs(peer->net_addr.sin_port));

    // TODO: try&fail after some attempts.
    // try to connect to peer
    while ((r = ev_connect(loop, clientsock,(struct sockaddr *)&peer->net_addr, sizeof(peer->net_addr))) < 0) 
        thread_yield();
    //if (r < 0) panic("DSMSRV: ERROR connecting to peer id: %x env_id: %x\n", (uint32_t)peer->id, net_env->envid);

    dsm_debug("DSMSRV: Successfully connected to peer id: %x my peer_id: %x env_id: %x sock: %d port: %d\n",
        peer->id, net_env->my_net_id, net_env->envid, clientsock, (uint32_t)ntohs(peer->net_addr.sin_port));

    // perform handshake with peer !
    if ((r = initiate_handshake(loop, net_env, peer)) < 0)
        return r;

    dsm_debug("DSMSRV: Successfully initiated peer connection: peer id: %x my peer_id: %x env_id: %x sock: %d port: %d\n",
        peer->id, net_env->my_net_id, net_env->envid, clientsock, (uint32_t)ntohs(peer->net_addr.sin_port));

    return 0;
}

static dsm_net_page_t* dsm_env_allocate_net_page_infos(net_env_data_t* net_env, size_t range_npages, size_t npeers) {
    int r;
    void *start_va = find_unmapped_va(DSM_VA_BEGIN);
    if (!start_va) {
        panic("DSM NPG: out of memory!! (no free VIRTUAL ADDRESS)");
        return NULL;
    }
    /*
    range_npages = the number NetPage structs we need to allocate.
    so the number of PHYSICAL PAGES we need from them to reside in is ROUNDUP((range_npages*sizeof(dsm_net_page_t)))
                                                                              ^^^total size of NetPage structs^^^
    */
    void *end_va = start_va + ROUNDUP(range_npages*sizeof(dsm_net_page_t), PGSIZE);
    void* cur_va;
    for (cur_va = start_va; cur_va < end_va; cur_va += PGSIZE){
        if ((r = sys_page_alloc(0, cur_va, (PTE_P|PTE_U|PTE_W))) < 0) {
            panic("DSM NPG: out of memory!! (no free PHYSICAL PAGE for NetPageInfos)");
            return NULL;
        }
        memset(cur_va, 0, PGSIZE);
    }

    net_env->net_page_arr = (dsm_net_page_t*)start_va;
    int i;
    for (i = 0; i < range_npages; ++i) {
        dsm_net_page_t* npg = &net_env->net_page_arr[i];

        npg->master_peer_id = i % (npeers + 1); // npeers DOESN'T count myself, so num of netenvs is +1...
        //npg->sem = sys_sem_new(1); //   TODO :: UNCOMMENT THIS!
        if (npg->master_peer_id == net_env->my_net_id) {
            npg->npg_state = NPS__EXCLUSIVE;
            if ((r = sys_page_alloc(net_env->envid, NPG2NVA(net_env, npg), (PTE_P|PTE_U|PTE_W))) < 0) {
                panic("DSM NPG: out of memory!! (no free PHYSICAL PAGE for MY SPACE)");
                return NULL;
            }
        } else {
            npg->npg_state = NPS__UNCACHED;
        }
    }

    return (dsm_net_page_t*)start_va;
}

static int dsm_env_init(events_loop_t *loop, envid_t envid, dsm_peer_net_id_t my_net_id,
                        void *range_start_va, size_t range_npages,
                        dsm_peer_t *peers, size_t npeers)
{
    int r;

    if (find_net_env_by_envid(envid) != NULL) return -E_FAULT;
    if (npeers > MAX_NET_ENV_PEERS) return -E_INVAL;

    net_env_data_t* net_env = find_free_net_env_buf();
    if (!net_env) return -E_FAULT;

    dsm_debug("DSMSRV: Received init request from env_id: %x. my net id: %x  va: %p, npages: %d npeers: %d\n",
        envid, (uint32_t)my_net_id, range_start_va, range_npages, npeers);

    net_env->envid = envid;
    net_env->my_net_id = my_net_id;
    net_env->range_start_va = range_start_va;
    net_env->range_npages = range_npages;
    net_env->npeers = npeers;
    memcpy(net_env->peers, peers, npeers*sizeof(*peers));

    dsm_peer_t *cur_peer = NULL;
    FOR_EACH_PEER(cur_peer, net_env) {
        cur_peer->next_req_id = (cur_peer->id < net_env->my_net_id ? 1 : 0);
        cur_peer->yield_turn = (cur_peer->id < net_env->my_net_id ? 1 : 0);
    }

    net_env->net_page_arr = dsm_env_allocate_net_page_infos(net_env, range_npages, npeers);


    /* by convension, initialize connection with all peers that has id
        which is bigger than mine. All other peers should initiate conn with me. */
    FOR_EACH_PEER(cur_peer, net_env) {
        if (cur_peer->id <= net_env->my_net_id) continue;
        dsm_debug("DSMSRV: Trying to connect peer id: %x. My peer_id: %x env_id: %x\n",
            (uint32_t)(cur_peer->id), (uint32_t)(net_env->my_net_id), net_env->envid);
        if ((r = initiate_peer_connection(loop, net_env, cur_peer)) < 0)
            return r; // TODO: free resources
    }

    /* Wait for connection from peers with id lower than mine */
    /* Note: `i` has no meaning here! We don't know the order of income connections. */
    FOR_EACH_PEER(cur_peer, net_env) {
        if (cur_peer->id >= net_env->my_net_id) continue;
        dsm_debug("DSMSRV: Trying to accept socket from peer. My peer id: %x env_id: %x\n",
            net_env->my_net_id, net_env->envid);
        if ((r = accept_peer_connection(loop, net_env)) < 0)
            return r; // TODO: free resources
    }

    return 0;
}




#define MAXPENDING 20    // Max connection requests
#define DSM_PORT 80 /* TODO: change to an ad-hoc port defined in GNUMakefile! */

int init_dsmsrv(events_loop_t *loop) {
    // Create the TCP socket
    if ((serversock = ev_socket(loop, PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
        panic("DSMSRV: Failed to create socket");

    // Construct the server sockaddr_in structure
    memset(&server_addr, 0, sizeof(server_addr));    // Clear struct
    server_addr.sin_family = AF_INET;                // Internet/IP
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY); // IP address
    server_addr.sin_port = htons(DSM_PORT);          // server port

    // Bind the server socket
    if (ev_bind(loop, serversock, (struct sockaddr *) &server_addr,
         sizeof(server_addr)) < 0)
    {
        panic("DSMSRV: Failed to bind the server socket");
    }

    // Listen on the server socket
    if (ev_listen(loop, serversock, MAXPENDING) < 0)
        panic("DSMSRV: Failed to listen on server socket");

    cprintf("DSMSRV: Successfully initialized listen port %d ...\n", DSM_PORT);

    FD_ZERO(&all_peers_sockset);

    server_initialized = 1;

    return 0;
}



static void initializer(uint32_t arg) {
    int r;
    events_loop_t * loop = (events_loop_t *)arg;
    assert(loop);

    dsm_debug("initializer runs...\n");

    if ((r = init_dsmsrv(loop)) < 0)
        panic("PANIC: DSM: dsm initializer: init_dsmsrv() failed. err: %e\n", r);

    r = ev_func_wait_for_ipc(loop, dsm_ipc_recv_handler, 1 /*repeated*/, NULL);
    if (r < 0) panic("PANIC: DSM: dsm initializer: couldnt init dsm_ipc_recv_handler. error: %d\n", r);


}

static void spinner(uint32_t arg) {
    int r;
    events_loop_t * loop = (events_loop_t *)arg;
    assert(loop);

    dsm_debug("spinner runs...\n");

    while(1) {
        r = ev_loop(loop);
        if (r < 0) panic("dsm spinner: loop error: %d\n", r);

        dsm_debug("loop escaped!\n\n");
    }
}

void
umain(int argc, char **argv)
{
    binaryname = "dsmsrv";

    int r;

    cprintf("DSMSRV: my env_id: %x\n\n", thisenv->env_id);

    thread_id_t tid;

    r = ev_init_loop(&main_loop);
    if (r < 0) panic("ev_init_loop() returned %e\n", r);

    r = thread_create(&tid, "initializer", initializer, (uint32_t)&main_loop);
    if (r < 0) panic("thread_create(initializer) returned %e\n", r);
    r = thread_create(&tid, "spinner", spinner, (uint32_t)&main_loop);
    if (r < 0) panic("thread_create(spinner) returned %e\n", r);
assert(thread_id() == 0);
    thread_yield(); // does not return bacause we are not in a thread now.
    assert(0);
}
