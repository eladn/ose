#include <inc/lib.h>

#ifndef min
#define min(a,b) (((a) < (b)) ? (a) : (b))
#endif
#ifndef max
#define max(a,b) (((a) > (b)) ? (a) : (b))
#endif


typedef struct mat {
	size_t n;
	size_t m;
	int values[0];
} mat_t;
#define mat_at(mat, i, j) (*(((mat)->values) + (i)*((mat)->m) + (j)))
#define mat_size(n, m) (sizeof(int) * ((n)*(m)) + sizeof(struct mat))
#define mat_npages(n ,m) (ROUNDUP(mat_size(n, m), PGSIZE) / PGSIZE)

typedef struct work_info {
	size_t segment;
	size_t i_start;
	size_t i_end;
	size_t j_start;
	size_t j_end;
} work_info_t;

static void* next_free_addr = ((void*)0xa00000);
static mat_t* intmat_alloc(size_t n, size_t m);
static mat_t* create_inverted_eye(size_t n);
static mat_t* create_range_mat(size_t n, size_t m);
static void print_mat(mat_t* mat);
static void mat_mul_worker();
static void mat_send(envid_t dstenvid, mat_t *mat, int grant_write_perm);
static mat_t* mat_recv(envid_t *workers_manager);
static void workers_manager(envid_t *workers, size_t nr_workers);
static void work_info_send(envid_t dstenvid, work_info_t work);
static void work_info_recv(envid_t *workers_manager, work_info_t *received_work);


// Share the matrices with each worker and tell each worker
// what portion of work it should do.
// Finally, wait for all the workers to complete 
// and print the result matrix.
void umain(int argc, char **argv) {

	// matrix size and work size per worker.
	size_t n = 33, m = 33;
	size_t row_slice = n/3, col_slice = m/3;
	size_t nr_workers = ((n*m) / (row_slice*col_slice));
	envid_t workers[nr_workers];

	mat_t* mat_A;
	mat_t* mat_B;
	mat_t* mat_C;
	envid_t sender;
	size_t i, worker;

	// create children workers
	for (i = 0; i < nr_workers; ++i){
		if ((workers[i] = fork()) != 0) continue;

		// this is the child
		mat_mul_worker();
		return;
	}

	/* main env - workers manager */

	// create the matrices
	mat_A = create_range_mat(n, m);
	if (mat_A == NULL)
		panic("no mem for mat A!");
	mat_B = create_inverted_eye(m);
	if (mat_B == NULL)
		panic("no mem for mat B!");
	mat_C = intmat_alloc(n, n);
	if (mat_C == NULL)
		panic("no mem for mat C!");

	// print matrices A & B
	cprintf("@@@ MAT MUL START @@@\n");
	cprintf("@@@ going to mul: @@@\n");
	print_mat(mat_A);
	cprintf("\n@@@ from right with: @@@\n");
	print_mat(mat_B);

	// divide the work into segments and send working segment for each child
	size_t next_i = 0, next_j = 0;
	for (worker = 0; worker < nr_workers; ++worker){
		work_info_t work = {
			.segment = worker,
			.i_start = next_i,
			.i_end = min(n-1, next_i + row_slice - 1),
			.j_start = next_j,
			.j_end = min(m-1, next_j + col_slice - 1)
		};
		work_info_send(workers[worker], work); // send work info
		mat_send(workers[worker], mat_A, 0); // MAT A - readonly
		mat_send(workers[worker], mat_B, 0); // MAT B - readonly
		mat_send(workers[worker], mat_C, 1); // MAT C - R/W

		if (work.j_end == m-1) {
			next_i += row_slice;
			next_j = 0;
		} else {
			next_j += col_slice;
		}
	}

	// wait for all workers to complete
	for (i = 0; i < nr_workers; ++i){
		int segment = ipc_recv(&sender, 0, 0);
		cprintf("[%08x] got message from worker: %x Who handled segment: %d\n", thisenv->env_id, sender, segment);
	}

	// print result matrix
	cprintf("@@@ MAT MUL DONE :) @@@\n");
	print_mat(mat_C);
}

// every worker works on a partial segment of the multipication
// it receives the matrices from the work manager
static void mat_mul_worker() {
	mat_t *mat_A, *mat_B, *mat_C;
	envid_t workers_manager;
	int row_half, col_half;
	work_info_t work;

	// receive my working segment
	work_info_recv(&workers_manager, &work);
	cprintf("[%08x] got message from: %x segment %u with work info: [%u,%u]*[%u,%u]\n", thisenv->env_id, workers_manager, work.segment, work.i_start, work.i_end, work.j_start, work.j_end);
		
	// receive the matrices we have to work on
	mat_A = mat_recv(&workers_manager);
	cprintf("[%08x] got message from: %x with matrix A\n", thisenv->env_id, workers_manager);
	mat_B = mat_recv(&workers_manager);
	cprintf("[%08x] got message from: %x with matrix B\n", thisenv->env_id, workers_manager);
	mat_C = mat_recv(&workers_manager);
	cprintf("[%08x] got message from: %x with matrix C\n", thisenv->env_id, workers_manager);

	cprintf("### MAT MUL CHANGING C @ segment %d ###\n", work.segment);
	
	assert(mat_A->m == mat_B->n);
	size_t p = mat_A->n, q = mat_A->m, r = mat_B->m;
	int i,j,k;
	for (i = work.i_start; i <= work.i_end; ++i) {
		for (j = work.j_start; j <= work.j_end; ++j) {
			mat_at(mat_C, i, j) = 0;
			for (k = 0; k < q; ++k)
				mat_at(mat_C, i, j) += mat_at(mat_A, i, k)*mat_at(mat_B, k, j);
		}
	}

	// send done message to parent
	ipc_send(workers_manager, work.segment, 0, 0);
}

static void work_info_send(envid_t dstenvid, work_info_t work) {
	ipc_send(dstenvid, work.segment, 0, 0);
	ipc_send(dstenvid, work.i_start, 0, 0);
	ipc_send(dstenvid, work.i_end, 0, 0);
	ipc_send(dstenvid, work.j_start, 0, 0);
	ipc_send(dstenvid, work.j_end, 0, 0);
}

static void work_info_recv(envid_t *workers_manager, work_info_t *received_work) {
	envid_t first_sender, sender;

	assert(received_work != NULL);

	received_work->segment = ipc_recv(&sender, 0, 0);
	received_work->i_start = ipc_recv(&sender, 0, 0);
	received_work->i_end = ipc_recv(&sender, 0, 0);
	received_work->j_start = ipc_recv(&sender, 0, 0);
	received_work->j_end = ipc_recv(&sender, 0, 0);

	if (workers_manager != NULL)
		*workers_manager = sender;
}

// receive a matrix to another env (might be several pages!)
static void mat_send(envid_t dstenvid, mat_t *mat, int grant_write_perm) {
	size_t npages = mat_npages(mat->n ,mat->m);
	size_t cur_page_idx;
	int perm = PTE_P | PTE_U;
	if (grant_write_perm) perm |= PTE_W;

	ipc_send(dstenvid, npages, 0, 0); // send MAT size
	for (cur_page_idx = 0; cur_page_idx < npages; ++cur_page_idx) {
		ipc_send(dstenvid, cur_page_idx, (void*)((uintptr_t)mat + (PGSIZE*cur_page_idx)), perm);
	}
}

// receive a matrix from another env (might be several pages!)
static mat_t* mat_recv(envid_t *workers_manager) {
	size_t cur_page_idx, value;
	size_t npages;
	mat_t *mat = (mat_t*)next_free_addr;
	int r;
	envid_t first_sender, sender;

	npages = ipc_recv(&first_sender, 0, 0);
	
	for (cur_page_idx = 0; cur_page_idx < npages; ++cur_page_idx) {
		value = ipc_recv(&sender, next_free_addr, 0);
		assert(sender == first_sender);
		if(value != cur_page_idx) {
			cprintf("value: %u cur_page_idx: %u\n\n\n", value, cur_page_idx);
		}
		next_free_addr = (void*)(((uintptr_t)next_free_addr) + PGSIZE);
	}
	if (workers_manager != NULL) {
		*workers_manager = first_sender;
	}
	return mat;
}

// allocate a new integers matrix
static mat_t* intmat_alloc(size_t n, size_t m) {
	size_t cur_page_idx, failed_page_idx;
	size_t npages = mat_npages(n ,m);
	mat_t *mat = (mat_t*)next_free_addr;
	int r;

	for (cur_page_idx = 0; cur_page_idx < npages; ++cur_page_idx) {
		if ((r = sys_page_alloc(thisenv->env_id, next_free_addr, (PTE_P | PTE_W | PTE_U))) < 0) {
			goto fail;
		}
		next_free_addr = (void*)(((uintptr_t)next_free_addr) + PGSIZE);
	}
	mat->n = n;
	mat->m = m;
	
	return mat;

fail:
	failed_page_idx = cur_page_idx;
	for (cur_page_idx = 0; cur_page_idx < failed_page_idx; ++cur_page_idx) {
		sys_page_unmap(thisenv->env_id, next_free_addr);
		next_free_addr = (void*)(((uintptr_t)next_free_addr) - PGSIZE);
	}
	return NULL;
}

static mat_t* create_inverted_eye(size_t n) {
	mat_t* mat = intmat_alloc(n, n);
	size_t i,j;
	if (mat == NULL) {
		return NULL;
	}
	
	// Initializing mat so that secondary diagonal is all ones (others zero)
	// which means multiplying by it from the right will FLIP COLUMNS
	for (i = 0; i < n; ++i) {
		for (j = 0; j < n; ++j) {
			mat_at(mat, i, j) = (i+j==(n-1))? 1 : 0;
		}
	}

	return mat;
}

static mat_t* create_range_mat(size_t n, size_t m) {
	mat_t* mat = intmat_alloc(n, m);
	size_t i,j;
	if (mat == NULL) {
		return NULL;
	}
	
	// Set cells to contain ascending range
	for (i = 0; i < n; ++i) {
		for (j = 0; j < m; ++j) {
			mat_at(mat, i, j) = (int)(i*m + j);
		}
	}

	return mat;
}

static void print_mat(mat_t* mat) {
	assert(mat != NULL);
	size_t i,j;
	
	for (i = 0; i < mat->n; ++i) {
		for (j = 0; j < mat->m; ++j) {
			cprintf(" %02d ", mat_at(mat,i,j));
		}
		cprintf("\n");
	}
}
