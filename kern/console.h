/* See COPYRIGHT for copyright information. */

#ifndef _CONSOLE_H_
#define _CONSOLE_H_
#ifndef JOS_KERNEL
# error "This is a JOS kernel header; user programs should not #include it"
#endif

#include <inc/types.h>
//#include <kern/io/serial_keys_map.h>

#define MONO_BASE	0x3B4
#define MONO_BUF	0xB0000
#define CGA_BASE	0x3D4
#define CGA_BUF		0xB8000

#define CRT_ROWS	25
#define CRT_COLS	80
#define CRT_SIZE	(CRT_ROWS * CRT_COLS)



/*  KEYBOARD/SERIAL INPUT HANDLING  */

#define KEY_MODIFIERS_LIST \
    X(NORMAL,   (1<<0),  0) \
    X(ALT,      (1<<1),  1) \
    X(CTRL,     (1<<2),  2) \
    X(SHIFT,    (1<<3),  3)

enum key_modifiers {
    #define X(name, code, idx) \
        KEY_##name = code,
    KEY_MODIFIERS_LIST
    #undef X
};

enum key_modifiers_idx {
    #define X(name, code, idx) \
        KEY_##name##_IDX = idx,
    KEY_MODIFIERS_LIST
    #undef X
};

static inline const char* key_modifier_str(int modifier) {
    #define X(name, code, idx) \
        (((modifier) & (code)) != 0) ? #name : 
    return KEY_MODIFIERS_LIST "";
    #undef X
}

/* used by `getkeypressed()` in order to pass the full info about the pressed key */
struct keypress {
	int16_t keycode;
    uint8_t asciicode;
    uint8_t modifiers;  /* NORMAL, ALT, CTRL, SHIFT */
};
#define INVALID_KEYPRESS_KEYCODE (-1)
#define UNKNOWN_KEYPRESS { .keycode = INVALID_KEYPRESS_KEYCODE, .asciicode = 0, .modifiers = 0 }
#define IS_INVALID_KEYPRESS(key) ((key).keycode == INVALID_KEYPRESS_KEYCODE)



void cons_init(void);
int cons_getc(void);
int cons_getc_peekable(int inc_buffer);

/*
	`nr_chars` might be negative.
	returns the actual number of chars the read cursor moved (value sign corresponding to move direction).
*/
int32_t cons_move_input_buffer_cursor(int32_t nr_chars);

void kbd_intr(void); // irq 1
void serial_intr(void); // irq 4

/*
	Unlike `cons_getc()`, `getkeypressed()` provides the full info of the pressed key.
	We get: keycode, modifiers (NORMAL, ALT, CTRL, SHIFT),
			and the actual asciicode it represents (if any, otherwize '\0').
*/
struct keypress get_key_pressed(void);

void clr_screen();
int get_serial_terminal_size(size_t *cols, size_t *rows);

#endif /* _CONSOLE_H_ */
