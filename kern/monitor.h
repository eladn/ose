#ifndef JOS_KERN_MONITOR_H
#define JOS_KERN_MONITOR_H
#ifndef JOS_KERNEL
# error "This is a JOS kernel header; user programs should not #include it"
#endif

#include <inc/stdio.h>
#include <inc/mmu.h>

struct Trapframe;

// Activate the kernel monitor,
// optionally providing a trap frame indicating the current state
// (NULL if none).
void monitor(struct Trapframe *tf);

// Functions implementing monitor commands.
/*int mon_help(int argc, char **argv, struct Trapframe *tf);
int mon_man(int argc, char **argv, struct Trapframe *tf);
int mon_kerninfo(int argc, char **argv, struct Trapframe *tf);
int mon_backtrace(int argc, char **argv, struct Trapframe *tf);
int mon_showmap(int argc, char **argv, struct Trapframe *tf);
int mon_modperm(int argc, char **argv, struct Trapframe *tf);
int mon_dumpmem(int argc, char **argv, struct Trapframe *tf);
int mon_shutdown(int argc, char **argv, struct Trapframe *tf);
int mon_continue(int argc, char **argv, struct Trapframe *tf);

int mon_argtest(int argc, char **argv, struct Trapframe *tf);
*/


struct Command {
	const char *name;
	const char *short_name;
	const char *desc;
	const char *usage;
	// return -1 to force monitor to exit
	int (*func)(int argc, char** argv, struct Trapframe* tf);
};

#define HLP_USAGE_STR  "just 'help' or 'h'"
#define MAN_USAGE_STR  "mon <command_name>"
#define KI_USAGE_STR   "just 'kerninfo' or 'info'"
#define BT_USAGE_STR   "just 'backtrace' or 'bt'"
#define SM_USAGE_STR   "showmappings [(v|p) <start> [<end>]]\nexamples:\n"\
						"showmappings                  - will display all of the mappings in the system.\n"\
                        "showmappings v 0x3000 0x5000  - will display the physical page mappings and corresponding\n"\
                        "                                permission bits that apply to the pages at virtual addresses\n"\
                        "                                0x3000, 0x4000, and 0x5000.\n"\
                        "showmappings p 0x3000 0x5000  - will display the full mapping information for all of the pages mapped\n"\
                        "                                to the frames at physical addresses 0x3000, 0x4000, and 0x5000.\n"\
                        "showmappings v 0x4000         - will display the full mapping information for the page at virtual\n"\
                        "                                address 0x4000.\n" \
                        "showmappings p 0x4000         - will display the full mapping information for all of the pages\n"\
                        "                                mapped to the frame at physical address 0x4000.\n"

// NOTE:: IN ANY USAGE OF THIS CONSTAT, YOU HAVE TO DEFINE FLG AS WELL
//        I HOPE GOD WILL FORGIVE ME FOR THIS SIN
//        (a good example can be found in monitor.c)
#define MP_USAGE_STR    "modperm <addr> <set|clr> <perm>+\nexamples:\n"\
                        "modperm 0x12345 set UW - will set permission bits 'User' and 'Write' for the page starting at 0x12345000.\n"\
                        "modperm 0x12345 clr DA - will clear permission bits 'Dirty' and 'Accessed' for the page starting at 0x12345000.\n"\
                        "\npermission bit values:\n" PTE_FLAGS_LIST

#define DM_USAGE_STR   "dumpmem [<start> [<end>]]\nexamples:\n"\
                        "dumpmem v 0x12345678 0x12346000  - will display the contents of memory in virtual addresses between 0x12345678 and 0x12346000.\n"\
                        "dumpmem p 0x12345678 0x12346000  - will display the contents of memory in physical addresses between 0x12345678 and 0x12346000.\n"\
                        "dumpmem v 0x12345678             - will display the a single 32-bit word of memory at virtual address 0x12345678.\n"\
                        "dumpmem p 0x12345678             - will display the a single 32-bit word of memory at physical address 0x12345678.\n"\
                        "\nthe range is inclusive. addresses will be properly aligned to a word boundary (start will be rounded down and end will be rounded up).\n"


#define MON_ENUM_PREFIX(name) MON_##name


// an X-Macro list of all supported monitor command in the system. each row represents a single command,
// the usage is #define X(ENUM_NAME, STR_NAME, STR_SHORT_NAME, STR_DESCRIPTION, STR_USAGE, HANDLER_FUNC_PTR)
// NOTE :: X-Macros are a known pattern for enabling static-polymorphism in C,
//         but could be difficult to understand at first glance.
//         in short - they allow you to generate polymorphic code with ease throught the entire project,
//         and when you need to change or add a new item (e.g. a new command) only a single
//         place (this LIST macro) needs to be modified.
#define MON_COMMAND_LIST \
	X(HELP,       "help",         "h",       "Display this list of commands.",                       HLP_USAGE_STR,   mon_help       ) \
	X(MAN,        "man",          NULL,      "Display information and usage for specific command.",  MAN_USAGE_STR,   mon_man        ) \
	X(KERNINFO,   "kerninfo",     "info",    "Display information about the kernel.",                KI_USAGE_STR,    mon_kerninfo   ) \
	X(BACKTRACE,  "backtrace",    "bt",      "Display information about active function calls.",     BT_USAGE_STR,    mon_backtrace  ) \
	X(SHOWMAP,    "showmappings", "sm",      "Display information about physical page mappings.",    SM_USAGE_STR,    mon_showmap    ) \
	X(MODPERM,    "modperm",      "mp",      "Modify permissions for a specified page.",             MP_USAGE_STR,    mon_modperm    ) \
	X(DUMPMEM,    "dumpmem",      "dm",      "Display memory contents at specified addresses.",      DM_USAGE_STR,    mon_dumpmem    ) \
	X(ARGTEST,    "argtest",      "at",      "ARGUMENT PARSER TEST ONLY.",                           "NO USAGE",      mon_argtest    ) \
	X(SHUTDOWN,   "shutdown",     "exit",    "Shutdown the QEMU.",                                   NULL,            mon_shutdown   ) \
	\
	X(CONTINUE,   "continue",     "c",       "Return from monitor.",                                 NULL,            mon_continue   ) \
	X(STEP,       "step",         "s",       "Return from monitor and step a single instruction.",   NULL,            mon_step       ) \
	X(CBP,        "cbp",          "b",       "Creates a new breakpint.",                             NULL,            mon_cbp        ) \
	X(DBP,        "dbp",          "d",       "Delete a breakpoint.",                                 NULL,            mon_dbp        ) \
	X(LBP,        "lbp",          NULL,      "List all breakpoints.",                                NULL,            mon_lbp        )

/* Define all the functions implementing monitor commands. */
#define X(name, str, short, desc, usage, func) \
	int func(int argc, char **argv, struct Trapframe *tf);
MON_COMMAND_LIST
#undef X

// an enum listing all existing montior commands.
enum mon_commands_names {
	#define X(NAME, STR_NAME, STR_S_NAME, STR_DESC, STR_USG, FUNC) \
		MON_ENUM_PREFIX(NAME),
	MON_COMMAND_LIST
	#undef X
	MON_NR_COMMANDS
};

// prints an "invalid usage" message alog with a valid usage explanation.
// used individually by each command when detecting invalid input, to maintain a constant format.
static inline void mon_invalid_usage(int command_name) {
	extern struct Command mon_commands[];
	cprintf("\n\nWrong usage of %s! Please use: \n%s\n\n", mon_commands[command_name].name, mon_commands[command_name].usage);
}

void monitor_set_exit_flag(int val);


#endif	// !JOS_KERN_MONITOR_H
