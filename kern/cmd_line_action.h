#ifndef JOS_INC_CMD_LINE_ACTION_H
#define JOS_INC_CMD_LINE_ACTION_H

/*
	`CmdLineAction` mechanism is currently not in use by the monitor.
	TODO: We have to change the design, so that each cell in the binding array
		  would uniquely identify a `keypress` (keycode+modifiers).
		  We also have to require that the array size would have at least `KEYCODE_MAX` elements,
		  even if empty, just in order to avoid accessing illegal/irelevant memory.
*/

struct CmdLineAction;

typedef int (*cmd_line_action_cb) (struct CmdLineAction *binding,
                               char* cmd_input_buffer,
                               int *cmd_buffer_cursor,
                               int invoking_key_stroke);

#define CMD_KEY_BIND_MAX_KEY_ASCII (512)

struct CmdLineAction {
	cmd_line_action_cb action;
};

#endif /* JOS_INC_CMD_LINE_ACTION_H */
