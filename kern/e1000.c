#include <kern/e1000.h>
#include <kern/pmap.h>
#include <kern/env.h>
#include <kern/sched.h>
#include <kern/trap.h>
#include <kern/picirq.h>
#include <kern/cpu.h>
#include <inc/assert.h>
#include <inc/error.h>
#include <inc/string.h>
#include <inc/utils.h>
#include <inc/ns.h>


// LAB 6: Your driver code here

physaddr_t e1000paddr;
size_t e1000_mmio_size;
volatile uint32_t *e1000;

static physaddr_t network_dev_buffer_paddr;
static physaddr_t tx_desc_array_paddr, rx_desc_array_paddr;
static volatile struct tx_desc* tx_desc_array;
static volatile struct rx_desc* rx_desc_array;

struct mac_addr e1000_mac;
struct mac_addr e1000_default_mac = {.full = 0x525400123456};


void e1000_irq_handler(struct Trapframe *tf, void* arg);
static int e1000_get_mac_from_eeprom(struct mac_addr *mac);

int e1000_init(struct pci_func *pcif) {
    int i;
    int r;
    volatile struct tx_desc *tx_desc;
    volatile struct rx_desc *rx_desc;

    pci_func_enable(pcif);
    e1000paddr = (uint32_t)(pcif->reg_base[0]);
    assert(e1000paddr >= 3U*(1U << 30));
    e1000_mmio_size = pcif->reg_size[0];


    // e1000paddr is the physical address of the E1000's MMIO region.
    // Map it in to virtual memory so we can access it.
    e1000 = (uint32_t*)mmio_map_region(e1000paddr, e1000_mmio_size);

    //cprintf(" >>>>>>>>>>>> e1000_init() status: 0x%x\n", E1000_REG_LGET(E1000_REG_DEV_STATUS) );


    /** Transmit Initialization: **/

    /*Allocate a region of memory for the transmit descriptor list.
    Software should insure this memory is aligned on a paragraph (16-byte) boundary. */
    network_dev_buffer_paddr = reserved_phys_regions[RESERVED_PHYS_REGION_NETDEV].reserved_start_paddr;
    tx_desc_array_paddr = TX_DESC_ARRAY_PADDR(network_dev_buffer_paddr);
    assert(IS_ALIGNED_TO(tx_desc_array_paddr, 16));
    tx_desc_array = KADDR(tx_desc_array_paddr);
    static_assert(sizeof(struct tx_desc) == 16);

    /*Allocate transmit buffers & initialize descriptors*/
    memset((void*)tx_desc_array, 0, TX_DESC_ARRAY_SIZE);
    for(tx_desc = tx_desc_array;
        (uint32_t)tx_desc < (uint32_t)(tx_desc_array + TX_DESC_ARRAY_ENTRIES);
        ++tx_desc) {
        // We use the DD flag to identify available descriptors to transmit.
        // In a general flow the hardware sets this bit after it frees a descriptor.
        // Here we synthetically set DD on because all descriptors are now free.
        tx_desc->status = E1000_TXD_STAT_DD;

        // Allocate tx buffer and assign to descriptor.
        tx_desc->addr = (uint64_t)TX_BUFFER_IDX2PADDR(network_dev_buffer_paddr, (tx_desc - tx_desc_array));
    }

    // We updated the descriptors in memory. We should use write memory barrier,
    // before telling the card it can use this descriptor.
    mb();

    /*Program the Transmit Descriptor Base Address (TDBAL/TDBAH) register(s)
    with the address of the region. TDBAL is used for 32-bit addresses
    and both TDBAL and TDBAH are used for 64-bit addresses.*/
    E1000_REG_LSET(E1000_TDBAL, tx_desc_array_paddr);
    E1000_REG_LSET(E1000_TDBAH, 0);
    
    /*Set the Transmit Descriptor Length (TDLEN) register to the size (in bytes)
    of the descriptor ring. This register must be 128-byte aligned.*/
    assert(IS_ALIGNED_TO(TX_DESC_ARRAY_SIZE, 128));
    E1000_REG_LSET(E1000_TDLEN, TX_DESC_ARRAY_SIZE);
    
    /*The Transmit Descriptor Head and Tail (TDH/TDT) registers are initialized
    (by hardware) to 0b after a power-on or a software initiated Ethernet controller
    reset. Software should write 0b to both these registers to ensure this.*/
    E1000_REG_LSET(E1000_TDH, 0);
    E1000_REG_LSET(E1000_TDT, 0);
    
    /*Initialize the Transmit Control Register (TCTL) for desired operation to include the following:*/
    uint32_t tctl_val = 0;

    /*Set the Enable (TCTL.EN) bit to 1b for normal operation.*/
    tctl_val |= E1000_TCTL_EN;
    
    /*Set the Pad Short Packets (TCTL.PSP) bit to 1b.*/
    tctl_val |= E1000_TCTL_PSP;
    
    /*Configure the Collision Threshold (TCTL.CT) to the desired value. Ethernet standard is 10h.
    This setting only has meaning in half duplex mode.*/
    tctl_val &= ~E1000_TCTL_CT;
    tctl_val |= (((0x10) << E1000_TCTL_CT_OFFSET) & E1000_TCTL_CT);
    
    /*Configure the Collision Distance (TCTL.COLD) to its expected value. For full duplex
    operation, this value should be set to 40h. For gigabit half duplex, this value should be set to
    200h. For 10/100 half duplex, this value should be set to 40h.*/
    tctl_val &= ~E1000_TCTL_COLD;
    tctl_val |= (((0x40) << E1000_TCTL_COLD_OFFSET) & E1000_TCTL_COLD);

    E1000_REG_LSET(E1000_TCTL, tctl_val);

    /*Program the Transmit IPG (TIPG) register with the following decimal
    values to get the minimum legal Inter Packet Gap:*/
    E1000_REG_LSET(E1000_TIPG, (6<<20)|(4<<10)|10);  // TODO: What to put here???



    /* Receive Initialization: */
    /*Allocate a region of memory for the receive descriptor list.
    Software should insure this memory is aligned on a paragraph (16-byte) boundary. */
    rx_desc_array_paddr = RX_DESC_ARRAY_PADDR(network_dev_buffer_paddr);
    assert(IS_ALIGNED_TO(rx_desc_array_paddr, 16));
    rx_desc_array = KADDR(rx_desc_array_paddr);
    static_assert(sizeof(struct rx_desc) == 16);

    /*Allocate transmit buffers & initialize descriptors*/
    memset((void*)rx_desc_array, 0, RX_DESC_ARRAY_SIZE);
    for(rx_desc = rx_desc_array;
        (uint32_t)rx_desc < (uint32_t)(rx_desc_array + RX_DESC_ARRAY_ENTRIES);
        ++rx_desc) {
        #ifdef E1000_ZERO_COPY
        // note: we alloc a page without mapping it (pp_ref == 0).
        // It will be mapped to the receiver when it is ready.
        // After a packet is received & transferred to user, a new page will 
        // be alloc'ed and the matching descriptor will be pointing to it.
        struct PageInfo * pg = page_alloc(0);
        if (!pg) {
            panic("Receive Initialization: rx buffer alloc failed!\n");
        }
        rx_desc->addr = page2pa(pg) + offsetof(struct jif_pkt, jp_data);
        #else
        // Allocate rx buffer and assign to descriptor.
        rx_desc->addr = (uint64_t)RX_BUFFER_IDX2PADDR(network_dev_buffer_paddr, (rx_desc - rx_desc_array));
        #endif /* E1000_ZERO_COPY */
    }

    // We updated the descriptors in memory. We should use write memory barrier,
    // before telling the card it can use this descriptor.
    mb();

    /*Program the Receive Address Register(s) (RAL/RAH) with the desired
    Ethernet addresses.*/
    if ((r = e1000_get_mac_from_eeprom(&e1000_mac)) < 0) {
        e1000_mac = e1000_default_mac;
    }
    E1000_REG_LSET(E1000_RAL0, __htonl(e1000_mac.low));
    E1000_REG_LSET(E1000_RAH0, ((uint32_t)__htons(e1000_mac.high)) | E1000_RAH_AV);
    for (i = 1; i < E1000_RA_REGISTERS; ++i) {
        // TODO: is that the right way to do it? (proabbly not..)
        E1000_REG_LSET(E1000_RAH_K(i), 0x0);
        E1000_REG_LSET(E1000_RAL_K(i), 0x0 & ~E1000_RAH_AV);
    }

    /*Initialize the MTA (Multicast Table Array) to 0b. Per software, entries
    can be added to this table as desired.*/
    for (i = 0; i < E1000_MTA_REGISTERS; ++i) {
        E1000_REG_LSET(E1000_MTA_K(i), 0);
    }

    /*Program the Interrupt Mask Set/Read (IMS) register to enable any interrupt
    the software driver wants to be notified of when the event occurs.
    Suggested bits include RXT, RXO, RXDMT, RXSEQ, and LSC. There is no immediate
    reason to enable the transmit interrupts.*/
    E1000_REG_LSET(E1000_IMS, E1000_IMS_RXT0 | E1000_IMS_TXDW); // TODO: implement interrupts mask!

    /*If software uses the Receive Descriptor Minimum Threshold Interrupt, the
    Receive Delay Timer (RDTR) register should be initialized with the desired delay time.*/
    // NOT USED

    /*Program the Receive Descriptor Base Address (RDBAL/RDBAH) register(s) with
    the address of the region. RDBAL is used for 32-bit addresses and both RDBAL
    and RDBAH are used for 64-bit addresses.*/
    E1000_REG_LSET(E1000_RDBAL, rx_desc_array_paddr);
    E1000_REG_LSET(E1000_RDBAH, 0);

    /*Set the Receive Descriptor Length (RDLEN) register to the size (in bytes) of
    the descriptor ring. This register must be 128-byte aligned.*/
    assert(IS_ALIGNED_TO(RX_DESC_ARRAY_SIZE, 128));
    E1000_REG_LSET(E1000_RDLEN, RX_DESC_ARRAY_SIZE);

    /*The Receive Descriptor Head and Tail registers are initialized (by hardware)
    to 0b after a power-on or a software-initiated Ethernet controller reset.
    Receive buffers of appropriate size should be allocated and pointers to these
    buffers should be stored in the receive descriptor ring. Software initializes
    the Receive Descriptor Head (RDH) register and Receive Descriptor Tail (RDT)
    with the appropriate head and tail addresses. Head should point to the first
    valid receive descriptor in the descriptor ring and tail should point to one
    descriptor beyond the last valid descriptor in the descriptor ring.*/
    E1000_REG_LSET(E1000_RDH, 0);
    E1000_REG_LSET(E1000_RDT, RX_DESC_ARRAY_ENTRIES-1); // TODO: verify!

    /*Program the Receive Control (RCTL) register with appropriate values for
    desired operation to include the following:*/
    uint32_t rctl_val = 0;

    /*Set the receiver Enable (RCTL.EN) bit to 1b for normal operation. However, it
    is best to leave the Ethernet controller receive logic disabled (RCTL.EN = 0b)
    until after the receive descriptor ring has been initialized and software is
    ready to process received packets.*/
    rctl_val |= E1000_RCTL_EN;

    /*Set the Long Packet Enable (RCTL.LPE) bit to 1b when processing packets
    greater than the standard Ethernet packet size. For example, this bit would be
    set to 1b when processing Jumbo Frames.*/
    rctl_val &= ~E1000_RCTL_LPE; // do NOT enable long packets!


    /*Loopback Mode (RCTL.LBM) should be set to 00b for normal operation.*/
    rctl_val |= E1000_RCTL_LBM_NO;

    /*Configure the Receive Descriptor Minimum Threshold Size (RCTL.RDMTS) bits to
    the desired value.*/
    rctl_val |= E1000_RCTL_RDMTS_HALF;

    /*Configure the Multicast Offset (RCTL.MO) bits to the desired value.*/
    // NOT USED

    /*Set the Broadcast Accept Mode (RCTL.BAM) bit to 1b allowing the hardware to 
    accept broadcast packets.*/
    rctl_val &= ~E1000_RCTL_BAM;

    /*Configure the Receive Buffer Size (RCTL.BSIZE) bits to reflect the size of the
    receive buffers software provides to hardware. Also configure the Buffer Extension
    Size (RCTL.BSEX) bits if receive buffer needs to be larger than 2048 bytes.*/
    rctl_val |= RX_BUFFER_SZ_CTRL_BIT_MASK;
    rctl_val &= ~E1000_RCTL_BSEX;

    /*Set the Strip Ethernet CRC (RCTL.SECRC) bit if the desire is for hardware to
    strip the CRC prior to DMA-ing the receive packet to host memory.*/
    rctl_val |= E1000_RCTL_SECRC;

    /*For the 82541xx and 82547GI/EI, program the Interrupt Mask Set/Read (IMS) register to
    enable any interrupt the driver wants to be notified of when the even occurs. Suggested bits
    include RXT, RXO, RXDMT, RXSEQ, and LSC. There is no immediate reason to enable the
    transmit interrupts. Plan to optimize interrupts later, including programming the interrupt
    moderation registers TIDV, TADV, RADV and IDTR.*/
    // NOT USED

    E1000_REG_LSET(E1000_RCTL, rctl_val);

    /*For the 82541xx and 82547GI/EI, if software uses the Receive Descriptor Minimum
    Threshold Interrupt, the Receive Delay Timer (RDTR) register should be initialized with the
    desired delay time.*/
    // NOT USED


    // init irq handler:
    uint8_t nic_irq = pcif->irq_line ? pcif->irq_line : IRQ_NIC;
    irq_ops[nic_irq].arg = NULL;
    irq_ops[nic_irq].handler = e1000_irq_handler;

    // Enable E1000 irq:
    cprintf("   Setup E1000 interrupt via 8259A\n");
    irq_setmask_8259A(irq_mask_8259A & ~(1 << nic_irq));
    cprintf("   unmasked E1000 interrupt\n");

    return 1;
}

static struct {
    physaddr_t addr;
    int zero_copy;
} tx_local_state[TX_DESC_ARRAY_ENTRIES];

int e1000_transmit_packet(void *send_buffer, size_t send_buffer_len) {
    uint32_t tail_offset = E1000_REG_LGET(E1000_TDT);
    uint32_t next_offset = (tail_offset + 1) % TX_DESC_ARRAY_ENTRIES;
    volatile struct tx_desc *tail_desc = (volatile struct tx_desc *)(tx_desc_array) + tail_offset;

    /* zero copy is possible when the buffer is interally contained in a single (phisical) page
    and this page is shared with other environments except for the sender. */
    int perform_zero_copy = (DIV_ROUND_UP(PGOFF(send_buffer) + send_buffer_len, PGSIZE) == 1
        && page_pp_ref(curr_pgdir, send_buffer) == 1);

    /* check that buffer len is not exeeding max packet size */
    if (send_buffer_len > TX_BUFFER_SIZE) {
        return -E_INVAL;
    }

    /* check whether the descriptor is free to use */
    if (!(tail_desc->status & E1000_TXD_STAT_DD)) {
        /* tx descriptors ring is full */
        return -E_FULL_QUEUE;
    }

#ifdef E100_ZERO_COPY
    /* if we already transmitted this page in zero-copy mechanism */
    if (tx_local_state[tail_offset].zero_copy) {
        assert(tx_local_state[tail_offset].addr == tail_desc->addr);
        struct PageInfo* pg = pa2page(tx_local_state[tail_offset].addr);
        page_decref(pg);
    }
#endif

    uint8_t cmd = 0;

    // Set the RS flag in order to tell the card to set the DD flag
    // in status field after a descriptor is freed.
    cmd |= E1000_TXD_CMD_RS;

    // Use legacy descriptor.
    cmd &= ~E1000_TXD_CMD_DEXT;

    // This descriptor is the last (and only) descriptor of this packet.
    // TODO: Do we want to allow sending one packet via multiple descriptors? why? when?
    cmd |= E1000_TXD_CMD_EOP;

    tail_desc->cmd = cmd;

    // clear the DD bit in the status field.
    tail_desc->status &= ~E1000_TXD_STAT_DD;

    // copy the data to send into the tx buffer of the descriptor.
    tx_local_state[tail_offset].zero_copy = perform_zero_copy;
    if (perform_zero_copy) {
        physaddr_t addr = va2pa(send_buffer);
        tail_desc->addr = addr;
        tx_local_state[tail_offset].addr = addr;
        assert(tail_desc->addr != ~0);
    } else {
        physaddr_t addr = (uint64_t)TX_BUFFER_IDX2PADDR(network_dev_buffer_paddr, (tail_desc - tx_desc_array));
        tail_desc->addr = addr;
        tx_local_state[tail_offset].addr = addr;
        memcpy(KADDR(addr), send_buffer, send_buffer_len);
    }
    tail_desc->length = send_buffer_len;

    // We updated a descriptor in memory. We should use write memory barrier,
    // before telling the card it can use this descriptor.
    mb();

    // tell the card it has one more descriptor to transmit.
    E1000_REG_LSET(E1000_TDT, next_offset);

    return 0;
}

// PTE_COW marks copy-on-write page table entries.
// It is one of the bits explicitly allocated to user processes (PTE_AVAIL).
#define PTE_COW     0x800

int sys_net_transmit_packet(void *send_buffer, size_t send_buffer_len, int block) {
    user_mem_assert(curenv, send_buffer, send_buffer_len, (PTE_P|PTE_U));

    pte_t *pte;
    struct PageInfo *pg;

    /* zero copy is possible when the buffer is interally contained in a single (phisical) page
    and this page is shared with other environments except for the sender. */
#ifdef E100_ZERO_COPY
    int perform_zero_copy = (DIV_ROUND_UP(PGOFF(send_buffer) + send_buffer_len, PGSIZE) == 1
        && page_pp_ref(curr_pgdir, send_buffer) == 1);
    if (perform_zero_copy) {
        // Increase the pp_ref count of the received page (instead of mapping it
        // inside the kernel). Also mark this page as COW for the user.
        pg = page_lookup(curr_pgdir, send_buffer, &pte);
        if (!pg || !pte) return -E_INVAL;
        if (PTE_FLAGS(*pte) & PTE_W) {
            *pte |= PTE_COW;
            *pte &= ~PTE_W;
        }
        pg->pp_ref += 1;
    }
#endif

    int r = e1000_transmit_packet(send_buffer, send_buffer_len);

    if (!block || r != -E_FULL_QUEUE) {
        return r;
    }

    // go to sleep. interrupt will wake you up when new packet transmited.
    curenv->env_net_waiting = ENV_NET_TX_WAITING;
    curenv->env_net_buffer_va = send_buffer;
    curenv->env_net_buffer_len = send_buffer_len;
    curenv->env_tf.tf_regs.reg_eax = 0;  /* simulate return 0 from this syscall */
    curenv->env_status = ENV_NOT_RUNNABLE;
    sched_yield(); /* does not return */
    assert(0);
}


#ifdef E1000_ZERO_COPY
int e1000_receive_packet(physaddr_t* received_packet_paddr)
#else
int e1000_receive_packet(void *buf_data, size_t buf_len)
#endif /* E1000_ZERO_COPY */
{
    uint32_t tail_offset = E1000_REG_LGET(E1000_RDT);
    uint32_t next_offset = (tail_offset + 1) % RX_DESC_ARRAY_ENTRIES;
    volatile struct rx_desc *next_desc = (volatile struct rx_desc *)(rx_desc_array) + next_offset;
    uint32_t received_len;

    /* check whether the descriptor is free to use */
    if (!(next_desc->status & E1000_RXD_STAT_DD)) {
        /* rx descriptors ring is empty */
        return -E_EMPTY_QUEUE;
    }

    // each packet is received in a single descriptor.
    assert(next_desc->status & E1000_RXD_STAT_EOP);

    received_len = next_desc->length;

#ifdef E1000_ZERO_COPY
    // give the receiver the received packet, and alloc new page for next time
    // the card uses this rx descriptor.
    struct PageInfo * pg = page_alloc(0);
    if (!pg) {
        return -E_NO_MEM;
    }
    *received_packet_paddr = next_desc->addr;
    next_desc->addr = page2pa(pg) + offsetof(struct jif_pkt, jp_data);
#else
    /* check that buffer len is enough to contain the whole received packet */
    if (buf_len < received_len) {
        return -E_FAULT;
    }

    // copy the received data into the pkt data buffer.
    memcpy(buf_data, KADDR(next_desc->addr), received_len);
#endif /* E1000_ZERO_COPY */

    // clear the DD bit in the status field.
    next_desc->status &= ~E1000_RXD_STAT_DD;

    // We updated a descriptor in memory. We should use write memory barrier,
    // before telling the card it can use this descriptor.
    mb();

    // tell the card it has one more descriptor to receive into.
    E1000_REG_LSET(E1000_RDT, next_offset);
    
    return received_len;
}

#define FOREACH_ENV_WAITING_ON(env, waiting_on) \
    for (env = &envs[0]; (uint32_t)(env) < (uint32_t)&envs[NENV]; env++) \
        if ((env)->env_net_waiting == (waiting_on))

static struct Env * find_net_env_waiting_on(enum EnvNetWaiting waiting_on) {
    int i;
    for (i = 0; i < NENV; i++)
        if (envs[i].env_net_waiting == waiting_on)
            return &envs[i];
    return NULL;
}
static inline struct Env * find_net_env_waiting_tx() {
    return find_net_env_waiting_on(ENV_NET_TX_WAITING);
}
static inline struct Env * find_net_env_waiting_rx() {
    return find_net_env_waiting_on(ENV_NET_RX_WAITING);
}

static int e1000_try_extract_received_packet(pde_t* pgdir, void* dstva) {
    int r;
    struct PageInfo *pg;

#ifdef E1000_ZERO_COPY
    physaddr_t received_packet_paddr = 0;
    r = e1000_receive_packet(&received_packet_paddr);
    if (r < 0) {
        return r;
    }
    pg = pa2page(received_packet_paddr);
    struct jif_pkt *pkt = (struct jif_pkt *)page2kva(pg);
#else
    pg = page_alloc(0);  // without incrementinc ppref
    if (!pg) {
        panic("e1000_try_extract_received_packet(): out of memory!\n");
        return r;
    }
    struct jif_pkt *pkt = (struct jif_pkt *)page2kva(pg);
    size_t buf_len = (PGSIZE - offsetof(struct jif_pkt, jp_data));
    r = e1000_receive_packet((void*)pkt->jp_data, buf_len);
    if (r < 0) {
        page_free(pg);
        assert(r == -E_EMPTY_QUEUE);
        return r;
    }
#endif /* E1000_ZERO_COPY */

    pkt->jp_len = r;

    if ((r = page_insert(pgdir, pg, dstva, PTE_P|PTE_U)) < 0) {
        assert(r == -E_NO_MEM);
        // todo: free the alloc'ed page `pg`..
        panic("e1000_try_extract_received_packet(): out of memory!\n");
        return r;
    }

    // NOTE: we don't have to unmmap the page here, because now only the user have it (pg->ppref == 1).

    return 0;
}

void e1000_irq_handler(struct Trapframe *tf, void* arg) {
    struct jif_pkt *pkt;
    int r;

    // read the interrupt cause
    uint32_t icr = E1000_REG_LGET(E1000_ICR);

    // a new packet received
    if (icr & E1000_ICR_RXT0) {
        struct Env *input_env = find_net_env_waiting_rx();
        if (input_env == NULL) {
            goto exit_irq;
        }

        if((r = e1000_try_extract_received_packet(input_env->env_pgdir, input_env->env_net_buffer_va)) < 0) {
            goto exit_irq;
        }

        input_env->env_net_waiting = ENV_NET_NOT_WAITING;
        input_env->env_net_buffer_va = NULL;
        input_env->env_net_buffer_len = 0;
        input_env->env_status = ENV_RUNNABLE;
    } else if (icr & E1000_ICR_TXDW) {
        struct Env *output_env;
        FOREACH_ENV_WAITING_ON(output_env, ENV_NET_TX_WAITING) {
            struct PageInfo *pg = page_lookup(output_env->env_pgdir, output_env->env_net_buffer_va, NULL);
            assert(pg != NULL);

            if((r = e1000_transmit_packet(page2kva(pg), output_env->env_net_buffer_len)) < 0) {
                //assert(r == -E_FULL_QUEUE);
                goto exit_irq;
            }

            output_env->env_net_waiting = ENV_NET_NOT_WAITING;
            output_env->env_net_buffer_va = NULL;
            output_env->env_net_buffer_len = 0;
            output_env->env_status = ENV_RUNNABLE;
        }
    }

exit_irq:
    irq_eoi();
    lapic_eoi();
    // TODO: ...
}


int sys_net_receive_packet(void* dstva) {
    int r;

    if ((uintptr_t)dstva >= UTOP || !IS_ALIGNED_TO_PAGE(dstva)) {
        return -E_INVAL;
    }

    // check if now we have something to give him!
    // remind that now interupts are off, so we dont race with them (no TOCTTOU).
    if((r = e1000_try_extract_received_packet(curenv->env_pgdir, dstva)) == 0) {
        return 0;
    }

    // go to sleep. interrupt will wake you up when new packet arrives.
    curenv->env_net_waiting = ENV_NET_RX_WAITING;
    curenv->env_net_buffer_va = dstva;
    curenv->env_tf.tf_regs.reg_eax = 0;  /* simulate return 0 from this syscall */
    curenv->env_status = ENV_NOT_RUNNABLE;
    sched_yield(); /* does not return */
    assert(0);
}

static int e1000_read_single_eeprom_word(uint8_t eeprom_addr, uint16_t *data) {
    // to read a word from EEPROM, we need to do the following (ref - e1000 manual section 5.3.1)
    uint32_t eeprom_val = 0;

    // 1. write the address to read from to EERD.ADDR
    // 2. write a 1b to EERD.START
    eeprom_val |= (((uint32_t)eeprom_addr) << E1000_EEPROM_RW_ADDR_SHIFT); // write the addr to EERD.ADDR
    eeprom_val |= E1000_EEPROM_RW_START_MASK; // write 1b to EERD.START

    E1000_REG_LSET(E1000_EERD, eeprom_val);

    // 3. poll EERD.DONE until set to 1b
    int32_t x = 1<<20; // to avoid endless loop.
    while (!(E1000_REG_LGET(E1000_EERD) & E1000_EEPROM_RW_DONE_MASK) && --x)
        /* empty loop */;
    if (x<=0) {
        return -E_IO; // polled for too long, returning error.
    }

    // 4. return the deisred data from EERD.DATA
    uint32_t eeprom_r = E1000_REG_LGET(E1000_EERD);
    eeprom_r &= E1000_EEPROM_RW_DATA_MASK;
    *data = (uint16_t)(eeprom_r >> E1000_EEPROM_RW_DATA_SHIFT);
    return 0;
}

static int
e1000_get_mac_from_eeprom(struct mac_addr *mac) {
    uint16_t data;
    int r;
    int i;
    for (i = 0; i<3; i++) {
        if((r = e1000_read_single_eeprom_word(EEPROM_ETHERNET_ADDR_K(i), &data)) < 0) {
            return r;
        }
        mac->word[2-i] = __htons(data);
    }
    return 0;
}

int sys_net_get_mac_addr(uint64_t *mac) {
    struct mac_addr mac_addr;
    int r;

    user_mem_assert(curenv, mac, sizeof(*mac), (PTE_P|PTE_U|PTE_W));

    if ((r = e1000_get_mac_from_eeprom(&mac_addr)) < 0) return r;

    *mac = mac_addr.full;
    return 0;
}

