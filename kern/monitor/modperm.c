#include <inc/string.h>
#include <inc/mmu.h>
#include <inc/memlayout.h>
#include <inc/stdio.h>
#include <inc/utils.h>
#include <kern/monitor.h>
#include <kern/pmap.h>

static void
pte_change_perm_bit(pte_t *pte, int perm, int is_set)
{
	*pte = (is_set)
	        ? (*pte | perm)
	        : (*pte & ~perm);
}

int
mon_modperm(int argc, char **argv, struct Trapframe *tf)
{
	/**
	TODO :: decide about these:
	1. should addr be a full 32-bit address, or a 20-bit VPN? [currently accepting a VPN]
	2. what to do in case address is not mapped? and if the PT for that addr does not exist? [currently printing an error and doig nothing]
	3. how about PDEs? technically can be accessed through the "self-loop" like any other address...
	4. how about ranges? yaani allowing to change perms for all the PTEs pointing to pages from 0x12345 to 0x19876in one go.
	5. some ALL for denoting all permission bits? (setting or clearing them all sound dangerous....)
	**/
	if (argc != 4) {
		mon_invalid_usage(MON_MODPERM);
		return 1;
	}

	if (!streq(argv[2],"clr") && !streq(argv[2],"set")){
		cprintf(COLOR("ERROR: invliad action '%s'. must be 'set' or 'clr'\n", FG_RED), argv[2]);
    	return 1;
	}
	int is_set = streq(argv[2],"set");

    uint32_t addr = xtoi(argv[1]) << PGSHIFT;
    pte_t *pte = pgdir_walk(curr_pgdir, (void *)addr, 0); // callign with 0, to prevent any mapping modifications
    if(!pte || !PTE_IS_MAPPED(*pte)) {
    	cprintf(COLOR("ERROR: the page at addr 0x%08x is not mapped\n", FG_RED), addr);
    	return 1;
    }

    char old_flags[NFLAGS+1];
    char new_flags[NFLAGS+1];

    sprint_pte_flags(PTE_FLAGS(*pte), old_flags);
    cprintf("OLD flags: %s\n", old_flags);

    char *c;
    for (c = argv[3]; *c; ++c)
    {
        #define FLG(f, symbol, value, n) if (streqn(c,#symbol,1)) pte_change_perm_bit(pte, value, is_set);
        PTE_FLAGS_LIST
        #undef FLG
    }

    sprint_pte_flags(PTE_FLAGS(*pte), new_flags);
    cprintf("NEW flags: %s\n", new_flags);

	return 0;
}
