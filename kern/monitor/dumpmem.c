#include <inc/types.h>
#include <inc/string.h>
#include <inc/mmu.h>
#include <inc/memlayout.h>
#include <inc/stdio.h>
#include <inc/utils.h>
#include <inc/kdbg_utils.h> /* for VA_IS_MAPPED() macro, and MAX_AVAILABLE_PHYSICAL_ADDR define */
#include <kern/monitor.h>
#include <kern/pmap.h>

#define DUMPMEM_WORDS_PER_LINE (4)

int
mon_dumpmem(int argc, char **argv, struct Trapframe *tf)
{
	/**
	TODO :: decide about these:
	1. add a "size" or "num words" param, so you can print "10 words starting at 0x12345678" instead of giving <start> and <end>
	2. some "grep" mechanism? for highlighting specific contents?
	   [currently "greying-out" 0x00000000, so it's easy to tell if a word is zero or not]
	3. is rounding down to a word boundary a good choice? gdb doesn't do it, and for a good
	   reason (if I have a ptr to a word-sized variable sitting at a non-word-aligned address).
	**/
	uintptr_t start_addr = 0, end_addr = 0;
	uint32_t *effective_start_addr, *effective_end_addr;

	if (argc < 3 || argc > 4) {
		mon_invalid_usage(MON_DUMPMEM);
		return 1;
	}

	assert(argc >= 3);
	start_addr = xtoi(argv[2]);
	
	if (argc >= 4) {
		assert(argc == 4);
		end_addr = xtoi(argv[3]);
	} else {
		end_addr = start_addr;
	}

	if (end_addr < start_addr) {
		cprintf(COLOR("ERROR: given end address is bigger than the start address.\n", FG_RED));
	  	return 1;
	}

	// rounding-down to a word boundary, because we print contents word-by-word.
	// unless there is only one word to print.
	if (end_addr != start_addr) {
		start_addr = ROUNDDOWN(start_addr, sizeof(uint32_t));
		end_addr = ROUNDUP(end_addr, sizeof(uint32_t));
	}

	if (!streq(argv[1],"pa") && !streq(argv[1],"va")){
		cprintf(COLOR("ERROR: invliad action '%s'. must be 'pa' (Physical Address) or 'va' (Virtual Address)\n", FG_RED), argv[1]);
    	return 1;
	}
	int is_phys = streq(argv[1],"pa");

	assert(start_addr <= end_addr); /* we already tested it before the rounding. */

	if(is_phys){
		if (end_addr > MAX_AVAILABLE_PHYSICAL_ADDR){
			cprintf(COLOR("ERROR: 'pa' chosen with invalid physical address range: [0x%08x, 0x%08x] physical addresses must be at most 0x%08x\n", FG_RED), start_addr, end_addr, MAX_AVAILABLE_PHYSICAL_ADDR);
    		return 1;
		}
		
		effective_start_addr = KADDR(start_addr);
		effective_end_addr = KADDR(end_addr);
	} else {
		effective_start_addr = (uint32_t*)start_addr;
		effective_end_addr = (uint32_t*)end_addr;
	}

	uint32_t *curr_addr;
	int i;

	// print header to help figure out offsets within each output line
	// only if we print more than one word
	if (effective_start_addr != effective_end_addr) {
		cprintf("%15s"," "); // equivalent of row descriptor, for alignment
		for (i = 0; i < DUMPMEM_WORDS_PER_LINE; ++i)
		{
			uintptr_t curr_off = ((uintptr_t)effective_start_addr)%(DUMPMEM_WORDS_PER_LINE*4) + i*4;
			cprintf("%10x    ", curr_off);
		}
		cprintf("\n");
	}
	for (curr_addr = effective_start_addr, i=0; curr_addr <= effective_end_addr; curr_addr += 1, ++i)
	{
		// Verify that the current address is indeed mapped.
		// It is sufficient to verify it one time per a page and once in the beggining of the scan.
		if ((i == 0 || IS_ALIGNED_TO_PAGE(curr_addr)) && !VA_IS_MAPPED_AND_EXISTS_MEM(curr_addr)){
			cprintf(COLOR("ERROR: the page at addr 0x%08x is not mapped.\n", FG_RED), curr_addr);
			return 1;
		}
		if (i % DUMPMEM_WORDS_PER_LINE == 0){
			cprintf("0x%08x:    ", curr_addr);
		}

		// print the current word. color magenta if it is zeros.
		uint32_t content = *((uint32_t *)curr_addr);
		cprintf(content ? "0x%08x    " : COLOR("0x%08x    ", FG_LIGHT_MAGENTA), content);

		if (i % DUMPMEM_WORDS_PER_LINE == (DUMPMEM_WORDS_PER_LINE-1)) {
			cprintf("\n");
		}
	}
	if (i % DUMPMEM_WORDS_PER_LINE != 0) {
		cprintf("\n");
	}
	return 0;
}