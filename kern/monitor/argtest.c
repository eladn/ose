#include <inc/string.h>
#include <inc/utils.h>
#include <inc/stdio.h>
#include <kern/monitor.h>
#include <utils/args_parser.h>
#include <inc/assert.h>


CLI_COMMAND(argtest, "Test the arg-parser.", 
	CLI_REQ_GRPS(
		CLI_REQ_GRP_ONE_AT_MOST(address),
		CLI_REQ_GRP_SINGLE(base_or_type),
		CLI_REQ_GRP_ONE_AT_MOST(number)
	),
	CLI_POS_PARAMS(
		CLI_PAR(CLI_PAR_LNAMES(va, vaddr), CLI_PAR_SNAMES('v'), 
				CLI_PAR_NARGS_AT_LEAST_ONE(), CLI_PAR_ARG_ADDR(), 
				CLI_PAR_REQ_GRP(address),
				CLI_PAR_DESCRIPTION("Virtual addresses to show.")),
		CLI_PAR_SEPARATOR(), // required after a positional parameter with variable number of args
		CLI_PAR(CLI_PAR_LNAMES(pa, physaddr), CLI_PAR_SNAMES('p'), 
				CLI_PAR_SINGLE_ARG(), CLI_PAR_ARG_ADDR(), 
				CLI_PAR_INT_RANGE(0x50000, 0xCE5FFFFF), 
				CLI_PAR_REQ_GRP(address),
				CLI_PAR_DESCRIPTION("Physical addresses to show.")),
		CLI_PAR(CLI_PAR_LNAMES(sh, show, details), CLI_PAR_SNAMES('s', 'd'), 
				CLI_PAR_SINGLE_ARG(), CLI_PAR_ARG_BOOL(), 
				CLI_PAR_DESCRIPTION("Whether to show details.")),
		/* `--` can be set explicity in order to allow skiping into this param without setting prev ones */
		CLI_PAR_SEPARATOR(),
		CLI_PAR(CLI_PAR_LNAMES(color,text-color), CLI_PAR_SNAMES('c'), 
				CLI_PAR_ARG_TEXT(), CLI_PAR_SINGLE_ARG(),
				CLI_PAR_CHOISES_STR("blue","red","green","yellow"),
				CLI_PAR_DESCRIPTION("Color for the printed lines."))
	), 
	CLI_NAMED_PARAMS(
		CLI_PAR(CLI_PAR_LNAMES(base), CLI_PAR_SNAMES('b'), 
				CLI_PAR_ARG_INT(), CLI_PAR_SINGLE_ARG(), 
				CLI_PAR_CHOISES_INT(5, 7, 14, 88), 
				CLI_PAR_REQ_GRP(base_or_type)),
		CLI_PAR(CLI_PAR_LNAMES(p_int), CLI_PAR_SNAMES('i'), 
				CLI_PAR_ARG_INT(), CLI_PAR_NARGS_ANY(), CLI_PAR_INT_POSITIVE(),
				CLI_PAR_REQ_GRP(number)),
		CLI_PAR(CLI_PAR_LNAMES(nn_int),
				CLI_PAR_ARG_INT(), CLI_PAR_NARGS(3,5), CLI_PAR_INT_NON_NEGATIVE(),
				CLI_PAR_REQ_GRP(number)),
		CLI_PAR(CLI_PAR_LNAMES(p_float), CLI_PAR_SNAMES('f'), 
				CLI_PAR_ARG_FLOAT(), CLI_PAR_NARGS_MIN(2), CLI_PAR_FLOAT_POSITIVE(),
				CLI_PAR_REQ_GRP(number)),
		CLI_PAR(CLI_PAR_LNAMES(nn_float),
				CLI_PAR_ARG_FLOAT(), CLI_PAR_NARGS_MAX(7), CLI_PAR_FLOAT_NON_NEGATIVE(),
				CLI_PAR_REQ_GRP(number)),
		CLI_PAR(CLI_PAR_LNAMES(type), CLI_PAR_SNAMES('t'), 
				CLI_PAR_ARG_TEXT(), CLI_PAR_NARGS_ANY(), 
				CLI_PAR_CHOISES_STR("aa","bb","ha"), 
				CLI_PAR_REQ_GRP(base_or_type)),
		CLI_PAR(CLI_PAR_LNAMES(usage), CLI_PAR_SNAMES('u'), 
				CLI_PAR_DESCRIPTION("Show full usage for this command."))
	)
);


int mon_argtest(int argc, char **argv, struct Trapframe *tf) {
	struct cmd_args_argtest args;
	int ret;
	int par_idx;

	ret = cli_parse(CLI_CMD_CONFIG(argtest), (struct cli_param_arguments*)(&args), argc, argv, CLI_VERBOSE_ERRORS);
	if (ret < 0) {
		return 0;
	}

	size_t nr_params = CLI_CMD_CONFIG(argtest)->nr_pos_params + CLI_CMD_CONFIG(argtest)->nr_named_params;
	assert(nr_params * sizeof(struct cli_param_arguments) == sizeof(struct cmd_args_argtest));

	struct cli_param *cur_param;
	struct cli_param_arguments *param_args;
	int par_arg_idx;
	FOREACH_CLI_PARAM(CLI_CMD_CONFIG(argtest), cur_param) {
		if (cur_param->is_separator) continue;
		param_args = CLI_PAR_ARGS_FIELD(CLI_CMD_CONFIG(argtest), &args, cur_param);
		cprintf("param `%s`  %s   ", 
			cur_param->long_names[0],
			param_args->is_flag_set ? "SET" : "unset");
		
		if (param_args->values[0].is_set) {
			cprintf("values: ");
		}
		for(par_arg_idx = 0; par_arg_idx < CLI_PARAM_MAX_NR_ARGS && param_args->values[par_arg_idx].is_set; ++par_arg_idx) {
			switch(cur_param->args_val_type) {
				case CLI_ARG_TYPE_INT:
					cprintf("[%d] ", param_args->values[par_arg_idx].value.as_int32_t);
					break;
				case CLI_ARG_TYPE_INT_RANGE:
					cprintf(cur_param->numeric_attr.default_numeric_base == 16 ? "[0x%x:0x%x] " : "[%d-%d] ", 
						param_args->values[par_arg_idx].value.as_range.start.as_int32_t,
						param_args->values[par_arg_idx].value.as_range.end.as_int32_t);
					break;
				case CLI_ARG_TYPE_BOOL:
					cprintf("[%s] ", (param_args->values[par_arg_idx].value.as_int ? "True" : "False"));
					break;
				case CLI_ARG_TYPE_FLOAT:
					cprintf("[%f] ", param_args->values[par_arg_idx].value.as_float);
					break;
				case CLI_ARG_TYPE_FLOAT_RANGE:
					cprintf("[%f-%f] ", 
						param_args->values[par_arg_idx].value.as_range.start.as_float,
						param_args->values[par_arg_idx].value.as_range.end.as_float);
					break;
				case CLI_ARG_TYPE_TEXT:
					cprintf("[%s] ", param_args->values[par_arg_idx].value.as_strptr);
					break;
				default:
					assert(0);
					break;
			}
		}
		cprintf("\n");
	}

	if (args.usage.is_flag_set) {
		cli_print_usage(CLI_CMD_CONFIG(argtest));
	}

	return 0;
}
