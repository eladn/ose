#include <inc/string.h>
#include <inc/mmu.h>
#include <inc/utils.h>
#include <inc/memlayout.h>
#include <inc/stdio.h>
#include <kern/monitor.h>
#include <kern/pmap.h>

static void print_pte_range(uintptr_t start_vpn, uint32_t start_pte_idx, pte_t *start_pte, pte_t *end_pte);
static int is_pte_range_pys_continuous(pte_t* start_pte, pte_t* end_pte);

/* used to store the input params and pass them as an argument to functions */
struct addr_range {
     int is_physical_addr;
     uintptr_t start_addr, end_addr;
     int is_start_addr, is_end_addr;
};

/* does not distinguish between physical/virtual addresses, use given addr as it is. */
#define IS_ADDR_RANGE_INTERSECT_PAGE(addr_range, page_start_addr) \
     (!( \
          (  (addr_range)->is_end_addr && (addr_range)->end_addr < (page_start_addr) ) ||  \
          (  (addr_range)->is_start_addr && (addr_range)->start_addr > ((page_start_addr) + (PGSIZE-1))  )  \
     ))

static inline int is_pte_intersect_addr_range(pte_t *pte, uint32_t pde_idx, uint32_t pte_idx, struct addr_range *addr_range) {
     if (addr_range->is_physical_addr) {
          /* physical addr */
          return PTE_IS_MAPPED(*pte) && IS_ADDR_RANGE_INTERSECT_PAGE(addr_range, PTE_ADDR(*pte));
     }

     /* virtual addr */
     return IS_ADDR_RANGE_INTERSECT_PAGE(addr_range, (VPN(pde_idx, pte_idx) << PTXSHIFT));
}


/* 
     used by monitor.c/runcmd(..), referenced in monitor.h `MON_COMMAND_LIST` macro 
     useage is defined in `SM_USAGE_STR` macro in monitor.h
*/
int
mon_showmap(int argc, char **argv, struct Trapframe *tf)
{
     struct addr_range addr_range = {.is_physical_addr = 0, .start_addr = 0, .end_addr = 0, .is_start_addr = 0, .is_end_addr = 0};

     /* parse & check input according to the usage definition */
	if (argc > (3+1) || argc == (1+1)) {
		mon_invalid_usage(MON_SHOWMAP);
		return 1;
	}
     if (argc >= (1+1)) {
          assert(argc >= (2+1)); // we've just verified above argc!=2
          if (!streq(argv[1], "p") && !streq(argv[1], "v")) {
               mon_invalid_usage(MON_SHOWMAP);
               return 1;
          }
          addr_range.is_physical_addr = streq(argv[1], "p");

          addr_range.is_start_addr = 1;
          addr_range.start_addr = xtoi(argv[2]);
          if (argc >= (3+1)) {
               addr_range.is_end_addr = 1;
               addr_range.end_addr = xtoi(argv[3]);
          } else {
               /* if only start addr is given, we treat it as a single addr */
               addr_range.is_end_addr = 1;
               addr_range.end_addr = addr_range.start_addr;
          }
     }
     if (addr_range.is_start_addr && addr_range.is_end_addr && addr_range.end_addr < addr_range.start_addr) {
          mon_invalid_usage(MON_SHOWMAP);
          return 1;
     }

     /*
     TODO: we might want to use `get_serial_terminal_size()` to know screen width,
           in order to define number of p.addresses per row (currently always `0x10`)
     */

     /*
          We are iterating over the PDEs & the PTEs inside each PDE.
          We print continuous physical mappings ranges grouped in the same line.
          Each mapping line is printed by the last PTE "invlolved" in this mapping range.
          So it is guaranteed that if the `cur_pte` is not mapped or should not be printed together
          with the last mapping-range-line, the last line has already been printed.
     */

     int top_header_printed = 0;
     pde_t *pde;
     pte_t *pg_table, *range_start_pte, *cur_pte;
     int contiguous_physical_addr = 1;

     FOREACH_PDE(curr_pgdir, pde) {
          pg_table = (pte_t *)KADDR(PTE_ADDR(*pde));
          uint32_t pde_idx = pde - curr_pgdir;
          int pde_header_printed = 0;

          range_start_pte = INVALID_PTE_ADDR;
          FOREACH_PTE(pg_table, cur_pte) {
               pte_t *next_pte = cur_pte + 1;

               if (!PTE_IS_VALID_ADDR(range_start_pte)) {
                    range_start_pte = cur_pte;
                    contiguous_physical_addr = 1;
               } else {
                    contiguous_physical_addr &= (PTE_ADDR(*(cur_pte-1)) + PGSIZE == PTE_ADDR(*cur_pte));
               }

               uint32_t cur_pte_idx = cur_pte - pg_table;
               uint32_t range_start_pte_idx = range_start_pte - pg_table;

               /*
                    Skip this PTE is not mapped or not in the chosen range by user input.
                    Note: We can just continue, because it is guaranteed that there isn't 
                          a range waiting to be printed, as explained in the comment above.
               */
               if(!PTE_IS_MAPPED(*cur_pte) || 
                    !is_pte_intersect_addr_range(cur_pte, pde_idx, cur_pte_idx, &addr_range)) {
                    range_start_pte = INVALID_PTE_ADDR;
                    contiguous_physical_addr = 0;
                    continue;
               }

               /*
                    Print the last range until current pte (includes), if one of the following holds:
                         1. Current PTE is the last in the page table.
                         2. This PTE is the last of a contiguous range of mapped PTEs:
                              OR: with the same flags;
                              OR: in indexes K*[0x00-0xFF] and the mapped physical address is not contiguous.
                              (If the mapped physical address is contiguous,
                               PTEs with same flags are shown in a single line)
                         3. The next PTE is out of the addresses range given as input.
                */
               if (    next_pte == &pg_table[NPTENTRIES] /* `cur_pte` is the last pte in PT*/
                    || !PTE_IS_MAPPED(*next_pte)         /* note: we can access `next_pte` because `cur_pte` is not the last */
                    || PTE_FLAGS(*next_pte) != PTE_FLAGS(*cur_pte)
                    || ((uintptr_t)(next_pte-pg_table) % 0x10 == 0 && !contiguous_physical_addr) /* end of 16 chunk */
                    || !is_pte_intersect_addr_range(next_pte, pde_idx, cur_pte_idx+1, &addr_range) /*  */ )
               {
                    if (!top_header_printed) {
                         top_header_printed = 1;
                         cprintf("  [ VPN range ]  PTE entries   Privilege  PFN range  \n");
                         cprintf("-----------------------------------------------------\n");
                    }
                    /* print PDE header. we print it here because we do not print headers
                       of PDEs out of the given addresses range */
                    if (!pde_header_printed) {
                         pde_header_printed = 1;
                         cprintf(/*CLR_BG_GRAY*/ CLR_BG_MAGENTA);
                         cprintf("+ ");
                         cprintf("[%05x-%05x]  PDE[%03x]      ", 
                              VPN(pde_idx, 0), VPN(pde_idx, NPTENTRIES-1), pde_idx);
                         print_pte_flags(PTE_FLAGS(*pde));
                         cprintf("                                    " CLR_BG_DEFAULT "\n");
                    }

                    /* if we reached here, `cur_pte` is mapped and intersect with the chosen addr ranges (if given any) */
                    print_pte_range(VPN(pde_idx, range_start_pte_idx), range_start_pte_idx, range_start_pte, cur_pte);
                    range_start_pte = INVALID_PTE_ADDR;
               }
          }

     }

     if (!top_header_printed) {
          cprintf("No matching mappings found.\n");
     }

	return 0;
}

static void print_pte_range(uintptr_t start_vpn, uint32_t start_pte_idx, pte_t *start_pte, pte_t *end_pte) {
     uint32_t nr_pages = (end_pte - start_pte + 1);

     cprintf("  ");
     if (nr_pages > 1) {
          cprintf("[%05x-%05x]  PTE[%03x-%03x]  ", 
               start_vpn, start_vpn + nr_pages - 1, 
               start_pte_idx, start_pte_idx + nr_pages - 1);
     } else {
          cprintf("[%05x]        PTE[%03x]      ", start_vpn, start_pte_idx);
     }

     print_pte_flags(PTE_FLAGS(*start_pte)); /* the whole range has the same flags */
     cprintf("  ");

     if (nr_pages > 1 && is_pte_range_pys_continuous(start_pte, end_pte)) {
          cprintf("%05x-%05x", PTE_PPN(*start_pte), PTE_PPN(*end_pte));
     } else {
          pte_t *pte;
          for (pte = start_pte; pte <= end_pte; ++pte) {
               cprintf("%05x ", PTE_PPN(*pte));
          }
          //poweroff();
     }

     cprintf("\n");
}

/* given PTEs range, checks whether the mapped physical memory is continuous */
static int is_pte_range_pys_continuous(pte_t* start_pte, pte_t* end_pte) {
     pte_t *pte;
     physaddr_t last = PTE_ADDR(*start_pte);
     for (pte = start_pte+1; pte <= end_pte; ++pte) {
          if (PTE_ADDR(*pte) != last + PGSIZE) return 0;
          last = PTE_ADDR(*pte);
     }
     return 1;
}
