// Simple command-line kernel monitor useful for
// controlling the kernel and exploring the system interactively.

#include <inc/stdio.h>
#include <inc/string.h>
#include <inc/memlayout.h>
#include <inc/assert.h>
#include <inc/x86.h>
#include <kern/io/serial_keys_map.h>
#include <kern/console.h>
#include <kern/monitor.h>
#include <kern/kdebug.h>
#include <kern/trap.h>
#include <kern/cmd_line_action.h>
#include <kern/user_debug.h>

/*
	The old way to detect ctrl+key by the ascii code.
	Eventually we are going to use `get_key_pressed()` in the monitor, as described below,
 		so this old way to detect CTRL will be deprecated.
 */
#define CTRL_KEY(x) ((x) - '@')


#define CMDBUF_SIZE	256	/* actually 80 is enough for one VGA text line */

/* Array with all of the commands defined in the system. see `MON_COMMAND_LIST` macro in monitor.h */
struct Command mon_commands[] = {
	#define X(NAME, STR_NAME, STR_S_NAME, STR_DESC, STR_USG, FUNC) \
		[MON_ENUM_PREFIX(NAME)] = { .name = (STR_NAME), .short_name = (STR_S_NAME), .desc = (STR_DESC), .usage = (STR_USG), .func = FUNC },
	#define FLG(f, symbol, v, name) "  " #symbol " - " name "\n"
	MON_COMMAND_LIST
	#undef FLG
	#undef X
};

/* defined now in `enum mon_commands_names` in monitor.h */
//#define MON_NR_COMMANDS (sizeof(mon_commands)/sizeof(mon_commands[0])) 


/***** Implementations of basic kernel monitor commands *****/
/***** (complex commands implemented in seperate files) *****/

static inline int find_command_by_name(const char* cmd_name) {
	int i;
	for (i = 0; i < MON_NR_COMMANDS; i++) {
		if (streq(cmd_name, mon_commands[i].name) || 
			((mon_commands[i].short_name != NULL) && streq(cmd_name, mon_commands[i].short_name)))
			return i;
	}
	return -1;
}

static inline void print_cmd_header_description(int cmd_idx) {
	const char* fmt;
	const char* short_name;

	assert(cmd_idx >= 0 && cmd_idx < MON_NR_COMMANDS);

	fmt = mon_commands[cmd_idx].short_name != NULL ? "%s(%s) - %s\n" : "%s%s - %s\n";
	short_name = mon_commands[cmd_idx].short_name != NULL ? mon_commands[cmd_idx].short_name : "";
	cprintf(fmt, mon_commands[cmd_idx].name, short_name, mon_commands[cmd_idx].desc);
}

int
mon_help(int argc, char **argv, struct Trapframe *tf)
{
	int i;

	for (i = 0; i < MON_NR_COMMANDS; i++) {
		print_cmd_header_description(i);
	}
	return 0;
}

int
mon_man(int argc, char **argv, struct Trapframe *tf)
{
	int cmd_idx;
	const char* cmd_name;

	if (argc != 2) {
		mon_invalid_usage(MON_DUMPMEM);
		return 1;
	}

	cmd_name = argv[1];
	cmd_idx = find_command_by_name(cmd_name);
	if (cmd_idx < 0 || cmd_idx >= MON_NR_COMMANDS) {
		assert(cmd_idx == -1);
		cprintf("Unknown command '%s'\n", cmd_name);
		return 1;
	}

	print_cmd_header_description(cmd_idx);
	cprintf("Usage: \n%s\n", mon_commands[cmd_idx].usage);

	return 0;
}

int
mon_kerninfo(int argc, char **argv, struct Trapframe *tf)
{
	extern char _start[], entry[], etext[], edata[], end[];

	cprintf("Special kernel symbols:\n");
	cprintf("  _start                  %08x (phys)\n", _start);
	cprintf("  entry  %08x (virt)  %08x (phys)\n", entry, entry - KERNBASE);
	cprintf("  etext  %08x (virt)  %08x (phys)\n", etext, etext - KERNBASE);
	cprintf("  edata  %08x (virt)  %08x (phys)\n", edata, edata - KERNBASE);
	cprintf("  end    %08x (virt)  %08x (phys)\n", end, end - KERNBASE);
	cprintf("Kernel executable memory footprint: %dKB\n",
		ROUNDUP(end - entry, 1024) / 1024);
	return 0;
}


int
mon_backtrace(int argc, char **argv, struct Trapframe *tf)
{
	uintptr_t cur_ebp, cur_eip;
	struct Eipdebuginfo info;

	cprintf("Stack backtrace:\n");
	for(cur_ebp = read_ebp(); cur_ebp != 0; cur_ebp = *((uintptr_t*)cur_ebp)) {
		cur_eip = GET_RET_EIP_BY_EBP(cur_ebp);
		cprintf("  ebp %08x  eip %08x  args %08x %08x %08x %08x %08x\n",
		cur_ebp,
		cur_eip /* the eip that this func will return to. */,
		GET_ARG_BY_EBP(cur_ebp, 1) /* arg1 */,
		GET_ARG_BY_EBP(cur_ebp, 2) /* arg2 */,
		GET_ARG_BY_EBP(cur_ebp, 3) /* arg3 */,
		GET_ARG_BY_EBP(cur_ebp, 4) /* arg4 */,
		GET_ARG_BY_EBP(cur_ebp, 5) /* arg5 */);

		debuginfo_eip(cur_eip, &info);
		cprintf("         %s:%d: %.*s+%d\n",
			info.eip_file, info.eip_line, info.eip_fn_namelen, info.eip_fn_name,
			(cur_eip - info.eip_fn_addr));
	}

	return 0;
}


int
mon_shutdown(int argc, char **argv, struct Trapframe *tf) {
	cprintf("Shutting down gracefully.\n\n");
	poweroff(); /* from `inc/x86.h` */
	return 0;
}


/***** Kernel monitor command interpreter *****/

#define WHITESPACE "\t\r\n "
#define MAXARGS 16

static int
runcmd(char *buf, struct Trapframe *tf)
{
	int argc;
	char *argv[MAXARGS];
	int i;

	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
	argv[argc] = 0;
	while (1) {
		// gobble whitespace
		while (*buf && strchr(WHITESPACE, *buf))
			*buf++ = 0;
		if (*buf == 0)
			break;

		// save and scan past next arg
		if (argc == MAXARGS-1) {
			cprintf("Too many arguments (max %d)\n", MAXARGS);
			return 0;
		}
		argv[argc++] = buf;
		while (*buf && !strchr(WHITESPACE, *buf))
			buf++;
	}
	argv[argc] = 0;

	// Lookup and invoke the command
	if (argc == 0)
		return 0;
	for (i = 0; i < MON_NR_COMMANDS; i++) {
		if (streq(argv[0], mon_commands[i].name) || ((mon_commands[i].short_name != NULL) && streq(argv[0], mon_commands[i].short_name)))
			return mon_commands[i].func(argc, argv, tf);
	}
	cprintf("Unknown command '%s'\n", argv[0]);
	return 0;
}


/*
	The new input method:
	We use `get_key_pressed()` instead of the old `getchar()`.
	Unlike `cons_getc()`, `getkeypressed()` provides the full info of the pressed key.
	We get: keycode, modifiers (NORMAL, ALT, CTRL, SHIFT),
			and the actual asciicode it represents (if any, otherwize '\0').
	This might be used in the near future to support advanced monitor features
		(like commands history and less like behaviour).
	
	Note: Currently this option is turned off, because of uncomplete implemention of the mapping.
		  The mapping infrastructure is fully implemented, but not all of the key are mapped,
		  causing to "read error"s when pressing unmapped keys (like CTRL+UP).
		  After completing the mapping, we will turn this feature on.
*/

//#define JOS_MONITOR_NEW_CMD_INPUT_METHOD 1

/*
	Note: underconstruction. see comment above.
	currently using the original `readline()` in `monitor()`.
*/
char *
shell_readline(char* buf, size_t buf_size, const char *prompt, struct CmdLineAction* key_binding_actions)
{
	int i, c = 0, echoing;
	struct keypress pressed_key;
	//struct KeyCodeTree *last_node = NULL, *next_node;

	if (prompt != NULL)
		cprintf("%s", prompt);

	i = 0;
	echoing = iscons(0);
	while (1) {
		#ifndef JOS_MONITOR_NEW_CMD_INPUT_METHOD
		c = getchar();
		// /* debug */ cprintf(" %x ", c); continue;
		#else

		pressed_key = get_key_pressed();
		if (IS_INVALID_KEYPRESS(pressed_key)) {
			/* means the recieved key codes series is not mapped in the key codes tree */
			cprintf("read error!\n");
			return NULL;
		}

		// TODO: handle special key press

		c = pressed_key.asciicode;

		/* handle keys that are not assigned with asciicode */
		/* TODO: assign those asciicodes to the table!! */
		if (pressed_key.keycode == KEYCODE_ENTER) {
			c = '\n';
		} else if (pressed_key.keycode == KEYCODE_BS) {
			c = '\b';
		} else if (pressed_key.keycode == KEYCODE_SPACE) {
			c = ' ';
		}

		/* debug */
		/*if (c == 0) {
			cprintf("%s  %s  %x\n", serial_key_code_to_str[pressed_key.keycode],
				key_modifier_str(pressed_key.modifiers), pressed_key.modifiers);
		}*/

		// /* debug */ cprintf("  %x  ", c); return NULL;
		/*if (key_binding_actions != NULL && key_binding_actions[c].action != NULL && c < CMD_KEY_BIND_MAX_KEY_ASCII) {
			buf[i] = 0;
			int ret = (key_binding_actions[c].action)(key_binding_actions, buf, &i, c);
			if (ret != 0) { // TODO: decide this constant and behaviour.
				return NULL;
			}
		}*/

		/*
		// debug
		if (last_node == NULL)
			cprintf("  last_node: NULL\n");
		else
			cprintf("  last_node: %x\n", last_node->path);

		next_node = keycode_find_child(last_node, c & 0xFF);
		if (next_node == NULL) {
			if (last_node != NULL) {
				if (!KEYCODE_NODE_IS_KEY(last_node)) {
					cprintf("read error: %e\n", c);
					return NULL;
				}
				// here use the last_node ...
				cprintf("%x   %s\n", (uint32_t)(last_node->path), last_node->keyname);
			}

			last_node = &keycodes[keycodes_root_mappings[c & 0xFF]];
			if (!KEYCODE_NODE_IS_VALID(last_node)) {
				cprintf("read error: %e\n", c);
				return NULL;
			}
			continue;
		} else {
			last_node = next_node;
			continue;
		}*/
		#endif

		if (c < 0) {
			cprintf("read error: %e\n", c);
			return NULL;
		} else if (c == '\b' || c == '\x7f') {
			if (i > 0) {
				if (echoing)
					cputchar('\b');
				i--;
			}
		} else if (c >= ' ' && i < buf_size-1) {
			if (echoing)
				cputchar(c);
			buf[i++] = c;
		} else if (c == '\n' || c == '\r') {
			if (echoing)
				cputchar('\n');
			buf[i] = 0;
			return buf;
		}
	}
}

static int keystroke_shutdown (struct CmdLineAction *binding,
                               char* cmd_input_buffer,
                               int *cmd_buffer_cursor,
                               int invoking_key_stroke) {
	cprintf("\nShutting down gracefully.\n\n");
	poweroff();
	return 0;
}

/*
	`keystrokes_bindings` is currently not in use.
	TODO: We have to change the design, so that each cell in the binding array
		  would uniquely identify a `keypress` (keycode+modifiers).
		  We also have to require that the array size would have at least `KEYCODE_MAX` elements,
		  even if empty, just in order to avoid accessing illegal/irelevant memory.
*/
struct CmdLineAction keystrokes_bindings[] = {
	[CTRL_KEY('C')] = { .action = keystroke_shutdown },
};


static int flg_exit_monitor;
void monitor_set_exit_flag(int val) {
	flg_exit_monitor = val;
}

void
monitor(struct Trapframe *tf)
{
	char buf[CMDBUF_SIZE];
	char *retbuf;

	cprintf("Welcome to the JOS kernel monitor!\n");
	cprintf("Type 'help' for a list of commands.\n");

	if (tf != NULL)
		print_trapframe(tf);

	flg_exit_monitor = 0;
	while (!flg_exit_monitor) {
		/* See comments above about the new input method. */
		#ifndef JOS_MONITOR_NEW_CMD_INPUT_METHOD
		retbuf = readline(COLOR("K> ", FG_LIGHT_MAGENTA));
		if (retbuf != NULL)
			if (runcmd(retbuf, tf) < 0)
				break;
		#else
		retbuf = shell_readline(buf, CMDBUF_SIZE, COLOR("K> ", FG_LIGHT_MAGENTA), keystrokes_bindings);
		if (retbuf != NULL)
			if (runcmd(retbuf, tf) < 0)
				break;
		#endif
	}
}
