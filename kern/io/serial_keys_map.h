#ifndef JOS_INC_KEYS_MAP_H
#define JOS_INC_KEYS_MAP_H

#include <inc/types.h>

#define ACTIVE_TERMINAL_NAME xterm
#define serial_keycodes_root_mappings xterm_serial_keycodes_root_mappings
#define serial_keycodes xterm_serial_keycodes
#define serial_key_code_to_str xterm_serial_key_code_to_str

#define KEYCODE_INVALID_NODE_ID (0)
#define KEYCODE_INVALID_SEQUENCE ((uint64_t)0x00)
#define KEYCODE_NODE_CHILD_N_VALID(node, n) \
    ((n) < KEYCODE_NODE_MAX_CHILDREN && !((node)->children[n][0] == 0 && (node)->children[n][1] == 0))
#define KEYCODE_NODE_HAS_CHILDREN(node) (KEYCODE_NODE_CHILD_N_VALID(node, 0))
#define KEYCODE_NODE_IS_KEY(node) ((node)->keycode >= 0 && (node)->sequence != KEYCODE_INVALID_SEQUENCE)
#define KEYCODE_NODE_IS_VALID(node) ((node)->sequence != KEYCODE_INVALID_SEQUENCE)
#define KEYCODE_NODE_MAX_CHILDREN (52)
#define KEYCODE_ROOT_NODE_FROM_SEQUENCE_FIRST_CHAR(ch) \
    &serial_keycodes[serial_keycodes_root_mappings[(ch) & 0xFF]]

struct KeyCodeTree {
    /*const char* keyname;*/  /* depricated field. use the `serial_key_code_to_str[]` mapping */
    int16_t keycode;  /* signed: we use -1 to represent internal nodes with no keycode */
    uint8_t asciicode;
    uint8_t modifiers;
    uint64_t sequence;
    uint16_t children[KEYCODE_NODE_MAX_CHILDREN][2];
};

extern const size_t serial_keycodes_root_mappings[];
extern const struct KeyCodeTree serial_keycodes[];
extern const char* const xterm_serial_key_code_to_str[];



#define KEYCODE_0              (11)
#define KEYCODE_1              (2)
#define KEYCODE_2              (3)
#define KEYCODE_3              (4)
#define KEYCODE_4              (5)
#define KEYCODE_5              (6)
#define KEYCODE_6              (7)
#define KEYCODE_7              (8)
#define KEYCODE_8              (9)
#define KEYCODE_9              (10)
#define KEYCODE_A              (30)
#define KEYCODE_B              (48)
#define KEYCODE_BACKQUOTE      (41)
#define KEYCODE_BACKSLASH      (43)
#define KEYCODE_BS             (14)
#define KEYCODE_C              (46)
#define KEYCODE_COMMA          (51)
#define KEYCODE_D              (32)
#define KEYCODE_DASH           (12)
#define KEYCODE_DEL            (111)
#define KEYCODE_DOWN           (108)
#define KEYCODE_E              (18)
#define KEYCODE_ENTER          (28)
#define KEYCODE_EQUAL          (13)
#define KEYCODE_ESC            (1)
#define KEYCODE_END            (107)
#define KEYCODE_F              (33)
#define KEYCODE_F1             (59)
#define KEYCODE_F10            (68)
#define KEYCODE_F11            (87)
#define KEYCODE_F12            (88)
#define KEYCODE_F2             (60)
#define KEYCODE_F3             (61)
#define KEYCODE_F4             (62)
#define KEYCODE_F5             (63)
#define KEYCODE_F6             (64)
#define KEYCODE_F7             (65)
#define KEYCODE_F8             (66)
#define KEYCODE_F9             (67)
#define KEYCODE_FORWARD_SLASH  (53)
#define KEYCODE_G              (34)
#define KEYCODE_H              (35)
#define KEYCODE_HOME           (102)
#define KEYCODE_I              (23)
#define KEYCODE_INSERT         (110)
#define KEYCODE_J              (36)
#define KEYCODE_K              (37)
#define KEYCODE_L              (38)
#define KEYCODE_L_BRACKETS     (26)
#define KEYCODE_LEFT           (105)
#define KEYCODE_M              (50)
#define KEYCODE_N              (49)
#define KEYCODE_NL_ASTERISK    (55)
#define KEYCODE_NP_5           (76)
#define KEYCODE_NP_FORWARD_SLASH (98)
#define KEYCODE_NP_MINUS       (74)
#define KEYCODE_NP_PLUS        (78)
#define KEYCODE_O              (24)
#define KEYCODE_P              (25)
#define KEYCODE_PERIOD         (52)
#define KEYCODE_PAUSE          (119)
#define KEYCODE_PGDN           (109)
#define KEYCODE_PGUP           (104)
#define KEYCODE_Q              (16)
#define KEYCODE_R              (19)
#define KEYCODE_R_BRACKETS     (27)
#define KEYCODE_RIGHT          (106)
#define KEYCODE_S              (31)
#define KEYCODE_SEMICOLON      (39)
#define KEYCODE_SINGLE_QUOTE   (40)
#define KEYCODE_SPACE          (57)
#define KEYCODE_T              (20)
#define KEYCODE_TAB            (15)
#define KEYCODE_U              (22)
#define KEYCODE_UP             (103)
#define KEYCODE_V              (47)
#define KEYCODE_W              (17)
#define KEYCODE_X              (45)
#define KEYCODE_Y              (21)
#define KEYCODE_Z              (44)
#define KEYCODE_MAX            (119)





static inline const struct KeyCodeTree* keycode_find_child(const struct KeyCodeTree* node, int c) {
    size_t i;
    size_t next_node_id;

    if (node == NULL) return NULL;
    for (i = 0; i < KEYCODE_NODE_MAX_CHILDREN && KEYCODE_NODE_CHILD_N_VALID(node, i); ++i) {
        if (node->children[i][0] == c) {
            next_node_id = node->children[i][1];
            if (next_node_id == KEYCODE_INVALID_NODE_ID) return NULL;
            return &serial_keycodes[next_node_id];
        }
    }
    return NULL;
}

#endif /* JOS_INC_KEYS_MAP_H */
