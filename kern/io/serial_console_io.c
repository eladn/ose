#include <kern/io/serial_keys_map.h>
#include <kern/console.h>

/*
	Traverse the keys mapping trie in order to translate the input chars sequence into 
	a valid key. See more details and explanation in `kern/io/xterm_serial_keys_map.c`.
*/
static const struct KeyCodeTree* serial_get_key_pressed_node(void)
{
	static int read_char_from_last_iter = 0;
	const struct KeyCodeTree *last_node = NULL, *next_node;
	int c;

	while (1) {
		if (read_char_from_last_iter != 0) {
			c = read_char_from_last_iter;
			read_char_from_last_iter = 0;
		} else {
			c = cons_getc();
		}

		if (c == 0) {
			if (last_node == NULL) continue;
			if (!KEYCODE_NODE_IS_KEY(last_node)) {
				return NULL;
			}
			return last_node;
		}

		if (last_node == NULL) {
			last_node = KEYCODE_ROOT_NODE_FROM_SEQUENCE_FIRST_CHAR((c & 0xFF));
			if (!KEYCODE_NODE_IS_VALID(last_node)) {
				return NULL;
			}
		} else {
			next_node = keycode_find_child(last_node, c & 0xFF);
			if (next_node == NULL) {
				read_char_from_last_iter = c;
				if (!KEYCODE_NODE_IS_KEY(last_node)) {
					return NULL;
				}
				return last_node;
			}
			last_node = next_node;
		}
	}
	return NULL;
}

/*
	Called by `get_key_pressed()` in `console.c` 
	`struct keypress` is our generic structure to describe a pressed key.
	`struct KeyCodeTree *` is just the node in the the serial mapping trie,
		but we need to convert it to a more genalized abstract representation,
		so that we could also represent input from the keyboard (not serial terminal),
		that isn't using those trie mappings.
*/
struct keypress serial_get_key_pressed(void) {
	const struct KeyCodeTree* node = serial_get_key_pressed_node();

	if (node == NULL) {
		struct keypress key = UNKNOWN_KEYPRESS;
		return key;
	}

	struct keypress key = {
		.keycode = node->keycode,
		.asciicode = node->asciicode,
		.modifiers = node->modifiers,
	};

	return key;
}
