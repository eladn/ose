#ifndef JOS_KERN_USER_DEBUG_H
#define JOS_KERN_USER_DEBUG_H

#include <inc/types.h>

#define USR_DBG_MAX_BPS 10
struct user_debugger {
	int is_continue;
	uintptr_t break_points[USR_DBG_MAX_BPS];
	size_t nr_breakpoints;
};

int user_debug_is_breakpoint_exists(uintptr_t eip);
int user_debug_add_breakpoint(uintptr_t eip);
extern struct user_debugger user_debug;

#endif /* JOS_KERN_USER_DEBUG_H */
