#include <kern/user_debug.h>
#include <kern/monitor.h>
#include <kern/trap.h>
#include <inc/assert.h>
#include <inc/utils.h>
#include <kern/kdebug.h>


struct user_debugger user_debug;

int user_debug_find_breakpoint_idx(uintptr_t eip) {
	int idx;
	for (idx = 0; idx < user_debug.nr_breakpoints; ++idx) {
		if (user_debug.break_points[idx] == eip) {
			return idx;
		}
	}
	return -1;
}
int user_debug_is_breakpoint_exists(uintptr_t eip) {
	return (user_debug_find_breakpoint_idx(eip) >= 0);
}
int user_debug_add_breakpoint(uintptr_t eip) {
	if (user_debug_is_breakpoint_exists(eip)) {
		return 1;
	}
	if (user_debug.nr_breakpoints == USR_DBG_MAX_BPS) {
		return -1;
	}

	user_debug.break_points[user_debug.nr_breakpoints++] = eip;
	return 0;
}
int user_debug_del_breakpoint(uintptr_t eip) {
	int idx = user_debug_find_breakpoint_idx(eip);
	if (idx < 0) {
		return -1; /* breakpoint not exists */
	}
	assert(idx < USR_DBG_MAX_BPS && idx < user_debug.nr_breakpoints);

	user_debug.break_points[idx] = user_debug.break_points[user_debug.nr_breakpoints-1];
	user_debug.nr_breakpoints--;
	return 0;
}

int
mon_continue(int argc, char **argv, struct Trapframe *tf) {
	assert(tf != NULL);
	cprintf("Continuing.\n");
	monitor_set_exit_flag(1);
	user_debug.is_continue = 1;
	tf->tf_eflags |= FL_TF;
	return 0;
}

int
mon_step(int argc, char **argv, struct Trapframe *tf) {
	assert(tf != NULL);
	cprintf("Step single instruction.\n");
	monitor_set_exit_flag(1);
	user_debug.is_continue = 0;
	tf->tf_eflags |= FL_TF;
	return 0;
}

int
mon_cbp(int argc, char **argv, struct Trapframe *tf) {
	assert(tf != NULL);
	if (argc != 2) {
		mon_invalid_usage(MON_CBP);
		return 1;
	}

	uintptr_t eip = xtoi(argv[1]);
	int ret = user_debug_add_breakpoint(eip);
	if (ret < 0) {
		cprintf("Already using max number of allowed breakpoints [%d].\n", USR_DBG_MAX_BPS);
		return 1;
	}

	if (ret == 1) {
		cprintf("Breakpoint already exists ", eip);
		print_eip_info(eip);
		cprintf("\n");
	} else if (ret == 0) {
		cprintf("Creating breakpoint ");
		print_eip_info(eip);
		cprintf("\n");
	}
	return 0;
}

int
mon_dbp(int argc, char **argv, struct Trapframe *tf) {
	assert(tf != NULL);
	if (argc != 2) {
		mon_invalid_usage(MON_DBP);
		return 1;
	}

	uintptr_t eip = xtoi(argv[1]);
	int ret = user_debug_del_breakpoint(eip);
	if (ret < 0) {
		cprintf("Breakpoint ");
		print_eip_info(eip);
		cprintf(" not exists.\n");
		return 1;
	}

	cprintf("Breakpoint ");
	print_eip_info(eip);
	cprintf(" deleted.\n");
	return 0;
}

int
mon_lbp(int argc, char **argv, struct Trapframe *tf) {
	assert(tf != NULL);
	
	int idx;
	for (idx = 0; idx < user_debug.nr_breakpoints; ++idx) {
		cprintf("Breakpoint ");
		print_eip_info(user_debug.break_points[idx]);
		cprintf("\n");
	}

	return 0;
}
