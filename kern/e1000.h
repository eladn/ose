#ifndef JOS_KERN_E1000_H
#define JOS_KERN_E1000_H

#include <inc/types.h>
#include <kern/pci.h>
#include <kern/e1000regs.h>
#include <net/net_types.h>

#define E1000_ZERO_COPY 1

#define PCI_E1000_VID 0x8086
#define PCI_E1000_DID 0x100e

#define E1000_REG_DEV_CTRL 0x00
#define E1000_REG_DEV_STATUS 0x08

extern volatile uint32_t *e1000;

struct tx_desc
{
    uint64_t addr;
    uint16_t length;
    uint8_t cso;
    uint8_t cmd;
    uint8_t status;
    uint8_t css;
    uint16_t special;
} __attribute__ ((packed));

struct rx_desc {
    uint64_t addr;       /* Address of the descriptor's data buffer */
    uint16_t length;     /* Length of data DMAed into data buffer */
    uint16_t csum;       /* Packet checksum */
    uint8_t status;      /* Descriptor status */
    uint8_t errors;      /* Descriptor Errors */
    uint16_t special;
} __attribute__ ((packed));

#define E1000_REG_GET(REG_OFFSET, type) ( *((type*)((uintptr_t)e1000 + (REG_OFFSET))) )
#define E1000_REG_SET(REG_OFFSET, type, value) ( *((type*)((uintptr_t)e1000 + (REG_OFFSET))) = (type)(value) )
#define E1000_REG_BGET(REG_OFFSET) E1000_REG_GET(REG_OFFSET, uint8_t)
#define E1000_REG_BSET(REG_OFFSET, value) E1000_REG_SET(REG_OFFSET, uint8_t, value)
#define E1000_REG_WGET(REG_OFFSET) E1000_REG_GET(REG_OFFSET, uint16_t)
#define E1000_REG_WSET(REG_OFFSET, value) E1000_REG_SET(REG_OFFSET, uint16_t, value)
#define E1000_REG_LGET(REG_OFFSET) E1000_REG_GET(REG_OFFSET, uint32_t)
#define E1000_REG_LSET(REG_OFFSET, value) E1000_REG_SET(REG_OFFSET, uint32_t, value)

#define ETHERNET_PACKET_MAX_SIZE 1518

#define MAX_TX_DESC_ARRAY_ENTRIES (1<<16) /*6K*/
#define TX_DESC_ARRAY_ENTRIES 64 /* for tests to detect overflow */ /* must be multiple of 8 */
#define TX_DESC_ARRAY_SIZE (TX_DESC_ARRAY_ENTRIES * sizeof(struct tx_desc)) /*1M for max #entries*/
#define TX_BUFFER_SIZE ETHERNET_PACKET_MAX_SIZE
#define TX_BUFFERS_ARRAY_SIZE (TX_BUFFER_SIZE * TX_DESC_ARRAY_ENTRIES)

#define MAX_RX_DESC_ARRAY_ENTRIES (1<<16) /*6K*/
#define RX_DESC_ARRAY_ENTRIES 256 /* must be multiple of 8 (?) */
#define RX_DESC_ARRAY_SIZE (RX_DESC_ARRAY_ENTRIES * sizeof(struct rx_desc)) /*1M for max #entries*/
#define RX_BUFFER_SIZE 2048
#define RX_BUFFER_SZ_CTRL_BIT_MASK E1000_RCTL_SZ_2048
#define RX_BUFFERS_ARRAY_SIZE (RX_BUFFER_SIZE * RX_DESC_ARRAY_ENTRIES)

/* offsets of tx/rx data structures inside of the reserved pyshical memory region */
/* | TX_DESC_ARRAY | TX_BUFFERS_ARRAY | RX_DESC_ARRAY | RX_BUFFERS_ARRAY | */
#define TX_DESC_ARRAY_PADDR(network_dev_buffer_paddr) \
	(network_dev_buffer_paddr)
#define TX_BUFFER_IDX2PADDR(network_dev_buffer_paddr, tx_buffer_idx) \
	(TX_DESC_ARRAY_PADDR(network_dev_buffer_paddr) + TX_DESC_ARRAY_SIZE + (tx_buffer_idx)*TX_BUFFER_SIZE)
#define RX_DESC_ARRAY_PADDR(network_dev_buffer_paddr) \
	(TX_DESC_ARRAY_PADDR(network_dev_buffer_paddr) + TX_DESC_ARRAY_SIZE + TX_BUFFERS_ARRAY_SIZE)
#define RX_BUFFER_IDX2PADDR(network_dev_buffer_paddr, rx_buffer_idx) \
	(RX_DESC_ARRAY_PADDR(network_dev_buffer_paddr) + RX_DESC_ARRAY_SIZE + (rx_buffer_idx)*RX_BUFFER_SIZE)

#define NETDEV_NR_PAGES (DIV_ROUND_UP((TX_DESC_ARRAY_SIZE + TX_BUFFERS_ARRAY_SIZE + RX_DESC_ARRAY_SIZE + RX_BUFFERS_ARRAY_SIZE), PGSIZE))

extern struct mac_addr e1000_mac;

int e1000_init(struct pci_func *pcif);
int e1000_transmit_packet(void *send_buffer, size_t send_buffer_len);

#endif	// JOS_KERN_E1000_H
