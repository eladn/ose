/* See COPYRIGHT for copyright information. */

#include <inc/x86.h>
#include <inc/memlayout.h>
#include <inc/kbdreg.h>
#include <inc/string.h>
#include <inc/assert.h>
#include <inc/utils.h>
#include <kern/console.h>
#include <kern/picirq.h>


/* 
	Sometimes we want to read input (ex: from serial) without echoing it to screen.
	Use case example: when we ask the serial connection for the size of the terminal screen.
*/
// TODO: do we want to locate these somewhere else?
static int __console_flg_echo_to_screen = 1;
#define SET_ECHO_TO_SCREEN(flg) do { \
	__console_flg_echo_to_screen = !!(flg); \
} while(0)
#define GET_ECHO_TO_SCREEN() (__console_flg_echo_to_screen)


static void cons_intr(int (*proc)(void));
static void cons_putc(int c);

// Stupid I/O delay routine necessitated by historical PC design flaws
static void
delay(void)
{
	inb(0x84);
	inb(0x84);
	inb(0x84);
	inb(0x84);
}

/***** Serial I/O code *****/

#define COM1		0x3F8

#define COM_RX		0	// In:	Receive buffer (DLAB=0)
#define COM_TX		0	// Out: Transmit buffer (DLAB=0)
#define COM_DLL		0	// Out: Divisor Latch Low (DLAB=1)
#define COM_DLM		1	// Out: Divisor Latch High (DLAB=1)
#define COM_IER		1	// Out: Interrupt Enable Register
#define   COM_IER_RDI	0x01	//   Enable receiver data interrupt
#define COM_IIR		2	// In:	Interrupt ID Register
#define COM_FCR		2	// Out: FIFO Control Register
#define COM_LCR		3	// Out: Line Control Register
#define	  COM_LCR_DLAB	0x80	//   Divisor latch access bit
#define	  COM_LCR_WLEN8	0x03	//   Wordlength: 8 bits
#define COM_MCR		4	// Out: Modem Control Register
#define	  COM_MCR_RTS	0x02	// RTS complement
#define	  COM_MCR_DTR	0x01	// DTR complement
#define	  COM_MCR_OUT2	0x08	// Out2 complement
#define COM_LSR		5	// In:	Line Status Register
#define   COM_LSR_DATA	0x01	//   Data available
#define   COM_LSR_TXRDY	0x20	//   Transmit buffer avail
#define   COM_LSR_TSRE	0x40	//   Transmitter off

static bool serial_exists;

static int
serial_proc_data(void)
{
	if (!(inb(COM1+COM_LSR) & COM_LSR_DATA))
		return -1;
	return inb(COM1+COM_RX);
}

void
serial_intr(void)
{
	if (serial_exists)
		cons_intr(serial_proc_data);
}

static void
serial_putc(int c)
{
	int i;
	size_t nr_chars = 1;
	size_t cur_char;
	char chars[4] = {c, 0, 0, 0};

	if (c == '\b') {
		nr_chars = 3;
		chars[1] = ' ';
		chars[2] = '\b';
	}

	for (cur_char = 0; cur_char < nr_chars; ++cur_char) {
		c = chars[cur_char];
		for (i = 0;
		     !(inb(COM1 + COM_LSR) & COM_LSR_TXRDY) && i < 12800;
		     i++)
			delay();

		outb(COM1 + COM_TX, c);
	}
}

static void
serial_init(void)
{
	// Turn off the FIFO
	outb(COM1+COM_FCR, 0);

	// Set speed; requires DLAB latch
	outb(COM1+COM_LCR, COM_LCR_DLAB);
	outb(COM1+COM_DLL, (uint8_t) (115200 / 9600));
	outb(COM1+COM_DLM, 0);

	// 8 data bits, 1 stop bit, parity off; turn off DLAB latch
	outb(COM1+COM_LCR, COM_LCR_WLEN8 & ~COM_LCR_DLAB);

	// No modem controls
	outb(COM1+COM_MCR, 0);
	// Enable rcv interrupts
	outb(COM1+COM_IER, COM_IER_RDI);

	// Clear any preexisting overrun indications and interrupts
	// Serial port doesn't exist if COM_LSR returns 0xFF
	serial_exists = (inb(COM1+COM_LSR) != 0xFF);
	(void) inb(COM1+COM_IIR);
	(void) inb(COM1+COM_RX);

	// Enable serial interrupts
	if (serial_exists)
		irq_setmask_8259A(irq_mask_8259A & ~(1<<4));
}



/***** Parallel port output code *****/
// For information on PC parallel port programming, see the class References
// page.

static void
lpt_putc(int c)
{
	int i;

	for (i = 0; !(inb(0x378+1) & 0x80) && i < 12800; i++)
		delay();
	outb(0x378+0, c);
	outb(0x378+2, 0x08|0x04|0x01);
	outb(0x378+2, 0x08);
}


/***** Text-Graphic Attributes for CGA/VGA display *****/

typedef enum {
	TXT_CGA_CLR__BLACK                  =0x0,
	TXT_CGA_CLR__BLUE                   =0x1,
	TXT_CGA_CLR__GREEN                  =0x2,
	TXT_CGA_CLR__CYAN                   =0x3,
	TXT_CGA_CLR__RED                    =0x4,
	TXT_CGA_CLR__MAGENTA                =0x5,
	TXT_CGA_CLR__BROWN                  =0x6,
	TXT_CGA_CLR__LIGHT_GRAY             =0x7,
	TXT_CGA_CLR__GRAY                   =0x8,
	TXT_CGA_CLR__LIGHT_BLUE             =0x9,
	TXT_CGA_CLR__LIGHT_GREEN            =0xA,
	TXT_CGA_CLR__LIGHT_CYAN             =0xB,
	TXT_CGA_CLR__LIGHT_RED              =0xC,
	TXT_CGA_CLR__LIGHT_MAGENTA          =0xD,
	TXT_CGA_CLR__YELLOW                 =0xE,
	TXT_CGA_CLR__WHITE                  =0xF,
	TXT_CGA_CLR__DEFAULT_FG             =TXT_CGA_CLR__LIGHT_GRAY,
	TXT_CGA_CLR__DEFAULT_BG             =TXT_CGA_CLR__BLACK,
} TXT_CGA_CLR;

typedef struct TextAppearance
{
	TXT_CGA_CLR foreground_color;
	TXT_CGA_CLR background_color;
} TextAppearance;

static const TextAppearance default_ctr_appearance = {TXT_CGA_CLR__DEFAULT_FG, TXT_CGA_CLR__DEFAULT_BG};
static TextAppearance crt_appearance;

#define SET_FG_CLR(ch, clr) (((ch) & ~0xF00) | ((clr) << 8))
#define SET_BG_CLR(ch, clr) (((ch) & ~0xF000) | ((clr) << 12))

static void
set_current_crt_foreground(TXT_CGA_CLR new_fg){
	crt_appearance.foreground_color = new_fg;
}

static void
set_current_crt_background(TXT_CGA_CLR new_bg){
	crt_appearance.background_color = new_bg;
}


static TXT_CGA_CLR ascii_to_cga[] = {
	TXT_CGA_CLR__BLACK,
	TXT_CGA_CLR__RED,
	TXT_CGA_CLR__GREEN,
	TXT_CGA_CLR__BROWN,
	TXT_CGA_CLR__BLUE,
	TXT_CGA_CLR__MAGENTA,
	TXT_CGA_CLR__CYAN,
	TXT_CGA_CLR__LIGHT_GRAY
};

static TXT_CGA_CLR ascii_to_cga_alt[] = {
	TXT_CGA_CLR__GRAY,
	TXT_CGA_CLR__LIGHT_RED,
	TXT_CGA_CLR__LIGHT_GREEN,
	TXT_CGA_CLR__YELLOW,
	TXT_CGA_CLR__LIGHT_BLUE,
	TXT_CGA_CLR__LIGHT_MAGENTA,
	TXT_CGA_CLR__LIGHT_CYAN,
	TXT_CGA_CLR__WHITE
};

#define TXT_APPR_INIT(txt) (txt = default_ctr_appearance)

static void
change_crt_color(int esc_data){
	int tens,ones;
	if (esc_data == 39){
		/* deafult FG color */
		set_current_crt_foreground(TXT_CGA_CLR__DEFAULT_FG);
		return;
	}
	if (esc_data == 49){
		/* deafult FG color */
		set_current_crt_background(TXT_CGA_CLR__DEFAULT_BG);
		return;
	}
	if(esc_data == 0){
		TXT_APPR_INIT(crt_appearance);
	}
	ones = esc_data%10;
	tens = esc_data/10;
	switch(tens){
		case 3:
			set_current_crt_foreground(ascii_to_cga[ones]);
			break;
		case 9:
			set_current_crt_foreground(ascii_to_cga_alt[ones]);
			break;
		case 4:
			set_current_crt_background(ascii_to_cga[ones]);
			break;
		case 10:
			set_current_crt_background(ascii_to_cga_alt[ones]);
			break;
	}
	
}

/***** Text-mode CGA/VGA display output *****/

static unsigned addr_6845;
static uint16_t *crt_buf;
static uint16_t crt_pos;

static void
cga_init(void)
{
	volatile uint16_t *cp;
	uint16_t was;
	unsigned pos;

	cp = (uint16_t*) (KERNBASE + CGA_BUF);
	was = *cp;
	*cp = (uint16_t) 0xA55A;
	if (*cp != 0xA55A) {
		cp = (uint16_t*) (KERNBASE + MONO_BUF);
		addr_6845 = MONO_BASE;
	} else {
		*cp = was;
		addr_6845 = CGA_BASE;
	}

	/* Extract cursor location */
	outb(addr_6845, 14);
	pos = inb(addr_6845 + 1) << 8;
	outb(addr_6845, 15);
	pos |= inb(addr_6845 + 1);

	crt_buf = (uint16_t*) cp;
	crt_pos = pos;


	TXT_APPR_INIT(crt_appearance);
}

#define CRT_TO_CHAR(ch) ((ch) & 0xff)

static void
cga_putc(int c)
{
	static int is_in_esc = 0;
	static int esc_data = 0;

	if (!(c & ~0xFF)){
		/* <<< ORIGINAL IMPL >>> c |= 0x0700; // if no attribute given, then use black on white */
		c = SET_FG_CLR(SET_BG_CLR(c,crt_appearance.background_color), crt_appearance.foreground_color);
	}

	switch (CRT_TO_CHAR(c)) {
	case '\b':
		if (crt_pos > 0) {
			crt_pos--;
			crt_buf[crt_pos] = (c & ~0xff) | ' ';
		}
		break;
	case '\n':
		crt_pos += CRT_COLS;
		/* fallthru */
	case '\r':
		crt_pos -= (crt_pos % CRT_COLS);
		break;
	case '\t':
		cons_putc(' ');
		cons_putc(' ');
		cons_putc(' ');
		cons_putc(' ');
		cons_putc(' ');
		break;
	case '\e':
		is_in_esc = 1;
		break;
	case '[':
		if(is_in_esc){
			break;
		} /* else - fallthrough to default (via digits and ;m) */
	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
		if(is_in_esc){
			esc_data = esc_data * 10 + (CRT_TO_CHAR(c) - '0');
			break;
		} /* else - fallthrough to default (via ;m)*/
	case ';':
	case 'm':
		if(is_in_esc){
			change_crt_color(esc_data);
			esc_data = 0;
			if (CRT_TO_CHAR(c) == 'm'){
				is_in_esc = 0;
			}
			break;
		} /* else - fallthrough to default */
	default:
		crt_buf[crt_pos++] = c;		/* write the character */
		break;
	}

	// What is the purpose of this?
	if (crt_pos >= CRT_SIZE) {
		int i;

		memmove(crt_buf, crt_buf + CRT_COLS, (CRT_SIZE - CRT_COLS) * sizeof(uint16_t));
		for (i = CRT_SIZE - CRT_COLS; i < CRT_SIZE; i++)
			crt_buf[i] = 0x0700 | ' ';
		crt_pos -= CRT_COLS;
	}

	/* move that little blinky thing */
	outb(addr_6845, 14);
	outb(addr_6845 + 1, crt_pos >> 8);
	outb(addr_6845, 15);
	outb(addr_6845 + 1, crt_pos);
}


/***** Keyboard input code *****/

#define NO		0

#define SHIFT		(1<<0)
#define CTL		(1<<1)
#define ALT		(1<<2)

#define CAPSLOCK	(1<<3)
#define NUMLOCK		(1<<4)
#define SCROLLLOCK	(1<<5)

#define E0ESC		(1<<6)

static uint8_t shiftcode[256] =
{
	[0x1D] = CTL,
	[0x2A] = SHIFT,
	[0x36] = SHIFT,
	[0x38] = ALT,
	[0x9D] = CTL,
	[0xB8] = ALT
};

static uint8_t togglecode[256] =
{
	[0x3A] = CAPSLOCK,
	[0x45] = NUMLOCK,
	[0x46] = SCROLLLOCK
};

static uint8_t normalmap[256] =
{
	NO,   0x1B, '1',  '2',  '3',  '4',  '5',  '6',	// 0x00
	'7',  '8',  '9',  '0',  '-',  '=',  '\b', '\t',
	'q',  'w',  'e',  'r',  't',  'y',  'u',  'i',	// 0x10
	'o',  'p',  '[',  ']',  '\n', NO,   'a',  's',
	'd',  'f',  'g',  'h',  'j',  'k',  'l',  ';',	// 0x20
	'\'', '`',  NO,   '\\', 'z',  'x',  'c',  'v',
	'b',  'n',  'm',  ',',  '.',  '/',  NO,   '*',	// 0x30
	NO,   ' ',  NO,   NO,   NO,   NO,   NO,   NO,
	NO,   NO,   NO,   NO,   NO,   NO,   NO,   '7',	// 0x40
	'8',  '9',  '-',  '4',  '5',  '6',  '+',  '1',
	'2',  '3',  '0',  '.',  NO,   NO,   NO,   NO,	// 0x50
	[0xC7] = KEY_HOME,	      [0x9C] = '\n' /*KP_Enter*/,
	[0xB5] = '/' /*KP_Div*/,      [0xC8] = KEY_UP,
	[0xC9] = KEY_PGUP,	      [0xCB] = KEY_LF,
	[0xCD] = KEY_RT,	      [0xCF] = KEY_END,
	[0xD0] = KEY_DN,	      [0xD1] = KEY_PGDN,
	[0xD2] = KEY_INS,	      [0xD3] = KEY_DEL
};

static uint8_t shiftmap[256] =
{
	NO,   033,  '!',  '@',  '#',  '$',  '%',  '^',	// 0x00
	'&',  '*',  '(',  ')',  '_',  '+',  '\b', '\t',
	'Q',  'W',  'E',  'R',  'T',  'Y',  'U',  'I',	// 0x10
	'O',  'P',  '{',  '}',  '\n', NO,   'A',  'S',
	'D',  'F',  'G',  'H',  'J',  'K',  'L',  ':',	// 0x20
	'"',  '~',  NO,   '|',  'Z',  'X',  'C',  'V',
	'B',  'N',  'M',  '<',  '>',  '?',  NO,   '*',	// 0x30
	NO,   ' ',  NO,   NO,   NO,   NO,   NO,   NO,
	NO,   NO,   NO,   NO,   NO,   NO,   NO,   '7',	// 0x40
	'8',  '9',  '-',  '4',  '5',  '6',  '+',  '1',
	'2',  '3',  '0',  '.',  NO,   NO,   NO,   NO,	// 0x50
	[0xC7] = KEY_HOME,	      [0x9C] = '\n' /*KP_Enter*/,
	[0xB5] = '/' /*KP_Div*/,      [0xC8] = KEY_UP,
	[0xC9] = KEY_PGUP,	      [0xCB] = KEY_LF,
	[0xCD] = KEY_RT,	      [0xCF] = KEY_END,
	[0xD0] = KEY_DN,	      [0xD1] = KEY_PGDN,
	[0xD2] = KEY_INS,	      [0xD3] = KEY_DEL
};

#define C(x) (x - '@')

static uint8_t ctlmap[256] =
{
	NO,      NO,      NO,      NO,      NO,      NO,      NO,      NO,
	NO,      NO,      NO,      NO,      NO,      NO,      NO,      NO,
	C('Q'),  C('W'),  C('E'),  C('R'),  C('T'),  C('Y'),  C('U'),  C('I'),
	C('O'),  C('P'),  NO,      NO,      '\r',    NO,      C('A'),  C('S'),
	C('D'),  C('F'),  C('G'),  C('H'),  C('J'),  C('K'),  C('L'),  NO,
	NO,      NO,      NO,      C('\\'), C('Z'),  C('X'),  C('C'),  C('V'),
	C('B'),  C('N'),  C('M'),  NO,      NO,      C('/'),  NO,      NO,
	[0x97] = KEY_HOME,
	[0xB5] = C('/'),		[0xC8] = KEY_UP,
	[0xC9] = KEY_PGUP,		[0xCB] = KEY_LF,
	[0xCD] = KEY_RT,		[0xCF] = KEY_END,
	[0xD0] = KEY_DN,		[0xD1] = KEY_PGDN,
	[0xD2] = KEY_INS,		[0xD3] = KEY_DEL
};

static uint8_t *charcode[4] = {
	normalmap,
	shiftmap,
	ctlmap,
	ctlmap
};

/*
 * Get data from the keyboard.  If we finish a character, return it.  Else 0.
 * Return -1 if no data.
 */
static int
kbd_proc_data(void)
{
	int c;
	uint8_t data;
	static uint32_t shift;

	if ((inb(KBSTATP) & KBS_DIB) == 0)
		return -1;

	data = inb(KBDATAP);
	if (data == 0xE0) {
		// E0 escape character
		shift |= E0ESC;
		return 0;
	} else if (data & 0x80) {
		// Key released
		data = (shift & E0ESC ? data : data & 0x7F);
		shift &= ~(shiftcode[data] | E0ESC);
		return 0;
	} else if (shift & E0ESC) {
		// Last character was an E0 escape; or with 0x80
		data |= 0x80;
		shift &= ~E0ESC;
	}

	shift |= shiftcode[data];
	shift ^= togglecode[data];

	c = charcode[shift & (CTL | SHIFT)][data];
	if (shift & CAPSLOCK) {
		if ('a' <= c && c <= 'z')
			c += 'A' - 'a';
		else if ('A' <= c && c <= 'Z')
			c += 'a' - 'A';
	}

	// Process special keys
	// Ctrl-Alt-Del: reboot
	if (!(~shift & (CTL | ALT)) && c == KEY_DEL) {
		cprintf("Rebooting!\n");
		outb(0x92, 0x3); // courtesy of Chris Frost
	}

	return c;
}

void
kbd_intr(void)
{
	cons_intr(kbd_proc_data);
}

static void
kbd_init(void)
{
	// Drain the kbd buffer so that QEMU generates interrupts.
	kbd_intr();
	irq_setmask_8259A(irq_mask_8259A & ~(1<<1));
}



/***** General device-independent console code *****/
// Here we manage the console input buffer,
// where we stash characters received from the keyboard or serial port
// whenever the corresponding interrupt occurs.

#define CONSBUFSIZE 512

static struct {
	uint8_t buf[CONSBUFSIZE];
	uint32_t rpos;  /* next index to read */
	uint32_t wpos;  /* next index to write */
} cons;

#define CONS_BUFFER_GET_NR_READABLE_CHARS() ((cons.wpos - cons.rpos) % CONSBUFSIZE)
#define CONS_BUFFER_GET_NR_WRITABLE_CHARS() ((cons.rpos - cons.wpos) % CONSBUFSIZE)

#define CONS_BUFFER_READ_CURSOR_MOVE(nr_chars) do { \
		cons.rpos = (uint32_t)(( (int32_t)(cons.rpos) + (int32_t)(nr_chars) ) % (int32_t)CONSBUFSIZE); \
	} while(0)

// called by device interrupt routines to feed input characters
// into the circular console input buffer.
static void
cons_intr(int (*proc)(void))
{
	int c;

	while ((c = (*proc)()) != -1) {
		if (c == 0)
			continue;
		cons.buf[cons.wpos++] = c;
		if (cons.wpos == CONSBUFSIZE)
			cons.wpos = 0;

		// added by elad to check if it happens. TODO: how to prevent it? block progress?
		if (cons.wpos == cons.rpos) {
			panic("console buffer overflow (overwrite before read) !");
		}
	}
}

// return the next input character from the console, or 0 if none waiting
int
cons_getc_peekable(int inc_read_cursor)
{
	int c;

	// poll for any pending input characters,
	// so that this function works even when interrupts are disabled
	// (e.g., when called from the kernel monitor).
	serial_intr();
	kbd_intr();

	// grab the next character from the input buffer.
	if (cons.rpos != cons.wpos) {
		c = cons.buf[cons.rpos];
		if (inc_read_cursor) {
			cons.rpos++;
			if (cons.rpos == CONSBUFSIZE)
				cons.rpos = 0;
		}
		return c;
	}
	return 0;
}
int
cons_getc(void) {
	return cons_getc_peekable(1);
}

int32_t cons_move_input_buffer_cursor(int32_t nr_chars) {
	// fix input limit
	if (nr_chars >= CONSBUFSIZE) {
		nr_chars = CONSBUFSIZE-1;
	} else if (nr_chars <= -CONSBUFSIZE) {
		nr_chars = -CONSBUFSIZE+1;
	}

	if (nr_chars > 0) {
		nr_chars = min(nr_chars, (int32_t)CONS_BUFFER_GET_NR_READABLE_CHARS());
		CONS_BUFFER_READ_CURSOR_MOVE(nr_chars);
	} else {
		nr_chars = -1 * min((-nr_chars), (int32_t)CONS_BUFFER_GET_NR_WRITABLE_CHARS());
		CONS_BUFFER_READ_CURSOR_MOVE(nr_chars);
	}

	return nr_chars;
}

// output a character to the console
static void
cons_putc(int c)
{
	serial_putc(c);
	lpt_putc(c);
	cga_putc(c);
}

// initialize the console devices
void
cons_init(void)
{
	cga_init();
	kbd_init();
	serial_init();

	if (!serial_exists)
		cprintf("Serial port does not exist!\n");
}


// `High'-level console I/O.  Used by readline and cprintf.

void
cputchar(int c)
{
	cons_putc(c);
}

int
getchar_peekable(int inc_read_cursor)
{
	int c;

	while ((c = cons_getc_peekable(inc_read_cursor)) == 0)
		/* do nothing */;
	return c;
}

int
getchar(void) {
	/*
		Implement with explicit calling to `cons_getc()` to reach test stop breakpoint.
		Otherwise, the implementation should be just: return getchar_peekable(1);
	*/

	int c;

	while ((c = cons_getc()) == 0)
		/* do nothing */;
	return c;
}

/*
	Used in order to get the 
*/
extern struct keypress serial_get_key_pressed(void); /* impl in kern/io/serial_console_io.c */
struct keypress get_key_pressed(void)
{
	// TODO: we have to distinguish between kbd and serial interrupt.
	return serial_get_key_pressed();
}

int
iscons(int fdnum)
{
	// used by readline
	return GET_ECHO_TO_SCREEN();
}

void clr_screen() {
     cprintf("\033c"); // clean the screen
}

int get_serial_terminal_size(size_t *rows, size_t *cols) {
     int nread;
     SET_ECHO_TO_SCREEN(0);
     cprintf("\0337\033[r\033[999;999H\033[6n\0338"); // get the screen size

     nread = cscanf("\033[%u;%uR", rows, cols);

     SET_ECHO_TO_SCREEN(1);
     return (nread == 2 ? 0 : -1);
}
