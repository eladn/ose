// Simple implementation of cscanf console output for the kernel,
// based on scanfmt() and the kernel console's getchar().

#include <inc/types.h>
#include <inc/stdio.h>
#include <inc/stdarg.h>


static int
getch(int *cnt, int inc_read_cursor)
{
	int ch = getchar_peekable(inc_read_cursor);
	*cnt++;
	return ch;
}

int
vcscanf(const char *fmt, va_list ap)
{
	int cnt = 0;

	vscanfmt((void*)getch, &cnt, fmt, ap);
	return cnt;
}

int
cscanf(const char *fmt, ...)
{
	va_list ap;
	int cnt;

	va_start(ap, fmt);
	cnt = vcscanf(fmt, ap);
	va_end(ap);

	return cnt;
}

