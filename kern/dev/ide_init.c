#include <inc/x86.h>
#include <inc/assert.h>
#include <inc/string.h>
#include <kern/pci.h>
#include <fs/idereg.h>
#include <inc/error.h>
#include <kern/pmap.h>
#include <kern/trap.h>
#include <kern/picirq.h>
#include <kern/env.h>

struct ide_channel *idec = NULL;

int
ide_init(struct pci_func *pcif)
{
    struct PageInfo *pp;
    pp = page_alloc(0);
    if (pp == NULL)
        return -E_NO_MEM;

    idec = page2kva(pp);
    memset(idec, 0, sizeof(struct ide_channel));
    static_assert(PGSIZE >= sizeof(*idec));
    pci_func_enable(pcif);
    if(1) // TODO: use debug flag
        cprintf("  ide_init: pcif->reg_base[0]: 0x%x, pcif->reg_base[1]: 0x%x\n",
                pcif->reg_base[0], pcif->reg_base[1]+2);

    // Use the first IDE channel on the IDE controller
    idec->cmd_addr = pcif->reg_size[0] ? pcif->reg_base[0] : 0x1f0;
    idec->ctl_addr = pcif->reg_size[1] ? pcif->reg_base[1] + 2 : 0x3f6;
    idec->bm_addr = pcif->reg_base[4];
    idec->irq = IRQ_IDE;  // TODO: distingish between 14 and 15.

    cprintf("  ide_init: cmd_addr: 0x%x, ctl_addr: 0x%x, bm_addr: 0x%x, irq: %d\n",
            idec->cmd_addr, idec->ctl_addr, idec->bm_addr, idec->irq);

    // Enable IDE irq
    cprintf("   Setup IDE interrupt via 8259A\n");
    irq_setmask_8259A(irq_mask_8259A & ~(1<<IRQ_IDE));
    cprintf("   unmasked IDE interrupt\n");

    // Note: we did not initialize the ide channel! it is done later by env fs.

    return 1;
}

int sys_map_ide_channel(void *dstva) {
    int err;
    assert(curenv != NULL);

    // Only FS environments can ask for ide channel mappings
    if (curenv->env_type != ENV_TYPE_FS)
        return -E_NO_PERM;

    // idec is not set yet by the system
    if (idec == NULL) return -E_INVAL;  // TOOD: return more informative

    if (((uintptr_t)dstva) >= UTOP || !IS_ALIGNED_TO_PAGE(dstva)) {
        return -E_INVAL;
    }

    err = page_insert(curenv->env_pgdir, kva2page(idec), dstva, PTE_P|PTE_U|PTE_W);
    if (err < 0) {
        assert(err == -E_NO_MEM);
        return err;
    }

    return 0;
}
