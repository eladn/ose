#include <fs/idereg.h>
#include <kern/trap.h>
#include <kern/env.h>
#include <kern/picirq.h>
#include <kern/cpu.h>
#include <inc/assert.h>
#include <inc/x86.h>

inline static void
ide_dma_irqack(struct ide_channel *idec)
{
    outb(idec->bm_addr + IDE_BM_STAT_REG,
         inb(idec->bm_addr + IDE_BM_STAT_REG));
}


void ide_irq_trap(struct Trapframe *tf) {
	//cprintf("!!!!!!!!!!!!!!!!!!!!! IDE IRQ !!!!!!!!!!!!!!!!!!!!!!\n");

	// Expect IRQ no 14 (we currenly do not support 15 from the other bus).
	// TODO: we might consider loading the right ide channel (idec) matching the IRQ no (14/15).
	assert(tf->tf_trapno == IRQ_OFFSET + IRQ_IDE);

	struct Env *env;
	envid_t fsenvid = env_find_envid_by_type(ENV_TYPE_FS);
	assert(fsenvid != 0);
	env = &envs[ENVX(fsenvid)];
	assert(env->env_id == fsenvid);

	extern struct ide_channel *idec;
	assert(idec != NULL);  // it should be already assigned here!

	if (idec->irq_wait == 0) {
		inb(idec->cmd_addr + IDE_REG_STATUS);
        assert((inb(idec->ctl_addr) & (IDE_CTL_NIEN)) == 0); // interrupts enabled
        goto exit;
    }

    uint8_t bm_stat;
    if (((bm_stat = inb(idec->bm_addr + IDE_BM_STAT_REG)) & IDE_BM_STAT_INTR) == 0) {
    	goto exit;
    }
    if (bm_stat & IDE_BM_STAT_ACTIVE) {
    	goto exit;
    }

    xchg(&idec->irq_wait, 0);
	env->env_status = ENV_RUNNABLE;
	assert((inb(idec->ctl_addr) & (IDE_CTL_NIEN)) == 0); // interrupts enabled
	
exit:
	ide_dma_irqack(idec);
	outb(IO_PIC2, 0x20);
    lapic_eoi();
	return;
}

void ide_irq_test_for_nonwakable_sleep() {
	struct Env *env;
	envid_t fsenvid = env_find_envid_by_type(ENV_TYPE_FS);
	assert(fsenvid != 0);
	env = &envs[ENVX(fsenvid)];
	assert(env->env_id == fsenvid);

	extern struct ide_channel *idec;
	assert(idec != NULL);  // it should be already assigned here!

	if (!idec->irq_wait || env->env_status == ENV_RUNNABLE) return;

	int bm_stat;
	if (((bm_stat = inb(idec->bm_addr + IDE_BM_STAT_REG)) & IDE_BM_STAT_ACTIVE) == 0) {
    	idec->irq_wait = 0;
		env->env_status = ENV_RUNNABLE;
    }
}
