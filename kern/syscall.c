/* See COPYRIGHT for copyright information. */

#include <inc/x86.h>
#include <inc/error.h>
#include <inc/string.h>
#include <inc/assert.h>
#include <inc/elf.h>
#include <inc/types.h>
#include <inc/cloak.h>

#include <kern/env.h>
#include <kern/pmap.h>
#include <kern/trap.h>
#include <kern/syscall.h>
#include <kern/console.h>
#include <kern/sched.h>
#include <kern/time.h>
#include <fs/idereg.h>

#define CRUCIAL_SYSCALL_PTE_FLAGS (PTE_U | PTE_P)
#define IS_VALID_SYSCALL_PTE_FLAGS(perm) \
	( ((perm) & ~PTE_SYSCALL) == 0 \
		&& ((perm) & CRUCIAL_SYSCALL_PTE_FLAGS) == CRUCIAL_SYSCALL_PTE_FLAGS )

#define INVALID_ENV_IPC_DSTVA ((void*)(~((uintptr_t)0)))

// Print a string to the system console.
// The string is exactly 'len' characters long.
// Destroys the environment on memory errors.
void
sys_cputs(const char *s, size_t len)
{
	// Check that the user has permission to read memory [s, s+len).
	// Destroy the environment if not.
	user_mem_assert(curenv, s, len, 0);

	// LAB 3: Your code here.

	// Print the string supplied by the user.
	cprintf("%.*s", len, s);
}

// Read a character from the system console without blocking.
// Returns the character, or 0 if there is no input waiting.
int
sys_cgetc(void)
{
	return cons_getc();
}

int
sys_cgetc_peekable(int inc_read_cursor) {
	return getchar_peekable(inc_read_cursor);
}

// Returns the current environment's envid.
envid_t
sys_getenvid(void)
{
	return curenv->env_id;
}

// Destroy a given environment (possibly the currently running environment).
//
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist,
//		or the caller doesn't have permission to change envid.
int
sys_env_destroy(envid_t envid)
{
	int r;
	struct Env *e;

	if ((r = envid2env(envid, &e, 1)) < 0)
		return r;
	env_destroy(e);
	return 0;
}

// Deschedule current environment and pick a different one to run.
void
sys_yield(void)
{
	sched_yield();
}

// Allocate a new environment.
// Returns envid of new environment, or < 0 on error.  Errors are:
//	-E_NO_FREE_ENV if no free environment is available.
//	-E_NO_MEM on memory exhaustion.
envid_t
sys_exofork(void)
{
	// Create the new environment with env_alloc(), from kern/env.c.
	// It should be left as env_alloc created it, except that
	// status is set to ENV_NOT_RUNNABLE, and the register set is copied
	// from the current environment -- but tweaked so sys_exofork
	// will appear to return 0.

	// LAB 4: Your code here.
	assert(curenv);
	struct Env *newenv;
	int err;
	err = env_alloc(&newenv, curenv->env_id);
	if (err < 0) {
		assert(err == -E_NO_FREE_ENV || err == -E_NO_MEM);
		return err;
	}
	newenv->env_status = ENV_NOT_RUNNABLE;
	newenv->env_tf = curenv->env_tf;
	newenv->env_tf.tf_regs.reg_eax = 0;

	assert(curenv->env_type < ENV_NTYPES);
	if (curenv->env_type != ENV_TYPE_USER && env_type_helpers[curenv->env_type] != 0) {
		newenv->env_type = env_type_helpers[curenv->env_type];
	}

	return newenv->env_id;
}

// Set envid's env_status to status, which must be ENV_RUNNABLE
// or ENV_NOT_RUNNABLE.
//
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist,
//		or the caller doesn't have permission to change envid.
//	-E_INVAL if status is not a valid status for an environment.
int
sys_env_set_status(envid_t envid, int status)
{
	// Hint: Use the 'envid2env' function from kern/env.c to translate an
	// envid to a struct Env.
	// You should set envid2env's third argument to 1, which will
	// check whether the current environment has permission to set
	// envid's status.

	// LAB 4: Your code here.
	if (status != ENV_RUNNABLE && status != ENV_NOT_RUNNABLE){
		return -E_INVAL;
	}
	struct Env *env;
	int err;
	err = envid2env(envid, &env, 1);
	if (err < 0){
		assert(err == -E_BAD_ENV);
		return err;
	}
	assert(env != NULL);
	
	env->env_status = status;

	return 0;
}

int sys_env_set_data(envid_t envid, void *data) {
	struct Env *env;
	int err;
	err = envid2env(envid, &env, 1);
	if (err < 0){
		assert(err == -E_BAD_ENV);
		return err;
	}
	assert(env != NULL);

	env->data = data;

	return 0;
}

// Set envid's trap frame to 'tf'.
// tf is modified to make sure that user environments always run at code
// protection level 3 (CPL 3) with interrupts enabled.
//
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist,
//		or the caller doesn't have permission to change envid.
int
sys_env_set_trapframe(envid_t envid, struct Trapframe *tf)
{
	// LAB 5: Your code here.
	// Remember to check whether the user has supplied us with a good
	// address!
	struct Env *env;
	int err;
	err = envid2env(envid, &env, 1);
	if (err < 0){
		assert(err == -E_BAD_ENV);
		return err;
	}
	assert(env != NULL);
	user_mem_assert(env, tf, sizeof(*tf), 0);

	env->env_tf = *tf;
	env->env_tf.tf_eflags |= FL_IF; // turn on interrupts.

	// set env's CPL to 3 for every segment.
	env->env_tf.tf_cs |= 3;
	env->env_tf.tf_ds |= 3;
	env->env_tf.tf_es |= 3;
	env->env_tf.tf_ss |= 3;

	return 0;
}

// Set the page fault upcall for 'envid' by modifying the corresponding struct
// Env's 'env_pgfault_upcall' field.  When 'envid' causes a page fault, the
// kernel will push a fault record onto the exception stack, then branch to
// 'func'.
//
// Returns 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist,
//		or the caller doesn't have permission to change envid.
int
sys_env_set_pgfault_upcall(envid_t envid, void *func)
{
	// LAB 4: Your code here.
	struct Env *env;
	int err;
	err = envid2env(envid, &env, 1);
	if (err < 0){
		assert(err == -E_BAD_ENV);
		return err;
	}
	assert(env != NULL);

	// TODO: it doesn't say to check it, but we should. what to do here in that way?
	user_mem_assert(env, func, 4/*TODO: what should be here?*/, 0);

	env->env_pgfault_upcall = func;
	return 0;
}

// Allocate a page of memory and map it at 'va' with permission
// 'perm' in the address space of 'envid'.
// The page's contents are set to 0.
// If a page is already mapped at 'va', that page is unmapped as a
// side effect.
//
// perm -- PTE_U | PTE_P must be set, PTE_AVAIL | PTE_W may or may not be set,
//         but no other bits may be set.  See PTE_SYSCALL in inc/mmu.h.
//
// Return 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist,
//		or the caller doesn't have permission to change envid.
//	-E_INVAL if va >= UTOP, or va is not page-aligned.
//	-E_INVAL if perm is inappropriate (see above).
//	-E_NO_MEM if there's no memory to allocate the new page,
//		or to allocate any necessary page tables.
int
sys_page_alloc(envid_t envid, void *va, int perm)
{
	// Hint: This function is a wrapper around page_alloc() and
	//   page_insert() from kern/pmap.c.
	//   Most of the new code you write should be to check the
	//   parameters for correctness.
	//   If page_insert() fails, remember to free the page you
	//   allocated!

	// LAB 4: Your code here.
	if (((uintptr_t)va) >= UTOP || !IS_ALIGNED_TO_PAGE(va)) {
		return -E_INVAL;
	}
	if (!IS_VALID_SYSCALL_PTE_FLAGS(perm)) {
		return -E_INVAL;
	}
	struct Env *env;
	int err;
	err = envid2env(envid, &env, 1);
	if (err < 0) {
		assert(err == -E_BAD_ENV);
		return err;
	}
	assert(env != NULL);

	struct PageInfo *p = NULL;
	if (!(p = page_alloc(1)))
		return -E_NO_MEM;

	if ((err = page_insert(env->env_pgdir, p, va, perm)) < 0) {
		assert(err == -E_NO_MEM);
		page_free(p);
		return err;
	}

	return 0;
}

// Map the page of memory at 'srcva' in srcenvid's address space
// at 'dstva' in dstenvid's address space with permission 'perm'.
// Perm has the same restrictions as in sys_page_alloc, except
// that it also must not grant write access to a read-only
// page.
//
// Return 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if srcenvid and/or dstenvid doesn't currently exist,
//		or the caller doesn't have permission to change one of them.
//	-E_INVAL if srcva >= UTOP or srcva is not page-aligned,
//		or dstva >= UTOP or dstva is not page-aligned.
//	-E_INVAL is srcva is not mapped in srcenvid's address space.
//	-E_INVAL if perm is inappropriate (see sys_page_alloc).
//	-E_INVAL if (perm & PTE_W), but srcva is read-only in srcenvid's
//		address space.
//	-E_NO_MEM if there's no memory to allocate any necessary page tables.
int
sys_page_map(envid_t srcenvid, void *srcva,
	     envid_t dstenvid, void *dstva, int perm)
{
	// Hint: This function is a wrapper around page_lookup() and
	//   page_insert() from kern/pmap.c.
	//   Again, most of the new code you write should be to check the
	//   parameters for correctness.
	//   Use the third argument to page_lookup() to
	//   check the current permissions on the page.

	// LAB 4: Your code here.
	if (((uintptr_t)srcva) >= UTOP || !IS_ALIGNED_TO_PAGE(srcva)) {
		return -E_INVAL;
	}
	if (((uintptr_t)dstva) >= UTOP || !IS_ALIGNED_TO_PAGE(dstva)) {
		return -E_INVAL;
	}
	if (!IS_VALID_SYSCALL_PTE_FLAGS(perm)) {
		return -E_INVAL;
	}
	struct Env *srcenv, *dstenv;
	int err;
	err = envid2env(srcenvid, &srcenv, 1);
	if (err < 0) {
		assert(err == -E_BAD_ENV);
		return err;
	}
	err = envid2env(dstenvid, &dstenv, 1);
	if (err < 0) {
		assert(err == -E_BAD_ENV);
		return err;
	}
	assert(srcenv != NULL && dstenv != NULL);

	pte_t *pte = NULL;
	struct PageInfo *page_info = page_lookup(srcenv->env_pgdir, srcva, &pte);
	if (page_info == NULL || pte == NULL) {
		return -E_INVAL;
	}
	assert(PTE_IS_MAPPED(*pte));
	if ((PTE_FLAGS(*pte) & PTE_W) == 0 && (perm & PTE_W)) {
		//return -E_INVAL;
	}

	err = page_insert(dstenv->env_pgdir, page_info, dstva, perm);
	if (err < 0) {
		assert(err == -E_NO_MEM);
		return err;
	}

	return 0;
}

// Unmap the page of memory at 'va' in the address space of 'envid'.
// If no page is mapped, the function silently succeeds.
//
// Return 0 on success, < 0 on error.  Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist,
//		or the caller doesn't have permission to change envid.
//	-E_INVAL if va >= UTOP, or va is not page-aligned.
int
sys_page_unmap(envid_t envid, void *va)
{
	// Hint: This function is a wrapper around page_remove().

	// LAB 4: Your code here.
	if (((uintptr_t)va) >= UTOP || !IS_ALIGNED_TO_PAGE(va)) {
		return -E_INVAL;
	}

	struct Env *env;
	int err;
	err = envid2env(envid, &env, 1);
	if (err < 0) {
		assert(err == -E_BAD_ENV);
		return err;
	}
	assert(env != NULL);

	page_remove(env->env_pgdir, va);

	return 0;
}

static ipc_msg_t * get_ipc_recv_msg_waiting_for_sender(struct Env * to_env, envid_t only_from /*0 for anyone*/) {
	assert(to_env);
	ipc_msg_t *msg = NULL;
	int i;
	for (i = 0; i < IPC_RECV_MSGS_PER_ENV; ++i) {
		msg = &to_env->env_ipc_recv_msgs[i];
		if (msg->waiting_on == ENV_IPC_WAITING_FOR_SENDER && msg->from == only_from)
			return msg;
	}
	for (i = 0; i < IPC_RECV_MSGS_PER_ENV; ++i) {
		msg = &to_env->env_ipc_recv_msgs[i];
		if (msg->waiting_on == ENV_IPC_WAITING_FOR_SENDER && msg->from == 0)
			return msg;
	}
	return NULL;
}

static ipc_msg_t * consume_ipc_recv_msg(struct Env * to_env, envid_t only_from /*0 for anyone*/) {
	assert(to_env);
	ipc_msg_t *msg = NULL;
	int i;
	for (i = 0; i < IPC_RECV_MSGS_PER_ENV; ++i) {
		msg = &to_env->env_ipc_recv_msgs[i];
		if (msg->waiting_on == ENV_IPC_WAITING_FOR_RECEIVER
			&& (only_from == 0 || msg->from == only_from))
			return msg;
	}
	return NULL;
}

static ipc_msg_t * get_free_ipc_recv_msg(struct Env * to_env, int async) {
	assert(to_env);
	ipc_msg_t *msg = NULL;
	int i;

	// The first place in the `env_ipc_recv_msgs` array is always saved
	//    for blocking-receivers (nonasync).
	if (!async) {
		msg = &curenv->env_ipc_recv_msgs[0];
		assert(msg->waiting_on == ENV_IPC_NOT_WAITING);
		return msg;
	}

	// If async, we start from 1 because the first place in the
	// `env_ipc_recv_msgs` array is saved for blocking-receivers.
	for (i = 1; i < IPC_RECV_MSGS_PER_ENV; ++i) {
		msg = &to_env->env_ipc_recv_msgs[i];
		if (msg->waiting_on == ENV_IPC_NOT_WAITING)
			return msg;
	}

	return NULL;
}

// Try to send 'value' to the target env 'envid'.
// If srcva < UTOP, then also send page currently mapped at 'srcva',
// so that receiver gets a duplicate mapping of the same page.
//
// The send fails with a return value of -E_IPC_NOT_RECV if the
// target is not blocked, waiting for an IPC.
//
// The send also can fail for the other reasons listed below.
//
// Otherwise, the send succeeds, and the target's ipc fields are
// updated as follows:
//    env_ipc_recving is set to 0 to block future sends;
//    env_ipc_from is set to the sending envid;
//    env_ipc_value is set to the 'value' parameter;
//    env_ipc_perm is set to 'perm' if a page was transferred, 0 otherwise.
// The target environment is marked runnable again, returning 0
// from the paused sys_ipc_recv system call.  (Hint: does the
// sys_ipc_recv function ever actually return?)
//
// If the sender wants to send a page but the receiver isn't asking for one,
// then no page mapping is transferred, but no error occurs.
// The ipc only happens when no errors occur.
//
// Returns 0 on success, < 0 on error.
// Errors are:
//	-E_BAD_ENV if environment envid doesn't currently exist.
//		(No need to check permissions.)
//	-E_IPC_NOT_RECV if envid is not currently blocked in sys_ipc_recv,
//		or another environment managed to send first.
//	-E_INVAL if srcva < UTOP but srcva is not page-aligned.
//	-E_INVAL if srcva < UTOP and perm is inappropriate
//		(see sys_page_alloc).
//	-E_INVAL if srcva < UTOP but srcva is not mapped in the caller's
//		address space.
//	-E_INVAL if (perm & PTE_W), but srcva is read-only in the
//		current environment's address space.
//	-E_NO_MEM if there's not enough memory to map srcva in envid's
//		address space.
int
sys_ipc_try_send(envid_t envid, uint32_t value, void *srcva, unsigned perm)
{
	// LAB 4: Your code here.
	int r;
	int flg_send_page = (((uintptr_t)srcva) < UTOP);
	int flg_receive_page = 0;
	pte_t *pte = NULL;
	struct PageInfo *page_info = NULL;
	struct Env *env = NULL;

	int err;
	err = envid2env(envid, &env, 0);
	if (err < 0) {
		assert(err == -E_BAD_ENV);
		return err;
	}
	assert(env != NULL);

	if (/*env->env_status != ENV_NOT_RUNNABLE ||*/ !env->env_ipc_recving) {
	// || (env->env_ipc_from != 0 /*allow ipc from anyone*/ && env->env_ipc_from != curenv->env_id)
		return -E_IPC_NOT_RECV;
	}

	ipc_msg_t *msg = get_ipc_recv_msg_waiting_for_sender(env, curenv->env_id);
	if (!msg) return -E_FULL; // TODO: check it in spinning lib `ipc_send()`.

	msg->perm = 0;

	flg_receive_page = (msg->dstva != INVALID_ENV_IPC_DSTVA);
	// TODO: do i have to check it also when !flg_receive_page ??
	if (flg_send_page) {
		if (!IS_ALIGNED_TO_PAGE(srcva) || !IS_VALID_SYSCALL_PTE_FLAGS(perm)) {
			return -E_INVAL;
		}
		page_info = page_lookup(curenv->env_pgdir, srcva, &pte);
		if (page_info == NULL || pte == NULL) {
			return -E_INVAL;
		}
		if ((perm & PTE_W) && (PTE_FLAGS(*pte) & PTE_W) != PTE_W) {
			return -E_INVAL;
		}
		if (flg_receive_page) {
			assert(IS_ALIGNED_TO_PAGE(msg->dstva));
			if ((r = page_insert(env->env_pgdir, page_info, msg->dstva, perm)) < 0) {
				assert(r == -E_NO_MEM);
				return r;
			}
			// Page insert automatically gives PTE_P to dstva's pte.
			// Later (in the async receive user lib function) we use 
			// the assumption that the sender shared a page with us
			// iff msg->perm is not zero.
			msg->perm = perm | PTE_P;
		}
		
	}

	// Note: It is important NOT to zero the dstva, even if the sender
	//       did not shared a page with us.

	//cprintf("sys_ipc_try_send() to env: %x from env: %x value: %d perm: %x flg_send_page: %d flg_receive_page: %d\n", envid, curenv->env_id, value, perm, flg_send_page, flg_receive_page);

	msg->waiting_on = ENV_IPC_WAITING_FOR_RECEIVER;
	msg->from = curenv->env_id;
	msg->value = value;
	env->env_ipc_recving--;

	// The first place in the `env_ipc_recv_msgs` array is saved for blocking-receivers.
	// If we sent to blocking sender, we have to wake it up,
	//    and set the waiting status to non-waiting (effectively "consume" the
	//    message now for the receiver).
	if (msg->blocked_receive && msg == &env->env_ipc_recv_msgs[0]) {
		msg->waiting_on = ENV_IPC_NOT_WAITING;
		env->env_status = ENV_RUNNABLE;
	}

	return 0;
}

// Block until a value is ready.  Record that you want to receive
// using the env_ipc_recving and env_ipc_dstva fields of struct Env,
// mark yourself not runnable, and then give up the CPU.
//
// If 'dstva' is < UTOP, then you are willing to receive a page of data.
// 'dstva' is the virtual address at which the sent page should be mapped.
//
// This function only returns on error, but the system call will eventually
// return 0 on success.
// Return < 0 on error.  Errors are:
//	-E_INVAL if dstva < UTOP but dstva is not page-aligned.
int
sys_ipc_recv_from_env(void *dstva, envid_t only_from_env, int async)
{
	assert(curenv);

	// LAB 4: Your code here.
	if ((uintptr_t)dstva < UTOP) {
		if (!IS_ALIGNED_TO_PAGE(dstva)) {
			return -E_INVAL;
		}
		//curenv->env_ipc_dstva dstva = dstva;
	} else {
		//curenv->env_ipc_dstva = INVALID_ENV_IPC_DSTVA;
		dstva = INVALID_ENV_IPC_DSTVA;
	}

	ipc_msg_t *msg = get_free_ipc_recv_msg(curenv, async);
	if (!msg) {
		assert(async);
		return -E_FULL;
	}

	// first msg always available to nonasync receive
	// the sender sets it to `ENV_IPC_NOT_WAITING` after sending.
	assert(msg->waiting_on == ENV_IPC_NOT_WAITING);

	msg->waiting_on = ENV_IPC_WAITING_FOR_SENDER;
	msg->from = only_from_env;
	msg->dstva = dstva;
	msg->blocked_receive = (!async);

	curenv->env_ipc_recving++;

	// if the receive is not async - we block the env till it gets a msg.
	if (!async) {
		curenv->env_tf.tf_regs.reg_eax = 0;  /* simulate return 0 from this syscall */
		curenv->env_status = ENV_NOT_RUNNABLE;

		sys_yield(); /* does not return */
		assert(0);
	}

	return 0;
}
int
sys_ipc_recv(void *dstva)
{
	return sys_ipc_recv_from_env(dstva, 0 /*allow ipc from anyone*/, 0 /* block - not async */);
}

int
sys_ipc_recv_async_consume(envid_t only_from /*0 for anyone*/, envid_t *from_env_store, 
						   int *value_store, void **pg_store, int *perm_store)
{
	ipc_msg_t * msg = consume_ipc_recv_msg(curenv, only_from);
	if (!msg) return -E_NO_AVAIL;

	user_mem_assert(curenv, from_env_store, sizeof(*from_env_store), (PTE_P|PTE_U|PTE_W));
	user_mem_assert(curenv, value_store, sizeof(*value_store), (PTE_P|PTE_U|PTE_W));
	user_mem_assert(curenv, pg_store, sizeof(*pg_store), (PTE_P|PTE_U|PTE_W));
	user_mem_assert(curenv, perm_store, sizeof(*perm_store), (PTE_P|PTE_U|PTE_W));

	*from_env_store = msg->from;
	*value_store = msg->value;
	*pg_store = msg->dstva;
	*perm_store = msg->perm;

	memset(msg, 0, sizeof(ipc_msg_t));
	msg->waiting_on = ENV_IPC_NOT_WAITING;

	return 0;
}

// Calls to multiple syscalls sequently.
// Receives an array of syscall frames, that each frame defines the syscall number,
//   the given parametes, and has a place for the return value (if such expected).
// The `stop_on_fail` flag means whether to stop calling the next call upon on error.
// Returns the number of syscalls that has been called.
// Stores the return values of the called syscalls in `calls_frame[i]->retval`.
int
sys_multiple_syscalls(syscall_frame_t* calls_frame, size_t nr_calls, int stop_on_fail) {
	size_t cur_syscall_idx;
	syscall_frame_t* frame;

	assert(curenv != NULL);
	user_mem_assert(curenv, calls_frame, sizeof(*calls_frame)*nr_calls, (PTE_U | PTE_P | PTE_W));
	
	for (cur_syscall_idx = 0; cur_syscall_idx < nr_calls; ++cur_syscall_idx) {
		frame = &calls_frame[cur_syscall_idx];
		frame->retval = syscall(frame->syscallno, 
			frame->a1, frame->a2, frame->a3, frame->a4, frame->a5);
		if (stop_on_fail && frame->retval < 0) {
			return (int)(cur_syscall_idx+1);
		}
	}
	return (int)cur_syscall_idx;
}

int
sys_exec(uint32_t eip, uint32_t esp, void * v_ph, uint32_t phnum)
{

	curenv->env_tf.tf_eip = eip;
	curenv->env_tf.tf_esp = esp;

	int perm, i;
	uint32_t tmp = ETEMP;
	uint32_t va, end;
	struct PageInfo * pg;

	struct Proghdr * ph = (struct Proghdr *) v_ph; 
	for (i = 0; i < phnum; i++, ph++) {
		if (ph->p_type != ELF_PROG_LOAD)
			continue;
		perm = PTE_P | PTE_U;
		if (ph->p_flags & ELF_PROG_FLAG_WRITE)
			perm |= PTE_W;

		end = ROUNDUP(ph->p_va + ph->p_memsz, PGSIZE);
		for (va = ROUNDDOWN(ph->p_va, PGSIZE); va != end; tmp += PGSIZE, va += PGSIZE) {
			if ((pg = page_lookup(curenv->env_pgdir, (void *)tmp, NULL)) == NULL) 
				return -E_NO_MEM;
			if (page_insert(curenv->env_pgdir, pg, (void *)va, perm) < 0)
				return -E_NO_MEM;
			page_remove(curenv->env_pgdir, (void *)tmp);
		}
	}

	if ((pg = page_lookup(curenv->env_pgdir, (void *)tmp, NULL)) == NULL) 
		return -E_NO_MEM;
	if (page_insert(curenv->env_pgdir, pg, (void *)(USTACKTOP - PGSIZE), PTE_P|PTE_U|PTE_W) < 0) 
		return -E_NO_MEM;
	page_remove(curenv->env_pgdir, (void *)tmp);
	
	env_run(curenv);// never return
	return 0; // just to make the compiler happy
}

int
sys_sleep_if_eq(uint32_t *a1, uint32_t *a2) {
	assert(curenv != NULL);
	user_mem_assert(curenv, a1, sizeof(uint32_t), (PTE_U | PTE_P));
	user_mem_assert(curenv, a2, sizeof(uint32_t), (PTE_U | PTE_P));
	if (*a1 != *a2) return 1;
	curenv->env_status = ENV_NOT_RUNNABLE;
	//sched_yield();
	return 0;
}

// Return the current time.
unsigned int
sys_time_msec(void)
{
	// LAB 6: Your code here.
	return time_msec();
}

int
sys_sleep_if_neq(uint32_t *a1, uint32_t *a2) {
	assert(curenv != NULL);
	user_mem_assert(curenv, a1, sizeof(uint32_t), (PTE_U | PTE_P));
	user_mem_assert(curenv, a2, sizeof(uint32_t), (PTE_U | PTE_P));
	if (*a1 == *a2) return 1;
	curenv->env_status = ENV_NOT_RUNNABLE;
	//sched_yield();
	return 0;
}

/* extern the syscall functions that may be defined in other .c file */
#define X(name, nparams, return_type, type1, type2, type3, type4, type5, gen_wrapper) \
	extern return_type SYSCALL_NAME2FUNC(name) CALL_WITH(nparams, type1, type2, type3, type4, type5);
SYSCALL_LIST
#undef X

// Dispatches to the correct kernel function, passing the arguments.
int32_t
syscall(uint32_t syscallno, uint32_t a1, uint32_t a2, uint32_t a3, uint32_t a4, uint32_t a5)
{
	// Call the function corresponding to the 'syscallno' parameter.
	// Return any appropriate return value.
	// LAB 3: Your code here.

	switch (syscallno) {
		#define COMPARE_void(x) x
		#define X(name, nparams, return_type, type1, type2, type3, type4, type5, gen_wrapper) \
			case SYSCALL_NAME2NUM(name): \
				WHEN(NOT_EQUAL(return_type, void))(return ) \
				(return_type)(SYSCALL_NAME2FUNC(name) \
					CALL_WITH(nparams, (type1)(a1), (type2)(a2), (type3)(a3), (type4)(a4), (type5)(a5))); \
			break;
		SYSCALL_LIST
		#undef X
		#undef COMPARE_void
	default:
		return -E_INVAL;
	}
	return 0;
}
