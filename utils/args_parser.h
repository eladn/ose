
#ifndef JOS_UTILS_CLI_ARGS_PARSER
#define JOS_UTILS_CLI_ARGS_PARSER

#include <inc/types.h>
#include <inc/utils.h>
#include <inc/assert.h>
#include <inc/addargs_to_macro.h>

/*
    Note: still under-construction.
*/

enum cli_parser_error_codes {
    CLI_E_OUTOFMEM = 1,
    CLI_E_OUTOFRANGE,
    CLI_E_OUTOFCHOISES,
    CLI_E_ARGTYPE,
    CLI_E_INVALIDNARGS,
    CLI_E_INVALIDSEPARATOR,
    CLI_E_UNKNOWNPARAM,
    CLI_E_ALREADYSET,
    CLI_E_PARREQ,
    CLI_E_PARREQGRP,
    CLI_E_NOTIMPL,
};

/*
    Used to define arguments structure of a command.
    TODO: write here full using instructions + examples!
*/

/*
    We define those prefixes to be nothing in order to revoke compile errors 
    after adding those as prefixes to each element in __VA_ARGS__ using `APPLYXn`.
    Sometimes trailing commas `,` with nothing after them adding redundant separate `R_` / `F_`
*/
#define R_
#define F_
#define IDX_

#define EVAL(...) __VA_ARGS__
#define ADD_R(...) \
    EVAL(PRIMITIVE_CAT(R_, __VA_ARGS__))
#define ADD_R__(...) \
    EVAL(PRIMITIVE_CAT__(R_, __VA_ARGS__))
#define ADD_F(...) \
    EVAL(PRIMITIVE_CAT(F_, __VA_ARGS__))
#define ADD_F__(...) \
    EVAL(PRIMITIVE_CAT__(F_, __VA_ARGS__))

/*
    Each one of the macros used inside `CLI_PAR` macro, have exactly 2 versions:
    one with `R_` prefix, and the other with `F_` prefix.
    For example, for `CLI_PAR_LNAMES` we have `R_CLI_PAR_LNAMES` and `F_CLI_PAR_LNAMES`.
    Anyway, for those macros (used inside `CLI_PAR`), we never define the actual macro,
    without a prefix (in the above example: `CLI_PAR_LNAMES` inself is not defined at all).
    That is because the preprocessor evaluates the macros from the inside to the outside,
    and we need to "re-define" the behaviour of the inner macros (ex: `CLI_PAR_LNAMES`) from the 
    outer macro (`COMMAND`), after it (the inner one) has been explicity wrote once without the prefix, 
    as an argument given to the outer macro ( example: OUTER(INNER(data)) ).
    In that way, the inner original written `CLI_PAR_LNAMES` is not defined at all, and hence not
    evaluated by the preprocessor when arriving as input args to the outer macro `COMMAND`, then, from the 
    outer-macro, we add different prefixes to those inner-macros and therefore make it finally evaluated by the 
    preprocessor and give it the wanted behaviour.
*/
/*#define ADD_R_PREFIX_TO_MACROS(T,N,cmd_name,...) PRIMITIVE_CAT(R_, __VA_ARGS__)
#define ADD_R_PREFIX_TO_MACROS_(T,N,cmd_name,...) PRIMITIVE_CAT__(R_, __VA_ARGS__)
#define ADD_F_PREFIX_TO_MACROS(T,N,cmd_name,...) PRIMITIVE_CAT(F_, __VA_ARGS__)
#define ADD_F_PREFIX_TO_MACROS_(T,N,cmd_name,...) PRIMITIVE_CAT__(F_, __VA_ARGS__)*/
#define ADD_R_PREFIX_TO_PARAM_MACRO(T,param_idx,cmd_name,...) ADD_R(AddArgs((cmd_name,param_idx), __VA_ARGS__))
#define WRAP_CURLY_BRACES_AND_ADD_R_PREFIX_TO_PARAM_MACRO(T,param_idx,cmd_name,...) { ADD_R_PREFIX_TO_PARAM_MACRO(T,param_idx,cmd_name,__VA_ARGS__) }, 
#define ADD_F_PREFIX_TO_PARAM_MACRO(T,param_idx,cmd_name,...) ADD_F(AddArgs((cmd_name,param_idx), __VA_ARGS__))
#define ADD_R_PREFIX_TO_PARAM_INNER_CONF_MACRO(T,N,cmd_name,...) ADD_R__(AddArgs(cmd_name, __VA_ARGS__))
#define ADD_F_PREFIX_TO_PARAM_INNER_CONF_MACRO(T,N,cmd_name,...) ADD_F__(AddArgs(cmd_name, __VA_ARGS__))

#define __CLI_PAR_NO_ARGS \
    .nargs = RANGE_INIT_BOTH(0,0)

#define __CLI_PAR_OPTIONAL \
    .require_group_idx = CLI_OPTIONAL_VAL
#define __CLI_PAR_REQUIRED \
    .require_group_idx = CLI_REQUIRED_VAL

#define __CLI_PAR_INITIAL_FIELDS \
    __CLI_PAR_NO_ARGS, \
    __CLI_PAR_OPTIONAL,


#define R_CLI_PAR_SEPARATOR(cmd_name,param_idx,...) \
    .is_separator = 1, 
#define F_CLI_PAR_SEPARATOR(cmd_name,param_idx,...) \
    struct cli_param_arguments SEPARATOR__ ## param_idx;
#define WRAP_CLI_PAR_SEPARATOR(...) ApplyMacro(CLI_PAR_SEPARATOR, __VA_ARGS__)


#define R_CLI_PAR(cmd_name,param_idx, ...) \
    __CLI_PAR_INITIAL_FIELDS \
    APPLYX_n(ADD_R_PREFIX_TO_PARAM_INNER_CONF_MACRO,cmd_name,__VA_ARGS__)
#define F_CLI_PAR(cmd_name,param_idx, ...) APPLYX_n(ADD_F_PREFIX_TO_PARAM_INNER_CONF_MACRO,cmd_name,__VA_ARGS__)
#define WRAP_CLI_PAR(...) ApplyMacro(CLI_PAR, __VA_ARGS__)


/*   < param inner configuration macros >   */

#define R_CLI_PAR_DESCRIPTION(cmd_name,text) \
    .description = text,
#define F_CLI_PAR_DESCRIPTION(cmd_name,text) 
#define WRAP_CLI_PAR_DESCRIPTION(...) ApplyMacro(CLI_PAR_DESCRIPTION, __VA_ARGS__)

#define R_CLI_PAR_LNAMES(cmd_name,...) \
    .long_names = {STRINGIFY_AND_COMMA___n(__VA_ARGS__)}, \
    /*.arg_field_nr = (uintptr_t)(&(((struct cmd_args_##cmd_name *)(0))->FIRST_ARG(__VA_ARGS__))), */
#define F_CLI_PAR_LNAMES(cmd_name,...) \
    struct cli_param_arguments FIRST_ARG(__VA_ARGS__);
#define WRAP_CLI_PAR_LNAMES(...) ApplyMacro(CLI_PAR_LNAMES, __VA_ARGS__)

#define R_CLI_PAR_SNAMES(cmd_name,...) \
    .short_names = { __VA_ARGS__ },
#define F_CLI_PAR_SNAMES(...) 
#define WRAP_CLI_PAR_SNAMES(...) ApplyMacro(CLI_PAR_SNAMES, __VA_ARGS__)

#define R_CLI_PAR_ARG_ADDR(cmd_name, ...) \
    .numeric_attr.default_numeric_base = 16, \
    .args_val_type = CLI_ARG_TYPE_INT_RANGE, 
#define F_CLI_PAR_ARG_ADDR(cmd_name, ...)
#define WRAP_CLI_PAR_ARG_ADDR(...) ApplyMacro(CLI_PAR_ARG_ADDR, __VA_ARGS__)

#define R_CLI_PAR_ARG_INT(cmd_name, ...) \
    .args_val_type = CLI_ARG_TYPE_INT,
#define R_CLI_PAR_ARG_FLOAT(cmd_name, ...) \
    .args_val_type = CLI_ARG_TYPE_FLOAT,
#define R_CLI_PAR_ARG_BOOL(cmd_name, ...) \
    .args_val_type = CLI_ARG_TYPE_BOOL,
#define R_CLI_PAR_ARG_TEXT(cmd_name, ...) \
    .args_val_type = CLI_ARG_TYPE_TEXT,
#define R_CLI_PAR_ARG_INT_RANGE(cmd_name, ...) \
    .args_val_type = CLI_ARG_TYPE_INT_RANGE,
#define R_CLI_PAR_ARG_FLOAT_RANGE(cmd_name, ...) \
    .args_val_type = CLI_ARG_TYPE_FLOAT_RANGE,
#define F_CLI_PAR_ARG_INT(cmd_name, ...)
#define F_CLI_PAR_ARG_FLOAT(cmd_name, ...)
#define F_CLI_PAR_ARG_BOOL(cmd_name, ...)
#define F_CLI_PAR_ARG_TEXT(cmd_name, ...)
#define F_CLI_PAR_ARG_INT_RANGE(cmd_name, ...)
#define F_CLI_PAR_ARG_FLOAT_RANGE(cmd_name, ...)
#define WRAP_CLI_PAR_ARG_INT(...) ApplyMacro(CLI_PAR_ARG_INT, __VA_ARGS__)
#define WRAP_CLI_PAR_ARG_FLOAT(...) ApplyMacro(CLI_PAR_ARG_FLOAT, __VA_ARGS__)
#define WRAP_CLI_PAR_ARG_BOOL(...) ApplyMacro(CLI_PAR_ARG_BOOL, __VA_ARGS__)
#define WRAP_CLI_PAR_ARG_TEXT(...) ApplyMacro(CLI_PAR_ARG_TEXT, __VA_ARGS__)
#define WRAP_CLI_PAR_ARG_INT_RANGE(...) ApplyMacro(CLI_PAR_ARG_INT_RANGE, __VA_ARGS__)
#define WRAP_CLI_PAR_ARG_FLOAT_RANGE(...) ApplyMacro(CLI_PAR_ARG_FLOAT_RANGE, __VA_ARGS__)


#define R_CLI_PAR_SINGLE_ARG(cmd_name, ...) \
    .nargs = RANGE_INIT_BOTH(1,1),
#define F_CLI_PAR_SINGLE_ARG(cmd_name, ...) 
#define WRAP_CLI_PAR_SINGLE_ARG(...) ApplyMacro(CLI_PAR_SINGLE_ARG, __VA_ARGS__)

#define R_CLI_PAR_NARGS_SINGLE(cmd_name, ...) R_CLI_PAR_SINGLE_ARG(cmd_name, ...)
#define F_CLI_PAR_NARGS_SINGLE(cmd_name, ...) 
#define WRAP_CLI_PAR_NARGS_SINGLE(...) ApplyMacro(CLI_PAR_NARGS_SINGLE, __VA_ARGS__)

#define R_CLI_PAR_NARGS_ANY(cmd_name, ...) \
    .nargs = RANGE_INIT_UNSET,
#define F_CLI_PAR_NARGS_ANY(cmd_name, ...)  
#define WRAP_CLI_PAR_NARGS_ANY(...) ApplyMacro(CLI_PAR_NARGS_ANY, __VA_ARGS__)

#define R_CLI_PAR_NARGS_AT_LEAST_ONE(cmd_name, ...) \
    .nargs = RANGE_INIT_MIN(1),
#define F_CLI_PAR_NARGS_AT_LEAST_ONE(cmd_name, ...) 
#define WRAP_CLI_PAR_NARGS_AT_LEAST_ONE(...) ApplyMacro(CLI_PAR_NARGS_AT_LEAST_ONE, __VA_ARGS__)

#define R_CLI_PAR_NARGS(cmd_name, min, max) \
    .nargs = RANGE_INIT_BOTH(min, max),
#define F_CLI_PAR_NARGS(cmd_name, min, max) 
#define WRAP_CLI_PAR_NARGS(...) ApplyMacro(CLI_PAR_NARGS, __VA_ARGS__)
    
#define R_CLI_PAR_NARGS_MIN(cmd_name, min) \
    .nargs = RANGE_INIT_MIN(min),
#define F_CLI_PAR_NARGS_MIN(cmd_name, min) 
#define WRAP_CLI_PAR_NARGS_MIN(...) ApplyMacro(CLI_PAR_NARGS_MIN, __VA_ARGS__)
    
#define R_CLI_PAR_NARGS_MAX(cmd_name, max) \
    .nargs = RANGE_INIT_MAX(max),
#define F_CLI_PAR_NARGS_MAX(cmd_name, max) 
#define WRAP_CLI_PAR_NARGS_MAX(...) ApplyMacro(CLI_PAR_NARGS_MAX, __VA_ARGS__)
    

#define R_CLI_PAR_ATT(cmd_name, ...) .__VA_ARGS__,
#define F_CLI_PAR_ATT(cmd_name, ...) 
#define WRAP_CLI_PAR_ATT(...) ApplyMacro(CLI_PAR_ATT, __VA_ARGS__)

#define R_CLI_PAR_OPTIONAL(cmd_name, ...) \
    __CLI_PAR_OPTIONAL,
#define F_CLI_PAR_OPTIONAL(cmd_name, ...)
#define WRAP_CLI_PAR_OPTIONAL(...) ApplyMacro(CLI_PAR_OPTIONAL, __VA_ARGS__)

#define R_CLI_PAR_REQUIRED(cmd_name, ...) \
    __CLI_PAR_REQUIRED,
#define F_CLI_PAR_REQUIRED(cmd_name, ...)
#define WRAP_CLI_PAR_REQUIRED(...) ApplyMacro(CLI_PAR_REQUIRED, __VA_ARGS__)

#define R_CLI_PAR_REQ_GRP(cmd_name, name) \
    .require_group_idx = XPASTE2(XPASTE2(cmd_name,_req_grp_),name),
#define F_CLI_PAR_REQ_GRP(cmd_name, name)
#define WRAP_CLI_PAR_REQ_GRP(...) ApplyMacro(CLI_PAR_REQ_GRP, __VA_ARGS__)

#define R_CLI_PAR_INT_RANGE(cmd_name, min, max) \
    .numeric_attr.range = MULTITYPE_RANGE_INIT_BOTH(long, min, max),
#define F_CLI_PAR_INT_RANGE(cmd_name, min, max) 
#define WRAP_CLI_PAR_INT_RANGE(...) ApplyMacro(CLI_PAR_INT_RANGE, __VA_ARGS__)
#define R_CLI_PAR_INT_RANGE_MIN(cmd_name, min) \
    .numeric_attr.range = MULTITYPE_RANGE_INIT_MIN(long, min),
#define F_CLI_PAR_INT_RANGE_MIN(cmd_name, min) 
#define WRAP_CLI_PAR_INT_RANGE_MIN(...) ApplyMacro(CLI_PAR_INT_RANGE_MIN, __VA_ARGS__)
#define R_CLI_PAR_INT_RANGE_MAX(cmd_name, max) \
    .numeric_attr.range = MULTITYPE_RANGE_INIT_MAX(long, max),
#define F_CLI_PAR_INT_RANGE_MAX(cmd_name, max) 
#define WRAP_CLI_PAR_INT_RANGE_MAX(...) ApplyMacro(CLI_PAR_INT_RANGE_MAX, __VA_ARGS__)
#define R_CLI_PAR_INT_POSITIVE(cmd_name, ...) \
    .numeric_attr.range = MULTITYPE_RANGE_INIT_MIN(long, 1),
#define F_CLI_PAR_INT_POSITIVE(cmd_name, ...) 
#define WRAP_CLI_PAR_INT_POSITIVE(...) ApplyMacro(CLI_PAR_INT_POSITIVE, __VA_ARGS__)
#define R_CLI_PAR_INT_NON_NEGATIVE(cmd_name, ...) \
    .numeric_attr.range = MULTITYPE_RANGE_INIT_MIN(long, 0),
#define F_CLI_PAR_INT_NON_NEGATIVE(cmd_name, ...) 
#define WRAP_CLI_PAR_INT_NON_NEGATIVE(...) ApplyMacro(CLI_PAR_INT_NON_NEGATIVE, __VA_ARGS__)

#define R_CLI_PAR_FLOAT_RANGE(cmd_name, min, max) \
    .numeric_attr.range = MULTITYPE_RANGE_INIT_BOTH(float, min, max),
#define F_CLI_PAR_FLOAT_RANGE(cmd_name, min, max) 
#define WRAP_CLI_PAR_FLOAT_RANGE(...) ApplyMacro(CLI_PAR_FLOAT_RANGE, __VA_ARGS__)
#define R_CLI_PAR_FLOAT_RANGE_MIN(cmd_name, min) \
    .numeric_attr.range = MULTITYPE_RANGE_INIT_MIN(float, min),
#define F_CLI_PAR_FLOAT_RANGE_MIN(cmd_name, min) 
#define WRAP_CLI_PAR_FLOAT_RANGE_MIN(...) ApplyMacro(CLI_PAR_FLOAT_RANGE_MIN, __VA_ARGS__)
#define R_CLI_PAR_FLOAT_RANGE_MAX(cmd_name, max) \
    .numeric_attr.range = MULTITYPE_RANGE_INIT_MAX(float, max),
#define F_CLI_PAR_FLOAT_RANGE_MAX(cmd_name, max) 
#define WRAP_CLI_PAR_FLOAT_RANGE_MAX(...) ApplyMacro(CLI_PAR_FLOAT_RANGE_MAX, __VA_ARGS__)
#define R_CLI_PAR_FLOAT_POSITIVE(cmd_name, ...) \
    .numeric_attr.range = MULTITYPE_RANGE_INIT_MIN(float, (float)FLOAT_EPS*0.5),
#define F_CLI_PAR_FLOAT_POSITIVE(cmd_name, ...) 
#define WRAP_CLI_PAR_FLOAT_POSITIVE(...) ApplyMacro(CLI_PAR_FLOAT_POSITIVE, __VA_ARGS__)
#define R_CLI_PAR_FLOAT_NON_NEGATIVE(cmd_name, ...) \
    .numeric_attr.range = MULTITYPE_RANGE_INIT_MIN(float, 0),
#define F_CLI_PAR_FLOAT_NON_NEGATIVE(cmd_name, ...) 
#define WRAP_CLI_PAR_FLOAT_NON_NEGATIVE(...) ApplyMacro(CLI_PAR_FLOAT_NON_NEGATIVE, __VA_ARGS__)

#define R_CLI_PAR_TEXT_RANGE(cmd_name, min, max) \
    .numeric_attr.range = MULTITYPE_RANGE_INIT_BOTH(strptr, min, max),
#define F_CLI_PAR_TEXT_RANGE(cmd_name, min, max) 
#define WRAP_CLI_PAR_TEXT_RANGE(...) ApplyMacro(CLI_PAR_TEXT_RANGE, __VA_ARGS__)
#define R_CLI_PAR_TEXT_RANGE_MIN(cmd_name, min) \
    .numeric_attr.range = MULTITYPE_RANGE_INIT_MIN(strptr, min),
#define F_CLI_PAR_TEXT_RANGE_MIN(cmd_name, min) 
#define WRAP_CLI_PAR_TEXT_RANGE_MIN(...) ApplyMacro(CLI_PAR_TEXT_RANGE_MIN, __VA_ARGS__)
#define R_CLI_PAR_TEXT_RANGE_MAX(cmd_name, max) \
    .numeric_attr.range = MULTITYPE_RANGE_INIT_MAX(strptr, max),
#define F_CLI_PAR_TEXT_RANGE_MAX(cmd_name, max) 
#define WRAP_CLI_PAR_TEXT_RANGE_MAX(...) ApplyMacro(CLI_PAR_TEXT_RANGE_MAX, __VA_ARGS__)

#define __CLI_PAR_SET_ARG_CHOISE_STR(T, N , AA, choise_str) \
    OPTIONAL_TYPE_INIT_INNER_FIELD_VAL(as_strptr, choise_str),
#define R_CLI_PAR_CHOISES_STR(cmd_name, ...) \
    .choises = { APPLYX___n(__CLI_PAR_SET_ARG_CHOISE_STR,,__VA_ARGS__) }, 
#define F_CLI_PAR_CHOISES_STR(cmd_name, ...) 
#define WRAP_CLI_PAR_CHOISES_STR(...) ApplyMacro(CLI_PAR_CHOISES_STR, __VA_ARGS__)

#define __CLI_PAR_SET_ARG_CHOISE_INT(T, N , AA, choise_str) \
    OPTIONAL_TYPE_INIT_INNER_FIELD_VAL(as_int, choise_str),
#define R_CLI_PAR_CHOISES_INT(cmd_name, ...) \
    .choises = { APPLYX___n(__CLI_PAR_SET_ARG_CHOISE_INT,,__VA_ARGS__) }, 
#define F_CLI_PAR_CHOISES_INT(cmd_name, ...) 
#define WRAP_CLI_PAR_CHOISES_INT(...) ApplyMacro(CLI_PAR_CHOISES_INT, __VA_ARGS__)

#define __CLI_PAR_SET_ARG_CHOISE_FLOAT(T, N , AA, choise_str) \
    OPTIONAL_TYPE_INIT_INNER_FIELD_VAL(as_int, choise_str),
#define R_CLI_PAR_CHOISES_FLOAT(cmd_name, ...) \
    .choises = { APPLYX___n(__CLI_PAR_SET_ARG_CHOISE_FLOAT,,__VA_ARGS__) }, 
#define F_CLI_PAR_CHOISES_FLOAT(cmd_name, ...) 
#define WRAP_CLI_PAR_CHOISES_FLOAT(...) ApplyMacro(CLI_PAR_CHOISES_FLOAT, __VA_ARGS__)

/*   </ param inner configuration macros >   */



#define CLI_PARAMS(...) __VA_ARGS__
#define CLI_POS_PARAMS CLI_PARAMS
#define CLI_NAMED_PARAMS CLI_PARAMS


#define ADD_ARRAY(T,N,cmd_name,X) \
    EVAL(PRIMITIVE_CAT(ARRAY_, X))
#define REQ_RPG_ADD_ENUM(T,N,cmd_name,X) \
    XPASTE2(XPASTE2(cmd_name, _req_grp_), PRIMITIVE_CAT___(ENUM_, X)),

#define CLI_REQ_GRPS(...) __VA_ARGS__

#define ARRAY_CLI_REQ_GRP_MINMAX(name, min_params, max_params) \
    { .min_nr_params = OPTIONAL_TYPE_INIT_VAL(min_params), .max_nr_params = OPTIONAL_TYPE_INIT_VAL(max_params) }, /* here a following comma `,` expected */
#define ARRAY_CLI_REQ_GRP_MAX(name, max_params) \
    { .min_nr_params = OPTIONAL_TYPE_INIT_UNSET, .max_nr_params = OPTIONAL_TYPE_INIT_VAL(max_params) }, /* here a following comma `,` expected */
#define ARRAY_CLI_REQ_GRP_MIN(name, min_params) \
    { .min_nr_params = OPTIONAL_TYPE_INIT_VAL(min_params), .max_nr_params = OPTIONAL_TYPE_INIT_UNSET }, /* here a following comma `,` expected */

#define ENUM_CLI_REQ_GRP_MINMAX(name, min_params, max_params) \
    name
#define ENUM_CLI_REQ_GRP_MAX(name, max_params) \
    name
#define ENUM_CLI_REQ_GRP_MIN(name, min_params) \
    name

#define CLI_REQ_GRP_SINGLE(name) CLI_REQ_GRP_MINMAX(name, 1, 1)
#define CLI_REQ_GRP_AT_LEAST_ONE(name) CLI_REQ_GRP_MIN(name, 1)
#define CLI_REQ_GRP_ONE_AT_MOST(name) CLI_REQ_GRP_MAX(name, 1)


#define CLI_PARSER_MAX_LONG_NAMES 3
#define CLI_PARSER_MAX_SHORT_NAMES 3
#define CLI_PARAM_MAX_NR_ARGS 20

#define CLI_PAR_MAX_NR_CHOISES 20
#define CLI_ARG_STR_MAX_LEN 20
typedef MULTITYPE_UNION_WITH_RANGE(CLI_ARG_STR_MAX_LEN) ARG_MULTITYPE;



#define CLI_PAR_ARG_NUMERIC_RANGE_IS_ANY_POSITIVE(param) ( \
    !CLI_PAR_HAS_CHOISES(param) \
    && CLI_PAR_IS_NUMERIC(param) \
    && !CLI_PAR_IS_UNSIGNED(param) \
    && (CLI_PAR_IS_INT_OR_INTRANGE(param) \
        ? MULTITYPE_RANGE_IS_ANY_INT_POSITIVE((param)->numeric_attr.range, long) \
        : MULTITYPE_RANGE_IS_ANY_FLOAT_POSITIVE((param)->numeric_attr.range, float)) \
    )
#define CLI_PAR_ARG_NUMERIC_RANGE_IS_ANY_NON_NEGATIVE(param) ( \
    !CLI_PAR_HAS_CHOISES(param) \
    && CLI_PAR_IS_NUMERIC(param) \
    && !CLI_PAR_IS_UNSIGNED(param) \
    && (CLI_PAR_IS_INT_OR_INTRANGE(param) \
        ? MULTITYPE_RANGE_IS_ANY_NON_NEGATIVE((param)->numeric_attr.range, long) \
        : MULTITYPE_RANGE_IS_ANY_NON_NEGATIVE((param)->numeric_attr.range, float)) \
    )


enum cli_args_val_type {
    CLI_ARG_TYPE_INT,
    CLI_ARG_TYPE_BOOL,
    CLI_ARG_TYPE_FLOAT,
    CLI_ARG_TYPE_TEXT,
    CLI_ARG_TYPE_INT_RANGE,
    CLI_ARG_TYPE_FLOAT_RANGE,

    CLI_ARG_TYPE_MAX
};

#define CLI_ARG_TYPE_IS_RANGE(args_val_type) \
    ( (args_val_type) == CLI_ARG_TYPE_INT_RANGE || (args_val_type) == CLI_ARG_TYPE_FLOAT_RANGE )

#define CLI_ARG_TYPE_TO_STR(args_val_type) \
    (((int32_t)args_val_type >= (int32_t)CLI_ARG_TYPE_MAX || (int32_t)args_val_type < 0) ? "Unknown type" : \
    cli_arg_type_to_str[args_val_type])

#define CLI_ARG_TYPE_STR_MAX_LEN 32

/* used to obtain the size of buffer needed to alloc for `CLI_PAR_ARG_TYPE_TO_STR()` */
#define CLI_PAR_ARG_TYPE_TO_STR_LEN(param) \
    ((  CLI_PAR_HAS_CHOISES(param) \
        ? (sizeof(OPTIONAL_TYPE(ARG_MULTITYPE)) * CLI_PAR_MAX_NR_CHOISES * 16) /*TODO:improve*/ \
        : (CLI_ARG_TYPE_STR_MAX_LEN + 16 /*base string*/) \
    ) + 4 /*" in " string*/ + 128 /*range*/ + 1 /*null-terminal*/ )



struct cli_nested_cmd {

};

#define CLI_REQUIRED_VAL -1
#define CLI_OPTIONAL_VAL -2
struct cli_req_group {
    //const char* name;
    RANGE_TYPE(size_t, , _nr_params);
};

struct cli_range {
    RANGE_TYPE(ARG_MULTITYPE, , _val);
};

struct cli_param_arguments {
    int is_flag_set;
    OPTIONAL_TYPE(ARG_MULTITYPE) values[CLI_PARAM_MAX_NR_ARGS];
};

struct cli_param {
    const char* long_names[CLI_PARSER_MAX_LONG_NAMES];
    char short_names[CLI_PARSER_MAX_SHORT_NAMES];
    const char* description;
    int is_separator;

    struct {
        RANGE_TYPE(size_t, , _val);
    } nargs;
    enum cli_args_val_type args_val_type;
    int require_group_idx; /* or `CLI_REQUIRED_VAL` or `CLI_OPTIONAL_VAL` */

    OPTIONAL_TYPE(ARG_MULTITYPE) choises[CLI_PAR_MAX_NR_CHOISES];
    //uintptr_t arg_field_nr;
    
    union {
        struct {
            struct cli_range range;
            uint32_t default_numeric_base;
            int is_positive;
            OPTIONAL_TYPE(ARG_MULTITYPE) default_value;
        } numeric_attr;
    };    
};

struct cli_params_config {
    const char* name;
    const char* description;
    
    int is_menu;
    union {
        struct {
            struct cli_req_group *req_groups;
            size_t nr_req_groups;
            struct cli_param *pos_params;
            size_t nr_pos_params;
            struct cli_param *named_params;
            size_t nr_named_params;
        };
        struct {
            struct cli_nested_cmd *nested_commands;
            size_t nr_nested_commands;
        };
    };
};




#define CLI_COMMAND(cmd_name, cmd_description, _req_groups, _pos_params, _named_params) \
    struct cmd_args_##cmd_name { \
        APPLYXn(ADD_F_PREFIX_TO_PARAM_MACRO,cmd_name, _pos_params) \
        APPLYXn(ADD_F_PREFIX_TO_PARAM_MACRO,cmd_name, _named_params) \
        /* Type param_name; */ \
    }; \
    struct cli_req_group req_groups_##cmd_name[] = { {/* req-grp @ index zero is reserved */}, \
                                                     APPLYXn(ADD_ARRAY, , _req_groups)  }; \
    enum cli_req_groups_##cmd_name { cmd_name##_req_group_##null, /* req-grp @ index zero is reserved */ \
                                         APPLYXn(REQ_RPG_ADD_ENUM, cmd_name, _req_groups)  }; \
    struct cli_param pos_params_##cmd_name[] = { APPLYXn(WRAP_CURLY_BRACES_AND_ADD_R_PREFIX_TO_PARAM_MACRO, cmd_name, _pos_params) }; \
    struct cli_param named_params_##cmd_name[] = { APPLYXn(WRAP_CURLY_BRACES_AND_ADD_R_PREFIX_TO_PARAM_MACRO, cmd_name, _named_params) }; \
    struct cli_params_config cmd_define_##cmd_name = { \
        .name = #cmd_name, \
        .is_menu = 0, \
        .description = cmd_description, \
        .req_groups = req_groups_##cmd_name, \
        .nr_req_groups = ARRAY_SIZE(req_groups_##cmd_name) /* first is dummy */, \
        .pos_params = pos_params_##cmd_name, \
        .nr_pos_params = ARRAY_SIZE(pos_params_##cmd_name), \
        .named_params = named_params_##cmd_name, \
        .nr_named_params = ARRAY_SIZE(named_params_##cmd_name), \
    }

#define CLI_CMD_CONFIG(cmd_name) (&(cmd_define_##cmd_name))

#define FOREACH_CLI_PARAM(config, cur_param) \
    for(cur_param = (config)->pos_params; \
        cur_param != NULL && \
        (CLI_PAR_IS_POS(config, cur_param) || \
        (cur_param == ((config)->pos_params + (config)->nr_pos_params) && (cur_param = (config)->named_params) && (config)->nr_named_params > 0) || \
        CLI_PAR_IS_NAMED(config, cur_param)); \
        cur_param = IS_LAST_NAMED_PARAM(config, cur_param) ? NULL : cur_param+1)

#define FOREACH_CLI_POS_PARAM(config, cur_param) \
    for(cur_param = (config)->pos_params; \
        (struct cli_param *)(cur_param) < (struct cli_param *)((config)->pos_params + (intptr_t)((config)->nr_pos_params)); \
        ++cur_param)

#define FOREACH_CLI_NAMED_PARAM(config, cur_param) \
    for(cur_param = (config)->named_params; \
        (struct cli_param *)(cur_param) < (struct cli_param *)((config)->named_params + (intptr_t)((config)->nr_named_params)); \
        ++cur_param)

/*
#define CLI_MENU_COMMAND(name, description, ) \
    struct cli_parse_define cmd_define_##name = { \
        .name = name, \
        .is_menu = 0, \
        .description = description, \
    }
*/

#define CLI_PAR_IS_POS(cmd_cnfg, param_ptr) \
    ( \
        ((struct cli_param *)(param_ptr) >= (cmd_cnfg)->pos_params) && \
        ((struct cli_param *)(param_ptr) < (cmd_cnfg)->pos_params + (intptr_t)(cmd_cnfg)->nr_pos_params) \
    )

#define CLI_PAR_IS_NAMED(cmd_cnfg, param_ptr) \
    ( \
        ((struct cli_param *)(param_ptr) >= (cmd_cnfg)->named_params) && \
        ((struct cli_param *)(param_ptr) < (cmd_cnfg)->named_params + (intptr_t)(cmd_cnfg)->nr_named_params) \
    )

#define CLI_PAR_IDX(cmd_cnfg, param_ptr) \
    ((uintptr_t)( \
        CLI_PAR_IS_POS(cmd_cnfg, param_ptr) ? \
        (uintptr_t)((struct cli_param *)(param_ptr) - (cmd_cnfg)->pos_params) : \
        (uintptr_t)((cmd_cnfg)->nr_pos_params) + (uintptr_t)((struct cli_param *)(param_ptr) - (cmd_cnfg)->named_params) \
    ))

#define CLI_PAR_ARGS_FIELD(cmd_cnfg, par_args_ptr, param_ptr) \
    ( ((struct cli_param_arguments *)(par_args_ptr)) + CLI_PAR_IDX(cmd_cnfg, param_ptr) )

#define IS_LAST_POS_PARAM(cmd_cnfg, param_ptr) \
    ((struct cli_param *)(param_ptr) == ((cmd_cnfg)->pos_params + (cmd_cnfg)->nr_pos_params - 1))
#define IS_LAST_NAMED_PARAM(cmd_cnfg, param_ptr) \
    ((struct cli_param *)(param_ptr) == ((cmd_cnfg)->named_params + (cmd_cnfg)->nr_named_params - 1))

#define IS_PAR_NARGS_VARIABLE(param_ptr) \
    (   (param_ptr)->nargs.max_val.is_set == 0 || \
        ((param_ptr)->nargs.min_val.is_set == 0 && (param_ptr)->nargs.max_val.value > 0) || \
        ((param_ptr)->nargs.min_val.is_set && (param_ptr)->nargs.min_val.value != (param_ptr)->nargs.max_val.value))

#define IS_PAR_MIGHT_GET_ARGS(param_ptr) \
    (   (param_ptr)->nargs.max_val.is_set == 0 || \
        (param_ptr)->nargs.max_val.value > 0   )

#define IS_PAR_MIGHT_GET_JUST_SINGLE_ARG(param_ptr) \
    (   (param_ptr)->nargs.max_val.is_set == 1 && \
        (param_ptr)->nargs.max_val.value == 1   )

#define IS_PAR_MUST_GET_ARGS(param_ptr) \
    (   (param_ptr)->nargs.min_val.is_set && \
        (param_ptr)->nargs.min_val.value > 0   )

#define CLI_PAR_GET_MAX_ARGS(par_ptr) \
    ( (par_ptr)->nargs.max_val.is_set ? \
        min(CLI_PARAM_MAX_NR_ARGS, (par_ptr)->nargs.max_val.value) : \
        CLI_PARAM_MAX_NR_ARGS )

/* TODO: re-consider how to impl. */
#define CLI_PAR_IS_UNSIGNED(par_ptr) \
    ((par_ptr)->numeric_attr.default_numeric_base == 16)

#define CLI_PAR_IS_NUM_IN_RANGE(par_ptr, val_ptr) \
    (   CLI_PAR_IS_UNSIGNED(par_ptr) ? \
        MULTITYPE_RANGE_IS_NUMERIC_VAL_IN((par_ptr)->numeric_attr.range, val_ptr, uint32_t) : \
        MULTITYPE_RANGE_IS_NUMERIC_VAL_IN((par_ptr)->numeric_attr.range, val_ptr, long)  )

#define CLI_PAR_IS_STRPTR_IN_RANGE(par_ptr, val_ptr) \
    ( (!(par_ptr)->numeric_attr.range.min_val.is_set || strcmp((par_ptr)->numeric_attr.range.min_val.value.as_strptr, (val_ptr)->as_strptr) <= 0) && \
      (!(par_ptr)->numeric_attr.range.max_val.is_set || strcmp((par_ptr)->numeric_attr.range.max_val.value.as_strptr, (val_ptr)->as_strptr) >= 0 ) )

#define CLI_PAR_IS_STR_IN_RANGE(par_ptr, val_ptr) \
    ( (!(par_ptr)->numeric_attr.range.min_val.is_set || strncmp((par_ptr)->numeric_attr.range.min_val.value.as_strptr, (val_ptr)->as_str, CLI_ARG_STR_MAX_LEN) <= 0) && \
      (!(par_ptr)->numeric_attr.range.max_val.is_set || strncmp((par_ptr)->numeric_attr.range.max_val.value.as_strptr, (val_ptr)->as_str, CLI_ARG_STR_MAX_LEN) >= 0 ) )

#define CLI_PAR_IS_VAL_IN_RANGE(par_ptr, val_ptr) \
    (   CLI_PAR_IS_NUMERIC(par_ptr) ? \
        CLI_PAR_IS_NUM_IN_RANGE(par_ptr, val_ptr) : \
        CLI_PAR_IS_STRPTR_IN_RANGE(par_ptr, val_ptr) )

#define CLI_PAR_IS_NUMERIC(par_ptr) \
    (   (par_ptr)->args_val_type == CLI_ARG_TYPE_INT || \
        (par_ptr)->args_val_type == CLI_ARG_TYPE_FLOAT || \
        (par_ptr)->args_val_type == CLI_ARG_TYPE_INT_RANGE || \
        (par_ptr)->args_val_type == CLI_ARG_TYPE_FLOAT_RANGE  )

#define CLI_PAR_IS_INT_OR_INTRANGE(par_ptr) \
    (   (par_ptr)->args_val_type == CLI_ARG_TYPE_INT || \
        (par_ptr)->args_val_type == CLI_ARG_TYPE_INT_RANGE  )

#define CLI_PAR_ARG_TYPE_NORANGE(args_val_type) \
    (   ((args_val_type) == CLI_ARG_TYPE_INT || \
         (args_val_type) == CLI_ARG_TYPE_INT_RANGE) \
            ? CLI_ARG_TYPE_INT \
            :   ((args_val_type) == CLI_ARG_TYPE_FLOAT || \
                 (args_val_type) == CLI_ARG_TYPE_FLOAT_RANGE) \
                    ? CLI_ARG_TYPE_FLOAT \
                    : (args_val_type)  )

#define CLI_PAR_HAS_CHOISES(par_ptr) \
    ((par_ptr)->choises[0].is_set)


enum cli_flags {
    CLI_VERBOSE_ERRORS = (1<<0),
    CLI_PRINT_SUGGESTIONS = (1<<1),
};

int cli_parse(  struct cli_params_config *config, 
                struct cli_param_arguments* params_args, 
                int nr_words, 
                char **words,
                int flags);
void cli_print_usage(struct cli_params_config *config);

#endif /* JOS_UTILS_CLI_ARGS_PARSER */
